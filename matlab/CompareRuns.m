function [ outval ] = CompareRuns(run_a_path, run_b_path, comment_a, comment_b, comparison_dir)

    function [ common, different ] = ListFields(fields_a, fields_b)
        n_a = numel(fields_a);
        n_b = numel(fields_b);
        n_m = max(n_a, n_b);
        common = cell([n_m, 1]);
        different = cell([n_m, 1]);
        c = 0;
        d = 0;
        for ii = 1:n_a
            found = false;
            for jj = 1:n_b
                if strcmp(fields_a{ii}, fields_b{jj})
                    c = c + 1;
                    common(c) = fields_a(ii);
                    found = true;
                    fields_b(jj) = {''};
                    break;
                end
            end    
            if ~found
                d = d + 1;
                different(c) = fields_a(ii);
            end
        end
        for ii = 1:n_b
            if ~strcmp(fields_b{ii}, '')
                d = d + 1;
                different(d) = fields_a(ii);
            end            
        end
        
        if c == 0
            common = {};
        else
            common = common(1:c,:);
        end
        
        if d == 0
            different = {};
        else
            different = different(1:d,:);
        end        
    end

    function [ res ] = Compare1D(val_name, val_a, val_b, desc_a, desc_b, dest_path)
        fprintf(sprintf('Comparing %s\n', val_name));
        
        n_min = min(numel(val_a), numel(val_b));
        diff = val_a(1:n_min) - val_b(1:n_min);
        
        figure('visible', 'off');
        l_h = [];
        subplot(1, 2, 1);
        
        l_h(end+1) = plot(val_a, 'displayname', desc_a, 'linewidth', 2);
        hold on;
        l_h(end+1) = plot(val_b, 'displayname', desc_b);
        title(sprintf('Comparison: \\texttt{%s}', val_name));
        l = legend(l_h);
        set(l, 'location', 'northeastoutside');
        
        subplot(1, 2, 2);
        l_h = [];
        l_h(end+1) = plot(diff, 'displayname', 'Difference');
        title(sprintf('Difference'));
        LatexifyPlot(gca, [12 10 12 10]);
        
        ResizePlot(gcf, [2 2 20 12]);
        SavePlot(gcf, dest_path, 1);
        close gcf;
    end
    
    function [ res ] = Compare2D(val_name, val_a, val_b, desc_a, desc_b, dest_path)
        fprintf(sprintf('Comparing %s\n', val_name));
        
        n_min = min(numel(val_a), numel(val_b));
        diff = val_a(1:n_min) - val_b(1:n_min);
        
        figure('visible', 'off');
        ResizePlot(gcf, [2 2 20 6]);
        
        lt = 0.06;
        fw = 0.16;
        
        l_h = [];
        subplot(1, 3, 1);
        mesh(val_a)
        title(sprintf('%s: \\texttt{%s}', desc_a, val_name));
        cb = colorbar;
        LatexifyPlot(gca, [12 10 12 10]);   
        p = get(gca, 'position');
        p(1) = lt + 0 * 1/3;
        p(3) = fw;
        p(4) = p(4) * 0.95;
        set(gca, 'position', p);
        
        subplot(1, 3, 2);
        mesh(val_b)
        title(sprintf('%s: \\texttt{%s}', desc_b, val_name));
        cb = colorbar;
        LatexifyPlot(gca, [12 10 12 10]);   
        p = get(gca, 'position');
        p(1) = lt + 1 * 1/3;
        p(3) = fw;
        p(4) = p(4) * 0.95;
        set(gca, 'position', p);     
        
        sz_a = size(val_a);
        sz_b = size(val_b);
        
        if sz_a(1) == sz_b(1) && sz_a(2) == sz_b(2)             
            subplot(1, 3, 3);
            mesh(val_a - val_b)
            title('Difference');
            cb = colorbar;
            LatexifyPlot(gca, [12 10 12 10]);   
            p = get(gca, 'position');
            p(1) = lt + 2 * 1/3;
            p(3) = fw;
            p(4) = p(4) * 0.95;
            set(gca, 'position', p);             
        end
        
        SavePlot(gcf, dest_path, 1);
        close gcf;
    end
    

    run_a = load_spice(run_a_path);
    run_b = load_spice(run_b_path);
    
    fields_a = fieldnames(run_a);
    fields_b = fieldnames(run_b);
    
    [ common, different ] = ListFields(fields_a, fields_b);
    
    n_c = numel(common);
    n_d = numel(different);
    
    if n_d > 0
        f_list = '';
        for i = 1:n_d
            f_list = [f_list, different{i}];
            if i < n_d
                f_list = [f_list, ', '];
            end
        end        
        warning(['There are some non-common fields in compared files: ', f_list]);
    end
    
    for i = 1:n_c
        field_name = common{i};
        if strcmp(field_name, 'Properties')
            continue
        end
        val_a = run_a.(field_name);
        val_b = run_b.(field_name);
        
        sz = size(val_a);        
        dest_path = [comparison_dir '/', field_name];
        
        if ndims(val_a) == 2 
            if sz(1) == 1 && sz(2) == 1
                fprintf(sprintf('Comparing %s\n%s = %d : %s = %d : diff = %d \n', field_name, comment_a, val_a, comment_b, val_b, val_a - val_b))
            elseif sz(1) == 1 || sz(2) == 1
                Compare1D(field_name, val_a, val_b, comment_a, comment_b, dest_path)
            else
                Compare2D(field_name, val_a, val_b, comment_a, comment_b, dest_path)
            end
        elseif ndims(val_b) == 3
            
        end
    end
end