function [ title_font_size, legend_font_size, label_font_size, tick_font_size ] = LatexifyPlot( gca_h, sizes, cb )
% LatexifyPlot( gca_h, font_sizes=[title_size, legend_size, label_size, tick_size] )
%   Detailed explanation goes here

title_font_size = 24;
legend_font_size = 22;
label_font_size = 20;
tick_font_size = 20;

if (~isempty(sizes))

    title_font_size = sizes(1);
    legend_font_size = sizes(2);
    label_font_size = sizes(3);
    tick_font_size = sizes(4);
end


set(gca_h, 'ticklabelinterpreter', 'latex', 'fontsize', tick_font_size);


h = get(gca_h, 'xlabel');
set(h, 'Interpreter', 'latex', 'fontsize', label_font_size);

h = get(gca_h, 'ylabel');
set(h, 'Interpreter', 'latex', 'fontsize', label_font_size);

if (isprop(gca_h, 'legend'))
    h = get(gca_h, 'legend');
    set(h, 'Interpreter', 'latex', 'fontsize', legend_font_size);
end

gca_h.XAxis.TickLabelFormat = '$%g$';



if (size(gca_h.YAxis, 1) > 1)
    gca_h.YAxis(1).TickLabelFormat = '$%g$';
    gca_h.YAxis(2).TickLabelFormat = '$%g$';
   
else
    gca_h.YAxis(1).TickLabelFormat = '$%g$';
end


gca_h.ZAxis.TickLabelFormat = '$%g$';

% if (isfield(gca_h, 'Title'))
    set(gca_h.Title, 'interpreter', 'latex', 'fontsize', title_font_size);
% end


if exist('cb', 'var')
 	cb.TickLabelInterpreter = 'latex';
 	cb.FontSize = tick_font_size;
 	cb.Label.Interpreter = 'latex';
 	cb.Title.Interpreter = 'latex';
 	cb.Label.FontSize = label_font_size;
    y_labels = cb.TickLabels;
    new_labels = cell(size(y_labels));
    for i = 1 : size(y_labels, 1)
        new_labels{i} = sprintf('$%g$', str2num(y_labels{i}));
    end
    cb.TickLabels = new_labels;
end
end

