function [ output_args ] = ResizePlot(fig_h, plot_size)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

set(fig_h, 'Units', 'centimeters', 'Position', plot_size)
set(fig_h, 'PaperUnits', 'centimeters', 'PaperPosition', [0 0 plot_size(3) plot_size(4)])
set(fig_h, 'PaperUnits', 'centimeters', 'PaperSize', [plot_size(3) plot_size(4)])

end

