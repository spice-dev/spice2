function [all_contents, all_variables] = load_spice_h5(h5_file_path, varargin)
%load_spice_h5(h5_file_path, varargin) Loads output from SPICE simulation
%stored in HDF5 format.
%   h5_file_path: Path to HDF5 file
%   varargin: 'mode': 'struct' (stores all variables to struct), 
%                     'add' (assigns variables to workspace), 
%                     'both' (does both)
% Example: load_spice_h5('file.h5') - same behaviour as load('file.mat')

% Extraction modes
% 1. Save variables to output of this function
% 2. Assigns variables to workspace calling this function (default)
% 3. both 1 & 2

function [tf] = str_in_list(needle, haystack)
    tf = false;
    for i = 1:numel(haystack)
        if strcmpi(needle, haystack{i}) 
            tf = true;
            break;
        end
    end
end


save_struct = 0;
assign_vars = 1;
var_list = varargin;
all_variables = {};

if numel(varargin) >= 2
    if strcmpi(varargin{1}, 'mode')
        var_list = {};
        if strcmpi(varargin{2}, 'struct') 
            save_struct = 1; assign_vars = 0;
        elseif strcmpi(varargin{2}, 'add') 
            save_struct = 0; assign_vars = 1;
        elseif strcmpi(varargin{2}, 'both') 
            save_struct = 1; assign_vars = 1;
        else
            throw(MException('SH5:ModeError', 'Extraction mode not recognized, use ''struct'', ''add'', or ''both''.'));
        end
    end
    if numel(varargin) > 2
        var_list = var_list(3:end);
    end
end

var_select = numel(var_list) > 0;

% Output can be written as struct
all_contents = struct;

% Lists available information about file contents (ie. stored variables).
% SPICE uses simple "flat" storage, all variables are stored in the root
% group of the file.
file_info = h5info(h5_file_path);

% To speed up variable extraction
% Iterate through file contents
for d_i = 1 : numel(file_info.Datasets)
    % Find the name of the variable.
    dataset = file_info.Datasets(d_i);
    var_name = dataset.Name;
    
    if var_select
        if ~str_in_list(var_name, var_list)
            continue;
        end
    end
    % Load it from the HDF5 file.
    var_value = h5read(h5_file_path, ['/' var_name]);
        
    % Add it to struct containing all file contents.
    if save_struct
        all_contents.(var_name) = var_value;
    end
    
    % Add the variable to the workspace calling this function.
    if assign_vars
        assignin('caller', var_name, var_value);
    end
end

end

