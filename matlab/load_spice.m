function [all_contents] = load_spice(file_path, varargin)
%load_spice(file_path, varargin) Loads output from SPICE simulation
%stored in HDF5 format.
%   file_path: Path to spice file without extension
%   varargin: list of variables to load
% Example: data = load_spice('file');

all_contents = struct;
var_list = varargin(1:end);

if exist([file_path '.mat'], 'file')
    mf = matfile([file_path '.mat']);
    if numel(var_list) == 0
        all_contents = mf;
    else
        for i = 1:numel(var_list)
            var_name = var_list{i};
            all_contents.(var_name) = mf.(var_name);
        end
    end
elseif exist([file_path '.h5'], 'file')
    args = {'mode', 'struct', var_list{:}};
    all_contents = load_spice_h5([file_path '.h5'], args{:});
end
% 
% fn = fieldnames(all_contents);
% for i = 1:numel(fn)    
%     assignin('caller', fn{i}, all_contents.(fn{i}));
% end

end