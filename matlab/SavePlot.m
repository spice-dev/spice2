function [ output_args ] = SavePlot(fig_h, path_base, nopdf)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

if (exist('nopdf', 'var'))
    
else
    fig_path = [path_base '.pdf'];
    saveas(fig_h, fig_path);
end

fig_path = [path_base '.png'];
print(fig_h, fig_path, '-dpng', '-r600');

end

