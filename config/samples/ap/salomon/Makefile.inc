# This is a include file for make.
#
# You can either use the file without any changes, or you should specify your own paths.
#
# 	*	If no changes are made, you should compile all external libraries first.
# 	*	If you are using your own libraries, be sure that all of them are compiled with the same compiler (eg. do not mix intelmpi and openmpi etc.). Change only appropriate assignments then.

# standard makefile shell assignment
SHELL = /bin/sh

# any other libraries (eg. mpi)
USR_LIB_DIR=~/usr/lib

# compiler
# SPICE2_MPIF90 is taken from environment.sh (do not edit appropriate include line out from makefile)
MPIF=$(SPICE2_MPIF90)
MPIF_FLAGS_COMMON=-no-prec-div -zero -mcmodel=medium -m64 -fPIC -fpp
MPIF_FLAGS_DEBUG=-g -C -check all -check noarg_temp_created
MPIF_FLAGS_RELEASE=-O3

# utilities
CLOCK_LIB=$(SPICE2_LIBDIR)/libclock.a
F2C_LIB=$(SPICE2_LIBDIR)/libf2c.a

# matio for output
MATIO_LIBS=$(SPICE2_LIBDIR)/libmatio.a -lz

#lapack etc libraries
#BLAS_LIB=-lopenblas
#LAPACK_LIB=$(SPICE2_LIBDIR)/liblapack.a
#BLACS_LIB=$(SPICE2_LIBDIR)/blacsF77init_MPI-LINUX-0.a $(SPICE2_LIBDIR)/blacs_MPI-LINUX-0.a
#SCALAPACK_LIB=$(SPICE2_LIBDIR)/libscalapack.a
#MATH_LIBS=$(SCALAPACK_LIB) $(BLACS_LIB) $(LAPACK_LIB) $(BLAS_LIB) $(SCALAPACK_LIB)

BLAS_LIB=$(MKLROOT)/lib/intel64/libmkl_blas95_lp64.a
MKL_INC=-I$(MKLROOT)/include/intel64/lp64 -I$(MKLROOT)/include
MKL_LIBS=$(MKLROOT)/lib/intel64/libmkl_lapack95_lp64.a $(MKLROOT)/lib/intel64/libmkl_scalapack_lp64.a -Wl,--start-group $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a $(MKLROOT)/lib/intel64/libmkl_intel_thread.a $(MKLROOT)/lib/intel64/libmkl_core.a $(MKLROOT)/lib/intel64/libmkl_blacs_intelmpi_lp64.a -Wl,--end-group -liomp5 -lpthread -lm -ldl
MATH_LIBS=$(BLAS_LIB) $(MKL_LIBS) $(MKL_INC)

# umfpack for centralized solver
UMFPACK_LIBS_BASIC=$(F2C_LIB) -Wl,--start-group $(SPICE2_LIBDIR)/libumfpack.a $(SPICE2_LIBDIR)/libamd.a $(SPICE2_LIBDIR)/umf4_f77wrapper64.o -Wl,--end-group -lm

# mumps parallel solver and its dependencies
SCOTCH_LIBS=-lptesmumps -lscotch -lptscotch -lptscotcherr
MUMPS_LIBS_BASIC=$(SPICE2_LIBDIR)/libdmumps.a  $(SPICE2_LIBDIR)/libmumps_common.a
MUMPS_LIBS=$(MUMPS_LIBS_BASIC) $(SCOTCH_LIBS)

# hypre libraries for MG solver

HYPRE_LIBS=-L$(SPICE2_LIBDIR) -lHYPRE -lstdc++ -I$(SPICE2_INCDIR)
PES2D_LIBS=$(SPICE2_LIBDIR)/libhypes2d.a $(HYPRE_LIBS)

# others...
MPI_LIBS=-lmpi
COMMON_LIBS=-L$(USR_LIB_DIR) $(MPI_LIBS) -lutil -ldl -lpthread

MPIF_INC=-I$(PICLIB_MPI_INC) -I$(SPICE2_INCDIR)
MPIF_LIB=-L$(SPICE2_LIBDIR)

HDF5_LIBS=-L$(SPICE2_LIBDIR) -I$(SPICE2_INCDIR) $(SPICE2_LIBDIR)/libhdf5_fortran.a $(SPICE2_LIBDIR)/libhdf5.a -lz -lm

# this line will appear in the makefile. this is only a reference
#$(SPICE2_LIBS)=$(MATIO_LIBS) $(MUMPS_LIBS) $(UMFPACK_LIBS) $(CLOCK_LIB) $(SUPPORT_LIBS) $(COMMON_LIBS)
