# This is a include file for make.
#
# You can either use the file without any changes, or you should specify your own paths.
#
# 	*	If no changes are made, you should compile all external libraries first.
# 	*	If you are using your own libraries, be sure that all of them are compiled with the same compiler (eg. do not mix intelmpi and openmpi etc.). Change only appropriate assignments then.

# standard makefile shell assignment
SHELL = /bin/sh

# source directories for external libraries
SPICE2_EXTDIR=$(SPICE2_DIR)/externals
SPICE2_LIBDIR=$(SPICE2_EXTDIR)/lib
SPICE2_OBJDIR=$(SPICE2_EXTDIR)/obj
SPICE2_INCDIR=$(SPICE2_EXTDIR)/inc
SPICE2_MODDIR=$(SPICE2_EXTDIR)/mod


# any other libraries (eg. mpi)
USR_LIB_DIR=~/usr/lib

# compiler
# SPICE2_MPIF90 is taken from environment.sh (do not edit appropriate include line out from makefile)
MPIF=$(SPICE2_MPIF90)
MPIF_FLAGS_COMMON=-no-prec-div -zero -mcmodel=medium
MPIF_FLAGS_DEBUG=-g -C -check all, noarg_temp_created
MPIF_FLAGS_RELEASE=-O3

# utilities
CLOCK_LIB=$(SPICE2_LIBDIR)/libclock.a
F2C_LIB=$(SPICE2_LIBDIR)/libf2c.a

# matio for output
MATIO_LIBS=$(SPICE2_LIBDIR)/libmatio.a -lz

#lapack etc libraries
BLAS_LIB=$(SPICE2_LIBDIR)/libgoto2.a
LAPACK_LIB=$(SPICE2_LIBDIR)/liblapack.a
BLACS_LIB=$(SPICE2_LIBDIR)/blacsF77init_MPI-LINUX-0.a $(SPICE2_LIBDIR)/blacs_MPI-LINUX-0.a
SCALAPACK_LIB=$(SPICE2_LIBDIR)/libscalapack.a

# umfpack for centralized solver
UFLIBS_DIR=$(SPICE2_EXTDIR)/_source/UF
UMFPACK_INCLUDE=-I$(UFLIBS_DIR)/UMFPACK/Include -I$(UFLIBS_DIR)/AMD/Include
UMFPACK_LIBS_BASIC=$(F2C_LIB) -Wl,--start-group $(SPICE2_LIBDIR)/libumfpack.a $(SPICE2_LIBDIR)/libamd.a $(SPICE2_OBJDIR)/umf4_f77wrapper.o -Wl,--end-group -lm

# mumps parallel solver and its dependencies
SCOTCH_LIBS=-L$(SPICE2_LIBDIR) -lptesmumps -lscotch -lptscotch -lptscotcherr
MUMPS_LIBS_BASIC=$(SPICE2_LIBDIR)/libdmumps.a  $(SPICE2_LIBDIR)/libmumps_common.a
MUMPS_LIBS=$(MUMPS_LIBS_BASIC) $(SCOTCH_LIBS)

# others...
MPI_LIBS=-lmpi
COMMON_LIBS=-L$(USR_LIB_DIR) $(MPI_LIBS) -lutil -ldl -lpthread

# this line will appear in the makefile. this is only a reference
#$(SPICE2_LIBS)=$(MATIO_LIBS) $(MUMPS_LIBS) $(UMFPACK_LIBS) $(CLOCK_LIB) $(SUPPORT_LIBS) $(COMMON_LIBS)
