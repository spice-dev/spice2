export SPICE2_ENVIRONMENT=openmpi

export SPICE2_DIR=/home/podolnik/Coding/SPICE2/repo
export SPICE2_LIBSRCDIR=$SPICE2_DIR/externals/_source

export SPICE2_FC=ifort
export SPICE2_MPIF90=~/usr/bin/mpif90

export SPICE2_F77=ifort
export SPICE2_MPIF77=~/usr/bin/mpif77

export SPICE2_CC=icc
export SPICE2_MPICC=~/usr/bin/mpicc

export SPICE2_MPI_DIR=~/Coding/lib/MPI/openmpi-1.6.2/ompi
export SPICE2_MPI_INC=~/Coding/lib/MPI/openmpi-1.6.2/ompi/include

export SPICE2_MPIRUN=~/usr/bin/mpirun


