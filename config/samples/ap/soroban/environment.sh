#!/bin/bash

# one of these is probably present 
test -f ../spice-lib/config/environment.sh && source ../spice-lib/config/environment.sh
test -f ../../spice-lib/config/environment.sh && source ../../spice-lib/config/environment.sh

export SPICE2_LIB=$SPICE_BASE/spice-lib
export SPICE2_DIR=$SPICE_BASE/spice2

export SPICE2_BINDIR=$SPICE_BASE/spice-lib/_install/bin
export SPICE2_LIBDIR=$SPICE_BASE/spice-lib/_install/lib
export SPICE2_INCDIR=$SPICE_BASE/spice-lib/_install/include

source $SPICE2_LIB/config/environment.sh

export DFLAGS=
#export SPICE2_MPIF90="HDF5_FC=$PICLIB_MPIF90; HDF5_FLINKER=$PICLIB_MPIF90; $SPICE2_BINDIR/h5fc"
export SPICE2_MPIF90=$PICLIB_MPIF90
export SPICE2_MPIRUN=$PICLIB_MPIRUN

# enable MPI large data transfer
export I_MPI_SHM_LMT=shm

echo "Environment for SPICE2 loaded"
