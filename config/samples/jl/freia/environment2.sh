export SPICE2_ENVIRONMENT=openmpi

export SPICE2_DIR=/home/jleland/Spice/spice2
export SPICE2_LIBSRCDIR=$SPICE2_DIR/externals/_source

export SPICE2_FC=ifort
export SPICE2_MPIF90=mpif90

export SPICE2_F77=ifort
export SPICE2_MPIF77=mpif77

export SPICE2_CC=icc
export SPICE2_MPICC=mpicc

export SPICE2_MPI_DIR=/usr/local/depot/openmpi-2.1.0-ifort-12.0
export SPICE2_MPI_INC=/usr/local/depot/openmpi-2.1.0-ifort-12.0/include

export SPICE2_MPIRUN=mpirun

export SPICE2_BLASTARGET=CORE2


