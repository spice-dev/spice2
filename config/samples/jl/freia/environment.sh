export SPICE2_ENVIRONMENT=openmpi

export SPICE2_DIR=/home/jleland/Spice/spice2
export SPICE2_LIBSRCDIR=$SPICE2_DIR/externals/_source

export SPICE2_FC=ifort
export SPICE2_MPIF90=mpif90

export SPICE2_F77=ifort
export SPICE2_MPIF77=mpif77

export SPICE2_CC=icc
export SPICE2_MPICC=mpicc

export SPICE2_MPI_DIR=/usr/local/depot/openmpi-1.8.2-ifort
export SPICE2_MPI_INC=/usr/local/depot/openmpi-1.8.2-ifort/include

export SPICE2_MPIRUN=mpirun


