#!/bin/bash

# current code version
export SPICE2_VERSION=2.13

# executable prefix
export SPICE2_NAME=spice

# suffix variant
export SPICE2_SUFFIX_DEBUG=-debug
export SPICE2_SUFFIX_RELEASE=-release
export SPICE2_SUFFIX_NO_POISSON=-np

# let's add a suffix for better cleaning
export SPICE2_SUFFIX_EXE=.bin
