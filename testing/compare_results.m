close all;
clear variables;

addpath('../matlab');

result_a = 'output/umfpack-small-o';
result_b = 'output/hypre-small-o';

res_a = load_spice(result_a, 'Potav', 'dens01', 'dens02');
pot_a = res_a.Potav;
res_b = load_spice(result_b, 'Potav', 'dens01', 'dens02');
pot_b = res_b.Potav;

mean_pot = mean(mean(pot_a));


pot_diff = abs((pot_a-pot_b) ./ mean_pot);

fig_plot = figure;

imagesc([0 128], [0 128], pot_diff);

set(gca, 'ydir', 'normal')
xlabel('$y$ [$\times \lambda_\mathrm{D}$]');
ylabel('$z$ [$\times \lambda_\mathrm{D}$]');
xticks(0:32:128)
yticks(0:32:128)

title('$V = V_\mathrm{Hypre}$')
colormap(gca, hot);
c = colorbar;
c.Label.String = '$\varepsilon_V = |(V - V_\mathrm{ref})/\langle V_\mathrm{ref} \rangle|$';
c.Label.Interpreter = 'latex';
c.TickLabelInterpreter = 'latex';
c.Ticks = [0 0.03 0.06 0.09 0.12];
c.Label.FontSize = 10;
caxis([0 0.12]);
axis tight equal;

LatexifyPlot(gca, [12 12 12 10]);


ax_pos = get(gca, 'Position');
ax_pos(1) = -0.04;
ax_pos(2) = 0.25;
ax_pos(3) = ax_pos(3) * 0.75;
ax_pos(4) = ax_pos(4) * 0.75;
set(gca, 'Position', ax_pos);

ax_pos(1) = 0.54;
ax_2 = axes('Position', ax_pos);
imagesc([0 128], [0 128], pot_a);
axis tight equal;


set(gca, 'ydir', 'normal')
xlabel('$y$ [$\times \lambda_\mathrm{D}$]');
ylabel('$z$ [$\times \lambda_\mathrm{D}$]');
xticks(0:32:128)
yticks(0:32:128)

title('$V_\mathrm{ref} = V_\mathrm{UMFPACK}$')
colormap(gca, parula);
c = colorbar;
c.Label.String = '$V_\mathrm{ref}$ [$k_\mathrm{B}T_\mathrm{e}/e$]';
c.Label.Interpreter = 'latex';
c.TickLabelInterpreter = 'latex';
c.Ticks = [-3 -1.5 0 1.5];
c.Label.FontSize = 10;
caxis([-3 1.5]);
axis tight equal;
LatexifyPlot(gca, [12 12 12 10]);

plot_size = [0 0 14 5]; 
set(fig_plot, 'Units', 'centimeters', 'Position', plot_size)
set(fig_plot, 'PaperUnits', 'centimeters', 'PaperPosition', plot_size)
set(fig_plot, 'PaperUnits', 'centimeters', 'PaperSize', [plot_size(3) plot_size(4)])


fig_filename = 'solver-pot_diff-128';
saveas(fig_plot, [fig_filename '.pdf']);