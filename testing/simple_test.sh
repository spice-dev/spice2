#!/bin/bash
# Run in testing directory!

source ../config/environment.sh

# just for a case
WDIR=$SPICE2_DIR/testing
ODIR=$SPICE2_DIR/testing/output

mkdir -p $ODIR

BINDIR=$SPICE2_DIR/bin
cd $BINDIR

NP=4
TEST_VAR=small
TEST_BIN=./spice-2.13-debug.bin

PIDNOS=""

##

# TEST_SOLVER=hypre

# TEST_CASE="$TEST_SOLVER-$TEST_VAR"
# CMD="$SPICE2_MPIRUN -outfile-pattern $ODIR/$TEST_CASE.%r.log -np $NP $TEST_BIN -v -5 -i $WDIR/$TEST_CASE.inp -o $ODIR/$TEST_CASE-o -t $ODIR/$TEST_CASE-t > $ODIR/$TEST_CASE.log 2>$ODIR/$TEST_CASE.err &"
# echo $CMD
# eval $CMD
# PIDNOS="$PIDNOS $!"

##

TEST_SOLVER=umfpack

TEST_CASE="$TEST_SOLVER-$TEST_VAR"
CMD="$SPICE2_MPIRUN -outfile-pattern $ODIR/$TEST_CASE.%r.log -np $NP $TEST_BIN -v -i $WDIR/$TEST_CASE.inp -o $ODIR/$TEST_CASE-o -t $ODIR/$TEST_CASE-t > $ODIR/$TEST_CASE.log 2>$ODIR/$TEST_CASE.err &"
echo $CMD
eval $CMD
PIDNOS="$PIDNOS $!"


##

# TEST_SOLVER=mumps

# TEST_CASE="$TEST_SOLVER-$TEST_VAR"
# CMD="$SPICE2_MPIRUN -outfile-pattern $ODIR/$TEST_CASE.%r.log -np $NP $TEST_BIN -v -5 -i $WDIR/$TEST_CASE.inp -o $ODIR/$TEST_CASE-o -t $ODIR/$TEST_CASE-t > $ODIR/$TEST_CASE.log 2>$ODIR/$TEST_CASE.err &"
# echo $CMD
# eval $CMD
# PIDNOS="$PIDNOS $!"

##

TEST_SOLVER=umfpack

TEST_CASE="pic_test_gnd"
CMD="$SPICE2_MPIRUN -outfile-pattern $ODIR/$TEST_CASE.%r.log -np $NP $TEST_BIN -v -i $WDIR/$TEST_CASE.inp -o $ODIR/$TEST_CASE-o -t $ODIR/$TEST_CASE-t > $ODIR/$TEST_CASE.log 2>$ODIR/$TEST_CASE.err &"
echo $CMD
eval $CMD
PIDNOS="$PIDNOS $!"

####

wait $PIDNOS

chmod -R ugo+rw $SPICE2_DIR/testing/output
