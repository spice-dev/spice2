source ../config/output.sh

# first parameter: release/debug
#	selects an executable to test
# second parameter: np/{empty}
#	if $2 == np, a variant without poisson solver is tested
#
# output: SPICE2TEST_EXECUTABLE

# Executable name consist of these parts:
#  * name -- "spice"
#  * hyphen
#  * version -- "2.10" (etc)
#  * code version suffix ("-debug"/"-release")
#  * non-poisson suffix -- np if disabled ("-np"/"") 
#  * executable suffix (".exe") -- for better filtering

build_executable_name() {

	if [ $1 == "debug" ]; then
		if [ ! -z $2 ] && [ $2 == "np" ]; then
			SPICE2TEST_EXECUTABLE="../bin/$SPICE2_NAME-$SPICE2_VERSION$SPICE2_SUFFIX_DEBUG$SPICE2_SUFFIX_NO_POISSON$SPICE2_SUFFIX_EXE"
		else
			SPICE2TEST_EXECUTABLE="../bin/$SPICE2_NAME-$SPICE2_VERSION$SPICE2_SUFFIX_DEBUG$SPICE2_SUFFIX_EXE"
		fi
	elif [ $1 == "release" ]; then
		if [ ! -z $2 ] && [ $2 == "np" ]; then
			SPICE2TEST_EXECUTABLE="../bin/$SPICE2_NAME-$SPICE2_VERSION$SPICE2_SUFFIX_RELEASE$SPICE2_SUFFIX_NO_POISSON$SPICE2_SUFFIX_EXE"
		else
			SPICE2TEST_EXECUTABLE="../bin/$SPICE2_NAME-$SPICE2_VERSION$SPICE2_SUFFIX_RELEASE$SPICE2_SUFFIX_EXE"
		fi
	else
		echo "No executable specified."
		exit
	fi

}