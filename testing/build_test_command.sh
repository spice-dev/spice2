source ../config/environment.sh
source ../config/output.sh

# $1: executable name
# $2: test name
# $3: processor count
# $4: steps limit

#/home/podolnik/usr/bin/mpirun -np 4 /home/podolnik/Simulations/spice-2.9-np -v -i SPICE2/130603/txtr-rect-20.inp -t SPICE2/130603/txtr-rect-20-t -o SPICE2/130603/txtr-rect-20-o >SPICE2/130603/txtr-rect-20.log

build_test_command() {

    PROCESSORS="-np 1"
    if [ ! -z $3 ]; then
        PROCESSORS="-np $3"
    fi

    STEP_LIMIT=""
    if [ ! -z $4 ]; then
        STEP_LIMIT="-s $4"
    fi

	if [ $SPICE2_ENVIRONMENT == "openmpi" ]; then
# -np: processor count
# -wdir: working directory (in case of SPICE2 contains injection data)
# -output-filename: output log name (some extensions will be added at the end)
		SPICE2TEST_CMD="$SPICE2_MPIRUN $PROCESSORS -wdir $SPICE2_DIR/bin -output-filename $SPICE2_DIR/testing/log/$2.log $1 -v -i $SPICE2_DIR/testing/$2.inp -t $SPICE2_DIR/testing/output/$2-t -o $SPICE2_DIR/testing/output/$2-o $STEP_LIMIT"
	elif [ $SPICE2_ENVIRONMENT == "intelmpi" ]; then
# --outfile-pattern: output log file pattern (in this case formated as openmpi output would be)
		SPICE2TEST_CMD="$SPICE2_MPIRUN $PROCESSORS -wdir $SPICE2_DIR/bin -outfile-pattern $SPICE2_DIR/testing/log/$2.log.0.%r $1 -v -i $SPICE2_DIR/testing/$2.inp -t $SPICE2_DIR/testing/output/$2-t -o $SPICE2_DIR/testing/output/$2-o $STEP_LIMIT"
	else
		echo "Incompatible environment"
		exit
	fi

}
