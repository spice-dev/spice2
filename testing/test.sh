#!/bin/bash

# $1: testing set
# $2: release/debug
# $3: np/{empty}

# these are used to generate proper names and paths
source build_executable_name.sh
source build_test_command.sh

# output directory (-p: if not present)
mkdir -p output
mkdir -p log

# this is the test command
SPICE2TEST_CMD=""
# executable name
SPICE2TEST_EXECUTABLE=""

# we will parse input parameters to these variables
SPICE2TEST_SET_HANDLE=""
SPICE2TEST_EXEC_HANDLE=""
SPICE2TEST_POISSON_HANDLE=""

# selection of the solver (mumps/umfpack)
if [ -z $1 ]; then
	echo "Using default testing schema - umfpack solver"
	SPICE2TEST_SET_HANDLE="umfpack"
else
	echo "Solver: $1"
	SPICE2TEST_SET_HANDLE=$1
fi

# selection of code version (debug/release)
if [ -z $2 ]; then
	echo "Using default testing schema - debug executable"
	SPICE2TEST_EXEC_HANDLE="debug"
else
	echo "Executable: $2"
	SPICE2TEST_EXEC_HANDLE=$2
fi

# select proper executable
build_executable_name $SPICE2TEST_EXEC_HANDLE $SPICE2TEST_POISSON_HANDLE

# create a command for this test run
# you can control the processor count and test step limits
PROC_COUNT=1
MAX_STEPS=51
build_test_command $SPICE2TEST_EXECUTABLE $SPICE2TEST_SET_HANDLE $PROC_COUNT $MAX_STEPS

# use only one thread per process
# this can change in the future...
export OMP_NUM_THREADS=1

echo $SPICE2TEST_CMD
eval $SPICE2TEST_CMD
