close all;
clear variables;

addpath('../matlab');

test_dir = 'basic-1/';
ref_dir = '/compass/Shared/Common/Theory-Modelling/PIC2/PIC_TESTS/pic_2.10_MK/';

test_names = {'pic_test_float', 'pic_test_gnd', 'pic_test_see_ebs_te'};

% for i = 1:numel(test_names)
% 
%     test_name = test_names{i};
% 
%     run_a = [test_dir, test_name, '-o'];
%     run_b = [ref_dir, test_name];
% 
%     comparison_dir = [test_dir 'comparison/' test_name];
%     if ~exist(comparison_dir, 'file')
%         mkdir(comparison_dir);
%     end 
% 
%     CompareRuns(run_a, run_b, 'Merged', 'Reference', comparison_dir)
% end


test_names = {'pic_test_coll', 'pic_test_float', 'pic_test_gnd', 'pic_test_see_ebs_te'};

for i = 1:numel(test_names)

    test_name = test_names{i};

    run_a = [test_dir, test_name, '-t'];
    run_b = [ref_dir, 't-', test_name];

    comparison_dir = [test_dir 'comparison-t/' test_name];
    if ~exist(comparison_dir, 'file')
        mkdir(comparison_dir);
    end 

    CompareRuns(run_a, run_b, 'Merged', 'Reference', comparison_dir)
end