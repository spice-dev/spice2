module hdf5_io

    use hdf5

    implicit none

    integer, parameter :: compression_level = 6

    contains

    ! simple testing subroutine for HDF5 output
    subroutine hdf5_test_write(filename, use_compression, n_x, n_y, n_z, val_d, val_i, array_d1d, array_d2d, array_d3d, array_i1d, array_i2d, array_i3d)
        implicit none

        character(len=2048), intent(in) :: filename
        logical, intent(in) :: use_compression
        integer, intent(in) :: n_x, n_y, n_z
        real*8, intent(in) :: val_d
        integer, intent(in) :: val_i
        real*8, dimension(n_x), intent(in) :: array_d1d
        real*8, dimension(n_x, n_y), intent(in) :: array_d2d
        real*8, dimension(n_x, n_y, n_z), intent(in) :: array_d3d
        integer, dimension(n_x), intent(in) :: array_i1d
        integer, dimension(n_x, n_y), intent(in) :: array_i2d
        integer, dimension(n_x, n_y, n_z), intent(in) :: array_i3d

        integer(HID_T) :: file_id       ! File identifier
        integer :: h5err


        write (*,*) 'Called hdf5_test_write'

        ! initialize HDF5
        call h5open_f(h5err)
        ! create HDF5 file
        call h5fcreate_f(trim(filename)//char(0), H5F_ACC_TRUNC_F, file_id, h5err)

        ! write data to the file
        call hdf5_w_i(file_id, val_i, 'val_i'//char(0))
        call hdf5_w_i1d(file_id, use_compression, n_x, array_i1d, 'array_i1d'//char(0))
        call hdf5_w_i2d(file_id, use_compression, n_x, n_y, array_i2d, 'array_i2d'//char(0))
        call hdf5_w_i3d(file_id, use_compression, n_x, n_y, n_z, array_i3d, 'array_i3d'//char(0))
        call hdf5_w_d(file_id, val_d, 'val_d'//char(0))
        call hdf5_w_d1d(file_id, use_compression, n_x, array_d1d, 'array_d1d'//char(0))
        call hdf5_w_d2d(file_id, use_compression, n_x, n_y, array_d2d, 'array_d2d'//char(0))
        call hdf5_w_d3d(file_id, use_compression, n_x, n_y, n_z, array_d3d, 'array_d3d'//char(0))

        ! close HDF5 file
        call h5fclose_f(file_id, h5err)
        ! shutdown HDF5
        call h5close_f(h5err)

    end subroutine hdf5_test_write

    subroutine hdf5_test_read(filename, n_x, n_y, n_z, val_d, val_i, array_d1d, array_d2d, array_d3d, array_i1d, array_i2d, array_i3d)
        implicit none

        character(len=2048), intent(in) :: filename
        integer, intent(in) :: n_x, n_y, n_z
        real*8, intent(out) :: val_d
        integer, intent(out) :: val_i
        integer, dimension(n_x), intent(out) :: array_i1d
        integer, dimension(n_x, n_y), intent(out) :: array_i2d
        integer, dimension(n_x, n_y, n_z), intent(out) :: array_i3d
        real*8, dimension(n_x), intent(out) :: array_d1d
        real*8, dimension(n_x, n_y), intent(out) :: array_d2d
        real*8, dimension(n_x, n_y, n_z), intent(out) :: array_d3d

        integer(HID_T) :: file_id       ! File identifier
        integer :: h5err

        val_i = 0
        val_d = 0
        array_i1d = 0
        array_i2d = 0
        array_i3d = 0
        array_d1d = 0
        array_d2d = 0
        array_d3d = 0
        write (*,*) 'Called hdf5_test_read'

        ! initialize HDF5
        call h5open_f(h5err)
        ! open HDF5 file for reading
        CALL h5fopen_f(trim(filename)//char(0), H5F_ACC_RDONLY_F, file_id, h5err)


        call hdf5_r_i(file_id, val_i, 'val_i'//char(0))
        call hdf5_r_i1d(file_id, n_x, array_i1d, 'array_i1d'//char(0))
        call hdf5_r_i2d(file_id, n_x, n_y, array_i2d, 'array_i2d'//char(0))
        call hdf5_r_i3d(file_id, n_x, n_y, n_z, array_i3d, 'array_i3d'//char(0))
        call hdf5_r_d(file_id, val_d, 'val_d'//char(0))
        call hdf5_r_d1d(file_id, n_x, array_d1d, 'array_d1d'//char(0))
        call hdf5_r_d2d(file_id, n_x, n_y, array_d2d, 'array_d2d'//char(0))
        call hdf5_r_d3d(file_id, n_x, n_y, n_z, array_d3d, 'array_d3d'//char(0))

        ! close HDF5 file
        call h5fclose_f(file_id, h5err)
        ! shutdown HDF5
        call h5close_f(h5err)

    end subroutine hdf5_test_read

    subroutine hdf5_r_i(file_id, val_i, var_name)
        implicit none
        integer(HID_T), intent(in) :: file_id       ! File identifier
        integer, intent(out) :: val_i
        character(len=*), intent(in) :: var_name

        integer, dimension(1) :: array_i

        call hdf5_r_i1d(file_id, 1, array_i, var_name)
        val_i = array_i(1)

    end subroutine

    subroutine hdf5_r_i1d(file_id, n_1, array_i, var_name)
        implicit none

        integer(HID_T), intent(in) :: file_id       ! File identifier
        integer, intent(in) :: n_1
        integer, dimension(n_1), intent(out) :: array_i
        character(len=*), intent(in) :: var_name

        integer, parameter :: n_dims = 1
        integer(HSIZE_T), dimension(n_dims) :: dims
        integer(HID_T) :: dset_id       ! Dataset identifier

        integer :: h5err

        array_i = 0
        dims = (/n_1/)

        call h5dopen_f(file_id, var_name, dset_id, h5err)
        call h5dread_f(dset_id, H5T_NATIVE_INTEGER, array_i, dims, h5err)
        call h5dclose_f(dset_id, h5err)
    end subroutine

    subroutine hdf5_r_i2d(file_id, n_1, n_2, array_i, var_name)
        implicit none

        integer(HID_T), intent(in) :: file_id       ! File identifier
        integer, intent(in) :: n_1, n_2
        integer, dimension(n_1, n_2), intent(out) :: array_i
        character(len=*), intent(in) :: var_name

        integer, parameter :: n_dims = 2
        integer(HSIZE_T), dimension(n_dims) :: dims
        integer(HID_T) :: dset_id       ! Dataset identifier

        integer :: h5err

        array_i = 0
        dims = (/n_1, n_2/)

        call h5dopen_f(file_id, var_name, dset_id, h5err)
        call h5dread_f(dset_id, H5T_NATIVE_INTEGER, array_i, dims, h5err)
        call h5dclose_f(dset_id, h5err)
    end subroutine

    subroutine hdf5_r_i3d(file_id, n_1, n_2, n_3, array_i, var_name)
        implicit none

        integer(HID_T), intent(in) :: file_id       ! File identifier
        integer, intent(in) :: n_1, n_2, n_3
        integer, dimension(n_1, n_2, n_3), intent(out) :: array_i
        character(len=*), intent(in) :: var_name

        integer, parameter :: n_dims = 3
        integer(HSIZE_T), dimension(n_dims) :: dims
        integer(HID_T) :: dset_id       ! Dataset identifier

        integer :: h5err

        array_i = 0
        dims = (/n_1, n_2, n_3/)

        call h5dopen_f(file_id, var_name, dset_id, h5err)
        call h5dread_f(dset_id, H5T_NATIVE_INTEGER, array_i, dims, h5err)
        call h5dclose_f(dset_id, h5err)
    end subroutine

    subroutine hdf5_r_d(file_id, val_d, var_name)
        implicit none

        integer(HID_T), intent(in) :: file_id       ! File identifier
        real*8, intent(out) :: val_d
        character(len=*), intent(in) :: var_name

        real*8, dimension(1) :: array_d

        call hdf5_r_d1d(file_id, 1, array_d, var_name)
        val_d = array_d(1)

    end subroutine

    subroutine hdf5_r_d1d(file_id, n_1, array_d, var_name)
        implicit none
        integer(HID_T), intent(in) :: file_id       ! File identifier
        integer, intent(in) :: n_1
        real*8, dimension(n_1), intent(out) :: array_d
        character(len=*), intent(in) :: var_name

        integer, parameter :: n_dims = 1
        integer(HSIZE_T), dimension(n_dims) :: dims
        integer(HID_T) :: dset_id       ! Dataset identifier

        integer :: h5err

        array_d = 0
        dims = (/n_1/)

        call h5dopen_f(file_id, var_name, dset_id, h5err)
        call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, array_d, dims, h5err)
        call h5dclose_f(dset_id, h5err)
    end subroutine

    subroutine hdf5_r_d2d(file_id, n_1, n_2, array_d, var_name)
        implicit none

        integer(HID_T), intent(in) :: file_id       ! File identifier
        integer, intent(in) :: n_1, n_2
        real*8, dimension(n_1, n_2), intent(out) :: array_d
        character(len=*), intent(in) :: var_name

        integer, parameter :: n_dims = 2
        integer(HSIZE_T), dimension(n_dims) :: dims
        integer(HID_T) :: dset_id       ! Dataset identifier

        integer :: h5err

        array_d = 0
        dims = (/n_1, n_2/)

        call h5dopen_f(file_id, var_name, dset_id, h5err)
        call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, array_d, dims, h5err)
        call h5dclose_f(dset_id, h5err)
    end subroutine

    subroutine hdf5_r_d3d(file_id, n_1, n_2, n_3, array_d, var_name)
        implicit none
        integer(HID_T), intent(in) :: file_id       ! File identifier
        integer, intent(in) :: n_1, n_2, n_3
        real*8, dimension(n_1, n_2, n_3), intent(out) :: array_d
        character(len=*), intent(in) :: var_name

        integer, parameter :: n_dims = 3
        integer(HSIZE_T), dimension(n_dims) :: dims
        integer(HID_T) :: dset_id       ! Dataset identifier

        integer :: h5err

        array_d = 0
        dims = (/n_1, n_2, n_3/)

        call h5dopen_f(file_id, var_name, dset_id, h5err)
        call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, array_d, dims, h5err)
        call h5dclose_f(dset_id, h5err)
    end subroutine

    subroutine hdf5_r_d4d(file_id, n_1, n_2, n_3, n_4, array_d, var_name)
        implicit none
        integer(HID_T), intent(in) :: file_id       ! File identifier
        integer, intent(in) :: n_1, n_2, n_3, n_4
        real*8, dimension(n_1, n_2, n_3, n_4), intent(out) :: array_d
        character(len=*), intent(in) :: var_name

        integer, parameter :: n_dims = 4
        integer(HSIZE_T), dimension(n_dims) :: dims
        integer(HID_T) :: dset_id       ! Dataset identifier

        integer :: h5err

        array_d = 0
        dims = (/n_1, n_2, n_3, n_4/)

        call h5dopen_f(file_id, var_name, dset_id, h5err)
        call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, array_d, dims, h5err)
        call h5dclose_f(dset_id, h5err)
    end subroutine

    subroutine hdf5_w_i(file_id, val_i, var_name)

        implicit none
        integer(HID_T), intent(in) :: file_id       ! File identifier
        integer, intent(in) :: val_i
        character(len=*), intent(in) :: var_name

        call hdf5_w_i1d(file_id, .false., 1, (/val_i/), var_name)
    end

    subroutine hdf5_w_i1d(file_id, use_compression, n_1, array_i, var_name)
        integer(HID_T), intent(in) :: file_id       ! File identifier
        logical, intent(in) :: use_compression
        integer, intent(in) :: n_1
        integer, dimension(n_1), intent(in) :: array_i
        character(len=*), intent(in) :: var_name

        integer, parameter :: n_dims = 1
        integer(HSIZE_T), dimension(n_dims) :: dims, chunk
        integer(HID_T) :: dset_id       ! Dataset identifier
        integer(HID_T) :: dspace_id     ! Dataspace identifier
        integer(HID_T) :: dcpl          ! Filter identifier

        integer :: rank
        integer :: h5err

        dims = (/n_1/)
        chunk = (/min(128, 1+n_1/2)/)

        call h5screate_simple_f(n_dims, dims, dspace_id, h5err)

        if (use_compression) then
            call h5pcreate_f(H5P_DATASET_CREATE_F, dcpl, h5err)
            call h5pset_deflate_f(dcpl, compression_level, h5err)
            call h5pset_chunk_f(dcpl, n_dims, chunk, h5err)

            call h5dcreate_f(file_id, trim(var_name)//char(0), H5T_NATIVE_INTEGER, dspace_id, &
                dset_id, h5err, dcpl)
        else
            call h5dcreate_f(file_id, trim(var_name)//char(0), H5T_NATIVE_INTEGER, dspace_id, &
                dset_id, h5err)
        endif

        call h5dwrite_f(dset_id, H5T_NATIVE_INTEGER, array_i, dims, h5err)
        call h5dclose_f(dset_id, h5err)
        call h5sclose_f(dspace_id, h5err)

    end

    subroutine hdf5_w_i2d(file_id, use_compression, n_1, n_2, array_i, var_name)
        integer(HID_T), intent(in) :: file_id       ! File identifier
        logical, intent(in) :: use_compression
        integer, intent(in) :: n_1, n_2
        integer, dimension(n_1, n_2), intent(in) :: array_i
        character(len=*), intent(in) :: var_name

        integer, parameter :: n_dims = 2
        integer(HSIZE_T), dimension(n_dims) :: dims, chunk
        integer(HID_T) :: dset_id       ! Dataset identifier
        integer(HID_T) :: dspace_id     ! Dataspace identifier
        integer(HID_T) :: dcpl          ! Filter identifier

        integer :: rank
        integer :: h5err

        dims = (/n_1, n_2/)
        chunk = (/min(128, 1+n_1/2), min(128, 1+n_2/2)/)


        call h5screate_simple_f(n_dims, dims, dspace_id, h5err)

        if (use_compression) then
            call h5pcreate_f(H5P_DATASET_CREATE_F, dcpl, h5err)
            call h5pset_deflate_f(dcpl, compression_level, h5err)
            call h5pset_chunk_f(dcpl, n_dims, chunk, h5err)

            call h5dcreate_f(file_id, trim(var_name)//char(0), H5T_NATIVE_INTEGER, dspace_id, &
                dset_id, h5err, dcpl)
        else
            call h5dcreate_f(file_id, trim(var_name)//char(0), H5T_NATIVE_INTEGER, dspace_id, &
                dset_id, h5err)
        endif

        call h5dwrite_f(dset_id, H5T_NATIVE_INTEGER, array_i, dims, h5err)
        call h5dclose_f(dset_id, h5err)
        call h5sclose_f(dspace_id, h5err)

    end

    subroutine hdf5_w_i3d(file_id, use_compression, n_1, n_2, n_3, array_i, var_name)
        integer(HID_T), intent(in) :: file_id       ! File identifier
        logical, intent(in) :: use_compression
        integer, intent(in) :: n_1, n_2, n_3
        integer, dimension(n_1, n_2, n_3), intent(in) :: array_i
        character(len=*), intent(in) :: var_name

        integer, parameter :: n_dims = 3
        integer(HSIZE_T), dimension(n_dims) :: dims, chunk
        integer(HID_T) :: dset_id       ! Dataset identifier
        integer(HID_T) :: dspace_id     ! Dataspace identifier
        integer(HID_T) :: dcpl          ! Filter identifier

        integer :: h5err

        dims = (/n_1, n_2, n_3/)
        chunk = (/min(128, 1+n_1/2), min(128, 1+n_2/2), min(128, 1+n_3/2)/)


        call h5screate_simple_f(n_dims, dims, dspace_id, h5err)

        if (use_compression) then
            call h5pcreate_f(H5P_DATASET_CREATE_F, dcpl, h5err)
            call h5pset_deflate_f(dcpl, compression_level, h5err)
            call h5pset_chunk_f(dcpl, n_dims, chunk, h5err)

            call h5dcreate_f(file_id, trim(var_name)//char(0), H5T_NATIVE_INTEGER, dspace_id, &
                dset_id, h5err, dcpl)
        else
            call h5dcreate_f(file_id, trim(var_name)//char(0), H5T_NATIVE_INTEGER, dspace_id, &
                dset_id, h5err)
        endif

        call h5dwrite_f(dset_id, H5T_NATIVE_INTEGER, array_i, dims, h5err)
        call h5dclose_f(dset_id, h5err)
        call h5sclose_f(dspace_id, h5err)

    end

    subroutine hdf5_w_d(file_id, val_d, var_name)

        implicit none
        integer(HID_T), intent(in) :: file_id       ! File identifier
        real*8, intent(in) :: val_d
        character(len=*), intent(in) :: var_name

        call hdf5_w_d1d(file_id, .false., 1, (/val_d/), var_name)
    end

    subroutine hdf5_w_d1d(file_id, use_compression, n_1, array_d, var_name)
        integer(HID_T), intent(in) :: file_id       ! File identifier
        logical, intent(in) :: use_compression
        integer, intent(in) :: n_1
        real*8, dimension(n_1), intent(in) :: array_d
        character(len=*), intent(in) :: var_name

        integer, parameter :: n_dims = 1
        integer(HSIZE_T), dimension(n_dims) :: dims, chunk
        integer(HID_T) :: dset_id       ! Dataset identifier
        integer(HID_T) :: dspace_id     ! Dataspace identifier
        integer(HID_T) :: dcpl          ! Filter identifier

        integer :: h5err

        dims = (/n_1/)
        chunk = (/min(128, 1+n_1/2)/)


        call h5screate_simple_f(n_dims, dims, dspace_id, h5err)

        if (use_compression) then
            call h5pcreate_f(H5P_DATASET_CREATE_F, dcpl, h5err)
            call h5pset_deflate_f(dcpl, compression_level, h5err)
            call h5pset_chunk_f(dcpl, n_dims, chunk, h5err)

            call h5dcreate_f(file_id, trim(var_name)//char(0), H5T_NATIVE_DOUBLE, dspace_id, &
                dset_id, h5err, dcpl)
        else
            call h5dcreate_f(file_id, trim(var_name)//char(0), H5T_NATIVE_DOUBLE, dspace_id, &
                dset_id, h5err)
        endif

        call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, array_d, dims, h5err)
        call h5dclose_f(dset_id, h5err)
        call h5sclose_f(dspace_id, h5err)

    end

    subroutine hdf5_w_d2d(file_id, use_compression, n_1, n_2, array_d, var_name)
        integer(HID_T), intent(in) :: file_id       ! File identifier
        logical, intent(in) :: use_compression
        integer, intent(in) :: n_1, n_2
        real*8, dimension(n_1, n_2), intent(in) :: array_d
        character(len=*), intent(in) :: var_name

        integer, parameter :: n_dims = 2
        integer(HSIZE_T), dimension(n_dims) :: dims, chunk
        integer(HID_T) :: dset_id       ! Dataset identifier
        integer(HID_T) :: dspace_id     ! Dataspace identifier
        integer(HID_T) :: dcpl          ! Filter identifier

        integer :: h5err

        dims = (/n_1, n_2/)
        chunk = (/min(128, 1+n_1/2), min(128, 1+n_2/2)/)


        call h5screate_simple_f(n_dims, dims, dspace_id, h5err)

        if (use_compression) then
            call h5pcreate_f(H5P_DATASET_CREATE_F, dcpl, h5err)
            call h5pset_deflate_f(dcpl, compression_level, h5err)
            call h5pset_chunk_f(dcpl, n_dims, chunk, h5err)

            call h5dcreate_f(file_id, trim(var_name)//char(0), H5T_NATIVE_DOUBLE, dspace_id, &
                dset_id, h5err, dcpl)
        else
            call h5dcreate_f(file_id, trim(var_name)//char(0), H5T_NATIVE_DOUBLE, dspace_id, &
                dset_id, h5err)
        endif

        call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, array_d, dims, h5err)
        call h5dclose_f(dset_id, h5err)
        call h5sclose_f(dspace_id, h5err)

    end

    subroutine hdf5_w_d3d(file_id, use_compression, n_1, n_2, n_3, array_d, var_name)
        integer(HID_T), intent(in) :: file_id       ! File identifier
        logical, intent(in) :: use_compression
        integer, intent(in) :: n_1, n_2, n_3
        real*8, dimension(n_1, n_2, n_3), intent(in) :: array_d
        character(len=*), intent(in) :: var_name

        integer, parameter :: n_dims = 3
        integer(HSIZE_T), dimension(n_dims) :: dims, chunk
        integer(HID_T) :: dset_id       ! Dataset identifier
        integer(HID_T) :: dspace_id     ! Dataspace identifier
        integer(HID_T) :: dcpl          ! Filter identifier

        integer :: h5err

        dims = (/n_1, n_2, n_3/)
        chunk = (/min(128, 1+n_1/2), min(128, 1+n_2/2), min(128, 1+n_3/2)/)


        call h5screate_simple_f(n_dims, dims, dspace_id, h5err)

        if (use_compression) then
            call h5pcreate_f(H5P_DATASET_CREATE_F, dcpl, h5err)
            call h5pset_deflate_f(dcpl, compression_level, h5err)
            call h5pset_chunk_f(dcpl, n_dims, chunk, h5err)

            call h5dcreate_f(file_id, trim(var_name)//char(0), H5T_NATIVE_DOUBLE, dspace_id, &
                dset_id, h5err, dcpl)
        else
            call h5dcreate_f(file_id, trim(var_name)//char(0), H5T_NATIVE_DOUBLE, dspace_id, &
                dset_id, h5err)
        endif

        call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, array_d, dims, h5err)
        call h5dclose_f(dset_id, h5err)
        call h5sclose_f(dspace_id, h5err)

    end

    subroutine hdf5_w_d4d(file_id, use_compression, n_1, n_2, n_3, n_4, array_d, var_name)
        integer(HID_T), intent(in) :: file_id       ! File identifier
        logical, intent(in) :: use_compression
        integer, intent(in) :: n_1, n_2, n_3, n_4
        real*8, dimension(n_1, n_2, n_3, n_4), intent(in) :: array_d
        character(len=*), intent(in) :: var_name

        integer, parameter :: n_dims = 4
        integer(HSIZE_T), dimension(n_dims) :: dims, chunk
        integer(HID_T) :: dset_id       ! Dataset identifier
        integer(HID_T) :: dspace_id     ! Dataspace identifier
        integer(HID_T) :: dcpl          ! Filter identifier

        integer :: h5err

        dims = (/n_1, n_2, n_3, n_4/)
        chunk = (/min(128, 1+n_1/2), min(128, 1+n_2/2), min(128, 1+n_3/2), min(128, 1+n_4/2)/)


        call h5screate_simple_f(n_dims, dims, dspace_id, h5err)

        if (use_compression) then
            call h5pcreate_f(H5P_DATASET_CREATE_F, dcpl, h5err)
            call h5pset_deflate_f(dcpl, 9, h5err) ! using maximal compression
            call h5pset_chunk_f(dcpl, n_dims, chunk, h5err)

            call h5dcreate_f(file_id, trim(var_name)//char(0), H5T_NATIVE_DOUBLE, dspace_id, &
                dset_id, h5err, dcpl)
        else
            call h5dcreate_f(file_id, trim(var_name)//char(0), H5T_NATIVE_DOUBLE, dspace_id, &
                dset_id, h5err)
        endif

        call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, array_d, dims, h5err)
        call h5dclose_f(dset_id, h5err)
        call h5sclose_f(dspace_id, h5err)

    end

end module hdf5_io
