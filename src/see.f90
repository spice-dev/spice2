! routines for the secondary electron emission and reflection

! angle of the newly generated electorn with respect to surface normal
subroutine generate_angular_fsee(theta)
    implicit none
    real*8:: theta,rndx

    rndx = 0.0
    ! generate uniform number between 0..1
    call G05FAF(0.d0,1.d0,1,rndx)
    ! calculate the angle
    theta = dasin(rndx)

end subroutine

! Energy of generated SEE (in eV)
subroutine generate_energy_fsse(E,Wf)
    implicit none
    real*8:: E,Wf,Emax,rndx,f,Etest
    integer::i,nsteps
    external G05CAF,G05CCF
    call G05CCF

    rndx =0.0
    Emax = 100.0 ! maximum energy to consder
    nsteps = 20; ! number of steps taken
    ! draw a random number
    call G05FAF(0.d0,1.d0,1,rndx)

    Etest = Emax/2.0
    do i=1,nsteps
        ! calculate the value of CDF for the test energy
        f = -Wf*Wf*(3.0*Etest + Wf)/(Etest + Wf)**3 + 1.0
        if (f .lt. rndx) then
            Etest = Etest + Emax/float(2**(i+1))
        else
            Etest= Etest - Emax/float(2**(i+1))
        end if

    end do

    ! final check_
    !write(*,*) 'R vs. f',rndx,f,Etest
    E = Etest
end subroutine

subroutine generate_see_yield(Einc,theta,delta,material)
    implicit none
    real*8:: Einc,theta
    real*8:: delta_m,E_m,k,x_m,a,b,e
    real*8:: delta_E,delta_theta,delta,theta_fix
    integer:: material
    ! fill the values from Panagiotis recepy
    if (material.eq.0) then
        delta_m = 0.927
        E_m = 600.0
        k = 1.38
        x_m = 2.193
        a = 0.596
        b = 1.358
    elseif (material.eq.1) then
        delta_m = 0.708
        E_m = 350.0
        k = 1.53
        x_m = 1.844
        a = 1.243
        b = 1.001

    end if
    e = 2.718281828

    ! updated: employ the formula only for theta < 75o and assume plateau for larger angles
    if (theta.lt.(75.0/180.0*3.14159)) then
        theta_fix = theta
    else
        theta_fix = 75.0/180.0*3.14159
    end if

    delta_E  = delta_m/(1.0 - e**(-x_m))*((Einc/E_m)**(1.0-k))*(1.0 - e**(-x_m*(Einc/E_m)**k))
    delta_theta = 1.0/(dcos(theta_fix)**(a*(1.0 - e**(-b*Einc/1000.0))))
    delta = delta_E*delta_theta
    !write(*,*) 'DELTA',delta_E,delta_theta,delta
end


subroutine inject_see(y,z,ux,uy,uz,stype,N_temp,Npts,sp,objects_current,objects_power_flux,edge_flux_b,edge_energy_flux_b,Nz,Ny,no_species,nobjects,E_inc,theta_inc,orientation,lambda_D,w_i,act_object,q,m,mks_info,edges_b,edge_velocity_x_b,edge_velocity_y_b,edge_velocity_z_b,use_surface_heat_tweaks,y_prim,z_prim,dz,delta_x,n_z,n_y,proc_no,Ax_half,Ay_half,Az_half,Bx_half,By_half,Bz_half,Cx_half,Cy_half,Cz_half,material,landing)
    use spice2_types
    implicit none

    integer:: Npts,N_temp,Nz,Ny,sp,no_species,nobjects,orientation,act_object,i,n_z,n_y,iyx,izx,n_zn,n_yn,proc_no,material,landing
    real*8:: E_inc,theta_inc,Wf,E_out,theta_out,psi_out,rndx,vtot,lambda_D,w_i,y_prim,z_prim,dz,delta_x,edge_tot
    real*8:: Ax_half,Ay_half,Az_half,Bx_half,By_half,Bz_half,Cx_half,Cy_half,Cz_half,ux_temp,uy_temp,uz_temp
    real*8, dimension(Npts):: y,z,ux,uy,uz
    integer*2, dimension(Npts):: stype
    real*8, dimension(Nz+1,Ny+1,no_species):: edge_flux_b,edge_energy_flux_b,edge_velocity_x_b,edge_velocity_y_b,edge_velocity_z_b
    integer, dimension(Nz+1,Ny+1):: edges_b
    real*8, dimension(2,nobjects):: objects_current,objects_power_flux
    real*8, dimension(no_species):: q,m
    logical:: use_surface_heat_tweaks
    type(mks_t):: mks_info

    if (material.eq.0) then
        ! tungsten
        Wf = 4.554
    elseif (material.eq.1) then
        ! carbon
        Wf = 5.00
    end if
    E_out = 0.0
    theta_out = 0.0
    psi_out = 0.0
    rndx = 0.0

    ! draw the energy
    call generate_energy_fsse(E_out,Wf)
    ! convert to normalized vtot
    vtot = dsqrt(E_out*2*1.602E-19/9.11E-31)/lambda_D/w_i
    N_temp = N_temp +1
    ! draw the angles
    call generate_angular_fsee(theta_out)
    call G05FAF(0.d0,1.d0,1,rndx)
    psi_out = 2*3.14159*rndx
    if (orientation.eq.1.or.((orientation.eq.5.or.orientation.eq.6).and.landing.eq.1)) then
        !upside surface
        uz(N_temp) = vtot*cos(theta_out)
        uy(N_temp) = vtot*sin(theta_out)*cos(psi_out)
        ux(N_temp) = vtot*sin(theta_out)*sin(psi_out)
        if (y_prim.gt.(n_y-1)*dz.and.y_prim.lt.(n_y*dz)) then
            y(N_temp) = y_prim
        else
            call G05FAF(0.d0,1.d0,1,rndx)
            y(N_temp) = (n_y)*dz + rndx*dz
        end if
        call G05FAF(0.d0,1.d0,1,rndx)
        z(N_temp) = (n_z)*dz + delta_x*(1.0 + rndx)

    elseif(orientation.eq.2.or.((orientation.eq.7.or.orientation.eq.8).and.landing.eq.1)) then
        ! downside surface
        uz(N_temp) = -vtot*cos(theta_out)
        uy(N_temp) = vtot*sin(theta_out)*cos(psi_out)
        ux(N_temp) = vtot*sin(theta_out)*sin(psi_out)
        if (y_prim.gt.(n_y-1)*dz.and.y_prim.lt.(n_y*dz)) then
            y(N_temp) = y_prim
        else
            call G05FAF(0.d0,1.d0,1,rndx)
            y(N_temp) = (n_y)*dz + rndx*dz
        end if
        call G05FAF(0.d0,1.d0,1,rndx)
        z(N_temp) = (n_z-1.0)*dz - delta_x*(1.0 + rndx)

    elseif(orientation.eq.4.or.((orientation.eq.6.or.orientation.eq.8).and.landing.eq.2)) then
        ! leftside surface
        uy(N_temp) = -vtot*cos(theta_out)
        uz(N_temp) = vtot*sin(theta_out)*cos(psi_out)
        ux(N_temp) = vtot*sin(theta_out)*sin(psi_out)
        call G05FAF(0.d0,1.d0,1,rndx)
        y(N_temp) = (n_y-1.0)*dz - delta_x*(1.0 + rndx)
        if (z_prim.gt.(n_z-1)*dz.and.z_prim.lt.(n_z*dz)) then
            z(N_temp) = z_prim
        else
            call G05FAF(0.d0,1.d0,1,rndx)
            z(N_temp) = (n_z)*dz + rndx*dz
        end if

    elseif(orientation.eq.3.or.((orientation.eq.5.or.orientation.eq.7).and.landing.eq.2)) then
        ! rightside surface
        uy(N_temp) = vtot*cos(theta_out)
        uz(N_temp) = vtot*sin(theta_out)*cos(psi_out)
        ux(N_temp) = vtot*sin(theta_out)*sin(psi_out)
        call G05FAF(0.d0,1.d0,1,rndx)
        y(N_temp) = (n_y)*dz + delta_x*(1.0 + rndx)
        if (z_prim.gt.(n_z-1)*dz.and.z_prim.lt.(n_z*dz)) then
            z(N_temp) = z_prim
        else
            call G05FAF(0.d0,1.d0,1,rndx)
            z(N_temp) = (n_z)*dz + rndx*dz
        end if
    end if
    stype(N_temp) = iint(real(sp))


    ! half_turn back for the LeapFrog
    ux_temp=ux(N_temp)*Ax_half + uy(N_temp)*Bx_half + uz(N_temp)*Cx_half

    uy_temp=ux(N_temp)*Ay_half + uy(N_temp)*By_half + uz(N_temp)*Cy_half

    uz_temp=ux(N_temp)*Az_half + uy(N_temp)*Bz_half + uz(N_temp)*Cz_half

    ux(N_temp)=ux_temp
    uy(N_temp)=uy_temp
    uz(N_temp)=uz_temp

    ! handle the surface diagnostics
    i = N_temp

    n_zn   = idint(z(i)/dz) + 1
    n_yn = idint(y(i)/dz) + 1
    if (n_zn.gt.(Nz+1).or.n_yn.gt.(Ny+1)) then
        write(*,*) 'SEE outside the grid',n_zn,n_yn
    end if

    !write(*,*) proc_no,'SEE',n_zn,n_yn,orientation
    iyx =y(i)/dz - n_yn +1
    izx =z(i)/dz - n_zn +1



    if (use_surface_heat_tweaks) then

        ! electrons: addition energy: Wf
        vtot = ux(i)*ux(i) + uy(i)*uy(i) + uz(i)*uz(i)+ 2.0*mks_info%mks_par1/m(stype(i))
        !			  vtot =  uz(i)*uz(i)+ 2*mks_info%mks_par1/m(stype(i))

    else
        vtot = ux(i)*ux(i) + uy(i)*uy(i) + uz(i)*uz(i)
    end if

    !      write(*,*) 'SEE OBC',act_object,- q(stype(i))*dsqrt(m(stype(i))*1837.0*mks_info%mks_main_ion_m)
    objects_current(2,act_object) = objects_current(2,act_object) - q(stype(i))*dsqrt(m(stype(i))*1837.0*mks_info%mks_main_ion_m)
    objects_power_flux(2,act_object) = objects_power_flux(2,act_object) - 0.5*m(stype(i))*dsqrt(m(stype(i))*1837.0*mks_info%mks_main_ion_m)*vtot
    edge_tot = 1.0e-6
    edge_tot = edge_tot + edges_b(n_zn,n_yn)*(1.d0-izx)*(1.d0-iyx) +  edges_b(n_zn,n_yn+1)*(1.d0-izx)*(iyx) + edges_b(n_zn +1,n_yn)*(izx)*(1.d0-iyx) +edges_b(n_zn+1,n_yn+1)*(izx)*(iyx)
    ! write(*,*) 'EDGE DIAG'
    !    write(*,*) 'edge_tot',edge_tot
    !   write(*,*) 'edges',edges_b(n_zn,n_yn), edges_b(n_zn,n_yn+1),edges_b(n_zn+1,n_yn),edges_b(n_zn+1,n_yn+1)
    ! if (edge_tot.le.0.0.or.edge_tot.gt.1.0.or. iyx.gt.1.0.or.izx.gt.1.0.or.iyx.lt.0.0.or.izx.lt.0.0) then
    ! write(*,*) proc_no,'Error in edge_tot:',edge_tot,z(i),y(i),izx,iyx
    ! stop
    !
    ! end if
    !    write(*,*) 'DGETOT',(1.d0-izx)*(1.d0-iyx)*edges_b(n_zn,n_yn)/edge_tot,(1.d0-izx)*iyx*edges_b(n_zn,n_yn+1)/edge_tot,izx*(1.d0-iyx)*edges_b(n_zn+1,n_yn)/edge_tot,izx*iyx*edges_b(n_zn+1,n_yn+1)/edge_tot
    !
    !   if ((1.d0-izx)*(1.d0-iyx)*edges_b(n_zn,n_yn)/edge_tot.gt.1.0.or.(1.d0-izx)*(1.d0-iyx)*edges_b(n_zn,n_yn)/edge_tot.lt.0.0)    then
    !       write(*,*) proc_no,'Wrong edge flux increment',(1.d0-izx)*(1.d0-iyx)*edges_b(n_zn,n_yn)/edge_tot,z(i),y(i)
    !       write(*,*) proc_no,'Comp',izx,iyx,edge_tot,edges_b(n_zn,n_yn),n_zn,n_yn
    ! stop
    ! end if


    edge_flux_b(n_zn,n_yn,stype(i)) = edge_flux_b(n_zn,n_yn,stype(i)) - (1.d0-izx)*(1.d0-iyx)*edges_b(n_zn,n_yn)/edge_tot
    edge_energy_flux_b(n_zn,n_yn,stype(i)) = edge_energy_flux_b(n_zn,n_yn,stype(i)) - (1.d0-izx)*(1.d0-iyx)*vtot*edges_b(n_zn,n_yn)/edge_tot
    edge_velocity_x_b(n_zn,n_yn,stype(i)) = edge_velocity_x_b(n_zn,n_yn,stype(i)) + (1.d0-izx)*(1.d0-iyx)*ux(i)*edges_b(n_zn,n_yn)/edge_tot
    edge_velocity_y_b(n_zn,n_yn,stype(i)) = edge_velocity_y_b(n_zn,n_yn,stype(i)) + (1.d0-izx)*(1.d0-iyx)*uy(i)*edges_b(n_zn,n_yn)/edge_tot
    edge_velocity_z_b(n_zn,n_yn,stype(i)) = edge_velocity_z_b(n_zn,n_yn,stype(i)) + (1.d0-izx)*(1.d0-iyx)*uz(i)*edges_b(n_zn,n_yn)/edge_tot


    !       if ((1.d0-izx)*iyx*edges_b(n_zn,n_yn+1)/edge_tot.gt.1.0.or.(1.d0-izx)*iyx*edges_b(n_zn,n_yn+1)/edge_tot.lt.0.0)    then
    !       write(*,*) proc_no,'Wrong edge flux increment',(1.d0-izx)*iyx*edges_b(n_zn,n_yn+1)/edge_tot
    !       write(*,*) proc_no,'Comp',izx,iyx,edge_tot,edges_b(n_zn,n_yn+1),n_zn,n_yn+1
    ! stop
    ! end if

    edge_flux_b(n_zn,n_yn+1,stype(i)) = edge_flux_b(n_zn,n_yn+1,stype(i)) - (1.d0-izx)*iyx*edges_b(n_zn,n_yn+1)/edge_tot
    edge_energy_flux_b(n_zn,n_yn+1,stype(i)) = edge_energy_flux_b(n_zn,n_yn+1,stype(i)) - (1.d0-izx)*iyx*vtot*edges_b(n_zn,n_yn+1)/edge_tot
    edge_velocity_x_b(n_zn,n_yn+1,stype(i)) = edge_velocity_x_b(n_zn,n_yn+1,stype(i)) + (1.d0-izx)*(iyx)*ux(i)*edges_b(n_zn,n_yn+1)/edge_tot
    edge_velocity_y_b(n_zn,n_yn+1,stype(i)) = edge_velocity_y_b(n_zn,n_yn+1,stype(i)) + (1.d0-izx)*(iyx)*uy(i)*edges_b(n_zn,n_yn+1)/edge_tot
    edge_velocity_z_b(n_zn,n_yn+1,stype(i)) = edge_velocity_z_b(n_zn,n_yn+1,stype(i)) + (1.d0-izx)*(iyx)*uz(i)*edges_b(n_zn,n_yn+1)/edge_tot
    !
    !      if (izx*(1.d0-iyx)*edges_b(n_zn+1,n_yn)/edge_tot.gt.1.0.or.izx*(1.d0-iyx)*edges_b(n_zn+1,n_yn)/edge_tot.lt.0.0)    then
    !       write(*,*) proc_no,'Wrong edge flux increment',izx*(1.d0-iyx)*edges_b(n_zn+1,n_yn)/edge_tot
    !       write(*,*) proc_no,'Comp',izx,iyx,edge_tot,edges_b(n_zn+1,n_yn),n_zn+1,n_yn
    ! stop
    ! end if

    edge_flux_b(n_zn+1,n_yn,stype(i)) = edge_flux_b(n_zn+1,n_yn,stype(i)) - izx*(1.d0-iyx)*edges_b(n_zn+1,n_yn)/edge_tot
    edge_energy_flux_b(n_zn+1,n_yn,stype(i)) = edge_energy_flux_b(n_zn+1,n_yn,stype(i)) - izx*(1.d0-iyx)*vtot*edges_b(n_zn+1,n_yn)/edge_tot
    edge_velocity_x_b(n_zn+1,n_yn,stype(i)) = edge_velocity_x_b(n_zn+1,n_yn,stype(i)) + (izx)*(1.d0-iyx)*ux(i)*edges_b(n_zn+1,n_yn)/edge_tot
    edge_velocity_y_b(n_zn+1,n_yn,stype(i)) = edge_velocity_y_b(n_zn+1,n_yn,stype(i)) + (izx)*(1.d0-iyx)*uy(i)*edges_b(n_zn+1,n_yn)/edge_tot
    edge_velocity_z_b(n_zn+1,n_yn,stype(i)) = edge_velocity_z_b(n_zn+1,n_yn,stype(i)) + (izx)*(1.d0-iyx)*uz(i)*edges_b(n_zn+1,n_yn)/edge_tot

    !      if (izx*iyx*edges_b(n_zn+1,n_yn+1)/edge_tot.gt.1.0.or.izx*iyx*edges_b(n_zn+1,n_yn+1)/edge_tot.lt.0.0)    then
    !       write(*,*) proc_no,'Wrong edge flux increment',izx*iyx*edges_b(n_zn+1,n_yn+1)/edge_tot
    !       write(*,*) proc_no,'Comp',izx,iyx,edge_tot,edges_b(n_zn+1,n_yn+1),n_zn+1,n_yn+1
    ! stop
    ! end if

    edge_flux_b(n_zn+1,n_yn+1,stype(i)) = edge_flux_b(n_zn+1,n_yn+1,stype(i)) - izx*iyx*edges_b(n_zn+1,n_yn+1)/edge_tot
    edge_energy_flux_b(n_zn+1,n_yn+1,stype(i)) = edge_energy_flux_b(n_zn+1,n_yn+1,stype(i)) - izx*iyx*vtot*edges_b(n_zn+1,n_yn+1)/edge_tot
    edge_velocity_x_b(n_zn+1,n_yn+1,stype(i)) = edge_velocity_x_b(n_zn+1,n_yn+1,stype(i)) + (izx)*(iyx)*ux(i)*edges_b(n_zn+1,n_yn+1)/edge_tot
    edge_velocity_y_b(n_zn+1,n_yn+1,stype(i)) = edge_velocity_y_b(n_zn+1,n_yn+1,stype(i)) + (izx)*(iyx)*uy(i)*edges_b(n_zn+1,n_yn+1)/edge_tot
    edge_velocity_z_b(n_zn+1,n_yn+1,stype(i)) = edge_velocity_z_b(n_zn+1,n_yn+1,stype(i)) + (izx)*(iyx)*uz(i)*edges_b(n_zn+1,n_yn+1)/edge_tot

    ! debug output
    !write(*,*) 'gen1',y(N_temp),z(N_temp)
    !write(*,*) 'gen2',uz(N_temp),uy(N_temp),ux(N_temp)

end subroutine


subroutine calculate_ebs_yield(Einc,theta,nu,material)
    ! calculate the yield for electron back-scattering according to Panagiotis' recipe
    implicit none
    real*8:: Einc,theta,nu
    real*8:: B_ebs_E,nu_E
    real*8:: a,b,c,e,a_ebs,b_ebs,c_ebs
    integer:: material
    ! coefficients by Panagiotis
    if (material.eq.0) then
        ! tungsten
        a = 0.96
        b = 1.118
        c = 0.706
        e = 2.718281828
        a_ebs = 0.418
        b_ebs = 1.493
        c_ebs = 0.831
    elseif (material.eq.1) then
        ! carbon
        a = 0.96
        b = 1.474
        c = 0.629
        e = 2.718281828
        a_ebs = 0.093
        b_ebs = 0.968
        c_ebs = 3.665
    end if

    B_ebs_E = a*(1 - e**(-b*(Einc/1000.0)**c))
    nu_E = a_ebs*(1 - e**(-b_ebs*((Einc/1000.0)**c_ebs)))
    nu = B_ebs_E*(nu_E/B_ebs_E)**cos(theta)
    !write(*,*) 'EBS',B_ebs,nu_E,nu
    !write(*,'("77777777, ",1x,F,5(",",F))') Einc,theta,B_ebs_E,nu_E,nu

end subroutine

subroutine precalculate_f_ebs(ns,Eout,f_ebs,cdf_ebs,material)
    implicit none
    integer:: ns,i,material
    real*8:: E,alpha,Z,bin
    real*8,dimension(ns):: Eout,f_ebs,cdf_ebs

    if (material.eq.0) then
        Z = 74 ! tungsten
    elseif (material.eq.1) then
        Z  = 6
    end if
    alpha = 0.045*Z

    ! first calculate the energy axis. it should be between 0 and 1 (Eout  = exit energy / incident energy)

    bin = 1.0/float(ns - 1)

    Eout(1) = 0.0
    do i=2,ns
        Eout(i) = Eout(i-1) + bin
    end do

    ! now calculate the function
    ! it won no. 1 in "complicated functions of 2019"
    do i=1,ns
        f_ebs(i) = 2.0/(1.0 + 2.0**alpha*(alpha - 1.0))*Eout(i)/(Eout(i)**2 -1.0)**2*((alpha*(Eout(i)**2 -1.0) -2.0)*(Eout(i)**2 +1.0)**alpha + 2**(alpha + 1.0))
    end do

    ! finally calculate the cdf
    ! simple trapezoidal
    cdf_ebs(1) = 0.0
    do i=2,(ns-1)
        cdf_ebs(i) = cdf_ebs(i-1) + (f_ebs(i-1) + f_ebs(i))/2.0*bin
    end do
    ! bit of a hack but...
    cdf_ebs(ns-1) = (cdf_ebs(ns-2) + 1.0)/2.0
    cdf_ebs(ns) = 1.0


    ! debug - write it to file
    open(UNIT=1,FILE='ebs.csv',FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")
    do i=1,ns
        write(1,*) Eout(i),f_ebs(i),cdf_ebs(i)
    end do
    close(UNIT=1)


    ! done!
end subroutine

subroutine generate_energy_ebs(ns,Eout,f_ebs,cdf_ebs,E)
    implicit none
    integer:: ns,i
    real*8:: rndx,E,bin,de1,de2
    real*8, dimension(ns):: Eout,f_ebs,cdf_ebs
    logical:: hit
    rndx = 0.0
    E = 0.0
    hit = .false.
    call G05FAF(0.d0,1.d0,1,rndx)
    do i=2,ns
        if (cdf_ebs(i-1).le.rndx.and.cdf_ebs(i).gt.rndx) then
            ! this is our slot, let's interpolate
            de1 = (rndx  - cdf_ebs(i-1))/(cdf_ebs(i) - cdf_ebs(i-1))
            de2 = (cdf_ebs(i) - rndx)/(cdf_ebs(i) - cdf_ebs(i-1))
            E = de2*Eout(i-1) + de1*Eout(i)
            hit = .true.
            !            write(*,*) 'EEBS', rndx,de1,de2,E
        end if
    end do
    if (.not.hit) then
        E = 1.0
    end if
end subroutine

subroutine generate_angular_ebs(theta,psi,material)
    implicit none
    real*8:: theta,psi,rndx,limit
    integer:: material
    rndx = 0.0
    call G05FAF(0.d0,1.d0,1,rndx)
    if (material.eq.0) then
        ! tungsten
        limit = 0.66
    elseif (material.eq.1) then
        ! carbon
        limit = 0.5
    end if

    ! updated to reflect 2/3 vs 1/3 ratio
    if (rndx.le.limit) then
        ! cosine reflection
        call G05FAF(0.d0,1.d0,1,rndx)
        ! calculate the angle
        theta = dasin(rndx)
        call G05FAF(0.d0,1.d0,1,rndx)
        psi = 2.0*3.14159*rndx

    else
        ! specular reflection
        ! theta remains, psi is shifted by pi
        psi = modulo(psi + 3.14159,2.0*3.14159)
    end if

end subroutine
subroutine inject_ebs(y,z,ux,uy,uz,stype,N_temp,Npts,sp,objects_current,objects_power_flux,edge_flux_b,edge_energy_flux_b,Nz,Ny,no_species,nobjects,Eout_ebs,theta,psi,orientation,lambda_D,w_i,act_object,q,m,mks_info,edges_b,edge_velocity_x_b,edge_velocity_y_b,edge_velocity_z_b,use_surface_heat_tweaks,y_prim,z_prim,dz,delta_x,n_z,n_y,proc_no,ninc,Ax_half,Ay_half,Az_half,Bx_half,By_half,Bz_half,Cx_half,Cy_half,Cz_half,landing)
    use spice2_types
    implicit none
    ! include "struct.h"

    integer:: Npts,N_temp,Nz,Ny,sp,no_species,nobjects,orientation,act_object,i,n_z,n_y,iyx,izx,n_zn,n_yn,proc_no,ninc,material,landing
    real*8:: E_inc,theta_inc,Wf,E_out,theta_out,psi_out,rndx,vtot,lambda_D,w_i,y_prim,z_prim,dz,delta_x,edge_tot,Eout_ebs,theta,psi
    real*8:: ux_temp,uy_temp,uz_temp,Ax_half,Ay_half,Az_half,Bx_half,By_half,Bz_half,Cx_half,Cy_half,Cz_half
    real*8, dimension(Npts):: y,z,ux,uy,uz
    integer*2, dimension(Npts):: stype
    real*8, dimension(Nz+1,Ny+1,no_species):: edge_flux_b,edge_energy_flux_b,edge_velocity_x_b,edge_velocity_y_b,edge_velocity_z_b
    integer, dimension(Nz+1,Ny+1):: edges_b
    real*8, dimension(2,nobjects):: objects_current,objects_power_flux
    real*8, dimension(no_species):: q,m
    logical:: use_surface_heat_tweaks
    type(mks_t):: mks_info

    N_temp = N_temp +1

    vtot = dsqrt(Eout_ebs*2*1.602E-19/9.11E-31)/lambda_D/w_i
    if (orientation.eq.1.or.((orientation.eq.5.or.orientation.eq.6).and.landing.eq.1)) then
        !upside surface
        uz(N_temp) = vtot*cos(theta)
        uy(N_temp) = vtot*sin(theta)*sin(psi)
        ux(N_temp) = vtot*sin(theta)*cos(psi)
        call G05FAF(0.d0,1.d0,1,rndx)
        y(N_temp) = y_prim
        z(N_temp) = (n_z)*dz + delta_x*(1.0 + rndx)

    elseif(orientation.eq.2.or.((orientation.eq.7.or.orientation.eq.8).and.landing.eq.1)) then
        ! downside surface
        uz(N_temp) = -vtot*cos(theta)
        uy(N_temp) = vtot*sin(theta)*sin(psi)
        ux(N_temp) = vtot*sin(theta)*cos(psi)
        call G05FAF(0.d0,1.d0,1,rndx)
        y(N_temp) = y_prim
        z(N_temp) = (n_z-1.0)*dz - delta_x*(1.0 + rndx)

    elseif(orientation.eq.4.or.((orientation.eq.6.or.orientation.eq.8).and.landing.eq.2)) then
        ! leftside surface
        uy(N_temp) = -vtot*cos(theta)
        uz(N_temp) = vtot*sin(theta)*sin(psi)
        ux(N_temp) = vtot*sin(theta)*cos(psi)
        call G05FAF(0.d0,1.d0,1,rndx)
        y(N_temp) = (n_y-1.0)*dz - delta_x*(1.0 + rndx)
        z(N_temp) = z_prim

    elseif(orientation.eq.3.or.((orientation.eq.5.or.orientation.eq.7).and.landing.eq.2)) then
        ! rightside surface
        uy(N_temp) = vtot*cos(theta)
        uz(N_temp) = vtot*sin(theta)*sin(psi)
        ux(N_temp) = vtot*sin(theta)*cos(psi)
        call G05FAF(0.d0,1.d0,1,rndx)
        y(N_temp) = (n_y)*dz + delta_x*(1.0 + rndx)
        z(N_temp) = z_prim

    end if
    stype(N_temp) = iint(real(sp))
    ! handle the surface diagnostics
    ! debug
    !write(*,*) 'EBS I',ux(ninc),uy(ninc),uz(ninc)
    !write(*,*) 'EBS A',theta,psi
    !write(*,*) 'EBS B',ux(N_temp),uy(N_temp),uz(N_temp)

    ! half_turn back for the LeapFrog
    ux_temp=ux(N_temp)*Ax_half + uy(N_temp)*Bx_half + uz(N_temp)*Cx_half

    uy_temp=ux(N_temp)*Ay_half + uy(N_temp)*By_half + uz(N_temp)*Cy_half

    uz_temp=ux(N_temp)*Az_half + uy(N_temp)*Bz_half + uz(N_temp)*Cz_half

    ux(N_temp)=ux_temp
    uy(N_temp)=uy_temp
    uz(N_temp)=uz_temp




    i = N_temp
    n_zn   = idint(z(i)/dz) + 1
    n_yn = idint(y(i)/dz) + 1
    if (n_zn.gt.(Nz+1).or.n_yn.gt.(Ny+1)) then
        write(*,*) 'EBS outside the grid',n_zn,n_yn
        write(*,*) 'EBS outside the grid P',z(i),y(i)
        write(*,*) 'EBS outside the grid OR',orientation, landing

        write(*,*) 'EBS outside the grid O',z_prim,y_prim
        write(*,*) 'EBS outside the grid O',vtot,theta,psi


    end if
    !write(*,*) proc_no,'SEE',n_zn,n_yn,orientation
    iyx =y(i)/dz - n_yn +1
    izx =z(i)/dz - n_zn +1



    if (use_surface_heat_tweaks) then

        ! electrons: addition energy: Wf
        vtot = ux(i)*ux(i) + uy(i)*uy(i) + uz(i)*uz(i)+ 2.0*mks_info%mks_par1/m(stype(i))
        !			  vtot =  uz(i)*uz(i)+ 2*mks_info%mks_par1/m(stype(i))

    else
        vtot = ux(i)*ux(i) + uy(i)*uy(i) + uz(i)*uz(i)
    end if

    !     write(*,*) 'EBS OBC',act_object,- q(stype(i))*dsqrt(m(stype(i))*1837.0*mks_info%mks_main_ion_m)

    objects_current(2,act_object) = objects_current(2,act_object) - q(stype(i))*dsqrt(m(stype(i))*1837.0*mks_info%mks_main_ion_m)
    objects_power_flux(2,act_object) = objects_power_flux(2,act_object) - 0.5*m(stype(i))*dsqrt(m(stype(i))*1837.0*mks_info%mks_main_ion_m)*vtot
    edge_tot = 1.0e-6
    edge_tot = edge_tot + edges_b(n_zn,n_yn)*(1.d0-izx)*(1.d0-iyx) +  edges_b(n_zn,n_yn+1)*(1.d0-izx)*(iyx) + edges_b(n_zn +1,n_yn)*(izx)*(1.d0-iyx) +edges_b(n_zn+1,n_yn+1)*(izx)*(iyx)


    edge_flux_b(n_zn,n_yn,stype(i)) = edge_flux_b(n_zn,n_yn,stype(i)) - (1.d0-izx)*(1.d0-iyx)*edges_b(n_zn,n_yn)/edge_tot
    edge_energy_flux_b(n_zn,n_yn,stype(i)) = edge_energy_flux_b(n_zn,n_yn,stype(i)) - (1.d0-izx)*(1.d0-iyx)*vtot*edges_b(n_zn,n_yn)/edge_tot
    edge_velocity_x_b(n_zn,n_yn,stype(i)) = edge_velocity_x_b(n_zn,n_yn,stype(i)) + (1.d0-izx)*(1.d0-iyx)*ux(i)*edges_b(n_zn,n_yn)/edge_tot
    edge_velocity_y_b(n_zn,n_yn,stype(i)) = edge_velocity_y_b(n_zn,n_yn,stype(i)) + (1.d0-izx)*(1.d0-iyx)*uy(i)*edges_b(n_zn,n_yn)/edge_tot
    edge_velocity_z_b(n_zn,n_yn,stype(i)) = edge_velocity_z_b(n_zn,n_yn,stype(i)) + (1.d0-izx)*(1.d0-iyx)*uz(i)*edges_b(n_zn,n_yn)/edge_tot


    edge_flux_b(n_zn,n_yn+1,stype(i)) = edge_flux_b(n_zn,n_yn+1,stype(i)) - (1.d0-izx)*iyx*edges_b(n_zn,n_yn+1)/edge_tot
    edge_energy_flux_b(n_zn,n_yn+1,stype(i)) = edge_energy_flux_b(n_zn,n_yn+1,stype(i)) - (1.d0-izx)*iyx*vtot*edges_b(n_zn,n_yn+1)/edge_tot
    edge_velocity_x_b(n_zn,n_yn+1,stype(i)) = edge_velocity_x_b(n_zn,n_yn+1,stype(i)) + (1.d0-izx)*(iyx)*ux(i)*edges_b(n_zn,n_yn+1)/edge_tot
    edge_velocity_y_b(n_zn,n_yn+1,stype(i)) = edge_velocity_y_b(n_zn,n_yn+1,stype(i)) + (1.d0-izx)*(iyx)*uy(i)*edges_b(n_zn,n_yn+1)/edge_tot
    edge_velocity_z_b(n_zn,n_yn+1,stype(i)) = edge_velocity_z_b(n_zn,n_yn+1,stype(i)) + (1.d0-izx)*(iyx)*uz(i)*edges_b(n_zn,n_yn+1)/edge_tot

    edge_flux_b(n_zn+1,n_yn,stype(i)) = edge_flux_b(n_zn+1,n_yn,stype(i)) - izx*(1.d0-iyx)*edges_b(n_zn+1,n_yn)/edge_tot
    edge_energy_flux_b(n_zn+1,n_yn,stype(i)) = edge_energy_flux_b(n_zn+1,n_yn,stype(i)) - izx*(1.d0-iyx)*vtot*edges_b(n_zn+1,n_yn)/edge_tot
    edge_velocity_x_b(n_zn+1,n_yn,stype(i)) = edge_velocity_x_b(n_zn+1,n_yn,stype(i)) + (izx)*(1.d0-iyx)*ux(i)*edges_b(n_zn+1,n_yn)/edge_tot
    edge_velocity_y_b(n_zn+1,n_yn,stype(i)) = edge_velocity_y_b(n_zn+1,n_yn,stype(i)) + (izx)*(1.d0-iyx)*uy(i)*edges_b(n_zn+1,n_yn)/edge_tot
    edge_velocity_z_b(n_zn+1,n_yn,stype(i)) = edge_velocity_z_b(n_zn+1,n_yn,stype(i)) + (izx)*(1.d0-iyx)*uz(i)*edges_b(n_zn+1,n_yn)/edge_tot

    edge_flux_b(n_zn+1,n_yn+1,stype(i)) = edge_flux_b(n_zn+1,n_yn+1,stype(i)) - izx*iyx*edges_b(n_zn+1,n_yn+1)/edge_tot
    edge_energy_flux_b(n_zn+1,n_yn+1,stype(i)) = edge_energy_flux_b(n_zn+1,n_yn+1,stype(i)) - izx*iyx*vtot*edges_b(n_zn+1,n_yn+1)/edge_tot
    edge_velocity_x_b(n_zn+1,n_yn+1,stype(i)) = edge_velocity_x_b(n_zn+1,n_yn+1,stype(i)) + (izx)*(iyx)*ux(i)*edges_b(n_zn+1,n_yn+1)/edge_tot
    edge_velocity_y_b(n_zn+1,n_yn+1,stype(i)) = edge_velocity_y_b(n_zn+1,n_yn+1,stype(i)) + (izx)*(iyx)*uy(i)*edges_b(n_zn+1,n_yn+1)/edge_tot
    edge_velocity_z_b(n_zn+1,n_yn+1,stype(i)) = edge_velocity_z_b(n_zn+1,n_yn+1,stype(i)) + (izx)*(iyx)*uz(i)*edges_b(n_zn+1,n_yn+1)/edge_tot

end subroutine
