
subroutine mumpssolver_direct_periodic(Ny,Nz,P0,PL,n,hzy,mumpsPar,flag,equipot)
    implicit none
    include 'mpif.h'
    include 'dmumps_struc.h'

    type (DMUMPS_STRUC), intent(inout):: mumpsPar

    !real*8 hzy,Ax,equipot,xpot,controllu,info,Axrow,chrg
    !integer n,Ap,Ai,flag,offset,numeric,symbolic,sys,Aprow,Airow,status
    !dimension Ap(n+1 - Nz),Ai(5*(n - Nz)),Ax(5*(n - Nz)),flag(n - Nz),equipot(n - Nz)
    !dimension chrg(n - Nz),xpot(n - Nz),controllu(20),info(90),Aprow(n+1 - Nz),Airow(5*(n - Nz)),Axrow(5*(n-Nz))
    real*8 hzy,Ax,equipot,xpot,controllu,info,chrg
    integer n,Ap,Ai,flag,offset,symbolic,sys,status

    dimension Ap(n+1 - Nz),Ai(5*(n - Nz)),Ax(5*(n - Nz)),flag(n - Nz),equipot(n - Nz)
    dimension chrg(n - Nz),xpot(n - Nz),controllu(20),info(90)

    integer idx1,idx2,index2
    !060124-AE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !integer, dimension((Ny-1)*Nz):: trow_dense
    real*8, dimension((Ny-1)*Nz):: trow_dense
    integer, dimension(5):: v

    integer:: Ny,Nz,i,j,s
    real*8:: P0,PL
    external umf4def,umf4sym,umf4pcon,umf4pinf,umf4num,umf4fsym

    logical :: debug

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!AE denotes changes and comments made by Zdenek Pekarek
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    write(*,*) 'Received params n hzy Ny Nz',n, hzy,Ny,Nz
    !init!!!
    Ap = 0
    Ai = 0
    Ax = 0

    sys = 0
    status = 0
    controllu = 0.0
    info = 0.0
    chrg = 0.0
    xpot = 0.0
    debug = false

    ! initialize mumps

    mumpsPar%JOB = -1
    mumpsPar%SYM = 0
    mumpsPar%PAR = 1
    mumpsPar%COMM = MPI_COMM_WORLD

    call dmumps(mumpsPar)

    if (debug == .false.) then
        mumpsPar%ICNTL(14) = 100
        mumpsPar%ICNTL(1) = -1
        mumpsPar%ICNTL(2) = -1
        mumpsPar%ICNTL(3) = -1
        mumpsPar%ICNTL(4) = -1
    endif

    if (mumpsPar%myId == 0) then

        !LU DECOMPOSITION (1/3): (1st stage - Creation, Transpose and Invertion of the matrix!)
        !=======================
        !create "1D" matrix in row description
        write(6,*) 'inversion of the matrix...please wait!'

        Ap(1)=0        !this sets the off-set for the beginning of each row in vectors Ax and Ai!

        mumpsPar%NZ = 5*(n - Nz)
        mumpsPar%N = n - Nz

        allocate(mumpsPar%IRN(mumpsPar%NZ))
        allocate(mumpsPar%JCN(mumpsPar%NZ))
        allocate(mumpsPar%A(mumpsPar%NZ))

        allocate(mumpsPar%RHS(mumpsPar%N))

        mumpsPar%IRN = 0
        mumpsPar%JCN = 0
        mumpsPar%A = 0
        mumpsPar%RHS = 1


        do j=1,Ny-1
            do i=1,Nz
                !     elseif
                !grid nodes with a fixed potential - AE
                if (flag(i+Nz*(j-1)).eq.0) then
                    !			trow_dense = 0.0
                    !			trow_dense(i+Nz*(j-1)) = 1.0

                    offset=Ap(i+Nz*(j-1))
                    Ap((i+Nz*(j-1)) + 1)=offset + 1
                    Ax(offset+1)=1.0
                    Ai(offset+1)=(i+Nz*(j-1)) - 1
                else if (flag(i+Nz*(j-1)).eq.2) then
                    ! still treat is as plasma
                    flag(i+Nz*(j-1)) = 1
                    ! zero e field condition
                    ! 4 point stencil only
                    trow_dense = 0.0

                    trow_dense(i+Nz*(j-1)) = 2.0*(1.0+hzy**2)
                    trow_dense(i+Nz*(j-1) -1) = -2.0
                    !trow_dense(i+Nz*(j-1) +1) = -1.0
                    if ((i+Nz*(j-1)).eq.((Ny-1)*Nz)) then
                        !use first line
                        trow_dense(Nz) = -hzy**2
                    else
                        !	trow_dense(MODULO((i+Nz*(j-1) + Nz),  ((Ny-1)*Nz))) = -hzy**2
                        trow_dense(i+Nz*(j-1) + Nz) = -hzy**2
                    end if

                    if ((i+Nz*(j-1)).eq.Nz) then
                        !use last line
                        trow_dense((Ny-1)*Nz) = -1.0*hzy**2
                    else
                        !	trow_dense(MODULO((i+Nz*(j-1) - Nz + ((Ny-1)*Nz)),  ((Ny-1)*Nz))) = -1.0*hzy**2
                        trow_dense(i+Nz*(j-1) - Nz) = -1.0*hzy**2
                    end if

                    offset=Ap(i+Nz*(j-1))

                    do idx1=1,Ny-1
                        do idx2=1,Nz
                            !060124-AE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            if (abs(trow_dense(idx2+Nz*(idx1-1))).ge.0.000001) then
                                offset = offset + 1
                                Ax(offset) = trow_dense(idx2+Nz*(idx1-1))
                                Ai(offset) = idx2+Nz*(idx1-1) -1

                            end if
                        end do
                    end do

                    Ap(i+Nz*(j-1) +1) = offset
                    ! bottom plane condition E=0
                else if (flag(i+Nz*(j-1)).eq.3) then
                    ! still treat is as plasma
                    flag(i+Nz*(j-1)) = 1
                    ! zero e field condition
                    ! 4 point stencil only
                    trow_dense = 0.0

                    trow_dense(i+Nz*(j-1)) = 2.0*(1.0+hzy**2)
                    !	trow_dense(i+Nz*(j-1) -1) = -2.0
                    trow_dense(i+Nz*(j-1) +1) = -2.0

                    if ((i+Nz*(j-1)).eq.((Ny-1)*Nz)) then
                        !use first line
                        trow_dense(1) = -hzy**2
                    else
                        trow_dense(MODULO((i+Nz*(j-1) + Nz),  ((Ny-1)*Nz))) = -hzy**2
                    end if

                    if ((i+Nz*(j-1)).eq.Nz) then
                        !use last line
                        trow_dense((Ny-2)*Nz+1) = -1.0*hzy**2
                    else
                        trow_dense(MODULO((i+Nz*(j-1) - Nz + ((Ny-1)*Nz)),  ((Ny-1)*Nz))) = -1.0*hzy**2
                    end if

                    offset=Ap(i+Nz*(j-1))

                    do idx1=1,Ny-1
                        do idx2=1,Nz
                            !060124-AE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            if (abs(trow_dense(idx2+Nz*(idx1-1))).ge.0.000001) then
                                offset = offset + 1
                                Ax(offset) = trow_dense(idx2+Nz*(idx1-1))
                                Ai(offset) = idx2+Nz*(idx1-1) -1

                            end if
                        end do
                    end do
                    Ap(i+Nz*(j-1) +1) = offset

                    !grid nodes immersed in plasma, 5-point stencil of the closest neighbours is used to compute their potential - AE
                else

                    !060124-AE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    ! improved speed by MK 2013
                    offset=Ap(i+Nz*(j-1))
                    !	trow_dense = 0.0

                    ! perhaps we need to order things?
                    v(1) = i+Nz*(j-1)
                    v(2) = i+Nz*(j-1) -1
                    v(3) = i+Nz*(j-1) +1
                    v(4) = MODULO((i+Nz*(j-1) + Nz),  ((Ny-1)*Nz))
                    v(5) = MODULO((i+Nz*(j-1) - Nz + ((Ny-1)*Nz)),  ((Ny-1)*Nz))
                    ! write(*,*) 'V1:I',v

                    call bubble_sort(v,5)
                    !write(*,*) 'V2:Q',v
                    do index2=1,5

                        if (v(index2).eq.(i+Nz*(j-1))) then
                            !		trow_dense(i+Nz*(j-1)) = 2.0*(1.0+hzy**2)
                            offset = offset + 1
                            Ax(offset) = 2.0*(1.0+hzy**2)
                            Ai(offset) = i+Nz*(j-1) -1
                            !		write (*,*) 'NM:',offset, Ax(offset),Ai(offset)
                        else if (v(index2).eq.(i+Nz*(j-1) -1)) then
                            !	trow_dense(i+Nz*(j-1) -1) = -1.0
                            offset = offset + 1
                            Ax(offset) = -1.0
                            Ai(offset) = i+Nz*(j-1) -1 -1
                            !		write (*,*) 'NM:',offset, Ax(offset),Ai(offset)
                        else if (v(index2).eq.(i+Nz*(j-1) +1)) then
                            !		trow_dense(i+Nz*(j-1) +1) = -1.0
                            offset = offset + 1
                            Ax(offset) = -1.0
                            Ai(offset) = i+Nz*(j-1) +1 -1
                            !		write (*,*) 'NM:',offset, Ax(offset),Ai(offset)
                        else if (v(index2).eq.(MODULO((i+Nz*(j-1) + Nz),  ((Ny-1)*Nz)))) then
                            !	trow_dense(MODULO((i+Nz*(j-1) + Nz),  ((Ny-1)*Nz))) = -hzy**2
                            offset = offset + 1
                            Ax(offset) = -hzy**2
                            Ai(offset) = v(index2) -1

                            !		write (*,*) 'NM:',offset, Ax(offset),Ai(offset)
                        else if (v(index2).eq.(MODULO((i+Nz*(j-1) - Nz + ((Ny-1)*Nz)),  ((Ny-1)*Nz)))) then
                            !	trow_dense(MODULO((i+Nz*(j-1) - Nz + ((Ny-1)*Nz)),  ((Ny-1)*Nz))) = -hzy**2
                            offset = offset + 1
                            Ax(offset) = -hzy**2
                            Ai(offset) = v(index2) -1
                            !		write (*,*) 'NM:',offset, Ax(offset),Ai(offset)
                        endif
                    enddo
                    !offset=Ap(i+Nz*(j-1))
                    ! 	do idx1=1,Ny-1
                    ! 		do idx2=1,Nz
                    ! 		write (*, '(36I2)') trow_dense(idx2+Nz*(idx1-1))
                    ! 		end do
                    !write(*,'(I I I I I I)') trow_dense
                    ! 	end do
                    ! write(*,'(30I2)') trow_dense
                    ! 	write(*,*) '*******'
                    !Ap((i+Nz*(j-1)) + 1)=offset + 5
                    !	do idx1=1,Ny-1
                    !		do idx2=1,Nz
                    !060124-AE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    !		if (abs(trow_dense(idx2+Nz*(idx1-1))).ge.0.000001) then
                    !			offset = offset + 1
                    !			Ax(offset) = trow_dense(idx2+Nz*(idx1-1))
                    !			Ai(offset) = idx2+Nz*(idx1-1) -1
                    !						write (*,*) 'OM:',offset, Ax(offset),Ai(offset)

                    !		end if
                    !		end do
                    !	end do
                    Ap(i+Nz*(j-1) +1) = offset
                endif
                ! write(*,'(42I2)') trow_dense
            enddo
        enddo

    endif

    !transpose the matrix from row to colum descrip. (for the lu-decomp. subroutines)
    !no longer needed
    !call umf4trnsps(n-Nz,n-Nz,Ap,Ai,Ax,Aprow,Airow,Axrow,status)

    !invert the matrix
    !---------------invert the matrix---------------1

    call umf4def(controllu)                                           !set the default parameters

    !we don't need *row anymore
    !call umf4sym(n-Nz,n-Nz,Aprow,Airow,Axrow,symbolic,controllu,info, status)       !symbolic decomposition
    call umf4sym(n-Nz,n-Nz,Ap,Ai,Ax,symbolic,controllu,info, status)       !symbolic decomposition
    write(6,*) 'DIAGNOSTIC - output of the symbolic decomposition (should be == 0)::::::::::::::', status

    write (6,*) 'Matrix Ap'
    ! !write (6,*) Ap
    write (6,*) 'Matrix Ai'
    !write (6,*) Ai
    write (6,*) 'Matrix Ax'
    !write (6,*) Ax

    write(6,*) 'DIAGNOSTIC - symbolic decomposition control settings'
    call umf4pcon(controllu)

    write(6,*) 'DIAGNOSTIC - output of the symbolic decomposition'
    call umf4pinf(info)

    !we don't need *row anymore
    !call umf4num(Aprow,Airow,Axrow,symbolic,numeric,controllu,info,status)   !numeric computation
    call umf4num(Ap,Ai,Ax,symbolic,numeric,controllu,info,status)   !numeric computation
    write(6,*) 'DIAGNOSTIC - output of the numeric decomposition (should be == 0)::::::::::::::', status

    write(6,*) 'DIAGNOSTIC - numeric decomposition control settings'
    call umf4pcon(controllu)

    write(6,*) 'DIAGNOSTIC - output of the numeric decomposition'
    call umf4pinf(info)

    call umf4fsym(symbolic)                                         !free the symbolic object
    !-----------------------------------------------2
    write(6,*) 'inversion of the matrix...done!'
    !call cpu_time(timerstop)
    !write(6,*) 'total time spent computing the inversion = ', timerstop - timerstart, ' s'
    !LU DECOMPOSITION: (end of 1st stage!)
    !=================
    write(*,*) "Numeric:",numeric


end subroutine



subroutine psolver_direct2_periodic(Ny,Nz,numeric,dz,dy,Pot,debug,rho_tot,N0,n,flag,equipot)
    implicit none
    integer:: j,k,Ny,Nz,N0
    real*8:: dz,dy,s
    integer:: n,Ap,Ai,flag,offset,symbolic,sys,status, baseidx
    integer*8:: numeric
    real*8, dimension(Nz,Ny):: Pot,charge,rho_tot
    real*8 hzy,Ax,equipot,xpot,controllu,info,chrg
    dimension chrg(n),xpot(n),controllu(20),info(90),equipot(n - Nz),flag(n - Nz)
    logical:: debug

    if (debug) then
        write(*,*) 'Received params numeric N0', numeric,N0
        write(*,*) 'Received params numeric n Nz', n, Nz

    end if
    !init
    xpot = 0.0
    controllu = 0.0
    info = 0.0
    chrg = 0.0
    symbolic = 0
    sys = 0
    status = 0
    ! baseidx = 0


    !Charge density:
    ! zero density debug
    ! s 	charge=rho_tot/(N0*dz*dy)
    ! 	charge = 0.0

    ! if (debug) then
    ! s = sum(charge)
    ! write(*,*) 'Sum of charge:',s
    ! end if
    chrg = 0.0
    !POTENTIAL (LU DECOMPOSITION (2/3) - 2nd stage!)
    !=========
    !convert 2D matrix [charge] to 1D array [chrg]
    !first copy
    do k=1,Ny-1
        do j=1,Nz
            chrg(j+Nz*(k-1))=dz**2*rho_tot(j,k)/(N0*dz*dy)    !we have to multiply by: dz**2 to have the same "units" (see starting eq.)
            !dz**2 doesn't mean the S of the cell, but originates in PDE discretization
        enddo
    enddo

    ! s = sum(chrg)
    ! if (debug) then
    ! write(*,*) 'Sum of chrg:',s
    ! end if

    ! !overwrite the new "chrg" vector with the values in "equipot"
    ! do k=1,Ny-1
    !    do j=1,Nz
    !       if (flag(j+Nz*(k-1)).eq.0) then
    !          chrg(j+Nz*(k-1))=equipot(j+Nz*(k-1))
    !       endif
    !    enddo
    ! enddo

    !overwrite the new "chrg" vector with the values in "equipot"
    !flag=1 == the grid node is in the plasma; flag==0 in the electrode/surface
    !no other values of flag() are allowed
    do k=1,Ny-1
        do j=1,Nz
            baseidx = j+Nz*(k-1)
            ! 	write(*,*) 'IDX',baseidx
            chrg(baseidx)=real(flag(baseidx))*chrg(baseidx) + (1.0 - real(flag(baseidx)) )*equipot(baseidx)
        enddo
    enddo
    ! s = sum(chrg)
    ! if (debug) then
    ! write(*,*) 'Sum of chrg:',s
    ! end if
    !
    ! s = sum(flag)
    ! if (debug) then
    ! write(*,*) 'Sum of flag:',s
    ! end if
    ! s = sum(equipot)
    ! if (debug) then
    ! write(*,*) 'Sum of equipot:',s
    ! end if


    !find a solution for each time step
    if (debug) then
        write(*,*) 'DIAGNOSTIC - now running the LU solver routine'
    end if
    !UMFPACK_At tells the solver to solve A^T x instead of Ax, so we don't need to transpose our matrix by hand anymore
    !sys=UMFPACK_At
    ! fortran doesn't know the constant above but it should be 1
    sys = 1
    if (debug) then
        write(*,*) 'sys:', sys
        write(*,*) 'numeric:', numeric
        !write(*,*) 'controllu:', controllu
        !write(*,*) 'info:', info



    end if
    call umf4sol(sys,xpot,chrg,numeric,controllu,info)         !solve the equation A.xpot=chrg (here: A=numeric)

    if (debug) then
        call umf4pinf(info)
        write(*,*) 'DIAGNOSTIC - LU solver routine finished'
    end if
    ! s = sum(xpot)
    ! if (debug) then
    ! write(*,*) 'Sum of xpot:',s
    ! end if

    !convert 1D array [xpot] to 2D matrix [Pot]
    !take the second copy
    do k=1,Ny-1
        do j=1,Nz
            Pot(j,k)=xpot(j+Nz*(k-1))
        enddo
    enddo

    do j=1,Nz
        Pot(j,Ny)=xpot(j)
    enddo

    !lu decomposition: (end of 2nd stage!)
    !-----------------
    ! s = sum(Pot)
    ! if (debug) then
    ! write(*,*) 'Sum of Pot:',s
    ! end if


end subroutine

