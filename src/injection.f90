module injection

    ! use module_name

    implicit none

    ! integer, parameter :: variables, go, here

contains
    ! subroutines concerning particle injection
    subroutine SFLUX(u,N_u,fu,X,Xu,ufu,GAMMAi0)
        implicit none
        real*8 u,fu,X,Xu,ufu,GAMMAi0
        integer N_u,j,i

        dimension X(N_u),Xu(N_u),ufu(N_u),u(N_u),fu(N_u)

        !Etape 1: calcul du tableau des X (correspondant aux u)
        !   X=0.d0*u
        do j=2,N_u
            X(j)=X(j-1)+(u(j)-u(j-1))*(fu(j)+fu(j-1))/2.d0
        enddo
        !write(*,*) 'X lining done:', sum(X)

        !Etape 2: normalisation de fu
        fu=fu/X(N_u)
        !write(*,*) 'fu norm done:', sum(fu)

        !   write(6,*) ' fu ',fu
        !   write(6,*) ' X(N_u)_old ',X(N_u)
        X=X/X(N_u)
        write(*,*) ' X(N_u)_norm ',X(N_u)

        !Etape 3: definition de ufu + son integrale
        !  Xu=0.d0*u
        !  ufu=0.d0
        do i=1,N_u
            ufu(i)=u(i)*fu(i)
        enddo
        !write(*,*) 'ufu lining done:', sum(ufu)

        do j=2,N_u
            Xu(j)=Xu(j-1)+(u(j)-u(j-1))*(ufu(j)+ufu(j-1))/2.d0
        enddo
        !write(*,*) 'Xu lining done:', sum(Xu)


        !ionic flux at t=0
        GAMMAi0=Xu(N_u)

        !Etape 4: normalisation de ufu
        ufu=ufu/Xu(N_u)
        !   write(6,*) ' u*fu',ufu
        !   write(6,*) ' Xu(N_u)_old',Xu(N_u)
        Xu=Xu/Xu(N_u)
        !   write(6,*) ' Xu(N_u)_norm ',Xu(N_u)

        return
    END

    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    subroutine VELOCITY(u,ufu,N_u,Xu,uzii)
        !          	call VELOCITY(u_z,ufu_z,N_u_z,Xu_z,uparl) !arbitrary normalized velocity
        implicit none
        real*8 u,ufu,Xu,uzii,randx
        integer N_u,j,jdwn_temp,jup_temp,it,jtemp,k

        dimension u(N_u),ufu(N_u),Xu(N_u)

        !velocity debug
        !Etape 1: Calcul des indices k tq: Xu(k)<randx<Xu(k+1)
        call G05FAF(0.d0,1.d0,1,randx)
        jdwn_temp=1
        jup_temp=N_u
        jtemp=idint((jup_temp-jdwn_temp)*1.d0/2.d0+0.5)
        k=0

        12 continue

        if (randx.gt.Xu(jtemp)) then
            jdwn_temp=jtemp

            if (randx.eq.Xu(jtemp+1)) then
                k=jtemp+1
            elseif (randx.lt.Xu(jtemp+1)) then
                k=jtemp
            else
                jtemp=jtemp+idint((jup_temp-jdwn_temp)*1.d0/2.d0+0.5)
                goto 12
            endif

        elseif (randx.lt.Xu(jtemp)) then
            jup_temp=jtemp

            if (randx.ge.Xu(jtemp-1)) then
                k=jtemp-1
            else
                jtemp=jtemp-idint((jup_temp-jdwn_temp)*1.d0/2.d0+0.5)
                goto 12
            endif
        endif

        !Etape 2: Calcul de la uzi en fonction du randx
        uzii=u(k)+(-ufu(k)+sqrt(ufu(k)*ufu(k)+2.d0*(ufu(k+1)-ufu(k))/(u(k+1)-u(k))*(randx-Xu(k))))/(ufu(k+1)-ufu(k))*(u(k+1)-u(k))
        ! write(*,*) 'Velocity: uzii',uzii
        ! return
    END


    subroutine inject_thermionic(Ninj,no_part,no_part_fin,bx,by,bz,z,y,ux,uy,uz,vtot_init,use_vtot,mono,Lz,Ly,dt, &
            utherm,sqbx2by2,N_u_z,u_z,ufu_z,Xu_z, &
            Ax_half,Bx_half,Cx_half,Ay_half,By_half,Cy_half,Az_half,Bz_half,Cz_half, &
            midLz,stype,sp,m,q,first_push,Umono,T,&
            spec_params,no_species,no_supra_loops,Ly_start,Ly_stop,z_start,z_stop,dz,Lz_disp,debug,proc_no,Nz,Ny,edges,objects,&
            dy,np,Ninj_r,edge_flux,edge_energy_flux,take_diag,objects_current,no_objects,&
            q_therm,use_surface_heat_tweaks,mks_info,Ts,N0,&
            have_impact_diag,fv_bin,emmit_diag,count,Na,cosalpha_xz,cosalpha_yz,alpha_yz,alpha_xz,ksi)

        !use IFPORT
        use spice2_types
        implicit none
        !INJECT new ions from plasma side (to replace the outflux)
        !=========================================================

        integer:: i,Ninj,no_part,no_part_fin,sp,no_species,no_supra_loops,k,r,z_start,z_stop,proc_no,Nz,Ny,iz,iy,np,y_start,y_stop,no_objects,act_object
        type(pinfo):: spec_params
        real*8:: Ninj_r,Ninj_fx,Umono,dz,Ninj_cell,Inj_counter,dy,Esec,Einc
        real*8:: bx,by,bz,first_push,Lz_disp,Ly1,Ly_total
        real*8, dimension(no_part_fin):: z,y,ux,uy,uz,vtot_init
        integer*2, dimension(no_part_fin):: stype
        logical:: mono,use_vtot
        real*8:: Lz,Ly,dt,utherm,sqbx2by2,midLz,m,q,T,Ly_extra,A,vtot_sum
        integer:: N_u_z,orientation
        real*8, dimension(N_u_z):: u_z,ufu_z,Xu_z
        real*8:: Ax_half,Bx_half,Cx_half,Ay_half,By_half,Cy_half,Az_half,Bz_half,Cz_half

        real*8:: upar1,uperp1,uperp2,ux_temp,uy_temp,uz_temp,Ly_start,Ly_stop
        real*8:: rndx, rndxn,rndx2
        logical:: discard_particle,debug,take_diag,use_surface_heat_tweaks,use_local_temp
        integer, dimension(Nz,Ny):: objects, edges
        ! injection per cell
        real*8:: init_displacement,zii,yii,vtot,q_therm
        real*8, dimension(Nz,Ny):: edge_flux,edge_energy_flux,thermionic_prob,Ts
        real*8:: no_injected_particles,no_surface_cells
        real*8, dimension(2,no_objects):: objects_current
        type(mks_t):: mks_info

        real*8:: act_temp
        real*8:: lambda_D,thermionic_A,thermionic_Wf
        integer:: N0
        ! emmit diag
        logical:: have_impact_diag
        integer:: fv_bin,count,Na
        real*8, dimension(no_species,no_objects,fv_bin,fv_bin):: emmit_diag
        real*8:: alpha_yz,alpha_xz,cosalpha_xz,cosalpha_yz,kx,ky,kz,utherm_loc,T_loc,Ts_loc,ksi,upos,uneg


        upos = 0.0
        uneg = 0.0

        ! how much are the electrons pushed out of the surface

        ! calculate the angles
        if (bx.eq.0.d0.AND.by.eq.0.d0) then
            kx = 0.0
            ky = 0.0
            kz = 1.0
        else if (alpha_yz.eq.90.0.and.alpha_xz.ne.90.0) then
            kx = cosalpha_xz
            ky = 0.0
            kz = dsqrt(1.0 - cosalpha_xz*cosalpha_xz)
        else if (alpha_yz.ne.90.0.and.alpha_xz.eq.90.0) then
            kx = 0.0
            ky = cosalpha_yz
            kz = dsqrt(1.0 - cosalpha_yz*cosalpha_yz)
        else
            kx = 1.0/dsqrt(1.0 + tan(alpha_xz)*tan(alpha_xz) + tan(alpha_xz)*tan(alpha_xz)/tan(alpha_yz)/tan(alpha_yz))
            ky = 1.0/dsqrt(1.0 + tan(alpha_yz)*tan(alpha_yz)  + tan(alpha_yz)*tan(alpha_yz)/tan(alpha_xz)/tan(alpha_xz))
            kz = 1.0/dsqrt(1.0 + 1.0/tan(alpha_yz)/tan(alpha_yz) + 1.0/tan(alpha_xz)/tan(alpha_xz))
        end if

        !    write(*,*) 'kx,ky,kx',kx,ky,kz
        ! check whether we set up the surface temperature
        if (sum(Ts).gt.0.0) then
            use_local_temp = .true.
            lambda_D = sqrt(8.85E-12*mks_info%mks_Te/mks_info%mks_n0/1.602E-19)
            ! A = 60E4 A/m2/K2 effective Richardson constant
            ! Wf = 4.55 eV based on experiment for tungsten

            thermionic_A = 60.0E4
            thermionic_Wf = 4.55
            if (debug) then
                write(*,*) 'Using local Ts',lambda_D
            end if

        else
            use_local_temp = .false.
        end if
        no_injected_particles =0
        no_surface_cells = 0
        init_displacement = spec_params%init_disp
        Ninj_cell = Ninj_r/Ly*dy
        !    write(*,*) 'Should inject particles',Ninj_r,Ninj_r/Ly
        !if (take_diag) then
        !    write(*,*) 'Edge flux prior injection',sum(edge_flux)
        !end if
        Ninj_r = 0.0
        vtot_sum = 0.0
        !     write(*,*) 'Ninj_cell',Ninj_cell
        ! Inj_counter = Ninj_r
        y_start = Ly_start/dy + 1
        y_stop = Ly_stop/dy + 1
        do iz=z_start,min(Nz,z_stop)-1
            do iy=y_start,y_stop-1
                ! surface facing up
                orientation = 0
                act_object = 0
                if (objects(iz,iy).gt.0.and.objects(iz,iy+1).gt.0.and.objects(iz+1,iy).eq.0.and.objects(iz+1,iy+1).eq.0) then
                    orientation  =1
                    act_object = objects(iz,iy)
                    ! surface facing down
                elseif (objects(iz,iy).eq.0.and.objects(iz,iy+1).eq.0.and.objects(iz+1,iy).gt.0.and.objects(iz+1,iy+1).gt.0) then
                    orientation = 2
                    act_object = objects(iz+1,iy)
                    ! surface facing left
                elseif (objects(iz,iy).gt.0.and.objects(iz,iy+1).eq.0.and.objects(iz+1,iy).gt.0.and.objects(iz+1,iy+1).eq.0) then
                    orientation = 3
                    act_object = objects(iz+1,iy)
                elseif (objects(iz,iy).eq.0.and.objects(iz,iy+1).gt.0.and.objects(iz+1,iy).eq.0.and.objects(iz+1,iy+1).gt.0) then
                    orientation = 4
                    act_object = objects(iz,iy+1)
                end if

                if (orientation.gt.0) then

                    if (use_local_temp) then
                        ! use temperature given by the grid point
                        act_temp = (Ts(iz,iy) + Ts(iz+1,iy) + Ts(iz,iy+1) +Ts(iz+1,iy+1))/dfloat(objects(iz,iy) + objects(iz+1,iy) +objects(iz,iy+1) + objects(iz+1,iy+1))

                        ! following the richardson's formula j = A*T^2*exp(-Wf/kT)
                        !    write(*,*) 'Thermionic factor A*T^2*exp(-Wf/kT)',thermionic_A*(spec_params(sp)%T*mks_info%mks_Te*11600)**2*exp(-thermionic_Wf/(spec_params(sp)%T*mks_info%mks_Te))
                        if (act_temp.gt.0.0) then
                            Inj_counter=thermionic_A/1.602E-19*(act_temp)**2*exp(-thermionic_Wf/(act_temp/11600))*dt*N0/(mks_info%mks_n0*lambda_D*(1.602E-19*mks_info%mks_B/mks_info%mks_main_ion_m/1.67E-27 ))*dy
                        else
                            Inj_counter = 0.0
                        end if
                        if (debug) then
                            write(*,*) 'Using local therm',iz,iy,act_temp,Inj_counter
                        end if
                        ! need to calculate the local utherm
                        utherm_loc =dsqrt(act_temp/11600.0/mks_info%mks_Te)*ksi/dsqrt(m)
                        T_loc = act_temp/11600.0/mks_info%mks_Te

                    else
                        Inj_counter = Ninj_cell
                        utherm_loc = utherm
                        T_loc = T
                    end if
                    no_surface_cells = no_surface_cells +1
                    ! loop over the number of particles to be injected
                    do while (Inj_counter.gt.1.0)
                        no_part=no_part+1
                        np = np+1
                        no_injected_particles  =no_injected_particles+1

                        ! generate the velocities

                        call VELOCITY(u_z,ufu_z,N_u_z,Xu_z,upar1) !arbitrary normalized velocity function
                        call G05FDF(0.d0,1.d0,1,rndxn)
                        uperp1=rndxn*utherm_loc                 !Maxwellian distribution
                        call G05FDF(0.d0,1.d0,1,rndxn)
                        uperp2=rndxn*utherm_loc                 !Maxwellian distribution
                        call G05FAF(0.d0,1.d0,1,rndx)

                        call G05FAF(0.d0,1.d0,1,rndx2)
                        ! for injected ions use only fixed dx
                        if (q.gt.0) then
                            rndx2 = 0.0
                        end if

                        if (orientation.eq.1) then
                            y(no_part) = (iy-1)*dy + dy*rndx

                            !  write(*,*) 'Injecting at y',y(no_part)
                            z(no_part) = (iz-1)*dz + init_displacement*(1.0 + rndx2)
                        elseif (orientation.eq.2) then
                            y(no_part) = (iy-1)*dy + dy*rndx
                            !   write(*,*) 'Injecting at y',y(no_part)
                            z(no_part) = (iz)*dz - init_displacement*(1.0 + rndx2)
                        elseif (orientation.eq.3) then
                            y(no_part) = (iy-1)*dy + init_displacement*(1.0 + rndx2)
                            z(no_part) = (iz-1)*dz + dz*rndx
                        elseif (orientation.eq.4) then
                            y(no_part) = (iy)*dy - init_displacement*(1.0 + rndx2)
                            z(no_part) = (iz-1)*dz + dz*rndx
                        else
                            write(*,*) 'Thermionic error - surface orientation not determined!',iy,iz
                        end if
                        stype(no_part) =  iint(real(sp))

                        if (orientation.eq.1.) then
                            uy(no_part)=uperp2
                            ux(no_part)=uperp1
                            uz(no_part)=-upar1*sqrt(T_loc)
                        else if (orientation.eq.2) then
                            uy(no_part)=uperp2
                            ux(no_part)=uperp1
                            uz(no_part)=upar1*sqrt(T_loc)
                        else if (orientation.eq.3) then
                            uy(no_part)=-upar1*sqrt(T_loc)
                            ux(no_part)=uperp1
                            uz(no_part)=uperp2
                        else if (orientation.eq.4) then
                            uy(no_part)=upar1*sqrt(T_loc)
                            ux(no_part)=uperp1
                            uz(no_part)=uperp2
                        end if


                        if (spec_params%motion_method.eq.0) then
                            ! regular motion
                            ! half turn back
                            ux_temp=ux(no_part)*Ax_half + uy(no_part)*Bx_half + uz(no_part)*Cx_half
                            uy_temp=ux(no_part)*Ay_half + uy(no_part)*By_half + uz(no_part)*Cy_half
                            uz_temp=ux(no_part)*Az_half + uy(no_part)*Bz_half + uz(no_part)*Cz_half

                            ux(no_part)=ux_temp
                            uy(no_part)=uy_temp
                            uz(no_part)=uz_temp
                        elseif (spec_params%motion_method.eq.1) then
                            ! gyrokinetic electrons
                            ! need to find the parallel velocity component
                            ux_temp = uz(no_part)*kz - uy(no_part)*ky  - ux(no_part)*kx

                            uy_temp = 0.0
                            uz_temp = dsqrt(ux(no_part)*ux(no_part)  + uy(no_part)*uy(no_part) + uz(no_part)*uz(no_part) - ux_temp*ux_temp)
                            ux(no_part) = ux_temp
                            uy(no_part) = uy_temp
                            uz(no_part) = uz_temp

                            if (ux(no_part).gt.0.0) then
                                upos = upos + 1.0
                            else
                                uneg = uneg + 1.0
                            end if
                            !     write(*,*) 'INJ',ux(no_part),uy(no_part),uz(no_part)
                            !        if (abs(ux(no_part)).lt.100.0) then
                            !         write(*,*) 'Cold INJ',ux(no_part),uy(no_part),uz(no_part)
                            !        write(*,*) 'Cold INJ POS',y(no_part),z(no_part),stype(no_part)

                            !       end if

                        end if

                        !	    write(*,*) 'THEZ',upar1
                        if (use_vtot) then
                            if (use_surface_heat_tweaks) then
                                ! add the work function
                                vtot_init(no_part) = ux(no_part)*ux(no_part) + uy(no_part)*uy(no_part) + uz(no_part)*uz(no_part) + 2*mks_info%mks_par1/spec_params%m
                            else
                                vtot_init(no_part) = ux(no_part)*ux(no_part) + uy(no_part)*uy(no_part) + uz(no_part)*uz(no_part)
                            end if
                            vtot_sum  = vtot_sum + vtot_init(no_part)
                        end if

                        !    if (uz(no_part).lt.0.0) then
                        !    write(*,*) 'Thermionic error - shooting backwards',uz(no_part),iz,iy
                        !    end if
                        ! add the objects current
                        Inj_counter = Inj_counter - 1 ! lower the counter
                        if (act_object .gt.0) then
                            objects_current(2,act_object)  = objects_current(2,act_object) -q_therm
                            !  write(*,*) 'Adding objects current',q_therm,objects_current(2,act_object)

                            if (have_impact_diag.and.count.gt.Na) then
                                call  add_to_impact_diag(ux(no_part),uy(no_part),uz(no_part),sp,fv_bin,no_species,no_objects,utherm,act_object,emmit_diag(sp,act_object,:,:))
                            end if

                        else
                            write(*,*)'Act obj failed',act_object,iz,iy,orientation
                        end if
                        ! diags
                        if (take_diag) then
                            zii  =z(no_part)/dz - idint(z(no_part)/dz)
                            yii  =y(no_part)/dy - idint(y(no_part)/dy)
                            if (yii.lt.0.0.or.yii.gt.1.0) then
                                write(*,*) 'Error in yii',yii,y(no_part),dy
                            end if
                            if (use_surface_heat_tweaks) then
                                vtot = ux(no_part)*ux(no_part) + uy(no_part)*uy(no_part) + uz(no_part)*uz(no_part)+ 2*mks_info%mks_par1/spec_params%m
                            else
                                vtot = ux(no_part)*ux(no_part) + uy(no_part)*uy(no_part) + uz(no_part)*uz(no_part)
                            end if

                            if (orientation.eq.1) then
                                !     write(*,*) 'Loop: Orientation 1,2: Adding',yii,1.0-yii
                                edge_flux(iz,iy) = edge_flux(iz,iy) - (1.0-yii)
                                edge_flux(iz,iy+1) = edge_flux(iz,iy+1) - yii
                                edge_energy_flux(iz,iy) = edge_energy_flux(iz,iy) - (1.0-yii)*vtot
                                edge_energy_flux(iz,iy+1) = edge_energy_flux(iz,iy+1) - yii*vtot
                            elseif (orientation.eq.2) then
                                edge_flux(iz+1,iy) = edge_flux(iz+1,iy) - (1.0-yii)
                                edge_flux(iz+1,iy+1) = edge_flux(iz+1,iy+1) - yii
                                edge_energy_flux(iz+1,iy) = edge_energy_flux(iz+1,iy) - (1.0-yii)*vtot
                                edge_energy_flux(iz+1,iy+1) = edge_energy_flux(iz+1,iy+1) - yii*vtot

                            elseif (orientation.eq.3) then
                                edge_flux(iz,iy) = edge_flux(iz,iy) - (1.0-zii)
                                edge_flux(iz+1,iy) = edge_flux(iz+1,iy) - zii
                                edge_energy_flux(iz,iy) = edge_energy_flux(iz,iy) - (1.0-zii)*vtot
                                edge_energy_flux(iz+1,iy) = edge_energy_flux(iz+1,iy) - zii*vtot
                            elseif (orientation.eq.4) then
                                edge_flux(iz,iy+1) = edge_flux(iz,iy+1) - (1.0-zii)
                                edge_flux(iz+1,iy+1) = edge_flux(iz+1,iy+1) - zii
                                edge_energy_flux(iz,iy+1) = edge_energy_flux(iz,iy+1) - (1.0-zii)*vtot
                                edge_energy_flux(iz+1,iy+1) = edge_energy_flux(iz+1,iy) - zii*vtot

                            else
                                write(*,*) 'Thermionic error - surface orientation not determined!',iy,iz
                            end if

                        end if


                    end do ! end of particle loop

                    ! give it a try for the last particle

                    call G05FAF(0.d0,1.d0,1,rndx)
                    ! write(*,*) 'Randing',rndx,Inj_counter
                    if (rndx.lt.Inj_counter) then
                        ! inject the particle here
                        no_part=no_part+1
                        no_injected_particles = no_injected_particles + 1
                        np = np+1

                        call VELOCITY(u_z,ufu_z,N_u_z,Xu_z,upar1) !arbitrary normalized velocity function
                        call G05FDF(0.d0,1.d0,1,rndxn)
                        uperp1=rndxn*utherm_loc                 !Maxwellian distribution
                        call G05FDF(0.d0,1.d0,1,rndxn)
                        uperp2=rndxn*utherm_loc                 !Maxwellian distribution
                        call G05FAF(0.d0,1.d0,1,rndx)

                        call G05FAF(0.d0,1.d0,1,rndx2)

                        if (orientation.eq.1) then
                            y(no_part) = (iy-1)*dy + dy*rndx

                            !  write(*,*) 'Injecting at y',y(no_part)
                            z(no_part) = (iz-1)*dz + init_displacement*(1.0 + rndx2)
                        elseif (orientation.eq.2) then
                            y(no_part) = (iy-1)*dy + dy*rndx
                            !   write(*,*) 'Injecting at y',y(no_part)
                            z(no_part) = (iz)*dz - init_displacement*(1.0 + rndx2)
                        elseif (orientation.eq.3) then
                            y(no_part) = (iy-1)*dy + init_displacement*(1.0 + rndx2)
                            z(no_part) = (iz-1)*dz + dz*rndx
                        elseif (orientation.eq.4) then
                            y(no_part) = (iy)*dy - init_displacement*(1.0 + rndx2)
                            z(no_part) = (iz-1)*dz + dz*rndx
                        else
                            write(*,*) 'Thermionic error - surface orientation not determined!',iy,iz
                        end if
                        stype(no_part) =  iint(real(sp))

                        if (orientation.eq.1.) then
                            uy(no_part)=uperp2
                            ux(no_part)=uperp1
                            uz(no_part)=-upar1*sqrt(T_loc)
                        else if (orientation.eq.2) then
                            uy(no_part)=uperp2
                            ux(no_part)=uperp1
                            uz(no_part)=upar1*sqrt(T_loc)
                        else if (orientation.eq.3) then
                            uy(no_part)=-upar1*sqrt(T)
                            ux(no_part)=uperp1
                            uz(no_part)=uperp2
                        else if (orientation.eq.4) then
                            uy(no_part)=upar1*sqrt(T_loc)
                            ux(no_part)=uperp1
                            uz(no_part)=uperp2
                        end if


                        if (spec_params%motion_method.eq.0) then
                            ! regular motion
                            ! half turn back
                            ux_temp=ux(no_part)*Ax_half + uy(no_part)*Bx_half + uz(no_part)*Cx_half
                            uy_temp=ux(no_part)*Ay_half + uy(no_part)*By_half + uz(no_part)*Cy_half
                            uz_temp=ux(no_part)*Az_half + uy(no_part)*Bz_half + uz(no_part)*Cz_half

                            ux(no_part)=ux_temp
                            uy(no_part)=uy_temp
                            uz(no_part)=uz_temp
                        elseif (spec_params%motion_method.eq.1) then
                            ! gyrokinetic electrons
                            ! need to find the parallel velocity component
                            !  write(*,*) 'INJ',uz(no_part),kz
                            ux_temp = uz(no_part)*kz - uy(no_part)*ky  - ux(no_part)*kx
                            uy_temp = 0.0
                            uz_temp = dsqrt(ux(no_part)*ux(no_part)  + uy(no_part)*uy(no_part) + uz(no_part)*uz(no_part) - ux_temp*ux_temp)
                            ux(no_part) = ux_temp
                            uy(no_part) = uy_temp
                            uz(no_part) = uz_temp
                            if (ux(no_part).gt.0.0) then
                                upos = upos + 1.0
                            else
                                uneg = uneg + 1.0
                            end if

                            !        write(*,*) 'INJ',no_part,ux(no_part)
                            !                write(*,*) 'INJN',ux(no_part),uy(no_part),uz(no_part)
                            !   if (abs(ux(no_part)).lt.100.0) then
                            !    write(*,*) 'Cold INJN',ux(no_part),uy(no_part),uz(no_part)
                            !      write(*,*) 'Cold INJN POS',y(no_part),z(no_part),stype(no_part)

                            !     end if

                        end if
                        if (use_surface_heat_tweaks) then
                            ! add the work function
                            vtot_init(no_part) = ux(no_part)*ux(no_part) + uy(no_part)*uy(no_part) + uz(no_part)*uz(no_part) + 2*mks_info%mks_par1/spec_params%m
                        else
                            vtot_init(no_part) = ux(no_part)*ux(no_part) + uy(no_part)*uy(no_part) + uz(no_part)*uz(no_part)
                        end if
                        vtot_sum  = vtot_sum + vtot_init(no_part)

                        if (have_impact_diag.and.count.gt.Na) then
                            call  add_to_impact_diag(ux(no_part),uy(no_part),uz(no_part),sp,fv_bin,no_species,no_objects,utherm,act_object,emmit_diag(sp,act_object,:,:))
                        end if

                        objects_current(2,act_object)  = objects_current(2,act_object) -q_therm
                        ! diags
                        if (take_diag) then
                            if (use_surface_heat_tweaks) then
                                vtot = ux(no_part)*ux(no_part) + uy(no_part)*uy(no_part) + uz(no_part)*uz(no_part)+ 2*mks_info%mks_par1/spec_params%m
                            else
                                vtot = ux(no_part)*ux(no_part) + uy(no_part)*uy(no_part) + uz(no_part)*uz(no_part)
                            end if

                            ! add the particle to edge_flux
                            zii  =z(no_part)/dz - idint(z(no_part)/dz)
                            yii  =y(no_part)/dz - idint(y(no_part)/dy)
                            if (yii.lt.0.0.or.yii.gt.1.0) then
                                write(*,*) 'Error in yii',yii,y(no_part),dy
                            end if

                            if (orientation.eq.1) then
                                !     write(*,*) 'Loop: Orientation 1,2: Adding',yii,1.0-yii
                                edge_flux(iz,iy) = edge_flux(iz,iy) - (1.0-yii)
                                edge_flux(iz,iy+1) = edge_flux(iz,iy+1) - yii
                                edge_energy_flux(iz,iy) = edge_energy_flux(iz,iy) - (1.0-yii)*vtot
                                edge_energy_flux(iz,iy+1) = edge_energy_flux(iz,iy+1) - yii*vtot
                            elseif (orientation.eq.2) then
                                edge_flux(iz+1,iy) = edge_flux(iz+1,iy) - (1.0-yii)
                                edge_flux(iz+1,iy+1) = edge_flux(iz+1,iy+1) - yii
                                edge_energy_flux(iz+1,iy) = edge_energy_flux(iz+1,iy) - (1.0-yii)*vtot
                                edge_energy_flux(iz+1,iy+1) = edge_energy_flux(iz+1,iy+1) - yii*vtot

                            elseif (orientation.eq.3) then
                                edge_flux(iz,iy) = edge_flux(iz,iy) - (1.0-zii)
                                edge_flux(iz+1,iy) = edge_flux(iz+1,iy) - zii
                                edge_energy_flux(iz,iy) = edge_energy_flux(iz,iy) - (1.0-zii)*vtot
                                edge_energy_flux(iz+1,iy) = edge_energy_flux(iz+1,iy) - zii*vtot
                            elseif (orientation.eq.4) then
                                edge_flux(iz,iy+1) = edge_flux(iz,iy+1) - (1.0-zii)
                                edge_flux(iz+1,iy+1) = edge_flux(iz+1,iy+1) - zii
                                edge_energy_flux(iz,iy+1) = edge_energy_flux(iz,iy+1) - (1.0-zii)*vtot
                                edge_energy_flux(iz+1,iy+1) = edge_energy_flux(iz+1,iy) - zii*vtot

                            else
                                write(*,*) 'Thermionic error - surface orientation not determined!',iy,iz
                            end if

                        end if
                        !  write(*,*) 'Fired last particle',Inj_counter,y(no_part)
                        !	if (z(no_part).lt.0.0.or.y(no_part).lt.0.0) then
                        !	      write(*,*) 'Thermionic injection Error:',no_part, z(no_part),y(no_part),iy,iz
                        !	end if
                        !	write(*,*) 'Thermionic velocity',ux(no_part),uy(no_part),uz(no_part)
                        !            Inj_counter = Inj_counter - 1



                    else ! this particle was not injected, add it to the next round
                        !   Ninj_r = Ninj_r + Inj_counter
                    end if ! prob


                end if ! surface   - orientation
            end do
        end do
        ! if (no_injected_particles.gt.0) then
        ! write(*,*) 'Injected particles',no_injected_particles,vtot_sum,use_vtot
        ! if (take_diag) then
        !	  write(*,*) 'Inj: Edge energy flux sum',sum(edge_energy_flux)
        !     end if
        !    end if
        !    write(*,*) 'Did inject particles',no_injected_particles
        !substract the number of injected particle from the residuals
        ! assume the periodic boudary includes 1 surface cell on the right
        !   Ninj_r = Ninj_r - real(no_injected_particles)/real(no_surface_cells +1)*Ly
        !  if (take_diag) then
        !   write(*,*) 'Edge_flux detected ',sum(edge_flux)
        !    end if
        ! final report
        !   write(*,*) 'U par ratio',upos/uneg,upos, uneg

    end subroutine




    subroutine inject_top(Ninj,no_part,no_part_fin,bx,by,bz,z,y,ux,uy,uz,mono,Lz,Ly,dt,&
            utherm,sqbx2by2,N_u_z,u_z,ufu_z,Xu_z,&
            Ax_half,Bx_half,Cx_half,Ay_half,By_half,Cy_half,Az_half,Bz_half,Cz_half,&
            midLz,stype,sp,m,q,first_push,Umono,T,&
            spec_params,no_species,no_supra_loops,Ly_start,Ly_stop,z_start,z_stop,dz,Lz_disp,debug,proc_no,total_energy,coll_test)
        !use IFPORT
        use spice2_types
        implicit none
        !INJECT new ions from plasma side (to replace the outflux)
        !=========================================================
        !include "struct.h"
        integer:: i,Ninj,no_part,no_part_fin,sp,no_species,no_supra_loops,k,r,z_start,z_stop,proc_no
        type(pinfo), dimension(no_species):: spec_params
        real*8:: Ninj_r,Ninj_fx,Umono,dz
        real*8:: bx,by,bz,first_push,Lz_disp,Ly1,Ly_total
        real*8, dimension(no_part_fin):: z,y,ux,uy,uz
        integer*2, dimension(no_part_fin):: stype
        logical:: mono
        real*8:: Lz,Ly,dt,utherm,sqbx2by2,midLz,m,q,T,Ly_extra,A
        integer:: N_u_z
        real*8, dimension(N_u_z):: u_z,ufu_z,Xu_z
        real*8:: Ax_half,Bx_half,Cx_half,Ay_half,By_half,Cy_half,Az_half,Bz_half,Cz_half
        ! MK: Renaud, je vais te tuer, pq uparL et pas upar1 (one)?????
        real*8:: upar1,uperp1,uperp2,ux_temp,uy_temp,uz_temp,Ly_start,Ly_stop
        real*8:: rndx, rndxn,rndx2
        real*8, dimension(no_species,4):: total_energy
        logical:: discard_particle,debug,coll_test
        ! write(*,*) 'Injecting', Ninj, 'particles'
        !Generate a velocity distribution for the ions.
        if (debug) then
            write(*,*) proc_no,'Injecting into y range',Ly_start,Ly_stop,(z_start-1)*dz,(z_stop-1)*dz
        end if

        ! this is the secondary
        if (abs(Lz_disp).gt.0.0) then
            Ly1 = abs(Lz_disp)/abs(bz/by)
            A = Ly*abs(bz/by)
        else
            Ly1 = 0.0
            A = 0.0
        end if

        !write(*,*) 'Lz_disp Ly1',Lz_disp,Ly1,bz/by
        if (Lz_disp.gt.0.0.and.by.lt.0.0) then
            Ly_total = Ly + Ly1
        else
            Ly_total = Ly
        end if
        if (debug) then
            write(*,*) 'Injection: Lz_disp Ly1 A Ly_total',Lz_disp,Ly1,A,Ly_total
        end if
        do i=1,Ninj
            no_part=no_part+1
            discard_particle=.false.
            !MK 14/08/06
            !now is the time to initalize particle
            z(no_part)=0.d0       !toroidal position (the initial position is defined in the injection module)
            y(no_part)=0.d0       !radial position (the initial position is defined in the injection module)
            uz(no_part)=0.d0      !parallel speed
            uy(no_part)=0.d0      !radial speed
            ux(no_part)=0.d0      !poloidal speed
            stype(no_part) = iint(real(sp))
            !=====================================OLD METHOD============================================
            if (bx.eq.0.d0.AND.by.eq.0.d0) then          ! Bz = z-axis !!! SO WE USE OLD METHOD!
                !in z-direction
                !--------------
                if (mono) then          !NEW RD
                    upar1=Umono
                    upar1 = -upar1
                else
                    call VELOCITY(u_z,ufu_z,N_u_z,Xu_z,upar1) !arbitrary normalized velocity function
                endif

                if (spec_params(stype(no_part))%motion_method.eq.1) then
                    ux(no_part) = upar1
                    uy(no_part)  = 0.0
                    uz(no_part) = 0.0
                    !        write(*,*) 'Old method gyro'
                    call G05FAF(0.d0,1.d0,1,rndx)
                    z(no_part)=Lz+ux(no_part)*dt*rndx               !uniform distribution in space

                else
                    !        write(*,*) 'Old method regular'
                    !! TEMPERATURE HACK - should have been here before???
                    uz(no_part)=upar1
                    !in y-direction
                    !--------------

                    ! tempoerary hack for collision testing
                    if (q.lt.0.0.and.coll_test) then
                        call G05FDF(0.d0,1.d0,1,rndxn)
                        uy(no_part)=rndxn*utherm*0.0001
                        !in x-direction
                        !--------------
                        call G05FDF(0.d0,1.d0,1,rndxn)
                        ux(no_part)=rndxn*utherm*0.0001
                    else
                        ! normal velocities
                        call G05FDF(0.d0,1.d0,1,rndxn)
                        uy(no_part)=rndxn*utherm
                        !in x-direction
                        !--------------
                        call G05FDF(0.d0,1.d0,1,rndxn)
                        ux(no_part)=rndxn*utherm
                        ! hack for testing normal incidence
                        if (mono) then
                            ux(no_part) = ux(no_part)/1.0E6
                            uy(no_part) = uy(no_part)/1.0E6
                        end if


                    end if
                    call G05FAF(0.d0,1.d0,1,rndx)
                    call G05FAF(0.d0,1.d0,1,rndx2)

                    z(no_part)=Lz+uz(no_part)*dt*rndx               !uniform distribution in space
                    if (z(no_part).lt.((z_start-1)*dz).or.(z(no_part).gt.((z_stop-1)*dz))) then
                        discard_particle = .true.
                        !                          if (z_stop.gt.80) then
                        !                      write(*,*) 'Discarded particle because of wrong origin',z(no_part)
                        !                            end if
                    end if

                end if


                call G05FAF(0.d0,1.d0,1,rndx)
                y(no_part)=rndx*Ly                          !uniform distribution in space

                if ((y(no_part).lt.Ly_start).or.(y(no_part).gt.Ly_stop)) then
                    discard_particle = .true.
                end if

                !--------------
                !1/2 acceleration to set v-1/2 at initial time (LeapFrog)
                !RD! IF WE WANT TO DO IT, WE SHOULD DEFINE Ezi(new-Particle) and izi, izi1 etc...
                !RD!uzi(N_i)=uzi(N_i)+dt0*ksi**2*Ezi(N_i)+dt0*(uxi(N_i)*by-uyi(N_i)*bx)

            else
                !=======================================NEW METHOD====================================
                !in the parallel direction (NEW!!)
                !-------------------------
                if (mono) then
                    upar1=Umono
                else
                    call VELOCITY(u_z,ufu_z,N_u_z,Xu_z,upar1) !arbitrary normalized velocity function
                endif
                upar1 = -upar1
                ! 	write(*,*) 'Upar1',sp,upar1

                !in the perpendicular-1 direction (NEW!!)
                !--------------------------------
                call G05FDF(0.d0,1.d0,1,rndxn)
                uperp1=rndxn*utherm                 !Maxwellian distribution
                ! 	write(*,*) 'Uperp1',sp,uperp1

                !in the perpendicular-2 direction
                !--------------------------------
                call G05FDF(0.d0,1.d0,1,rndxn)
                uperp2=rndxn*utherm                 !Maxwellian distribution
                ! bad hack
                !       uperp1 = 0.0
                !       uperp2 = 0.0

                ! 	write(*,*) 'Uperp2',sp,uperp2
                !hack to have non-gyrating ions
                !if (q.gt.0.0) then
                !uperp1 = 0.0
                !uperp2 = 0.0
                ! hack to test normal injection
                if (mono) then
                    uperp1 = uperp1/1.0E6
                    uperp2 = uperp2/1.0E6
                end if

                !end if
                !===========================================================================================
                if (spec_params(stype(no_part))%motion_method.eq.1) then
                    ! guiding center approximation motion
                    !         upar1 = -5.0
                    !         uperp1 = 0.0
                    !         uperp2 = 0.0
                    ux(no_part) = -upar1
                    !            ux(no_part) = -5.0
                    uy(no_part) = 0.0
                    uz(no_part) = dsqrt(uperp1*uperp1 + uperp2*uperp2)
                    call G05FAF(0.d0,1.d0,1,rndx)
                    !  call G05FAF(0.d0,1.d0,1,rndx2)

                    z(no_part) =midLz+upar1*dt*rndx*bz !+ Lz_disp*rndx2*20
                    !  call G05FAF(0.d0,1.d0,1,rndxn)
                    ! shuffle thing
                    !       do r=1,int(rndxn*10.0)
                    !          call G05FAF(0.d0,1.d0,1,rndx)
                    !
                    !    end do
                    call G05FAF(0.d0,1.d0,1,rndx)

                    y(no_part) = Ly*rndx
                    !        write(*,*) 'New method gyro
                    !write (*,*) 'INJ',z(no_part),y(no_part),ux(no_part)
                else
                    ! regular motion injection
                    !        write(*,*) 'New method regular'

                    !in z-direction
                    !--------------
                    uz(no_part)=upar1*bz+uperp2*sqbx2by2

                    ! version 0.7 had this
                    ! call G05FAF(0.d0,1.d0,1,rand)
                    !       ze(N_e)=midLze+uparle*dt*rand*bz-uperp1e/mu*sqbx2by2
                    ! this generates electrons inside the simulation region!!!

                    ! decide if we inject in the regular  part or the extra one
                    if (Ly1.gt.0) then
                        call G05FAF(0.d0,1.d0,1,rndx)
                        if (rndx.lt.Ly/Ly_total) then
                            ! regular injection
                            call G05FAF(0.d0,1.d0,1,rndx)
                            z(no_part)=midLz+upar1*dt*rndx*bz*first_push+q*uperp1*m*sqbx2by2 + A !+ Lz_disp*rndx2*20   !+uperp1i is ok for the ions!!
                            if (z(no_part).lt.((z_start-1)*dz).or.(z(no_part).gt.((z_stop-1)*dz))) then
                                discard_particle = .true.
                            end if
                            call G05FAF(0.d0,1.d0,1,rndx)
                            if (Lz_disp.ge.0.and.by.lt.0.0) then
                                y(no_part)=rndx*Ly
                            else
                                y(no_part)=rndx*(Ly - Ly1)

                            end if
                            if ((y(no_part).lt.Ly_start).or.(y(no_part).gt.Ly_stop)) then
                                discard_particle = .true.
                            end if
                            !  write(*,*) 'Regular particle',y(no_part),z(no_part),uz(no_part),uy(no_part),stype(no_part)
                        else
                            !write(*,*) 'Extra injection',rndx,Ly,Ly_total
                            ! extra injection
                            call G05FAF(0.d0,1.d0,1,rndx)
                            Ly_extra = Ly1*rndx
                            k = floor(Ly_extra/Ly)
                            call G05FAF(0.d0,1.d0,1,rndx)
                            z(no_part)=midLz+upar1*dt*rndx*bz*first_push+q*uperp1*m*sqbx2by2  - k*A !+ Lz_disp*rndx2*20   !+uperp1i is ok for the ions!!
                            !    write(*,*) 'Extra inj',midLz,A,k,Ly_extra
                            if (z(no_part).lt.((z_start-1)*dz).or.(z(no_part).gt.((z_stop-1)*dz))) then
                                discard_particle = .true.
                            end if
                            if (by.lt.0) then
                                y(no_part)=Ly_extra - k*Ly                           !uniform distribution in space
                            else
                                y(no_part)=Ly - (Ly_extra - k*Ly)        !for reverse injection we stick the extra injection to y=Ly
                                ! write(*,*) 'Extra particle',y(no_part),z(no_part),uz(no_part),stype(no_part)
                            end if
                            if ((y(no_part).lt.Ly_start).or.(y(no_part).gt.Ly_stop)) then
                                discard_particle = .true.
                            end if



                        end if ! reguler/extra injection region
                        uy(no_part)=upar1*by+uperp1*bx/sqbx2by2-uperp2*by*bz/sqbx2by2
                        ux(no_part)=upar1*bx-uperp1*by/sqbx2by2-uperp2*bx*bz/sqbx2by2

                        ! injection without the BC jump
                    else
                        call G05FAF(0.d0,1.d0,1,rndx)
                        call G05FAF(0.d0,1.d0,1,rndx2)

                        z(no_part)=midLz+upar1*dt*rndx*bz*first_push+q*uperp1*m*sqbx2by2 !+ Lz_disp*rndx2*20   !+uperp1i is ok for the ions!!
                        !zi(N_i)=Lz+uparli*dt*rndx*bz+(uperp1i+dsqrt(uperp1i**2+uperp2i**2))*sqbx2by2      !NEW 4/7/06 (JG)
                        !     if (z(no_part) < Lz) then
                        ! 	write(*,*) 'Warning, particle injected inside the box!!!',z(no_part),m,upar1,uperp1,uperp2
                        if (z(no_part).lt.((z_start-1)*dz).or.(z(no_part).gt.((z_stop-1)*dz))) then
                            discard_particle = .true.
                        end if

                        ! end if
                        !in y-direction
                        !--------------
                        uy(no_part)=upar1*by+uperp1*bx/sqbx2by2-uperp2*by*bz/sqbx2by2

                        call G05FAF(0.d0,1.d0,1,rndx)
                        y(no_part)=rndx*Ly                           !uniform distribution in space
                        if ((y(no_part).lt.Ly_start).or.(y(no_part).gt.Ly_stop)) then
                            discard_particle = .true.
                        end if

                        !in x-direction
                        !--------------
                        ux(no_part)=upar1*bx-uperp1*by/sqbx2by2-uperp2*bx*bz/sqbx2by2
                    end if ! BC injection
                end if
                !==================================================================================
            endif   !if (bx.eq.0.d0.AND.by.eq.0.d0) then

            !1/2 wind back for next L.F
            !--------------------------
            if (spec_params(stype(no_part))%motion_method.eq.0) then

                ! get some statistics
                !  if (stype(no_part).gt.1) then
                !  write(*,*) 'INJEP',ux(no_part),uy(no_part),uz(no_part)
                !  end if

                ux_temp=ux(no_part)*Ax_half + uy(no_part)*Bx_half + uz(no_part)*Cx_half

                uy_temp=ux(no_part)*Ay_half + uy(no_part)*By_half + uz(no_part)*Cy_half

                uz_temp=ux(no_part)*Az_half + uy(no_part)*Bz_half + uz(no_part)*Cz_half

                ux(no_part)=ux_temp
                uy(no_part)=uy_temp
                uz(no_part)=uz_temp
                !  if (stype(no_part).gt.1) then
                !   write(*,*) 'INJEP',ux(no_part),uy(no_part),uz(no_part)
                !   end if

                !        if (q < 0) then
                !        write(*,*) 'QINTH',z(no_part),y(no_part)
                !        write(*,*) 'QINTV',ux(no_part),uy(no_part),uz(no_part)
                !
                !
                !
                !       end if

            else if (spec_params(stype(no_part))%motion_method.eq.2) then
                do k=1,no_supra_loops
                    ux_temp=ux(no_part)*Ax_half + uy(no_part)*Bx_half + uz(no_part)*Cx_half

                    uy_temp=ux(no_part)*Ay_half + uy(no_part)*By_half + uz(no_part)*Cy_half

                    uz_temp=ux(no_part)*Az_half + uy(no_part)*Bz_half + uz(no_part)*Cz_half

                    ux(no_part)=ux_temp
                    uy(no_part)=uy_temp
                    uz(no_part)=uz_temp
                    ! write(*,*) 'Starting uz:', uz(no_part)
                end do
            else if (spec_params(stype(no_part))%motion_method.eq.1) then
                ! no windback here
                !    write(*,*) 'GINJ: ',ux(no_part),uy(no_part),uz(no_part)

            end if
            ! if (sp.eq.2) then
            ! 	write(*,*) "Dux",sp,ux(no_part)
            ! 	write(*,*) "Duy",sp,uy(no_part)
            ! 	write(*,*) "Duz",sp,uz(no_part)
            !  	write(*,*) "Pos",y(no_part), z(no_part)

            ! end if
            if (discard_particle) then
                no_part = no_part -1
            else
                total_energy(stype(no_part),1) = total_energy(stype(no_part),1) + ux(no_part)*ux(no_part) +  uy(no_part)*uy(no_part)  +uz(no_part)*uz(no_part)
            end if
        enddo
        ! debug stat
        ! write(*,*) 'Inject stats ux:', sum(ux(no_part - Ninj:no_part))/real(Ninj)
        ! write(*,*) 'Inject stats uy:', sum(uy(no_part - Ninj:no_part))/real(Ninj)
        ! write(*,*) 'Inject stats uz:', sum(uz(no_part - Ninj:no_part))/real(Ninj)

    end subroutine


    subroutine inject_bottom(Ninj,no_part,no_part_fin,bx,by,bz,z,y,ux,uy,uz,mono,Lz,Ly,dt,&
            utherm,sqbx2by2,N_u_z,u_z,ufu_z,Xu_z,&
            Ax_half,Bx_half,Cx_half,Ay_half,By_half,Cy_half,Az_half,Bz_half,Cz_half,&
            midLz,stype,sp,m,q,first_push,Umono,T,&
            spec_params,no_species,no_supra_loops,Ly_start,Ly_stop,z_start,z_stop,dz,proc_no)
        !use IFPORT
        use spice2_types
        implicit none
        !INJECT new ions from plasma side (to replace the outflux)
        !=========================================================
        !include "struct.h"
        integer:: i,Ninj,no_part,no_part_fin,sp,no_species,no_supra_loops,k,r,z_start,z_stop,proc_no
        type(pinfo), dimension(no_species):: spec_params
        real*8:: Ninj_r,Ninj_fx,Umono,dz
        real*8:: bx,by,bz,first_push
        real*8, dimension(no_part_fin):: z,y,ux,uy,uz
        integer*2, dimension(no_part_fin):: stype
        logical:: mono
        real*8:: Lz,Ly,dt,utherm,sqbx2by2,midLz,m,q,T
        integer:: N_u_z
        real*8, dimension(N_u_z):: u_z,ufu_z,Xu_z
        real*8:: Ax_half,Bx_half,Cx_half,Ay_half,By_half,Cy_half,Az_half,Bz_half,Cz_half
        real*8:: upar1,uperp1,uperp2,ux_temp,uy_temp,uz_temp,Ly_start,Ly_stop
        real*8:: rndx, rndxn
        logical:: discard_particle
        ! write(*,*) 'Injecting', Ninj, 'particles'
        !Generate a velocity distribution for the ions.
        do i=1,Ninj
            no_part=no_part+1
            discard_particle=.false.
            !MK 14/08/06
            !now is the time to initalize particle
            z(no_part)=0.d0       !toroidal position (the initial position is defined in the injection module)
            y(no_part)=0.d0       !radial position (the initial position is defined in the injection module)
            uz(no_part)=0.d0      !parallel speed
            uy(no_part)=0.d0      !radial speed
            ux(no_part)=0.d0      !poloidal speed
            stype(no_part) = iint(real(sp))
            !=====================================OLD METHOD============================================
            if (bx.eq.0.d0.AND.by.eq.0.d0) then          ! Bz = z-axis !!! SO WE USE OLD METHOD!
                !in z-direction
                !--------------
                if (mono) then          !NEW RD
                    upar1=Umono
                    !	      upar1 = -upar1
                else
                    call VELOCITY(u_z,ufu_z,N_u_z,Xu_z,upar1) !arbitrary normalized velocity function
                endif

                if (spec_params(stype(no_part))%motion_method.eq.1) then
                    ux(no_part) = upar1
                    uy(no_part)  = 0.0
                    uz(no_part) = 0.0
                    !        write(*,*) 'Old method gyro'
                    call G05FAF(0.d0,1.d0,1,rndx)
                    z(no_part)=ux(no_part)*dt*rndx               !uniform distribution in space

                else
                    !        write(*,*) 'Old method regular'
                    !! TEMPERATURE HACK - should have been here before???
                    uz(no_part)=-upar1
                    !in y-direction
                    !--------------
                    call G05FDF(0.d0,1.d0,1,rndxn)
                    uy(no_part)=rndxn*utherm
                    !in x-direction
                    !--------------
                    call G05FDF(0.d0,1.d0,1,rndxn)
                    ux(no_part)=rndxn*utherm
                    call G05FAF(0.d0,1.d0,1,rndx)
                    z(no_part)=uz(no_part)*dt*rndx               !uniform distribution in space
                    !		  write(*,*) proc_no,'Injection check',z(no_part),(z_start-1)*dz
                    if (z(no_part).lt.((z_start-1)*dz).or.(z(no_part).gt.((z_stop-1)*dz))) then
                        discard_particle = .true.
                        !                    write(*,*) proc_no,'Particle discarded'
                        !                          if (z_stop.gt.80) then
                        !                      write(*,*) 'Discarded particle because of wrong origin',z(no_part)
                        !                            end if
                    end if
                end if


                call G05FAF(0.d0,1.d0,1,rndx)
                y(no_part)=rndx*Ly                           !uniform distribution in space

                if ((y(no_part).lt.Ly_start).or.(y(no_part).gt.Ly_stop)) then
                    discard_particle = .true.
                end if


                !--------------
                !1/2 acceleration to set v-1/2 at initial time (LeapFrog)
                !RD! IF WE WANT TO DO IT, WE SHOULD DEFINE Ezi(new-Particle) and izi, izi1 etc...
                !RD!uzi(N_i)=uzi(N_i)+dt0*ksi**2*Ezi(N_i)+dt0*(uxi(N_i)*by-uyi(N_i)*bx)

            else
                !=======================================NEW METHOD====================================
                !in the parallel direction (NEW!!)
                !-------------------------
                if (mono) then
                    upar1=Umono
                else
                    call VELOCITY(u_z,ufu_z,N_u_z,Xu_z,upar1) !arbitrary normalized velocity function
                endif
                !      upar1 = -upar1
                ! 	write(*,*) 'Upar1',sp,upar1

                !in the perpendicular-1 direction (NEW!!)
                !--------------------------------
                call G05FDF(0.d0,1.d0,1,rndxn)
                uperp1=rndxn*utherm                 !Maxwellian distribution
                ! 	write(*,*) 'Uperp1',sp,uperp1

                !in the perpendicular-2 direction
                !--------------------------------
                call G05FDF(0.d0,1.d0,1,rndxn)
                uperp2=rndxn*utherm                 !Maxwellian distribution
                ! bad hack
                !       uperp1 = 0.0
                !       uperp2 = 0.0

                ! 	write(*,*) 'Uperp2',sp,uperp2
                !hack to have non-gyrating ions
                !if (q.gt.0.0) then
                !uperp1 = 0.0
                !uperp2 = 0.0

                !end if
                !===========================================================================================
                if (spec_params(stype(no_part))%motion_method.eq.1) then
                    !         upar1 = -5.0
                    !         uperp1 = 0.0
                    !         uperp2 = 0.0
                    ux(no_part) = -upar1
                    !            ux(no_part) = -5.0
                    uy(no_part) = 0.0
                    uz(no_part) = 0.0
                    call G05FAF(0.d0,1.d0,1,rndx)

                    z(no_part) =Lz - midLz+upar1*dt*rndx*bz
                    call G05FAF(0.d0,1.d0,1,rndxn)
                    ! shuffle thing
                    do r=1,int(rndxn*10.0)
                        call G05FAF(0.d0,1.d0,1,rndx)

                    end do
                    call G05FAF(0.d0,1.d0,1,rndx)

                    y(no_part) = Ly*rndx
                    !        write(*,*) 'New method gyro'
                    !write (*,*) 'INJ',z(no_part),y(no_part),ux(no_part)
                else
                    !        write(*,*) 'New method regular'

                    !in z-direction
                    !--------------
                    uz(no_part)=upar1*bz+uperp2*sqbx2by2

                    ! version 0.7 had this
                    ! call G05FAF(0.d0,1.d0,1,rand)
                    !       ze(N_e)=midLze+uparle*dt*rand*bz-uperp1e/mu*sqbx2by2
                    ! this generates electrons inside the simulation region!!!
                    call G05FAF(0.d0,1.d0,1,rndx)
                    z(no_part)=Lz -midLz+upar1*dt*rndx*bz*first_push+q*uperp1*m*sqbx2by2   !+uperp1i is ok for the ions!!
                    !zi(N_i)=Lz+uparli*dt*rndx*bz+(uperp1i+dsqrt(uperp1i**2+uperp2i**2))*sqbx2by2      !NEW 4/7/06 (JG)
                    !     if (z(no_part) < Lz) then
                    ! 	write(*,*) 'Warning, particle injected inside the box!!!',z(no_part),m,upar1,uperp1,uperp2
                    write(*,*) proc_no,'Injection check',z(no_part),(z_start-1)*dz
                    if (z(no_part).lt.((z_start-1)*dz).or.(z(no_part).gt.((z_stop-1)*dz))) then
                        discard_particle = .true.
                    end if

                    ! end if
                    !in y-direction
                    !--------------
                    uy(no_part)=upar1*by+uperp1*bx/sqbx2by2-uperp2*by*bz/sqbx2by2

                    call G05FAF(0.d0,1.d0,1,rndx)
                    y(no_part)=rndx*(Ly_stop - Ly_start) + Ly_start                           !uniform distribution in space

                    !in x-direction
                    !--------------
                    ux(no_part)=upar1*bx-uperp1*by/sqbx2by2-uperp2*bx*bz/sqbx2by2
                end if
                !==================================================================================
            endif   !if (bx.eq.0.d0.AND.by.eq.0.d0) then

            !1/2 wind back for next L.F
            !--------------------------
            if (spec_params(stype(no_part))%motion_method.eq.0) then
                ux_temp=ux(no_part)*Ax_half + uy(no_part)*Bx_half + uz(no_part)*Cx_half

                uy_temp=ux(no_part)*Ay_half + uy(no_part)*By_half + uz(no_part)*Cy_half

                uz_temp=ux(no_part)*Az_half + uy(no_part)*Bz_half + uz(no_part)*Cz_half

                ux(no_part)=ux_temp
                uy(no_part)=uy_temp
                uz(no_part)=uz_temp
                !        if (q < 0) then
                !        write(*,*) 'QINTH',z(no_part),y(no_part)
                !        write(*,*) 'QINTV',ux(no_part),uy(no_part),uz(no_part)
                !
                !
                !
                !       end if

            else if (spec_params(stype(no_part))%motion_method.eq.2) then
                do k=1,no_supra_loops
                    ux_temp=ux(no_part)*Ax_half + uy(no_part)*Bx_half + uz(no_part)*Cx_half

                    uy_temp=ux(no_part)*Ay_half + uy(no_part)*By_half + uz(no_part)*Cy_half

                    uz_temp=ux(no_part)*Az_half + uy(no_part)*Bz_half + uz(no_part)*Cz_half

                    ux(no_part)=ux_temp
                    uy(no_part)=uy_temp
                    uz(no_part)=uz_temp
                    ! write(*,*) 'Starting uz:', uz(no_part)
                end do
            else if (spec_params(stype(no_part))%motion_method.eq.1) then
                ! windback here
                ! write(*,*) 'GINJ: ',ux(no_part),uy(no_part),uz(no_part)

            end if
            ! if (sp.eq.2) then
            ! 	write(*,*) "Dux",sp,ux(no_part)
            ! 	write(*,*) "Duy",sp,uy(no_part)
            ! 	write(*,*) "Duz",sp,uz(no_part)
            !  	write(*,*) "Pos",y(no_part), z(no_part)

            ! end if
            if (discard_particle) then
                no_part = no_part -1
                !   write(*,*) proc_no,'Rollling back'
            end if
        enddo
        ! debug stat
        ! write(*,*) 'Inject stats ux:', sum(ux(no_part - Ninj:no_part))/real(Ninj)
        ! write(*,*) 'Inject stats uy:', sum(uy(no_part - Ninj:no_part))/real(Ninj)
        ! write(*,*) 'Inject stats uz:', sum(uz(no_part - Ninj:no_part))/real(Ninj)

    end subroutine

    subroutine prepare_SOL_injection(SOL_W,SOL_a,SOL_b,SOL_asq,SOL_bsq,SOL_bminusa,SOL_Ns,SOL_dW,Nz,Ny,no_species,ksi,dt,bz,dz,Lz,Lsh,N0,W0,inj_ratio,scenario,injection_method,spec_params)
        use spice2_types
        implicit none
        !include "struct.h"

        integer:: Nz,Ny,no_species,Lsh,z,sp,N0,source_start,i,scenario,source_stop
        real*8:: ksi,dt,bz,dz,W0,Lz
        real*8, dimension(no_species,Nz,Ny):: SOL_W
        real*8, dimension(no_species,Nz,Ny):: SOL_a,SOL_b,SOL_asq,SOL_bsq,SOL_bminusa,SOL_Ns,SOL_dW
        real*8, dimension(no_species):: inj_ratio
        integer, dimension(no_species):: injection_method
        type(pinfo), dimension(no_species):: spec_params
        ! This code is taken from Jamie's 1D code
        ! in our case we have a source located between Lsh - Lz and Lz

        !Define source distribution.
        !The source is zero in a layer of thickness Lsh adjacent to each wall.
        !The integral of W over z gives ksi*bz.
        !The bz is needed to account for the increase of SOL dwell time with
        !decreasing magnetic field angle.
        !Each ion should be exchanged once (definition of SOL thickness in our
        !normalization)
        SOL_W=0.0
        source_start = Nz - Lsh
        source_stop = Lsh +1
        ! write(*,*) 'source starts at:', source_start
        ! write(*,*) Lsh,bz,ksi,W0
        do sp=1,no_species
            W0 = spec_params(sp)%w
            write(*,*) sp,'SOL injection strength',W0
            do z=1,Nz
                if (z.ge.source_start) then
                    SOL_W(sp,z,:)=(1.0+dcos(real(z-Nz)/real(Lsh)*3.14159d0)+1.d-6)/real(Lsh)/2.0*abs(bz)*ksi*W0
                    ! write(*,*) 'W',z,SOL_W(sp,z,1)
                end if
                if (z.le.source_stop.and.scenario.eq.4) then
                    SOL_W(sp,z,:)=(1.0+dcos(real(z-1)/real(Lsh)*3.14159d0)+1.d-6)/real(Lsh)/2.0*abs(bz)*ksi*W0

                end if
            enddo

            ! 	write(*,*) 'W integral:', sum(SOL_W)/20.0
            !Ns is the number of particles injected into each cell per time step.
            do i=1,Nz-1
                ! assume source homogeneous in y direction
                if (SOL_W(sp,i,1).eq.SOL_W(sp,i+1,1)) then
                    SOL_Ns(sp,i,:)=N0*dz*(1.d0-dexp(-SOL_W(sp,i,1)*dt))*inj_ratio(sp)
                else
                    SOL_Ns(sp,i,:)=N0*dz*(1.d0+(dexp(-SOL_W(sp,i+1,1)*dt)-dexp(-SOL_W(sp,i,1)*dt))/(SOL_W(sp,i+1,1)-SOL_W(sp,i,1))/dt)*inj_ratio(sp)
                    !write(*,*) 'Ns',i,SOL_Ns(sp,i,1)
                    !write (*,*)   SOL_W(sp,i+1,1),SOL_W(sp,i,1),SOL_W(sp,i+1,1) - SOL_W(sp,i,1)
                    !write (*,*) dexp(-SOL_W(sp,i+1,1)*dt),dexp(-SOL_W(sp,i,1)*dt), 1.d0+dexp(-SOL_W(sp,i+1,1)*dt)-dexp(-SOL_W(sp,i,1)*dt)
                endif
            enddo
            ! write(*,*) 'SUM Ns',sp,sum(SOL_Ns(sp,:,1))

        enddo
        ! differentiate between scenario 3 and 4
        do sp=1,no_species
            if (injection_method(sp).eq.3) then
                ! these particle are only injected from the top
                SOL_Ns(sp,1:(source_stop+1),:) = 0.0
            end if
            if (injection_method(sp).eq.4) then
                ! these particle are only injected from the bottom
                SOL_Ns(sp,(source_start-1):Nz,:) = 0.0
            end if


        end do

        do i=1,Nz-1
            if (SOL_Ns(1,i,1).gt.0) then
                SOL_a(:,i,:)=SOL_W(:,i,:)/SOL_Ns(:,i,:)
                SOL_b(:,i,:)=SOL_W(:,i+1,:)/SOL_Ns(:,i,:)
            end if
        end do
        SOL_bminusa=SOL_b-SOL_a
        SOL_asq=SOL_a*SOL_a
        SOL_bsq=SOL_b*SOL_b
        SOL_dW(:,1:Nz-1,:)=(SOL_W(:,2:Nz,:)-SOL_W(:,1:Nz-1,:))/dz
        SOL_W=SOL_W*dt
        SOL_dW=SOL_dW*dt
        SOL_Ns = SOL_Ns*(Ny-1)

        do i=1,Nz-1
            !write(*,*) 'SOLA',i,SOL_a(1,i,1)
            !write(*,*) 'SOLB',i,SOL_b(1,i,1)

        end do

    end subroutine

    ! this sink works both for scenarios 3 and 4
    ! everything depends on the profile of W

    subroutine sink_SOL(Nz,Ny,no_species,SOL_W,SOL_dW,no_part,z,y,ux,uy,uz,stype,iz1,iy1,no_particles,spec_params,dz)

        use spice2_types
        implicit none

        !include 'struct.h'
        integer:: no_part,Nz,Ny,sp,i,N_itemp,no_species
        real*8, dimension(no_species,Nz,Ny):: SOL_W,SOL_dW
        real*8, dimension(no_part):: z,y,ux,uy,uz
        integer*2,dimension(no_part):: stype
        integer,dimension(no_part):: iz1,iy1
        integer,dimension(no_species):: no_particles,injection_method
        type(pinfo), dimension(no_species):: spec_params
        integer:: ion_counter
        real*8:: rand,dz
        rand = 0.0
        ion_counter = 0
        N_itemp=no_part
        ! recalculate iz&, iy1
        iz1 = idint(z/dz) + 1
        iy1 = idint(y/dz) + 1

        do i=no_part,1,-1
            if (spec_params(stype(i))%injection_method.eq.3.or.spec_params(stype(i))%injection_method.eq.4) then

                call G05FAF(0.d0,1.d0,1,rand)
                ! assume homogeneous W in y direction
                !   write(*,*) 'SWP',z(i),dexp(-(SOL_W(stype(i),iz1(i),iy1(i))+SOL_dW(stype(i),iz1(i),iy1(i))*(z(i)-int(z(i)/dz)*dz)))
                rand=rand-1.d0+dexp(-(SOL_W(stype(i),iz1(i),iy1(i))+SOL_dW(stype(i),iz1(i),iy1(i))*(z(i)-int(z(i)/dz)*dz)))
                if (rand < 0.0) then
                    !Eliminate the scattered ion from the inventory.
                    no_particles(stype(i)) = no_particles(stype(i)) -1
                    if (stype(i) .eq.1) then
                        ion_counter = ion_counter +1
                    end if
                    if (i<N_itemp) then
                        z(i)=z(N_itemp)
                        y(i)=y(N_itemp)
                        stype(i) = stype(N_itemp)
                        ux(i)=ux(N_itemp)
                        uy(i)=uy(N_itemp)
                        uz(i)=uz(N_itemp)
                        iy1(i)=iy1(N_itemp)
                        iz1(i)=iz1(N_itemp)
                    endif  !if (i<N_itemp) then
                    N_itemp=N_itemp-1
                endif  !if (rand < 0) then
            endif ! SOL particle
        enddo  !do i=N_i,1,-1
        !write(*,*) 'Sout',ion_counter
        !write(*,*) 'SOL sink:',no_part - N_itemp,real(no_part - N_itemp)/real(no_part)
        no_part=N_itemp


    end subroutine
    ! should be called for eqch specie!
    subroutine source_sol(sp,no_part,Nz,Ny,Ly,Lz,Nsr,Ns,apn,SOL_a,SOL_b,SOL_asq,SOL_bsq,SOL_bminusa,no_tot,dz,dy,bx,by,bz,utherm,sqbx2by2,Ax_half,Bx_half,Cx_half,Ay_half,By_half,Cy_half,Az_half,Bz_half,Cz_half,y,z,ux,uy,uz,stype)

        implicit none
        integer:: Nz,Ny,sp,i,j,k,no_part,apn,no_tot,injection_method
        real*8:: Lz,Ly,rand,dz,dy,bx,by,bz,randn,utherm,uparl,uperp1,uperp2,sqbx2by2,ux_temp,uy_temp,uz_temp,Ax_half,Bx_half,Cx_half,Ay_half,By_half,Cy_half,Az_half,Bz_half,Cz_half
        real*8,dimension(Nz,Ny):: Nsr,Ns
        integer,dimension(Nz,Ny):: Nsrint
        real*8,dimension(Nz,Ny):: SOL_a,SOL_b,SOL_asq,SOL_bsq,SOL_bminusa
        real*8,dimension(no_tot):: z,y,ux,uy,uz
        integer*2,dimension(no_tot):: stype
        logical:: debug
        debug = .false.
        rand = 0.0
        randn = 0.0
        !Inject ions into the wake from the source region.
        !In cells near the wall it is possible that less than one particle
        !is injected per time step. That is why I keep track of the fractional
        !number of injected particles. e.g. if 0.1 particles are injected per time
        !step, it means we need to inject 1 particle every 10 time steps.

        Nsrint = idint(Nsr)
        Nsr=Nsr-idint(Nsr)
        if (sp.eq.1) then
            !write(*,*) 'Sin',sum(Nsrint(:,1))
        end if

        do i=1,Nz-1
            !  do j=1,Ny
            j=1
            if (Nsrint(i,1)>0) then
                do k=1,Nsrint(i,1)
                    apn=apn+1
                    no_part = no_part + 1
                    stype(apn) = sp
                    ! z coordinate
                    call G05FAF(0.d0,1.d0,1,rand)
                    if (SOL_b(i,j).eq.SOL_a(i,j)) then
                        z(apn)=(i-1)*dz+dz*rand
                    else
                        z(apn)=(i-1)*dz + dz*(-SOL_a(i,j)+dsqrt(SOL_asq(i,j)+ (SOL_bsq(i,j)-SOL_asq(i,j))*rand))/SOL_bminusa(i,j)
                        if (debug) then
                            write(*,*) 'Generatedz:',i,j,z(apn)
                        end if
                    endif
                    ! y coordinate - no weigting for now
                    call G05FAF(0.d0,1.d0,1,rand)

                    ! y(apn)=(j-1)*dy+dy*rand
                    y(apn)=Ly*rand
                    if (debug) then
                        write(*,*) 'Generatedy:',i,j,y(apn)
                    end if

                    if (bx.eq.0.d0.AND.by.eq.0.d0) then
                        call G05FDF(0.d0,1.d0,1,randn)
                        ux(apn)=randn*utherm
                        if (debug) then
                            write(*,*) 'Generatedux:',i,j,ux(apn)
                        end if
                        call G05FDF(0.d0,1.d0,1,randn)
                        uy(apn)=randn*utherm
                        if (debug) then
                            write(*,*) 'Generateduy:',i,j,uy(apn)
                        end if
                        call G05FDF(0.d0,1.d0,1,randn)
                        uz(apn)=randn*utherm
                        if (debug) then
                            write(*,*) 'Generateduz:',i,j,uz(apn)
                        end if
                    else
                        call G05FDF(0.d0,1.d0,1,randn)
                        uparl=randn*utherm
                        call G05FDF(0.d0,1.d0,1,randn)
                        uperp1=randn*utherm
                        call G05FDF(0.d0,1.d0,1,randn)
                        uperp2=randn*utherm
                        ux(apn)=uparl*bx-uperp1*by/sqbx2by2-uperp2*bx*bz/sqbx2by2
                        uy(apn)=uparl*by+uperp1*bx/sqbx2by2-uperp2*by*bz/sqbx2by2
                        uz(apn)=uparl*bz+uperp2*sqbx2by2


                        ux_temp=ux(apn)*Ax_half + uy(apn)*Bx_half + uz(apn)*Cx_half

                        uy_temp=ux(apn)*Ay_half + uy(apn)*By_half + uz(apn)*Cy_half

                        uz_temp=ux(apn)*Az_half + uy(apn)*Bz_half + uz(apn)*Cz_half

                        ux(apn)=ux_temp
                        uy(apn)=uy_temp
                        uz(apn)=uz_temp
                    endif  !if (bx.eq.0.d0.AND.by.eq.0.d0) then
                enddo  !do j=1,Nsint(i)
            endif  !if (Nsint(i)>0) then
            !  enddo  !do j=1,Ny-1
        enddo  !do i=1,Nz-1


    end subroutine
    subroutine emit_cos(Einc,Esec,mu,ux,uy,uz,gauss_flag)

        !Emit an electron with a cosine distribution in the z direction.
        !Its energy is chosen from a Gaussian distribution of characteristic
        !energy Esec if gauss_flag==1 (but with E<Einc nonetheless).
        !Otherwise it is a delta function of energy Einc.
        ! This routine was taken from Jamie's 1D PIC code because it is smarter than anything I could programme :)


        IMPLICIT NONE

        real*8 E,Ec,Einc,Esec,mu,v,ux,uy,uz,a,b,c1,c2,pi,rand,randn
        integer*4 gauss_flag
        pi=dacos(-1.d0)

        call G05FAF(0.d0,1.d0,1,rand)
        a=dasin(rand)
        call G05FAF(0.d0,1.d0,1,rand)
        b=rand*2.0*pi

        if (gauss_flag==1) then
            !call G05FDF(0.d0,1.d0,1,randn)
            !v=dsqrt(Ec*2.0*mu)*dabs(randn)
            Ec=Esec/20.0
            c1=dexp(-0.5*(Ec/Esec)**2.0)
            c2=dexp(-0.5*(Einc/Esec)**2.0)
            call G05FAF(0.d0,1.d0,1,rand)
            E=Esec*dsqrt(-2.0*dlog(rand*(c2-c1)+c1))
            v=dsqrt(2.0*mu*E)
        else
            v=dsqrt(Einc*2.0*mu)
        endif
        uz=v*cos(a)
        ux=v*sin(a)*cos(b)
        uy=v*sin(a)*sin(b)

    END subroutine emit_cos
end module
