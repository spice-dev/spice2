! Subroutines related to solving the Poisson equation
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

subroutine psolver_gs (Pot,Pot_old,Pot_b4,crit_conv,Ny,Nz,dy,dz,cte_geo,cte_geo2,Ncell,Np,frelax,relax,Escz,Escy,rho_tot,N0,&
        debug,PL)
    implicit none
    !-----------------------------------------------------------------------------------------------------1
    !SOLVE THE POISSON EQUATION.
    !===========================
    !We determine the E-field Escz and Escy and potential Pot using boundary
    !condition PL imposed on the potential at =Lz, for z=Lz1,Lz we use periodic cond., other surf. are float.
    integer:: test_it,nbit,j,k,Np,Ny,Nz,Ny1,Ny2,Nz1,N0,count
    real*8, dimension(Nz,Ny):: charge,Escz,Escy,Pot,Pot_old,Potav,Pot_b4,rho_tot
    real*8:: erreur,erreur2,cte_geo,cte_geo2,erreur_max0,frelax,dz,dy,crit_conv,Ncell
    real*8, dimension(Np):: nit_pot,erreur_max
    logical:: relax,debug
    real*8:: sc,PL
    !Charge density:
    charge=rho_tot/(N0*dz*dy)
    ! old stuff
    Nz1 = 0
    Ny1 = 0
    Ny2 = 0
    if (debug) then
        write(*,*) 'Iterative Poisson solver'
        write(*,*) 'Convergence criterium:',crit_conv
        write(*,*) 'Number of cells',Ncell
        write(*,*) 'Simulated region (cells)',Ny,Nz,Ncell
        write(*,*) 'Number of timesteps',Np
        write(*,*) 'konstants',cte_geo, cte_geo2, frelax, relax, N0

        ! sum to see if it ain't 0
        sc = sum(Pot)
        write(*,*) 'Sum of potential:',sc
        sc = sum(charge)
        write(*,*) 'Sum of charge:',sc
        sc = sum(Escz)
        write(*,*) 'Sum of Escz:',sc
        sc = sum(Escy)
        write(*,*) 'Sum of Escy:',sc

    end if
    !POTENTIAL
    !=========

    Pot_old=Pot

    erreur2=1.d0
    test_it=1
    nbit=0

    do while (erreur2.gt.crit_conv)
        !	write(*,*) 'Error:',erreur2
        Pot_b4=Pot
        erreur=0.d0
        erreur_max0=0.d0
        nbit=nbit+1
        !-----------------------------------------------------
        do j=Nz1+1,Nz-1
            !PC - potential using periodic contitions on y=0
            Pot(j,1)=cte_geo2*(dy**2*(Pot(j+1,1)+Pot(j-1,1))+&
                dz**2*(Pot(j,2)+Pot(j,Ny-1)))+cte_geo*charge(j,1)
            erreur=erreur+( 2.d0*abs(Pot(j,1)-Pot_b4(j,1)) )     !factor 2 from periodicity!!!
            erreur_max0=abs(Pot(j,1)-Pot_b4(j,1))
            do k=2,Ny-1
                !potential in the core-1 of the simulation box
                Pot(j,k)=cte_geo2*(dy**2*(Pot(j+1,k)+Pot(j-1,k))+&
                    dz**2*(Pot(j,k+1)+Pot(j,k-1)))+cte_geo*charge(j,k)
                erreur=erreur+( abs(Pot(j,k)-Pot_b4(j,k)) )
                erreur_max0=max(erreur_max0,abs(Pot(j,k)-Pot_b4(j,k)))
            enddo
            !PC - potential periodic on y=Ny
            Pot(j,Ny)=Pot(j,1)
        enddo
        do j=2,Nz1
            do k=Ny1+1,Ny2-1
                !potential in the core-2 of the simulation box
                Pot(j,k)=cte_geo2*(dy**2*(Pot(j+1,k)+Pot(j-1,k))+&
                    dz**2*(Pot(j,k+1)+Pot(j,k-1)))+cte_geo*charge(j,k)
                erreur=erreur+( abs(Pot(j,k)-Pot_b4(j,k)) )
                erreur_max0=max(erreur_max0,abs(Pot(j,k)-Pot_b4(j,k)))
            enddo
        enddo
        erreur2=erreur/Ncell
        !-----------------------------------------------------
    enddo   !while(erreur2.gt.1.d-10) do

    nit_pot(count)=nbit*1.d0
    erreur_max(count)=erreur_max0

    !RD - Potential Relaxation -                             NEW!
    if (relax.AND.count.ge.2) then
        Pot=Pot_old*frelax+Pot*(1.d0-frelax)
    endif


end subroutine


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




subroutine psolver_direct_periodic(Ny,Nz,P0,PL,n,hzy,numeric,flag,equipot,Nz_disp)
    implicit none
    !real*8 hzy,Ax,equipot,xpot,controllu,info,Axrow,chrg
    !integer n,Ap,Ai,flag,offset,numeric,symbolic,sys,Aprow,Airow,status
    !dimension Ap(n+1 - Nz),Ai(5*(n - Nz)),Ax(5*(n - Nz)),flag(n - Nz),equipot(n - Nz)
    !dimension chrg(n - Nz),xpot(n - Nz),controllu(20),info(90),Aprow(n+1 - Nz),Airow(5*(n - Nz)),Axrow(5*(n-Nz))
    real*8 hzy,Ax,equipot,xpot,controllu,info,chrg
    integer Ap,Ai,offset
    integer n,flag,sys,status,Nz_disp,Nz_disp2
    integer*8:: numeric,symbolic
    dimension Ap(n+1 - Nz),Ai(5*(n - Nz)),Ax(5*(n - Nz)),flag(n - Nz),equipot(n - Nz)
    dimension chrg(n - Nz),xpot(n - Nz),controllu(20),info(90)

    integer*8 :: Ap64, Ai64, n_mat64
    dimension Ap64(n+1 - Nz),Ai64(5*(n - Nz))

    integer idx1,idx2,index2
    !060124-AE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !integer, dimension((Ny-1)*Nz):: trow_dense
    real*8, dimension((Ny-1)*Nz):: trow_dense
    integer, dimension(5):: v

    integer:: Ny,Nz,i,j,s,k
    real*8:: P0,PL
    external umf4def,umf4sym,umf4pcon,umf4pinf,umf4num,umf4fsym
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!AE denotes changes and comments made by Zdenek Pekarek
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    write(*,*) 'Received params n hzy Ny Nz',n, hzy,Ny,Nz
    !init!!!
    Ap = 0
    Ai = 0
    Ax = 0
    numeric = 0
    sys = 0
    status = 0
    symbolic = 0
    controllu = 0.0
    info = 0.0
    chrg = 0.0
    xpot = 0.0
    Nz_disp2 = Nz_disp

    Ap64 = 0
    Ai64 = 0

    !LU DECOMPOSITION (1/3): (1st stage - Creation, Transpose and Invertion of the matrix!)
    !=======================
    !create "1D" matrix in row description
    write(6,*) 'inversion of the matrix...please wait!'

    Ap(1)=0        !this sets the off-set for the beginning of each row in vectors Ax and Ai!

    do j=1,Ny-1
        do i=1,Nz
            !     elseif
            !grid nodes with a fixed potential - AE
            if (flag(i+Nz*(j-1)).eq.0) then
                !			trow_dense = 0.0
                !			trow_dense(i+Nz*(j-1)) = 1.0

                offset=Ap(i+Nz*(j-1))
                Ap((i+Nz*(j-1)) + 1)=(offset + 1)
                Ax(offset+1)=(1.0)
                Ai(offset+1)=(i+Nz*(j-1) - 1)
            else if (flag(i+Nz*(j-1)).eq.2) then
                ! still treat is as plasma
                flag(i+Nz*(j-1)) = 1
                ! zero e field condition
                ! 4 point stencil only
                trow_dense = 0.0

                trow_dense(i+Nz*(j-1)) = 2.0*(1.0+hzy**2)
                trow_dense(i+Nz*(j-1) -1) = -2.0
                !trow_dense(i+Nz*(j-1) +1) = -1.0
                if ((i+Nz*(j-1)).eq.((Ny-1)*Nz)) then
                    !use first line
                    trow_dense(Nz) = -hzy**2
                else
                    !	trow_dense(MODULO((i+Nz*(j-1) + Nz),  ((Ny-1)*Nz))) = -hzy**2
                    trow_dense(i+Nz*(j-1) + Nz) = -hzy**2
                end if

                if ((i+Nz*(j-1)).eq.Nz) then
                    !use last line
                    trow_dense((Ny-1)*Nz) = -1.0*hzy**2
                else
                    !	trow_dense(MODULO((i+Nz*(j-1) - Nz + ((Ny-1)*Nz)),  ((Ny-1)*Nz))) = -1.0*hzy**2
                    trow_dense(i+Nz*(j-1) - Nz) = -1.0*hzy**2
                end if

                offset=Ap(i+Nz*(j-1))

                do idx1=1,Ny-1
                    do idx2=1,Nz
                        !060124-AE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        if (abs(trow_dense(idx2+Nz*(idx1-1))).ge.0.000001) then
                            offset = offset + 1
                            Ax(offset) = (trow_dense(idx2+Nz*(idx1-1)))
                            Ai(offset) = (idx2+Nz*(idx1-1) -1)

                        end if
                    end do
                end do

                Ap(i+Nz*(j-1) +1) = offset
                ! bottom plane condition E=0
            else if (flag(i+Nz*(j-1)).eq.3) then
                ! still treat is as plasma
                flag(i+Nz*(j-1)) = 1
                ! zero e field condition
                ! 4 point stencil only
                trow_dense = 0.0

                trow_dense(i+Nz*(j-1)) = 2.0*(1.0+hzy**2)
                !	trow_dense(i+Nz*(j-1) -1) = -2.0
                trow_dense(i+Nz*(j-1) +1) = -2.0

                if ((i+Nz*(j-1)).eq.((Ny-1)*Nz)) then
                    !use first line
                    trow_dense(1) = -hzy**2
                else
                    trow_dense(MODULO((i+Nz*(j-1) + Nz),  ((Ny-1)*Nz))) = -hzy**2
                end if

                if ((i+Nz*(j-1)).eq.Nz) then
                    !use last line
                    trow_dense((Ny-2)*Nz+1) = -1.0*hzy**2
                else
                    trow_dense(MODULO((i+Nz*(j-1) - Nz + ((Ny-1)*Nz)),  ((Ny-1)*Nz))) = -1.0*hzy**2
                end if

                offset=Ap(i+Nz*(j-1))

                do idx1=1,Ny-1
                    do idx2=1,Nz
                        !060124-AE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        if (abs(trow_dense(idx2+Nz*(idx1-1))).ge.0.000001) then
                            offset = offset + 1
                            Ax(offset) = (trow_dense(idx2+Nz*(idx1-1)))
                            Ai(offset) = (idx2+Nz*(idx1-1) -1)

                        end if
                    end do
                end do
                Ap(i+Nz*(j-1) +1) = offset

                !grid nodes immersed in plasma, 5-point stencil of the closest neighbours is used to compute their potential - AE
            else

                !060124-AE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                ! improved speed by MK 2013
                offset=Ap(i+Nz*(j-1))
                !	trow_dense = 0.0

                ! perhaps we need to order things?
                v(1) = i+Nz*(j-1)
                v(2) = i+Nz*(j-1) -1
                v(3) = i+Nz*(j-1) +1
                if (Nz_disp2.gt.0.and.j.eq.Ny-1.and.i.lt.Nz -Nz_disp) then
                    v(4) =  MODULO((i+Nz*(j-1) + Nz+ Nz_disp),  ((Ny-1)*Nz))
                    !    write(*,*) 'V4 extra',i,j,v(4),i+Nz*(j-1) + Nz

                else
                    v(4) = MODULO((i+Nz*(j-1) + Nz),  ((Ny-1)*Nz))
                    !    write(*,*) 'V4',i,j,v(4),i+Nz*(j-1) + Nz
                end if
                if (Nz_disp2.gt.0.and.j.eq.1.and.i.gt.Nz_disp) then
                    v(5) =  MODULO((i+Nz*(j-1) - Nz - Nz_disp + ((Ny-1)*Nz)),  ((Ny-1)*Nz))
                    !  k = modulo(V(5),Nz)

                    !	write(*,*) 'V5 extra',i,j,k,(v(5) - k)/Nz + 1
                else
                    v(5) = MODULO((i+Nz*(j-1) - Nz + ((Ny-1)*Nz)),  ((Ny-1)*Nz))
                    ! k = modulo(V(5),Nz)

                    !	write(*,*) 'V5',i,j,k,(v(5) - k)/Nz + 1
                end if
                ! write(*,*) 'V1:I',v
                if (v(5).lt.1.or.v(5).gt.(Ny-1)*Nz) then
                    write(*,*) 'V(5) out of bounds',v(5),i,j
                    stop
                end if



                call bubble_sort(v,5)
                !write(*,*) 'V2:Q',v
                do index2=1,5

                    if (v(index2).eq.(i+Nz*(j-1))) then
                        !		trow_dense(i+Nz*(j-1)) = 2.0*(1.0+hzy**2)
                        offset = offset + 1
                        Ax(offset) = (2.0*(1.0+hzy**2))
                        Ai(offset) = (i+Nz*(j-1) -1)
                        !		write (*,*) 'NM:',offset, Ax(offset),Ai(offset)
                    else if (v(index2).eq.(i+Nz*(j-1) -1)) then
                        !	trow_dense(i+Nz*(j-1) -1) = -1.0
                        offset = offset + 1
                        Ax(offset) = (-1.0)
                        Ai(offset) = (i+Nz*(j-1) -1 -1)
                        !		write (*,*) 'NM:',offset, Ax(offset),Ai(offset)
                    else if (v(index2).eq.(i+Nz*(j-1) +1)) then
                        !		trow_dense(i+Nz*(j-1) +1) = -1.0
                        offset = offset + 1
                        Ax(offset) = (-1.0)
                        Ai(offset) = (i+Nz*(j-1) +1 -1)
                        !		write (*,*) 'NM:',offset, Ax(offset),Ai(offset)
                    else if ((Nz_disp2.gt.0.and.j.eq.Ny-1.and.i.lt.Nz -Nz_disp.and.v(index2).eq.(MODULO((i+Nz*(j-1) + Nz+ Nz_disp),  ((Ny-1)*Nz)))) .or.(v(index2).eq.(MODULO((i+Nz*(j-1) + Nz),  ((Ny-1)*Nz))))) then

                        !else if (v(index2).eq.(MODULO((i+Nz*(j-1) + Nz),  ((Ny-1)*Nz)))) then


                        !	trow_dense(MODULO((i+Nz*(j-1) + Nz),  ((Ny-1)*Nz))) = -hzy**2
                        offset = offset + 1
                        Ax(offset) = (-hzy**2)
                        Ai(offset) = (v(index2) -1)

                        !		write (*,*) 'NM:',offset, Ax(offset),Ai(offset)
                    else if ((Nz_disp2.gt.0.and.j.eq.1.and.i.gt.Nz_disp.and.v(index2).eq.(MODULO((i+Nz*(j-1) - Nz - Nz_disp + ((Ny-1)*Nz)),  ((Ny-1)*Nz)))).or.(v(index2).eq.(MODULO((i+Nz*(j-1) - Nz + ((Ny-1)*Nz)),  ((Ny-1)*Nz)))) ) then
                        !else if (v(index2).eq.(MODULO((i+Nz*(j-1) - Nz + ((Ny-1)*Nz)),  ((Ny-1)*Nz))))  then

                        !	trow_dense(MODULO((i+Nz*(j-1) - Nz + ((Ny-1)*Nz)),  ((Ny-1)*Nz))) = -hzy**2
                        offset = offset + 1
                        Ax(offset) = (-hzy**2)
                        Ai(offset) = (v(index2) -1)
                        !		write (*,*) 'NM:',offset, Ax(offset),Ai(offset)
                    else
                        write(*,*) 'Undefined case!!!',index2,v(index2),i,j
                        stop

                    endif
                enddo
                !offset=Ap(i+Nz*(j-1))
                ! 	do idx1=1,Ny-1
                ! 		do idx2=1,Nz
                ! 		write (*, '(36I2)') trow_dense(idx2+Nz*(idx1-1))
                ! 		end do
                !write(*,'(I I I I I I)') trow_dense
                ! 	end do
                ! write(*,'(30I2)') trow_dense
                ! 	write(*,*) '*******'
                !Ap((i+Nz*(j-1)) + 1)=offset + 5
                !	do idx1=1,Ny-1
                !		do idx2=1,Nz
                !060124-AE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                !		if (abs(trow_dense(idx2+Nz*(idx1-1))).ge.0.000001) then
                !			offset = offset + 1
                !			Ax(offset) = trow_dense(idx2+Nz*(idx1-1))
                !			Ai(offset) = idx2+Nz*(idx1-1) -1
                !						write (*,*) 'OM:',offset, Ax(offset),Ai(offset)

                !		end if
                !		end do
                !	end do
                Ap(i+Nz*(j-1) +1) = offset
            endif
            ! write(*,'(42I2)') trow_dense
        enddo
    enddo

    !stop
    !transpose the matrix from row to colum descrip. (for the lu-decomp. subroutines)
    !no longer needed
    !call umf4trnsps(n-Nz,n-Nz,Ap,Ai,Ax,Aprow,Airow,Axrow,status)

    !invert the matrix
    !---------------invert the matrix---------------1

    !call saveMatrix('matrix.mat', n+1 - Nz, Ap, 5*(n - Nz), Ai, 5*(n - Nz), Ax)

    call umf4def(controllu)                                           !set the default parameters
    controllu(1) = 3

    n_mat64 = int8(n-Nz)
    write(*,*) n_mat64
    do i=1,n+1 - Nz
        Ap64(i) = int8(Ap(i))
    enddo
    do i=1,5*(n - Nz)
        Ai64(i) = int8(Ai(i))
!        write(*,*) i, Ax(i)
    enddo

    !we don't need *row anymore
    !call umf4sym(n-Nz,n-Nz,Aprow,Airow,Axrow,symbolic,controllu,info, status)       !symbolic decomposition
    call umf4sym(n_mat64,n_mat64,Ap64,Ai64,Ax,symbolic,controllu,info)       !symbolic decomposition
    write(6,*) 'DIAGNOSTIC - output of the symbolic decomposition (should be == 0)::::::::::::::', status

    !write (6,*) 'Matrix Ap'
    !write (6,*) Ap
    !write (6,*) 'Matrix Ai'
    !write (6,*) Ai
    !write (6,*) 'Matrix Ax'
    !write (6,*) Ax

    write(6,*) 'DIAGNOSTIC - symbolic decomposition control settings'
    call umf4pcon(controllu)

    write(6,*) 'DIAGNOSTIC - output of the symbolic decomposition'
    call umf4pinf(controllu, info)

    !we don't need *row anymore
    !call umf4num(Aprow,Airow,Axrow,symbolic,numeric,controllu,info,status)   !numeric computation
    call umf4num(Ap64,Ai64,Ax,symbolic,numeric,controllu,info)   !numeric computation
    write(6,*) 'DIAGNOSTIC - output of the numeric decomposition (should be == 0)::::::::::::::', status

    write(6,*) 'DIAGNOSTIC - numeric decomposition control settings'
    call umf4pcon(controllu)

    write(6,*) 'DIAGNOSTIC - output of the numeric decomposition'
    call umf4pinf(controllu, info)

    call umf4fsym(symbolic)                                         !free the symbolic object
    !-----------------------------------------------2
    write(6,*) 'inversion of the matrix...done!'
    !call cpu_time(timerstop)
    !write(6,*) 'total time spent computing the inversion = ', timerstop - timerstart, ' s'
    !LU DECOMPOSITION: (end of 1st stage!)
    !=================
    write(*,*) "Numeric:",numeric


end subroutine



subroutine psolver_direct2_periodic(Ny,Nz,numeric,dz,dy,Pot,debug,rho_tot,N0,n,flag,equipot,Nz_disp)
    use ap_utils
    implicit none
    integer:: j,k,Ny,Nz,N0
    real*8:: dz,dy,s
    integer:: n,Ap,Ai,flag,offset,status, baseidx,Nz_disp
    integer*8:: numeric,symbolic,sys
    real*8, dimension(Nz,Ny):: Pot,charge,rho_tot
    real*8 hzy,Ax,equipot,xpot,controllu,info,chrg
    dimension chrg(n),xpot(n),controllu(20),info(90),equipot(n - Nz),flag(n - Nz)
    logical:: debug

    if (debug) then
        write(*,*) 'Received params numeric N0', numeric,N0
        write(*,*) 'Received params numeric n Nz', n, Nz,Nz_disp

    end if
    !init
    xpot = 0.0
    controllu = 0.0
    info = 0.0
    chrg = 0.0
    symbolic = 0
    sys = 0
    status = 0
    ! baseidx = 0


    !Charge density:
    ! zero density debug
    ! s 	charge=rho_tot/(N0*dz*dy)
    ! 	charge = 0.0

    ! if (debug) then
    ! s = sum(charge)
    ! write(*,*) 'Sum of charge:',s
    ! end if
    chrg = 0.0
    !POTENTIAL (LU DECOMPOSITION (2/3) - 2nd stage!)
    !=========
    !convert 2D matrix [charge] to 1D array [chrg]
    !first copy
    do k=1,Ny-1
        do j=1,Nz
            chrg(j+Nz*(k-1))=dz**2*rho_tot(j,k)/(N0*dz*dy)    !we have to multiply by: dz**2 to have the same "units" (see starting eq.)
            !dz**2 doesn't mean the S of the cell, but originates in PDE discretization
        enddo
    enddo

    ! s = sum(chrg)
    ! if (debug) then
    ! write(*,*) 'Sum of chrg:',s
    ! end if

    ! !overwrite the new "chrg" vector with the values in "equipot"
    ! do k=1,Ny-1
    !    do j=1,Nz
    !       if (flag(j+Nz*(k-1)).eq.0) then
    !          chrg(j+Nz*(k-1))=equipot(j+Nz*(k-1))
    !       endif
    !    enddo
    ! enddo

    !overwrite the new "chrg" vector with the values in "equipot"
    !flag=1 == the grid node is in the plasma; flag==0 in the electrode/surface
    !no other values of flag() are allowed
    do k=1,Ny-1
        do j=1,Nz
            baseidx = j+Nz*(k-1)
            ! 	write(*,*) 'IDX',baseidx
            chrg(baseidx)=real(flag(baseidx))*chrg(baseidx) + (1.0 - real(flag(baseidx)) )*equipot(baseidx)
        enddo
    enddo

    !call saveChrg('chrg.mat', n, chrg, nz, ny, rho_tot, dz, dy)

    ! s = sum(chrg)
    ! if (debug) then
    ! write(*,*) 'Sum of chrg:',s
    ! end if
    !
    ! s = sum(flag)
    ! if (debug) then
    ! write(*,*) 'Sum of flag:',s
    ! end if
    ! s = sum(equipot)
    ! if (debug) then
    ! write(*,*) 'Sum of equipot:',s
    ! end if


    !find a solution for each time step
    if (debug) then
        write(*,*) 'DIAGNOSTIC - now running the LU solver routine'
    end if
    !UMFPACK_At tells the solver to solve A^T x instead of Ax, so we don't need to transpose our matrix by hand anymore
    !sys=UMFPACK_At
    ! fortran doesn't know the constant above but it should be 1
    sys = 1
    if (debug) then
        write(*,*) 'sys:', sys
        write(*,*) 'numeric:', numeric
        !write(*,*) 'controllu:', controllu
        !write(*,*) 'info:', info



    end if
    call umf4sol(sys,xpot,chrg,numeric,controllu,info)         !solve the equation A.xpot=chrg (here: A=numeric)


!    call printRealMatrix(1, (nY-1) * nZ, chrg)

    if (debug) then
        call umf4pcon(controllu)

        call umf4pinf(controllu,info)
        write(*,*) 'DIAGNOSTIC - LU solver routine finished'
    end if
    ! s = sum(xpot)
    ! if (debug) then
    ! write(*,*) 'Sum of xpot:',s
    ! end if

    !convert 1D array [xpot] to 2D matrix [Pot]
    !take the second copy
    do k=1,Ny-1
        do j=1,Nz
            Pot(j,k)=xpot(j+Nz*(k-1))
        enddo
    enddo

    !do j=1,Nz
    do j=1,(Nz - Nz_disp)
        Pot(j,Ny)=xpot(j+Nz_disp)
        !      Pot(j,Ny)=xpot(j)
    enddo

    if (abs(Nz_disp).gt.0) then
        do j=(Nz - Nz_disp),Nz
            Pot(j,Ny)=0.0
        enddo
    end if


    !lu decomposition: (end of 2nd stage!)
    !-----------------
    ! s = sum(Pot)
    ! if (debug) then
    ! write(*,*) 'Sum of Pot:',s
    ! end if


end subroutine

RECURSIVE SUBROUTINE Qsort(a)
    INTEGER, INTENT(IN OUT) :: a(:)
    INTEGER :: split

    IF(size(a) > 1) THEN
        CALL Partition(a, split)
        CALL Qsort(a(:split-1))
        CALL Qsort(a(split:))
    END IF
END SUBROUTINE Qsort

SUBROUTINE Partition(a, marker)
    INTEGER, INTENT(IN OUT) :: a(:)
    INTEGER, INTENT(OUT) :: marker
    INTEGER :: left, right, pivot, temp

    pivot = (a(1) + a(size(a))) / 2  ! Average of first and last elements to prevent quadratic
    left = 0                         ! behavior with sorted or reverse sorted data
    right = size(a) + 1

    DO WHILE (left < right)
        right = right - 1
        DO WHILE (a(right) > pivot)
            right = right-1
        END DO
        left = left + 1
        DO WHILE (a(left) < pivot)
            left = left + 1
        END DO
        IF (left < right) THEN
            temp = a(left)
            a(left) = a(right)
            a(right) = temp
        END IF
    END DO

    IF (left == right) THEN
        marker = left + 1
    ELSE
        marker = left
    END IF
END SUBROUTINE Partition


subroutine bubble_sort(a,n)

    integer:: n,i,j,do_loop,stop_loop,turn_count
    integer, dimension(n)::a

    do_loop =1

    do while (do_loop==1)
        turn_count =0
        do i=1,n-1
            if (a(i).gt.a(i+1)) then
                temp =a(i)
                a(i) = a(i+1)
                a(i+1) = temp
                turn_count = turn_count +1
            endif
        enddo
        if (turn_count.eq.0) then
            exit
        endif
    enddo
    !	write(*,*) 'Final sequence',a
end subroutine

