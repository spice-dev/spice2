PROGRAM pin
!     This is a simple mock-up run of SPICE to reconstruct the pin diagnostic 
!     for IV simulations, in case that something bad happens and pin is not stored properly.
!     Due to different rounding schemes, this is nearly impossible to do in postprocessing.
!
!     Compile:
!     make pin
!     Run:
!     ./pin.bin -t path/to/t-file_without_extension -o output/path/file_without_extension
    
    implicit none
    
    character :: okey
    character(len=2048) :: ofile='001'//char(0)
    character(len=2048) :: tfile='001'//char(0)

    integer :: Np, Nc, Na, p
 
    do
        okey=getopt('o:t:x')
        if(okey.eq.'>') exit
        if(okey.eq.'o') then
            ofile=trim(optarg)
            write(6,*) 'Output file ',trim(optarg)
        endif
        if(okey.eq.'t') then
            tfile=trim(optarg)
            write(6,*) 'Input file ',trim(optarg)
        endif
    end do

    call load_ntime(tfile, Na, Np, Nc)
    
    write (*,*) 'Time limits', Nc, Na, Np
    
    call MAINLOOP(ofile, Nc, Na, Np)

END PROGRAM pin

!******************************************************************************
!******************************************************************************

SUBROUTINE MAINLOOP(ofile, Nc, Na, Np)

    implicit none
    !data structure for simulation, grid and particle parameters
    !include "struct.h"

    character(len=2048) :: ofile

    integer :: Nc, Na, Np
    integer:: nbsteps,bstep,bint
    integer:: count_iv_objects
    integer :: count
    
    real*8 :: vstart, vstop, v_act
    real*8, dimension(Np - Nc) :: pin
    
    logical :: debug
    
    debug = .false.
    
    nbsteps = 400
    vstart = -10.0
    vstop = 1.0
    bstep = 0
    count_iv_objects = 1
    bint = int(real(Np - Na)/real(nbsteps))
    v_act = vstart
    
    
    do count=1,Np
        !write(6,*) '%%%%%%%%%%%%'
        if (debug) then
            write(6,*) '% Count=',count,' %'
        endif
        
        if (count.gt.Na) then
            !  write(*,*) 'Bias sweep check',bint
            if (real(count - Na).gt.bstep*bint.and.count_iv_objects.gt.0) then
                bstep = bstep + 1
                write(*,*) 'step ', count
                write(*,*) 'Bias sweeping step ',bstep
                v_act  = vstart + (vstop - vstart)/real(nbsteps)*real(bstep)
                write(*,*) 'New bias voltage ',v_act
            endif
        endif
        
        if (count.ge.Nc.and.count.le.Np) then        
            pin(count - Nc + 1) = v_act
        endif
        
    enddo
    
    call save_pin(ofile, Nc, Na, Np, pin)
    
    return


END

subroutine save_pin(outputFile, Nc, Na, Np, pin)
    use matio
    implicit none
    integer :: Nc, Na, Np
    real*8, dimension(Np - Nc) :: pin
    
    
    character*2048, intent(in):: outputFile

    integer:: use_compression, ierr, version
    type(MAT_T):: MAT
    type(MATVAR_T):: MATVAR

    use_compression = 0

    write (*,*) '#	Saving output file '//trim(outputFile)

    CALL fmat_loginit('savepin')

    ierr = fmat_create(trim(outputFile)//'.mat'//char(0), mat)

    ierr = fmat_varcreate('Nc', Nc, matvar)
    ierr = fmat_varwrite(mat, matvar, Nc, use_compression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('Na', Na, matvar)
    ierr = fmat_varwrite(mat, matvar, Na, use_compression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('Np', Np, matvar)
    ierr = fmat_varwrite(mat, matvar, Np, use_compression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('pin', pin, matvar)
    ierr = fmat_varwrite(mat, matvar, pin, use_compression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_close(mat)
    write (6,*) '#	Pin written.'
end subroutine

subroutine load_ntime(tfile, Na, Np, Nc)
    USE MATIO
    implicit none    
    character*2048, intent(in):: tfile
    integer, intent(out) :: Na, Np, Nc
    
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    integer :: ierr
    
    
    
    CALL FMAT_LOGINIT('ntimes')
    
    ierr = FMat_Open(trim(tfile)//'.mat'//char(0),MAT_ACC_RDONLY,MAT)
    
    ierr = FMat_VarReadInfo(MAT,'Na',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,Na)
    
    ierr = FMat_VarReadInfo(MAT,'Nc',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,Nc)
    
    ierr = FMat_VarReadInfo(MAT,'Np',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,Np)
    
    
    IERR = FMAT_CLOSE(MAT)
    

end subroutine
