module pes_mc

    use mpi
    use sm_tools
    use pes_m

    implicit none

    contains

    subroutine pes_mc_init(mumpsPar, MPI_COMM_SOLVER, nZ, nY, nZ_disp, dZ, dY, flagMatrix)
        implicit none

        type (DMUMPS_STRUC), intent(inout):: mumpsPar

        integer, intent(in) :: MPI_COMM_SOLVER
        integer, intent(in) :: nZ, nY, nZ_disp
        real*8, intent(in) :: dZ, dY
        integer, dimension(nZ, nY), intent(inout) :: flagMatrix

        write (*,*) '*** PES *** Starting MUMPS'
        call pes_mc_start(mumpsPar, MPI_COMM_SOLVER)
        write (*,*) '*** PES *** Defining the system'
        call pes_mc_define(mumpsPar, nZ, nY, nZ_disp, dZ, dY, flagMatrix, .false.)

    end subroutine pes_mc_init

    subroutine pes_mc_start(mumpsPar, MPI_COMM_SOLVER)

        implicit none

        type (DMUMPS_STRUC), intent(inout):: mumpsPar
        integer, intent(in) :: MPI_COMM_SOLVER


        mumpsPar%JOB = -1
        mumpsPar%SYM = 0
        mumpsPar%PAR = 1
        mumpsPar%COMM = MPI_COMM_SOLVER

        write (*,*) '*** PES *** MUMPS initialisation started'
        call dmumps(mumpsPar)

        mumpsPar%ICNTL(14) = 100
        mumpsPar%ICNTL(1) = -1
        mumpsPar%ICNTL(2) = -1
        mumpsPar%ICNTL(3) = -1
        mumpsPar%ICNTL(4) = -1
        mumpsPar%ICNTL(7) = 2

        write (*,*) '*** PES *** MUMPS initialisation completed'
    end subroutine pes_mc_start


    subroutine pes_mc_define(mumpsPar, nZ, nY, nZ_disp, dZ, dY, flagMatrix, reset)

        implicit none

        type (DMUMPS_STRUC), intent(inout):: mumpsPar

        integer, intent(in) :: nZ, nY, nZ_disp
        real*8, intent(in) :: dZ, dY
        integer, dimension(nZ, nY), intent(inout) :: flagMatrix

        logical, intent(in) :: reset

        integer :: elementCount
        integer :: matrixDimension
        integer :: ierr

        integer :: z, y, i, bottom, left, right, top, center, nYAdjusted

        integer, dimension(:), allocatable :: testIRN, testJCN
        real*8, dimension(:), allocatable :: testA

        integer*8 :: timeStart, timeEnd
        real*8 :: timeDiff

        real*8 :: hzy, hzySquared, centerValue, sideValue, sideValue2

        hzy = dz / dy
        hzySquared = hzy * hzy

        centerValue = 2.0 * (1.0 + hzySquared)
        sideValue = -1.0 * hzySquared
        sideValue2 = - 2.0 * hzySquared

        nYAdjusted = nY - 1

        if (reset) then
            call pes_mc_stop(mumpsPar)
            call pes_mc_start(mumpsPar, mumpsPar%COMM)
        endif


        write (*,*) "P"
        matrixDimension = nZ * nYAdjusted

        if (mumpsPar%myId == 0) then
            write (*,*) '*** PES *** Counting matrix'
            call sm_count(nZ, nYAdjusted, flagMatrix(1:nZ,1:nYAdjusted), elementCount)

            mumpsPar%NZ = elementCount
            mumpsPar%N = matrixDimension

            allocate(mumpsPar%IRN(mumpsPar%NZ))
            allocate(mumpsPar%JCN(mumpsPar%NZ))
            allocate(mumpsPar%A(mumpsPar%NZ))

            allocate(mumpsPar%RHS(mumpsPar%N))

            mumpsPar%IRN = 0
            mumpsPar%JCN = 0
            mumpsPar%A = 0
            mumpsPar%RHS = 1

            write (*,*) '*** PES *** Creating matrix'
            call sm_create(nZ, nYAdjusted, nZ_disp, flagMatrix(1:nZ,1:nYAdjusted), elementCount, dZ, dY, mumpsPar%IRN, mumpsPar%JCN, mumpsPar%A)

        endif

        call pes_sync()

        mumpsPar%JOB = 4
        call dmumps(mumpsPar)

    end subroutine pes_mc_define

    subroutine pes_mc_stop(mumpsPar)
        implicit none

        ! solver
        type (DMUMPS_STRUC), intent(inout):: mumpsPar

        if (mumpsPar%MYID .eq. 0 )then
            deallocate(mumpsPar%IRN)
            deallocate(mumpsPar%JCN)
            deallocate(mumpsPar%A)
            deallocate(mumpsPar%RHS)
        endif

        mumpsPar%JOB = -2
        call dmumps(mumpsPar)


    end subroutine pes_mc_stop

end module pes_mc
