! subroutines concerning grid - coordinates, weighting
subroutine grid_particles(count,z,y,iy1,iz1,dz,dy,no_part)
    implicit none
    ! calculates grid coordinates of a given set of particles
    ! we don't need iz and iy any more
    !let's use vector notation, possible speedup (loop unrolling etc)
    integer:: no_part,i,count
    real*8, dimension(no_part):: z,y
    !opt
    real*8:: iz,iy
    integer, dimension(no_part):: iz1,iy1
    real*8:: dz,dy
    real*8:: tz,ty,zy

    if ((dz.ne.1.0).or.(dy.ne.1.0)) then
        do i=1,no_part
            !Calculate the 2D weighting for each particle.          new!
            iz1(i)=idint(z(i)/dz) + 1
            iz= z(i)/dz - iz1(i) -1
            !iz1(i)=iz1(i)+1          !index of cell in j-direction

            iy1(i)=idint(y(i)/dy) + 1
            iy =y(i)/dy - iy1(i) - 1
            !        iy1(i)=iy1(i)+1          !index of cell in k-direction

            !NEW JG 6/12/5
            !  	A11(i)=(1.d0-iz)*(1.d0-iy)
            !  	A12(i)=(1.d0-iz)*iy
            !  	A21(i)=iz*(1.d0-iy)
            !  	A22(i)=iz*iy
            ! write(*,*) 'part dz<1', z(i), y(i), iz1(i), iy1(i)
        enddo  !do i=N_i,1,-1
    else
        ! fast way without dy and dz
        do i=1,no_part
            ! 	if (z(i).gt.0.0) then
            !Calculate the 2D weighting for each particle.          new!
            iz1(i)=idint(z(i)) + 1
            ! 	if (z(i).lt.0.0) then
            ! 		write(*.*) 'Conversion',z(i),iz1(i)
            ! 	end if

            iz= z(i) - float(iz1(i) - 1)
            !        iz1(i)=iz1(i)+1          !index of cell in j-direction

            iy1(i)=idint(y(i)) + 1
            iy =y(i) - float(iy1(i) -1)
            !        iy1(i)=iy1(i)+1          !index of cell in k-direction

            !NEW JG 6/12/5
            !  	A11(i)=(1.d0-iz)*(1.d0-iy)
            !  	A12(i)=(1.d0-iz)*iy
            !  	A21(i)=iz*(1.d0-iy)
            !  	A22(i)=iz*iy
            !trick - use addition rather then multiplication
            zy = iz*iy
            !   	A11(i)=1.0 - iz - iy + zy
            !   	A12(i)=iy- zy
            !   	A21(i)=iz- zy
            !   	A22(i)=zy
            ! write(*,*) 'part', z(i), y(i), iz1(i), iy1(i)
        enddo  !do i=N_i,1,-1
    end if



end subroutine

subroutine weight_particles(count,no_part,y,z,ux,uy,uz,Lz,Nz,Ny,Na,Nv,iz1,iy1,no_species,q,stype,rho_tot,&
        rho,weighting_type,debug,flag,domain_decomp,y_start,y_stop,mpi_rank,proc_no,surface_matrix,dy,dz,&
        take_diag,jv,vx,vy,vz,vx2,vy2,vz2,vx3,vy3,vz3,dv,vpar,y_stop_large,Nz_max,z_start,z_stop,z_start_box,z_stop_box)
    implicit none
    !Calculates charge density from a given set of particles and
    !velocity diagnostics
    !MK diag now in separate loop to enhance rho_tot calculations
    ! using rho_big to get rid of if clauses - it has indexes 0: Nz +1 and 0 - Ny:
    !at the end of loop we cut rho_tot out of it
    !This should enable loop unrolling and vectorization
    integer:: no_part,i,Na,count,jv,Nv,Nz,Ny,j,k,sp,no_species,weighting_type,y_start,y_stop,proc_no,y_stop_large,Nz_max,z_start,z_stop,z_start_box,z_stop_box
    real*8, dimension(no_part):: y,z,ux,uy,uz
    integer*2, dimension(no_part):: stype
    integer, dimension(no_part):: iz1,iy1
    real*8, dimension(no_species,Nz_max,Ny):: rho
    real*8, dimension(Nz,Ny):: rho_tot,surface_matrix
    real*8, dimension(no_species,Nz,Ny):: vx,vy,vz,vx2,vy2,vz2,vx3,vy3,vz3
    real*8,dimension(Nv):: vpar
    real*8, dimension(no_species):: w_ref,q
    real*8, dimension(no_species,Nz_max+1,Ny+1):: rho_big
    real*8:: Lz,w,dv,v1x,v1y,v1z,v2x,v2y,v2z,v3x,v3y,v3z
    logical:: debug,domain_decomp,take_diag
    integer:: n
    integer, dimension(Nz,Ny):: flag
    integer, dimension(no_species):: mpi_rank
    real*8:: iz,iy,dy,dz
    ! temporary weight coefficients
    real*8:: A11x,A12x,A21x,A22x

    ! write(*,*) 'Received q',q
    do sp=1,no_species
        if(mpi_rank(sp).eq.proc_no) then

            rho_big(sp,z_start:(z_stop+1),y_start:(y_stop+1)) = 0.0
        end if
    end do

    if (debug) then
        write(*,*) 'Grid: ',y_start,y_stop
    end if
    if (take_diag) then
        ! zero out diagnostic fields
        do sp=1,no_species
            if(mpi_rank(sp).eq.proc_no) then

                vx(sp,:,:) = 0.0
                vy(sp,:,:) = 0.0
                vz(sp,:,:) = 0.0
                vx2(sp,:,:) = 0.0
                vy2(sp,:,:) = 0.0
                vz2(sp,:,:) = 0.0
                vx3(sp,:,:) = 0.0
                vy3(sp,:,:) = 0.0
                vz3(sp,:,:) = 0.0

            end if
        end do



    end if


    ! rho_tot = 0.0
    do i=1,no_part
        !weighting coef
        ! 	if (weighting_type.eq.0) then
        ! 	!no weighting
        ! 		w = 1.0
        ! 	else if (weighting_type.eq.1) then
        ! 		w = w_ref(stype(i))/dsqrt(uy(i)*uy(i) + uz(i)*uz(i))
        !
        ! 	end if
        iz1(i)=idint(z(i)/dz) + 1
        iz= z(i)/dz - iz1(i) +1
        !iz1(i)=iz1(i)+1          !index of cell in j-direction

        iy1(i)=idint(y(i)/dy) + 1
        iy =y(i)/dy - iy1(i) + 1
        !        iy1(i)=iy1(i)+1          !index of cell in k-direction

        !NEW JG 6/12/5
        !  	A11(i)=(1.d0-iz)*(1.d0-iy)
        !  	A12(i)=(1.d0-iz)*iy
        !  	A21(i)=iz*(1.d0-iy)
        !  	A22(i)=iz*iy
        A11x=(1.d0-iz)*(1.d0-iy)
        A12x=(1.d0-iz)*iy
        A21x=iz*(1.d0-iy)
        A22x=iz*iy
        if (iy.lt.0.or.iz.lt.0) then

            write(*,*) 'Particle out of bounds POS:',stype(i),z(i),y(i)
            write(*,*) 'Particle out of bounds VEL:',ux(i),uy(i),uz(i)


        end if


        !write(*,*) 'z1,y1',iy1(i),iz1(i),i,stype(i)
        ! if (y(i).gt.(y_stop -1).or.y(i).lt.(y_start -1)) then
        !    write(*,*) 'Error, particle out of slice'
        !    write(*,*) 'Slice',y_start,y_stop
        !    write(*,*) 'Count,i',count,i
        !    write(*,*) 'pos',z(i),y(i)
        !    write(*,*) 'stype',stype(i)
        !
        !
        ! !         read(*,*) w
        ! end if
        ! if (debug) then
        ! if ((iz1(i).lt.1).or.(iy1(i).lt.1).or.(iy1(i).gt.Ny).or.(iz1(i).gt.Nz)) then
        ! 		write(*,*) 'Griding error!!!!'
        ! 		write(*,*) 'Particle',i,stype(i)
        ! 		write(*,*) 'Coordinates',iz1(i),iy1(i)
        ! 		write(*,*) 'Dimensions',Nz,Ny
        !
        ! stop
        ! end if
        ! end if
        !       !---------------------A-weighting 2D--------------------1
        !       rho_big(stype(i),iz1(i),iy1(i))=rho_big(stype(i),iz1(i),iy1(i))+q(stype(i))*A11(i)
        !       !---------------------B-weighting 2D--------------------1
        !          rho_big(stype(i),iz1(i)+1,iy1(i))=rho_big(stype(i),iz1(i)+1,iy1(i))+q(stype(i))*A21(i)
        !       !---------------------B-weighting 2D--------------------2
        !       !---------------------C-weighting 2D--------------------1
        !          rho_big(stype(i),iz1(i)+1,iy1(i)+1)=rho_big(stype(i),iz1(i)+1,iy1(i)+1)+q(stype(i))*A22(i)
        !       !---------------------C-weighting 2D--------------------2
        !       !---------------------D-weighting 2D--------------------1
        !          rho_big(stype(i),iz1(i),iy1(i)+1)=rho_big(stype(i),iz1(i),iy1(i)+1)+q(stype(i))*A12(i)
        !       !---------------------D-weighting 2D--------------------2

        !---------------------A-weighting 2D--------------------1
        rho_big(stype(i),iz1(i),iy1(i))=rho_big(stype(i),iz1(i),iy1(i))+q(stype(i))*A11x
        !---------------------B-weighting 2D--------------------1
        rho_big(stype(i),iz1(i)+1,iy1(i))=rho_big(stype(i),iz1(i)+1,iy1(i))+q(stype(i))*A21x
        !---------------------B-weighting 2D--------------------2
        !---------------------C-weighting 2D--------------------1
        rho_big(stype(i),iz1(i)+1,iy1(i)+1)=rho_big(stype(i),iz1(i)+1,iy1(i)+1)+q(stype(i))*A22x
        !---------------------C-weighting 2D--------------------2
        !---------------------D-weighting 2D--------------------1
        rho_big(stype(i),iz1(i),iy1(i)+1)=rho_big(stype(i),iz1(i),iy1(i)+1)+q(stype(i))*A12x
        if ((z(i).le.Lz).and.(z(i).gt.0.0)) then  !RD - for PARTICLE INJECTION with an angle   NEW!!

            if (take_diag) then
                v1z=uz(i)
                v1y=uy(i)
                v1x=ux(i)
                vz(stype(i),iz1(i),iy1(i))=vz(stype(i),iz1(i),iy1(i))+v1z*A11x
                vy(stype(i),iz1(i),iy1(i))=vy(stype(i),iz1(i),iy1(i))+v1y*A11x
                vx(stype(i),iz1(i),iy1(i))=vx(stype(i),iz1(i),iy1(i))+v1x*A11x

                v2z=v1z*uz(i)
                v2y=v1y*uy(i)
                v2x=v1x*ux(i)
                vz2(stype(i),iz1(i),iy1(i))=vz2(stype(i),iz1(i),iy1(i))+v2z*A11x
                vy2(stype(i),iz1(i),iy1(i))=vy2(stype(i),iz1(i),iy1(i))+v2y*A11x
                vx2(stype(i),iz1(i),iy1(i))=vx2(stype(i),iz1(i),iy1(i))+v2x*A11x

                v3z=v2z*uz(i)
                v3y=v2y*uy(i)
                v3x=v2x*ux(i)
                vz3(stype(i),iz1(i),iy1(i))=vz3(stype(i),iz1(i),iy1(i))+v3z*A11x
                vy3(stype(i),iz1(i),iy1(i))=vy3(stype(i),iz1(i),iy1(i))+v3y*A11x
                vx3(stype(i),iz1(i),iy1(i))=vx3(stype(i),iz1(i),iy1(i))+v3x*A11x

                !jv=idint((uz(i)-vpar(1)-dv/2)/dv+1)
                ! jv is a damned dinosaur
                jv = 1
                if (iz1(i)<Nz) then
                    vz(stype(i),iz1(i)+1,iy1(i))=vz(stype(i),iz1(i)+1,iy1(i))+v1z*A21x
                    vy(stype(i),iz1(i)+1,iy1(i))=vy(stype(i),iz1(i)+1,iy1(i))+v1y*A21x
                    vx(stype(i),iz1(i)+1,iy1(i))=vx(stype(i),iz1(i)+1,iy1(i))+v1x*A21x
                    vz2(stype(i),iz1(i)+1,iy1(i))=vz2(stype(i),iz1(i)+1,iy1(i))+v2z*A21x
                    vy2(stype(i),iz1(i)+1,iy1(i))=vy2(stype(i),iz1(i)+1,iy1(i))+v2y*A21x
                    vx2(stype(i),iz1(i)+1,iy1(i))=vx2(stype(i),iz1(i)+1,iy1(i))+v2x*A21x
                    vz3(stype(i),iz1(i)+1,iy1(i))=vz3(stype(i),iz1(i)+1,iy1(i))+v3z*A21x
                    vy3(stype(i),iz1(i)+1,iy1(i))=vy3(stype(i),iz1(i)+1,iy1(i))+v3y*A21x
                    vx3(stype(i),iz1(i)+1,iy1(i))=vx3(stype(i),iz1(i)+1,iy1(i))+v3x*A21x
                endif  !if (iz1(i)<Nz) then
                if (iz1(i)<Nz.AND.iy1(i)<Ny) then
                    vz(stype(i),iz1(i)+1,iy1(i)+1)=vz(stype(i),iz1(i)+1,iy1(i)+1)+v1z*A22x
                    vy(stype(i),iz1(i)+1,iy1(i)+1)=vy(stype(i),iz1(i)+1,iy1(i)+1)+v1y*A22x
                    vx(stype(i),iz1(i)+1,iy1(i)+1)=vx(stype(i),iz1(i)+1,iy1(i)+1)+v1x*A22x
                    vz2(stype(i),iz1(i)+1,iy1(i)+1)=vz2(stype(i),iz1(i)+1,iy1(i)+1)+v2z*A22x
                    vy2(stype(i),iz1(i)+1,iy1(i)+1)=vy2(stype(i),iz1(i)+1,iy1(i)+1)+v2y*A22x
                    vx2(stype(i),iz1(i)+1,iy1(i)+1)=vx2(stype(i),iz1(i)+1,iy1(i)+1)+v2x*A22x
                    vz3(stype(i),iz1(i)+1,iy1(i)+1)=vz3(stype(i),iz1(i)+1,iy1(i)+1)+v3z*A22x
                    vy3(stype(i),iz1(i)+1,iy1(i)+1)=vy3(stype(i),iz1(i)+1,iy1(i)+1)+v3y*A22x
                    vx3(stype(i),iz1(i)+1,iy1(i)+1)=vx3(stype(i),iz1(i)+1,iy1(i)+1)+v3x*A22x
                endif  !if (iz1(i)<Nz.AND.iy1(i)<Ny) then
                if (iy1(i)<Ny) then
                    vz(stype(i),iz1(i),iy1(i)+1)=vz(stype(i),iz1(i),iy1(i)+1)+v1z*A12x
                    vy(stype(i),iz1(i),iy1(i)+1)=vy(stype(i),iz1(i),iy1(i)+1)+v1y*A12x
                    vx(stype(i),iz1(i),iy1(i)+1)=vx(stype(i),iz1(i),iy1(i)+1)+v1x*A12x
                    vz2(stype(i),iz1(i),iy1(i)+1)=vz2(stype(i),iz1(i),iy1(i)+1)+v2z*A12x
                    vy2(stype(i),iz1(i),iy1(i)+1)=vy2(stype(i),iz1(i),iy1(i)+1)+v2y*A12x
                    vx2(stype(i),iz1(i),iy1(i)+1)=vx2(stype(i),iz1(i),iy1(i)+1)+v2x*A12x
                    vz3(stype(i),iz1(i),iy1(i)+1)=vz3(stype(i),iz1(i),iy1(i)+1)+v3z*A12x
                    vy3(stype(i),iz1(i),iy1(i)+1)=vy3(stype(i),iz1(i),iy1(i)+1)+v3y*A12x
                    vx3(stype(i),iz1(i),iy1(i)+1)=vx3(stype(i),iz1(i),iy1(i)+1)+v3x*A12x
                endif  !if (iy1(i)<Ny) then




            end if


        endif   !NEW!!

    enddo  !do i=1,N_i
    ! let's cut the rho_tot
    !   write(*,*) 'Weigting loop finished'
    do sp=1,no_species
        if(mpi_rank(sp).eq.proc_no.and.z_start.lt.Nz) then
            rho(sp,z_start:z_stop,y_start:y_stop) =  rho_big(sp,z_start:z_stop,y_start:y_stop)
            if (sum(rho(sp,z_start:z_stop,y_start:y_stop)).ne.sum(rho_big(sp,z_start:z_stop,y_start:y_stop))) then
                !write(*,*) 'Charge lost!',sum(rho(sp,z_start:z_stop,y_start:y_stop)),sum(rho_big(sp,z_start:z_stop,y_start:y_stop))
            end if
        end if
    end do
    ! write(*,*) proc_no,'Survived rho1',z_start_box,z_stop_box,z_start,z_stop
    ! we will have to do this later
    ! when all the part of rho will be together
    !      do j=1,Nz
    !         rho(:,j,1)=(rho(:,j,1)+rho(:,j,Ny))/2.0   !PC
    !         rho(:,j,Ny)=rho(:,j,1)
    !      enddo

    !     do k=1,Ny
    !         rho(:,Nz,k)=rho(:,Nz,k)*2.d0
    !         if (k.le.Ny1) then
    !            rho(:,Nz1,k)=rho(:,Nz1,k)*2.d0
    !         elseif (k.gt.Ny1.AND.k.lt.Ny2) then
    !            rho(:,1,k)=rho(:,1,k)*2.d0
    !         elseif (k.ge.Ny2) then
    !            rho(:,Nz1,k)=rho(:,Nz1,k)*2.d0
    !         endif
    !     enddo
    !     rho(:,1,Ny1)=rho(:,1,Ny1)*2.d0
    !     rho(:,1,Ny2)=rho(:,1,Ny2)*2.d0
    !     do j=1,Nz1
    !        rho(:,j,Ny1)=rho(:,j,Ny1)*2.d0
    !        rho(:,j,Ny2)=rho(:,j,Ny2)*2.d0
    !     enddo
    !     rho(:,Nz1,Ny1)=rho(:,Nz1,Ny1)/3.d0
    !     rho(:,Nz1,Ny2)=rho(:,Nz1,Ny2)/3.d0



    do sp=1,no_species
        if(mpi_rank(sp).eq.proc_no) then

            if (z_start.lt.Nz) then
                call fix_borders_general_2D(rho(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(z_start_box:z_stop_box,y_start:y_stop),Nz_max,z_start_box,z_stop_box)
                ! write(*,*) proc_no,'Survived rho2'

            end if
            ! unfix the top line
            ! if (z_start.le.Nz.and.z_stop.ge.Nz) then
            !     rho(sp,Nz,:) = rho(sp,Nz,:)/2.0
            ! end if
            if (z_start.lt.Nz) then

                rho_tot(z_start_box:z_stop_box,y_start:y_stop) = rho_tot(z_start_box:z_stop_box,y_start:y_stop) + rho(sp,z_start_box:z_stop_box,y_start:y_stop)
                ! write(*,*) proc_no,'Survived rho3'

            end if

        end if
    end do

    if (take_diag) then
        do sp=1,no_species
            if(mpi_rank(sp).eq.proc_no.and.z_start.lt.Nz) then

                call fix_borders_general_2D(vx(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(z_start_box:z_stop_box,y_start:y_stop),Nz,z_start_box,z_stop_box)
                call fix_borders_general_2D(vy(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(z_start_box:z_stop_box,y_start:y_stop),Nz,z_start_box,z_stop_box)
                call fix_borders_general_2D(vz(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(z_start_box:z_stop_box,y_start:y_stop),Nz,z_start_box,z_stop_box)
                call fix_borders_general_2D(vx2(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(z_start_box:z_stop_box,y_start:y_stop),Nz,z_start_box,z_stop_box)
                call fix_borders_general_2D(vy2(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(z_start_box:z_stop_box,y_start:y_stop),Nz,z_start_box,z_stop_box)
                call fix_borders_general_2D(vz2(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(z_start_box:z_stop_box,y_start:y_stop),Nz,z_start_box,z_stop_box)
                call fix_borders_general_2D(vx3(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(z_start_box:z_stop_box,y_start:y_stop),Nz,z_start_box,z_stop_box)
                call fix_borders_general_2D(vy3(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(z_start_box:z_stop_box,y_start:y_stop),Nz,z_start_box,z_stop_box)
                call fix_borders_general_2D(vz3(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(z_start_box:z_stop_box,y_start:y_stop),Nz,z_start_box,z_stop_box)
            end if
        end do

    end if

end subroutine


subroutine grid_diag(no_species,no_part,jv,Nv,Nz,Ny,y,z,ux,uy,uz,A11,A12,A21,A22,q,stype,iz1,iy1,vx,vy,vz,&
        vx2,vy2,vz2,vx3,vy3,vz3,rho,dv,vpar,Lz,n,flag_vector,flag,y_start,y_stop,surface_matrix,mpi_rank,proc_no)
    implicit none
    integer:: j,k,i,sp,no_species,no_part,jv,Nv,Nz,Ny,y_start,y_stop,proc_no,z_start_box,z_stop_box
    real*8, dimension(no_part):: y,z,ux,uy,uz,A11,A12,A21,A22
    integer*2, dimension(no_part):: stype
    integer, dimension(no_part):: iz1,iy1
    real*8, dimension(no_species,Nz,Ny):: vx,vy,vz,vx2,vy2,vz2,vx3,vy3,vz3,rho
    real*8:: v1x,v1y,v1z,v2x,v2y,v2z,v3x,v3y,v3z,dv,Lz
    real*8,dimension(Nv):: vpar
    integer, dimension(Nz,Ny):: flag
    real*8, dimension(Nz,y_start:y_stop):: surface_matrix
    integer, dimension(no_species):: mpi_rank
    real,dimension(no_species):: q

    integer:: n
    integer, dimension(n - Nz):: flag_vector

    do i=1,no_part
        if ((z(i).lt.Lz).and.(z(i).gt.0.0)) then
            v1z=uz(i)
            v1y=uy(i)
            v1x=ux(i)
            vz(stype(i),iz1(i),iy1(i))=vz(stype(i),iz1(i),iy1(i))+v1z*A11(i)
            vy(stype(i),iz1(i),iy1(i))=vy(stype(i),iz1(i),iy1(i))+v1y*A11(i)
            vx(stype(i),iz1(i),iy1(i))=vx(stype(i),iz1(i),iy1(i))+v1x*A11(i)

            v2z=v1z*uz(i)
            v2y=v1y*uy(i)
            v2x=v1x*ux(i)
            vz2(stype(i),iz1(i),iy1(i))=vz2(stype(i),iz1(i),iy1(i))+v2z*A11(i)
            vy2(stype(i),iz1(i),iy1(i))=vy2(stype(i),iz1(i),iy1(i))+v2y*A11(i)
            vx2(stype(i),iz1(i),iy1(i))=vx2(stype(i),iz1(i),iy1(i))+v2x*A11(i)

            v3z=v2z*uz(i)
            v3y=v2y*uy(i)
            v3x=v2x*ux(i)
            vz3(stype(i),iz1(i),iy1(i))=vz3(stype(i),iz1(i),iy1(i))+v3z*A11(i)
            vy3(stype(i),iz1(i),iy1(i))=vy3(stype(i),iz1(i),iy1(i))+v3y*A11(i)
            vx3(stype(i),iz1(i),iy1(i))=vx3(stype(i),iz1(i),iy1(i))+v3x*A11(i)

            !jv=idint((uz(i)-vpar(1)-dv/2)/dv+1)
            ! jv is a damned dinosaur
            jv = 1
            if (iz1(i)<Nz) then
                vz(stype(i),iz1(i)+1,iy1(i))=vz(stype(i),iz1(i)+1,iy1(i))+v1z*A21(i)
                vy(stype(i),iz1(i)+1,iy1(i))=vy(stype(i),iz1(i)+1,iy1(i))+v1y*A21(i)
                vx(stype(i),iz1(i)+1,iy1(i))=vx(stype(i),iz1(i)+1,iy1(i))+v1x*A21(i)
                vz2(stype(i),iz1(i)+1,iy1(i))=vz2(stype(i),iz1(i)+1,iy1(i))+v2z*A21(i)
                vy2(stype(i),iz1(i)+1,iy1(i))=vy2(stype(i),iz1(i)+1,iy1(i))+v2y*A21(i)
                vx2(stype(i),iz1(i)+1,iy1(i))=vx2(stype(i),iz1(i)+1,iy1(i))+v2x*A21(i)
                vz3(stype(i),iz1(i)+1,iy1(i))=vz3(stype(i),iz1(i)+1,iy1(i))+v3z*A21(i)
                vy3(stype(i),iz1(i)+1,iy1(i))=vy3(stype(i),iz1(i)+1,iy1(i))+v3y*A21(i)
                vx3(stype(i),iz1(i)+1,iy1(i))=vx3(stype(i),iz1(i)+1,iy1(i))+v3x*A21(i)
            endif  !if (iz1(i)<Nz) then
            if (iz1(i)<Nz.AND.iy1(i)<Ny) then
                vz(stype(i),iz1(i)+1,iy1(i)+1)=vz(stype(i),iz1(i)+1,iy1(i)+1)+v1z*A22(i)
                vy(stype(i),iz1(i)+1,iy1(i)+1)=vy(stype(i),iz1(i)+1,iy1(i)+1)+v1y*A22(i)
                vx(stype(i),iz1(i)+1,iy1(i)+1)=vx(stype(i),iz1(i)+1,iy1(i)+1)+v1x*A22(i)
                vz2(stype(i),iz1(i)+1,iy1(i)+1)=vz2(stype(i),iz1(i)+1,iy1(i)+1)+v2z*A22(i)
                vy2(stype(i),iz1(i)+1,iy1(i)+1)=vy2(stype(i),iz1(i)+1,iy1(i)+1)+v2y*A22(i)
                vx2(stype(i),iz1(i)+1,iy1(i)+1)=vx2(stype(i),iz1(i)+1,iy1(i)+1)+v2x*A22(i)
                vz3(stype(i),iz1(i)+1,iy1(i)+1)=vz3(stype(i),iz1(i)+1,iy1(i)+1)+v3z*A22(i)
                vy3(stype(i),iz1(i)+1,iy1(i)+1)=vy3(stype(i),iz1(i)+1,iy1(i)+1)+v3y*A22(i)
                vx3(stype(i),iz1(i)+1,iy1(i)+1)=vx3(stype(i),iz1(i)+1,iy1(i)+1)+v3x*A22(i)
            endif  !if (iz1(i)<Nz.AND.iy1(i)<Ny) then
            if (iy1(i)<Ny) then
                vz(stype(i),iz1(i),iy1(i)+1)=vz(stype(i),iz1(i),iy1(i)+1)+v1z*A12(i)
                vy(stype(i),iz1(i),iy1(i)+1)=vy(stype(i),iz1(i),iy1(i)+1)+v1y*A12(i)
                vx(stype(i),iz1(i),iy1(i)+1)=vx(stype(i),iz1(i),iy1(i)+1)+v1x*A12(i)
                vz2(stype(i),iz1(i),iy1(i)+1)=vz2(stype(i),iz1(i),iy1(i)+1)+v2z*A12(i)
                vy2(stype(i),iz1(i),iy1(i)+1)=vy2(stype(i),iz1(i),iy1(i)+1)+v2y*A12(i)
                vx2(stype(i),iz1(i),iy1(i)+1)=vx2(stype(i),iz1(i),iy1(i)+1)+v2x*A12(i)
                vz3(stype(i),iz1(i),iy1(i)+1)=vz3(stype(i),iz1(i),iy1(i)+1)+v3z*A12(i)
                vy3(stype(i),iz1(i),iy1(i)+1)=vy3(stype(i),iz1(i),iy1(i)+1)+v3y*A12(i)
                vx3(stype(i),iz1(i),iy1(i)+1)=vx3(stype(i),iz1(i),iy1(i)+1)+v3x*A12(i)
            endif  !if (iy1(i)<Ny) then

        end if!geometry
    end do ! diag loop
    do sp=1,no_species
        do j=1,Nz
            do k=1,Ny
                if (rho(sp,j,k).ne.0.d0) then
                    vz(sp,j,k)=vz(sp,j,k)/abs(rho(sp,j,k))*abs(q(sp))
                    vy(sp,j,k)=vy(sp,j,k)/abs(rho(sp,j,k))*abs(q(sp))
                    vx(sp,j,k)=vx(sp,j,k)/abs(rho(sp,j,k))*abs(q(sp))
                    vz2(sp,j,k)=vz2(sp,j,k)/abs(rho(sp,j,k))*abs(q(sp))
                    vy2(sp,j,k)=vy2(sp,j,k)/abs(rho(sp,j,k))*abs(q(sp))
                    vx2(sp,j,k)=vx2(sp,j,k)/abs(rho(sp,j,k))*abs(q(sp))
                    vz3(sp,j,k)=vz3(sp,j,k)/abs(rho(sp,j,k))*abs(q(sp))
                    vy3(sp,j,k)=vy3(sp,j,k)/abs(rho(sp,j,k))*abs(q(sp))
                    vx3(sp,j,k)=vx3(sp,j,k)/abs(rho(sp,j,k))*abs(q(sp))
                endif  !if (rhoi(j,k)>0.d0) then
            enddo  !do k=1,Ny
        enddo  !do j=1,Nz
    end do !sp
    !At boundaries...

    ! call fix_borders(vx,no_species,Nz,Ny,Nz1,Ny1,Ny2)
    ! call fix_borders(vy,no_species,Nz,Ny,Nz1,Ny1,Ny2)
    ! call fix_borders(vz,no_species,Nz,Ny,Nz1,Ny1,Ny2)
    ! call fix_borders(vx2,no_species,Nz,Ny,Nz1,Ny1,Ny2)
    ! call fix_borders(vy2,no_species,Nz,Ny,Nz1,Ny1,Ny2)
    ! call fix_borders(vz2,no_species,Nz,Ny,Nz1,Ny1,Ny2)
    ! call fix_borders(vx3,no_species,Nz,Ny,Nz1,Ny1,Ny2)
    ! call fix_borders(vy3,no_species,Nz,Ny,Nz1,Ny1,Ny2)
    ! call fix_borders(vz3,no_species,Nz,Ny,Nz1,Ny1,Ny2)
    !general fixing algorithm
    do sp=1,no_species
        if(mpi_rank(sp).eq.proc_no) then

            call fix_borders_general_2D(vx(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(:,y_start:y_stop),1,Nz)
            call fix_borders_general_2D(vy(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(:,y_start:y_stop),1,Nz)
            call fix_borders_general_2D(vz(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(:,y_start:y_stop),1,Nz)
            call fix_borders_general_2D(vx2(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(:,y_start:y_stop),1,Nz)
            call fix_borders_general_2D(vy2(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(:,y_start:y_stop),1,Nz)
            call fix_borders_general_2D(vz2(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(:,y_start:y_stop),1,Nz)
            call fix_borders_general_2D(vx3(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(:,y_start:y_stop),1,Nz)
            call fix_borders_general_2D(vy3(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(:,y_start:y_stop),1,Nz)
            call fix_borders_general_2D(vz3(sp,z_start_box:z_stop_box,y_start:y_stop),no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix(:,y_start:y_stop),1,Nz)
        end if
    end do
    !----------------------------------------------------------------------------------------




end subroutine


subroutine weight_tube_ngp(count,no_part,y,z,ux,uy,uz,Lz,Nz,Ny,Na,Nv,Ny1,Ny2,Nz1,no_species,q,&
        stype,rho_tot,rho,weighting_type,w_ref,dt,dy,dz,debug,Ly)
    implicit none
    ! Needed for the steady-state code - calculates charge density using the NGP for a set of charge tubes
    integer:: no_part,i,Na,count,jv,Nv,Nz,Ny,j,k,Ny1,Ny2,Nz1,sp,no_species,weighting_type,pass
    real*8, dimension(no_part):: y,z,ux,uy,uz,A11,A12,A21,A22,q
    integer*2, dimension(no_part):: stype
    integer, dimension(no_part):: iz1,iy1
    !big matrix for BC
    real*8, dimension(no_species,Nz,Ny):: rho
    real*8, dimension(Nz,Ny):: rho_tot
    real*8, dimension(no_species):: w_ref
    real*8, dimension(no_species,2*Nz,3*Ny):: rho_big
    real*8:: Lz,w,dt,dy,dz,Ly
    real*8:: z0,y0,z1,y1,i0z,i0y,i1y,i1z,z_test,y_int,z_int,l
    integer, dimension(2):: next_cell
    integer:: iy0,iz0,iy0_ngp,iz0_ngp,iy1_ngp,iz1_ngp,pc_shift
    logical move_cell, debug
    real*8:: dyh,dzh

    rho_big = 0.0
    dyh = dy/2.0
    dzh = dy/2.0

    do i=1,no_part

        z0 = z(i)
        y0 = y(i)
        z1 = z0 + uz(i)*dt
        y1 = y0 + uy(i)*dt


        if (((z1.lt.(Lz + dz)).or.(z0.lt.(Lz + dz))).and.(z0.gt.dzh)) then
            !correct the final point if z  less then 0
            if (z1.lt.(-dzh)) then
                y1 = y0 + (y1 - y0)/(z1 - z0)*(-dzh - z0)
                z1 = - dzh + 1.0E-6
            end if

            !watch out for negative y!!!
            pc_shift = 0
            do while ((y0.lt.dyh).or.(y1.lt.dyh))
                y0 = y0 + Ly
                y1 = y1 + Ly
                pc_shift = pc_shift + 1
            end do
            !check if there arent fast electrosn
            if (pc_shift.gt.2) then
                write(*,*) 'Error, particle passed at once the whole simulation region!!!'
                write(*,*) 'Particle:', i,stype(i)
                write(*,*) 'Pos:',y(i), z(i)
                write(*,*) 'Vel:',ux(i),uy(i), uz(i)

            end if


            iy0 = idint(y0/dy) +1
            iz0 = idint(z0/dz) +1
            iy1 = idint(y1/dy) +1
            iz1 = idint(z1/dz) +1
            iy0_ngp = idint(y0/dy + 0.5) +1
            iz0_ngp = idint(z0/dy + 0.5) +1
            iy1_ngp = idint(y1/dy + 0.5) +1
            iz1_ngp = idint(z1/dy + 0.5) +1


            !i0y = y0 - idint(y0/dy)*dy
            ! i0z = z0 - idint(z0/dz)*dz
            ! i1y = y1 - idint(y1/dy)*dy
            ! i1z = z1 - idint(z1/dz)*dz
            ! write(*,*) 'End point:',y1,z1
            ! write(*,*) 'End point:',iy1_ngp,iz1_ngp
            if (debug) then
                write(*,*) 'Inters.',y0,z0
            end if
            ! simple case - both ends in the same NGP area
            if ((iz0_ngp.eq.iz1_ngp).and.(iy0_ngp.eq.iy1_ngp)) then
                move_cell = .false.
            else
                move_cell = .true.

            end if
            !debug
            ! move_cell = .false.
            pass = 0
            do while(move_cell)
                pass = pass +1
                ! write(*,*) 'Pass:',pass
                ! write(*,*) '--------------------'
                ! write(*,*) 'y int',y0,y1
                ! write(*,*) 'z int',z0,z1
                if (pass.gt.400) then
                    write(*,*) 'Particle passed through too many cells!!!'
                    write(*,*) 'Particle:', i, stype(i)
                    write(*,*) 'Pos:',y(i), z(i)
                    write(*,*) 'Vel:',ux(i),uy(i), uz(i)
                    write(*,*) 'Period. shift',pc_shift



                    stop
                end if
                ! particle crossing borders of the NGP area
                ! we need to find the intersection of the tube and NGP area
                if (uy(i) > 0.0) then
                    !tube can hit top, right or the bottom border
                    z_test = (z1 - z0)/(y1 - y0)*(real(iy0_ngp -1)*dy + dy/2.0 - y0) + z0
                    ! write(*,*) 'Testing ',z_test
                    if (z_test.gt.(max(z0,z1) - 1.0E-6).and.z_test.lt.(min(z1,z0) + 1.0E-6)) then
                        ! write(*,*) 'z_test right on the tube'
                    else
                        ! 			 write(*,*) 'Error: z_test off the tube'
                        ! 			 write(*,*) z_test,z0,z1
                        ! 			write(*,*) 'Particle:', i
                        ! 			write(*,*) 'Pos:',y(i), z(i)
                        ! 			write(*,*) 'Vel:',ux(i),uy(i), uz(i)

                    end if
                    !test the border
                    if (z_test.gt.(real(iz0_ngp -1)*dz + dz/2.0)) then
                        !top border
                        ! write(*,*) 'Particle passing through the top border'
                        z_int = real(iz0_ngp -1)*dz + dz/2.0
                        y_int = y0 + (y1 - y0)/(z1 - z0)*(z_int - z0)
                        next_cell(1) = 1
                        next_cell(2) = 0
                    else if (z_test.lt.(real(iz0_ngp -1)*dz - dz/2.0)) then
                        !bottom border
                        ! write(*,*) 'Particle passing through the bottom border'
                        z_int = real(iz0_ngp -1)*dz - dz/2.0
                        y_int = y0 + (y1 - y0)/(z1 - z0)*(z_int - z0)
                        next_cell(1) = -1
                        next_cell(2) = 0
                    else
                        !right border
                        ! write(*,*) 'Particle passing through the right border'
                        y_int = real(iy0_ngp -1)*dy + dy/2
                        z_int = z_test
                        next_cell(1) = 0
                        next_cell(2) = 1
                    end if

                else
                    !uy(1) < 0
                    !tube can hit top, right or the bottom border
                    z_test = (z1 - z0)/(y1 - y0)*(real(iy0_ngp -1)*dy - dyh - y0) + z0
                    if (debug) then
                        write(*,*) 'Testing ',z_test
                    end if
                    if (z_test.gt.(max(z1,z0) - 1.0E-6).and.z_test.lt.(min(z0,z1) + 1.0E-6)) then
                        ! write(*,*) 'z_test right on the tube'
                    else
                        ! 			 write(*,*) 'Error: z_test off the tube'
                        ! 			 write(*,*) z_test,z0,z1
                        ! 			write(*,*) 'Particle:', i
                        ! 			write(*,*) 'Pos:',y(i), z(i)
                        ! 			write(*,*) 'Vel:',ux(i),uy(i), uz(i)

                    end if
                    !test the border
                    if (z_test.gt.(real(iz0_ngp -1)*dz + dzh)) then
                        !top border
                        if (debug) then
                            write(*,*) 'Particle passing through the top border'
                        end if
                        z_int = real(iz0_ngp-1)*dz + dzh
                        y_int = y0 + (y1 - y0)/(z1 - z0)*(z_int - z0)

                        next_cell(1) = 1
                        next_cell(2) = 0
                    else if (z_test.lt.(real(iz0_ngp -1)*dz - dzh)) then
                        !bottom border
                        if (debug) then
                            write(*,*) 'Particle passing through the bottom border'
                        end if
                        z_int = real(iz0_ngp -1)*dz - dzh
                        y_int = y0 + (y1 - y0)/(z1 - z0)*(z_int - z0)
                        next_cell(1) = -1
                        next_cell(2) = 0
                    else
                        !right border
                        if (debug) then
                            write(*,*) 'Particle passing through the left border'
                        end if
                        y_int = real(iy0_ngp -1)*dy - dyh
                        z_int = z_test
                        next_cell(1) = 0
                        next_cell(2) = -1
                    end if

                end if
                if (debug) then
                    write(*,*) 'Inters.',y_int,z_int
                end if
                ! tube length
                l = dsqrt((y0 - y_int)**2 + (z0 - z_int)**2)
                ! write(*,*) 'Tube length in the NGP area',l

                rho_big(stype(i),iz0_ngp,iy0_ngp) = rho_big(stype(i),iz0_ngp,iy0_ngp) + w_ref(stype(i))*l*q(stype(i))&
                    /dt/dsqrt(uy(i)**2 + uz(i)**2)
                !move on to the next cell
                ! 	iy0 = iy0 + next_cell(2)
                ! 	iz0 = iz0 + next_cell(2)
                y0 = y_int
                z0 = z_int
                !little push over the border
                if (abs(next_cell(2)).gt.0) then
                    ! 		write(*,*) "Push y", real(next_cell(2)/abs(next_cell(2)))
                    y0 = y0 + real(next_cell(2)/abs(next_cell(2)))*1.0E-5
                    ! 		write(*,*) "Pushed y", y0

                end if

                if (abs(next_cell(1)).gt.0) then
                    z0 = z0 + real(next_cell(1)/abs(next_cell(1)))*1.0E-6
                end if
                iy0 = idint(y0/dy) +1
                iz0 = idint(z0/dz) +1
                iy0_ngp = idint(y0/dy + 0.5) +1
                ! 	write(*,*) 'NGP y:',iy0_ngp
                iz0_ngp = idint(z0/dy + 0.5) +1
                if ((iz0_ngp.eq.iz1_ngp).and.(iy0_ngp.eq.iy1_ngp)) then
                    move_cell = .false.
                else
                    move_cell = .true.
                end if
                !debug
                !   	move_cell = .false.
            end do
            l = dsqrt((y0 - y1)**2 + (z0 - z1)**2)

            ! give the NGP the rest of charge
            ! write(*,*) 'Good, tube in one NGP area:',iz0_ngp,iy0_ngp
            ! write(*,*) 'y int',y0,y1
            ! write(*,*) 'z int',z0,z1
            if (debug) then
                write(*,*) 'Inters.',y1,z1
            end if
            rho_big(stype(i),iz0_ngp,iy0_ngp) = rho_big(stype(i),iz0_ngp,iy0_ngp) + w_ref(stype(i))*l*q(stype(i))&
                /dt/dsqrt(uy(i)**2 + uz(i)**2)

            ! write(*,*) 'final addition',l*q(stype(i))/dt/dsqrt(uy(i)**2 + uz(i)**2)


        end if !Lz
        if (i.eq.50000*int(i/50000.0)) then
            write(*,*) count,'::: Weight:',i
        end if

    end do !part
    !copy over the outside bits of the charge density
    ! write(*,*) 'Rho summing1',sum(rho_big(1,:,:))
    rho(:,1:Nz,1:Ny) = rho(:,1:Nz,1:Ny) + rho_big(:,1:Nz,1:Ny) +   rho_big(:,1:Nz,(Ny+1):2*Ny ) + rho_big(:,1:Nz,(2*Ny + 1):3*Ny)
    ! rho(:,1:Nz,1:Ny) = rho(:,1:Nz,1:Ny) + rho_big(:,1:Nz,1:Ny)
    ! write(*,*) 'Rho summing2',sum(rho(1,:,:))


    !sum up the rho_tot
    ! do sp=1,no_species
    ! 	rho_tot = rho_tot + rho(sp,1:Nz,1:Ny)
    ! end do
end subroutine


subroutine weight_tube_cic (dz,dy,Lz,Ly,no_part,y,z,uy,uz,rho,Nz,Ny,stype,no_species,q,w,dt,debug)
    implicit none
    real*8:: dz,dy,Lz,Ly,dt
    integer:: no_part,Nz,Ny,sp,no_species,i
    real*8, dimension(no_species,Nz,Ny)::rho
    real*8, dimension(no_species,2*Nz,3*Ny)::rho_big
    real*8, dimension(no_part):: z,y,uz,uy
    integer*2, dimension(no_part):: stype
    real*8, dimension(no_species):: q,w
    ! internal vars
    real*8:: z0,y0,z1,y1,z_int,y_int,t_low,t_high
    real*8:: z_1,z_2,z_3,z_4
    real*8:: y_1,y_2,y_3,y_4
    real*8:: z_test,q_part
    real*8:: A12_low,A12_high,A21_low,A21_high,A22_low,A22_high,A11_low,A11_high
    integer:: iy0,iy1,iz1,iz0
    integer:: pass
    logical:: move_cell,debug
    integer, dimension(2):: next_cell
    !init vars
    rho_big = 0.0
    if (debug) then
        write (*,*) 'Lz,Ly',Lz,Ly
    end if

    do i=1,no_part
        z0 = z(i)
        y0 = y(i)
        z1 = z0 + uz(i)*dt
        y1 = y0 + uy(i)*dt
        !first check that particle is actually in the simulation region
        if ((z0.lt.Lz).or.(z1.lt.Lz)) then
            !if the particle croses periodic boundary at y =0 shift it by Ly
            pass = 0
            do while ((y0.lt.0.0).or.(y1.lt.0.0))
                y0 = y0 + Ly
                y1 = y1 + Ly
                pass = pass +1
            end do
            if (pass.gt.2) then
                write(*,*) 'Particle moving too fast (l > 2*Ly)'
                write(*,*) 'Increase Ly or decrease dt'
                write(*,*) 'Particle:', i, stype(i)
                write(*,*) 'Pos:',y(i), z(i)
                write(*,*) 'Vel:',uy(i), uz(i)
                !critical error
                stop
            end if
            !if particle goes with z bellow 0, cut the trajectory
            if (z1.lt.0.0) then
                y1 = y0 + (y1 - y0)/(z1 - z0)*(0.0 - z0)
                z1 = 0.0 + 1.0E-6
            end if
            !now that we have [z0,y0] and [z1,y1] settled, let's calculate grid coords
            if (debug) then
                write(*,*) 'Inters.',y0,z0
            end if

            iz0 = idint(z0/dz)
            iy0 = idint(y0/dy)
            iz1 = idint(z1/dz)
            iy1 = idint(y1/dy)
            !calculate the lower integral - it's easy = 0
            A11_low = 0.0
            A12_low = 0.0
            A21_low = 0.0
            A22_low = 0.0
            z_1 = real(iz0)*dz
            y_1 = real(iy0)*dz
            z_3 = real(iz0 + 1)*dz
            y_3 = real(iy0 + 1)*dz

            t_low = 0.0
            !check if starting and end point are in the same cell
            if ((iz0.eq.iz1).and.(iy0.eq.iy1)) then
                move_cell = .false.
            else
                move_cell = .true.
            end if
            pass = 0
            ! do weighting in each cell along the particle's trajectory
            do while (move_cell)
                pass = pass + 1
                if (pass.gt.400) then
                    write(*,*) 'Particle crossed too many cells'
                    write(*,*) 'Possible algorithm error'
                    write(*,*) 'Particle:', i, stype(i)
                    write(*,*) 'Pos:',y(i), z(i)
                    write(*,*) 'Vel:',uy(i), uz(i)
                    !critical error
                    ! 				stop
                end if
                !find the intersection with cell border
                if (uy(i) > 0.0) then
                    !tube can hit top, right or the bottom border
                    z_test = (z1 - z0)/(y1 - y0)*(y_3 - y0) + z0
                    ! write(*,*) 'Testing ',z_test
                    !test the border
                    if (z_test.gt.z_3) then
                        !top border
                        ! write(*,*) 'Particle passing through the top border'
                        z_int = z_3
                        y_int = y0 + (y1 - y0)/(z1 - z0)*(z_int - z0)
                        next_cell(1) = 1
                        next_cell(2) = 0
                    else if (z_test.lt.z_1) then
                        !bottom border
                        ! write(*,*) 'Particle passing through the bottom border'
                        z_int = z_1
                        y_int = y0 + (y1 - y0)/(z1 - z0)*(z_int - z0)
                        next_cell(1) = -1
                        next_cell(2) = 0
                    else
                        !right border
                        ! write(*,*) 'Particle passing through the right border'
                        y_int = y_3
                        z_int = z_test
                        next_cell(1) = 0
                        next_cell(2) = 1
                    end if
                else
                    !uy(1) < 0
                    !tube can hit top, right or the bottom border
                    z_test = (z1 - z0)/(y1 - y0)*(y_1 - y0) + z0
                    if (debug) then
                        write(*,*) 'Testing ',z_test
                    end if
                    !test the border
                    if (z_test.gt.z_3) then
                        !top border
                        if (debug) then
                            write(*,*) 'Particle passing through the top border'
                        end if
                        z_int = z_3
                        y_int = y0 + (y1 - y0)/(z1 - z0)*(z_int - z0)
                        next_cell(1) = 1
                        next_cell(2) = 0
                    else if (z_test.lt.z_1) then
                        !bottom border
                        if (debug) then
                            write(*,*) 'Particle passing through the bottom border'
                        end if
                        z_int = z_1
                        y_int = y0 + (y1 - y0)/(z1 - z0)*(z_int - z0)
                        next_cell(1) = -1
                        next_cell(2) = 0
                    else
                        !right border
                        if (debug) then
                            write(*,*) 'Particle passing through the left border'
                        end if
                        y_int = y_1
                        z_int = z_test
                        next_cell(1) = 0
                        next_cell(2) = -1
                    end if

                end if
                if (debug) then
                    write(*,*) 'Inters.',y_int,z_int
                end if

                !calculate the higher integral (using intersection coords)
                t_high = abs((y_int - y0)/uy(i))
                q_part = q(stype(i))*dsqrt((z_int - z0)**2 +(y_int - y0)**2)/dsqrt(uy(i)**2 + uz(i)**2)/dt
                if (debug) then
                    write(*,*) 'this cell gets ', q_part, ' of charge'
                end if

                A11_high =  (z_3 - z0)*(y_3 - y0)*t_high - 0.5*t_high**2*(uy(i)*(z_3 - z0) + uz(i)*(y_3 - y0 )) + t_high**3/3.0*uy(i)*uz(i)
                A12_high = (z0 - z_1)*(y_3 - y0)*t_high - 0.5*t_high**2*(uy(i)*(z0 - z_1) - uz(i)*(y_3 - y0)) - t_high**3/3.0*uz(i)*uy(i)
                A21_high =(y0 - y_1)*(z_3 - z0)*t_high - 0.5*t_high**2*(uz(1)*(y0 - y_1) - uy(i)*(z_3 - z0)) - t_high**3/3.0*uy(i)*uz(i)
                A22_high = (y0 - y_1)*(z0 - z_1)*t_high + 0.5*t_high**2*(uy(i)*(z0 - z_1) + uz(i)*(y0 - y_1)) + t_high**3/3.0*uz(i)*uy(i)
                !add the charge to the corresponding grid points
                if (debug) then
                    write(*,*) 'A11 check::',(A11_high - A11_low)*q_part/(dz*dy*(t_high - t_low))
                end if
                rho_big(stype(i),iz0 +1,iy0 +1) = rho_big(stype(i),iz0 +1,iy0 +1) + w(stype(i))*(A11_high - A11_low)*q_part/(dz*dy*(t_high - &
                    t_low))
                rho_big(stype(i),iz0 +1,iy0 +2) = rho_big(stype(i),iz0 +1,iy0 +2) + w(stype(i))*(A12_high - A12_low)*q_part/(dz*dy*(t_high - &
                    t_low))
                rho_big(stype(i),iz0 +2,iy0 +1) = rho_big(stype(i),iz0 +2,iy0 +1) + w(stype(i))*(A21_high - A21_low)*q_part/(dz*dy*(t_high - &
                    t_low))
                rho_big(stype(i),iz0 +2,iy0 +2) = rho_big(stype(i),iz0 +2,iy0 +2) + w(stype(i))*(A22_high - A22_low)*q_part/(dz*dy*(t_high - &
                    t_low))
                ! move [z0,y0] for the next cell
                y0 = y_int
                z0 = z_int
                !little push over the border
                if (abs(next_cell(2)).gt.0) then
                    y0 = y0 + real(next_cell(2)/abs(next_cell(2)))*1.0E-5
                end if
                if (abs(next_cell(1)).gt.0) then
                    z0 = z0 + real(next_cell(1)/abs(next_cell(1)))*1.0E-6
                end if
                iz0 = idint(z0/dz)
                iy0 = idint(y0/dy)
                !get the new grid coords
                z_1 = real(iz0)*dz
                y_1 = real(iy0)*dz
                z_3 = real(iz0 + 1)*dz
                y_3 = real(iy0 + 1)*dz
                !advance time
                !check if this is the last cell
                if ((iz0.eq.iz1).and.(iy0.eq.iy1)) then
                    move_cell = .false.
                else
                    move_cell = .true.
                end if

            end do !move cell
            !calculate the final upper integral
            t_high = (y1 - y0)/uy(i)
            q_part = q(stype(i))*dsqrt((z1 - z0)**2 +(y1 - y0)**2)/dsqrt(uy(i)**2 + uz(i)**2)/dt
            if (debug) then
                write(*,*) 'this cell gets ', q_part, ' of charge'
            end if

            A11_high =  (z_3 - z0)*(y_3 - y0)*t_high - 0.5*t_high**2*(uy(i)*(z_3 - z0) + uz(i)*(y_3 - y0 )) + &
                t_high**3/3.0*uy(i)*uz(i)
            A12_high =(z0 - z_1)*(y_3 - y0)*t_high - 0.5*t_high**2*(uy(i)*(z0 - z_1) - uz(i)*(y_3 - y0)) - &
                t_high**3/3.0*uz(i)*uy(i)
            A21_high =(y0 - y_1)*(z_3 - z0)*t_high - 0.5*t_high**2*(uz(1)*(y0 - y_1) - uy(i)*(z_3 - z0)) - &
                t_high**3/3.0*uy(i)*uz(i)
            A22_high =(y0 - y_1)*(z0 - z_1)*t_high + 0.5*t_high**2*(uy(i)*(z0 - z_1) + uz(i)*(y0 - y_1)) + &
                t_high**3/3.0*uz(i)*uy(i)
            !add the charge to the corresponding grid points
            rho_big(stype(i),iz0 +1,iy0 +1) = rho_big(stype(i),iz0 +1,iy0 +1) + w(stype(i))*(A11_high - A11_low)*&
                q_part/(dz*dy*(t_high - t_low))
            rho_big(stype(i),iz0 +1,iy0 +2) = rho_big(stype(i),iz0 +1,iy0 +2) + w(stype(i))*(A12_high - A12_low) *&
                q_part/(dz*dy*(t_high - t_low))
            rho_big(stype(i),iz0 +2,iy0 +1) = rho_big(stype(i),iz0 +2,iy0 +1) + w(stype(i))*(A21_high - A21_low) *&
                q_part/(dz*dy*(t_high - t_low))
            rho_big(stype(i),iz0 +2,iy0 +2) = rho_big(stype(i),iz0 +2,iy0 +2) + w(stype(i))*(A22_high - A22_low) *&
                q_part/(dz*dy*(t_high - t_low))


        end if ! z < Lz
        if (i.eq.50000*int(i/50000.0)) then
            ! 		write(*,*) ':::weighting:',i
        end if
    end do
    ! sum up all the charge density (PC)
    rho = rho + rho_big(:,1:Nz,1:Ny) + rho_big(:,1:Nz,Ny:2*Ny -1) + rho_big(:,1:Nz,2*Ny:3*Ny -1 )



end subroutine
subroutine fix_borders(m,no_species,Nz,Ny,Nz1,Ny1,Ny2)
    implicit none
    integer:: Nz,Ny,Nz1,Ny1,Ny2,j,k,no_species
    real*8, dimension(no_species,Nz,Ny):: m
    do k=1,Ny
        m(:,Nz,k)=m(:,Nz,k)*2.d0
        if (k.le.Ny1) then
            m(:,Nz1,k)=m(:,Nz1,k)*2.d0
        elseif (k.gt.Ny1.AND.k.lt.Ny2) then
            m(:,1,k)=m(:,1,k)*2.d0
        elseif (k.ge.Ny2) then
            m(:,Nz1,k)=m(:,Nz1,k)*2.d0
        endif
    enddo
    m(:,1,Ny1)=m(:,1,Ny1)*2.d0
    m(:,1,Ny2)=m(:,1,Ny2)*2.d0
    do j=1,Nz1
        m(:,j,Ny1)=m(:,j,Ny1)*2.d0
        m(:,j,Ny2)=m(:,j,Ny2)*2.d0
    enddo
    do j=Nz1,Nz
        m(:,j,1)=m(:,j,1)*2.d0
        m(:,j,Ny)=m(:,j,Ny)*2.d0
    end do
    m(:,Nz1,Ny1)=m(:,Nz1,Ny1)/3.d0
    m(:,Nz1,Ny2)=m(:,Nz1,Ny2)/3.d0

end subroutine
! subroutine fix_borders_general(m,no_species,Nz,Ny,n,flag)
! implicit none
! integer:: Nz,Ny,no_species,n
! real*8, dimension(no_species,Nz,Ny):: m
! integer, dimension(Nz,Ny):: flag
!
! ! integer, dimension(n - Nz):: flag
! !helping vars
! integer i,j,quadrants,s
! logical:: debug
!
! end subroutine
subroutine make_surface_matrix(Nz,Ny,flag,surface_matrix,debug,Nz_max)
    implicit none
    integer:: Nz,Ny,Nz_max
    integer, dimension(Nz,Ny):: flag
    real*8, dimension(Nz,Ny)::surface_matrix
    ! integer, dimension(n - Nz):: flag
    !helping vars
    integer i,j,quadrants,s
    logical:: debug
    ! debug =.false.
    surface_matrix = 1.0
    do i=2,Nz-2
        do j=2,Ny - 1
            if (flag(i,j).eq.0) then
                !this could be border point
                !count the quandrants in surface
                quadrants = 0
                s = 0
                !first quandrant
                s = flag(i -1,j-1) + flag(i,j-1) +flag(i -1,j)
                if (s.eq.0) then
                    quadrants = quadrants + 1
                end if
                !second quadrant
                s = flag(i,j-1) + flag(i+1,j-1) +flag(i +1,j)
                if (s.eq.0) then
                    quadrants = quadrants + 1
                end if
                ! third quadrant
                s = flag(i+1,j) + flag(i+1,j +1) +flag(i,j+1)
                if (s.eq.0) then
                    quadrants = quadrants + 1
                end if
                !forth quandrant
                s = flag(i,j +1) + flag(i-1,j +1) +flag(i-1,j)
                if (s.eq.0) then
                    quadrants = quadrants + 1
                end if
                ! now decide the additional weighting corrections
                ! periodic fix - we want continuous lines over BC
                if (j.eq.Ny-1.and.quadrants.eq.1) then
                    quadrants = 2
                end if
                if (quadrants.eq.1) then
                    surface_matrix(i,j) = 4.0/3.0
                    if (debug) then
                        ! write(*,*) '4/3 point',i,j
                    end if
                else if (quadrants.eq.2) then
                    surface_matrix(i,j) = 4.0/2.0
                    if (debug) then
                        ! write(*,*) '4/2 point',i,j
                    end if
                else if (quadrants.eq.3) then
                    surface_matrix(i,j) = 4.0/1.0
                    if (debug) then
                        ! write(*,*) '4/1 point',i,j
                    end if
                end if

            end if

        end do
    end do

    if(Nz.eq.Nz_max) then
        surface_matrix(Nz,:) = 2.0
    end if


    ! periodic lines
    i = 1
    do j=2,Ny-2
        quadrants = 0
        s = 0
        !first quandrant
        s = flag(i,j -1) + flag(i+1,j -1) +flag(i +1,j)
        if (s.eq.0) then
            quadrants = quadrants + 1
        end if
        s = 0
        !second q
        s = flag(i,j +1) + flag(i+1,j +1) +flag(i +1,j)
        if (s.eq.0) then
            quadrants = quadrants + 1
        end if
        s = 0
        if (quadrants.eq.1) then
            surface_matrix(i,j) = 4.0/2.0
            if (debug) then
                write(*,*) '4/2 point',i,j
            end if

        end if
    end do

    j = 1
    do i=2,Nz-2
        quadrants = 0
        s = 0
        !first quandrant
        s = flag(i-1,j) + flag(i-1,j +1) +flag(i,j+1)
        if (s.eq.0) then
            quadrants = quadrants + 1
        end if
        s = 0
        !second q
        s = flag(i+1 ,j ) + flag(i+1, j+1) +flag(i,j+1)
        if (s.eq.0) then
            quadrants = quadrants + 1
        end if
        s = 0
        if (quadrants.eq.1) then
            surface_matrix(i,j) = 4.0/2.0
            if (debug) then
                write(*,*) '4/2 point',i,j
            end if

        end if

    end do
    surface_matrix(:,Ny) =surface_matrix(:,1)

    ! Boundaries
    !bottom line - partial checking
    surface_matrix(1,:) = surface_matrix(1,:)*2.0


    surface_matrix(:,1) = surface_matrix(:,1)*2.0

    !bottom line - partial checking
    surface_matrix(:,Ny) =surface_matrix(:,Ny)*2.0

    !surface_matrix(Nz,1) = 4.0
    !surface_matrix(Nz,Ny) = 4.0
    !surface_matrix(1,1) = 4.0
    !surface_matrix(1,Ny) = 4.0


end subroutine

subroutine fix_borders_general(m,no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix)
    implicit none
    integer:: Nz,Ny,no_species,n,y_start,y_stop,sp
    real*8, dimension(no_species,Nz,y_start:y_stop):: m
    real*8, dimension(Nz,y_start:y_stop)::surface_matrix
    integer, dimension(Nz,Ny):: flag

    ! integer, dimension(n - Nz):: flag
    !helping vars
    integer i,j,quadrants,s
    logical:: debug
    debug =.false.
    ! periodic conditions averaging - reduces oscilations?
    !m(:,:,1) = (m(:,:,1) + m(:,:,Ny))/2.0
    if (debug) then
        ! write(*,*) 'Flag: ',n,Nz,n - Nz
    end if
    do sp=1,no_species
        m(sp,:,y_start:y_stop) = m(sp,:,y_start:y_stop)*surface_matrix(:,y_start:y_stop)
    end do
end subroutine

subroutine fix_borders_general_2D(m,no_species,Nz,Ny,n,flag,y_start,y_stop,surface_matrix,Nz_max,z_start_box,z_stop_box)
    implicit none
    integer:: Nz,Ny,no_species,n,y_start,y_stop,sp,Nz_max,z_start_box,z_stop_box
    real*8, dimension(z_start_box:z_stop_box,y_start:y_stop):: m
    real*8, dimension(z_start_box:z_stop_box,y_start:y_stop):: surface_matrix
    integer, dimension(Nz,Ny):: flag

    ! integer, dimension(n - Nz):: flag
    !helping vars
    integer i,j,quadrants,s
    logical:: debug
    debug =.false.
    ! periodic conditions averaging - reduces oscilations?
    !m(:,:,1) = (m(:,:,1) + m(:,:,Ny))/2.0
    if (debug) then
        ! write(*,*) 'Flag: ',n,Nz,n - Nz
    end if
    ! write(*,*) 'Inside fix borders',z_start_box,z_stop_box
    m(z_start_box:z_stop_box,y_start:y_stop) = m(z_start_box:z_stop_box,y_start:y_stop)*surface_matrix(z_start_box:z_stop_box,y_start:y_stop)
    ! for rho matrix do not fix the upper row

end subroutine


subroutine find_edges (Nz,Ny,objects,edges)
    implicit none
    integer:: Nz,Ny,i,j,s,p
    integer, dimension(Nz,Ny):: objects,edges
    !zero the edges
    edges = 0

    ! first do the interion of the area
    do i=2,Nz-1
        do j=2,Ny -1
            s = abs(objects(i -1,j))*abs(objects(i +1,j))*abs(objects(i,j+1))*abs(objects(i,j-1))*abs(objects(i -1,j-1))*&
                abs(objects(i+1,j-1))*abs(objects(i+1,j+1))*abs(objects(i-1,j+1))
            p = objects(i,j)

            if  (s.eq.0.and.p.ne.0) then
                ! we have found an edge
                edges(i,j) = 1
            end if
        end do
    end do
    !bottom line
    i = 1
    do j=2,Ny -1
        s = abs(objects(i +1,j))*abs(objects(i,j+1))*abs(objects(i,j-1))*abs(objects(i+1,j-1))*abs(objects(i+1,j+1))
        p = objects(i,j)

        if  (s.eq.0.and.p.ne.0) then
            ! we have found an edge
            edges(i,j) = 1
        end if

    end do
    !top line
    i = Nz
    do j=2,Ny -1
        s = abs(objects(i -1,j))*abs(objects(i,j+1))*abs(objects(i,j-1))*abs(objects(i -1,j-1))*abs(objects(i-1,j+1))
        p = objects(i,j)

        if  (s.eq.0.and.p.ne.0) then
            ! we have found an edge
            edges(i,j) = 1
        end if

    end do
    ! periodic boundary
    j = 1
    do i = 2,Nz -1
        s  = abs(objects(i+1,j))*abs(objects(i-1,j))
        p = objects(i,j)
        if  (s.eq.0.and.p.ne.0) then
            ! we have found an edge
            edges(i,j) = 1
        end if

    end do
    !copy over
    edges(:,Ny) = edges(:,1)







end subroutine


subroutine add_edge_charge(z,y,uz,uy,q,dz,dy,Nz,Ny,edge,edge_charge,debug,dt)
    implicit none
    real*8:: z,y,uz,uy,dz,dy,zo,yo,zn,yn,dt,A1,A2,q,z_int,y_int,zold,yold
    integer:: Nz,Ny,icount,zd1,zd2,yd1,yd2
    real*8, dimension(Nz,Ny):: edge_charge
    integer,dimension(Nz,Ny):: edge
    logical:: debug,do_loop
    A1 = 0.0
    A2 = 0.0
    zn = z
    yn = y
    z_int = 0.0
    y_int = 0.0
    zd1 = 0
    yd1 = 0
    zd2 = 0
    yd2 = 0
    zo = z - uz*dt
    zold = zo

    yo = y - uy*dt
    yold = yo
    do_loop = .true.
    icount = 0
    do while (do_loop)
        icount = icount +1
        call find_intersection(zo,yo,zn,yn,uz,uy,z_int,y_int,dz,dy,zd1,yd1,zd2,yd2,debug,A1,A2)
        if (int(zo/dz).eq.int(zn/dz).and.int(yo/dy).eq.int(yn/dy)) then
            write(*,*) 'Same cell!!!'
            write(*,*) 'Pos: ',zo,yo
            write(*,*) 'Orig Pos: ',zold,yold
            write(*,*) 'Final Pos: ',zn,yn

            write(*,*) 'Vel: ',uz,uy

            !this happens, all we have to do is to shift the old position a bit backwards
            zo = zo -uz*1E-6
            yo = yo -uy*1E-6
            !		do_loop = .false.
        else
            if (edge(zd1+1,yd1+1).eq.1.and.edge(zd2+1,yd2+1).eq.1) then
                ! good news, we found surface
                do_loop = .false.
            else
                zo = z_int + uz*1E-12
                yo = y_int + uy*1E-12
            end if
        end if
        if (icount.gt.100) then
            ! something is wrong
            write(*,*) 'Too many iterations!!!'
            write(*,*) 'Pos: ',z,y
            write(*,*) 'Orig Pos: ',zo,yo

            write(*,*) 'Vel: ',uz,uy

            do_loop  = .false.

        end if

    end do
    ! add the charge if its on the grid
    ! increment by 1
    zd1 = zd1 +1
    zd2 = zd2 +1
    yd1 = yd1 +1
    yd2 = yd2 +1
    if (zd1.lt.1.and.zd2.lt.1.and.yd1.lt.1.and.yd2.lt.1.and.zd1.gt.Nz.and.zd2.gt.Nz.and.yd1.gt.Ny.and.yd2.gt.Ny) then
        ! something is wrong
        write(*,*) 'charge out of matrix!!!!'
        write(*,*) 'Pos: ',z,y
        write(*,*) 'Vel: ',uz,uy
        stop
    end if
    if(A1.lt.0.0.or.A2.lt.0.0 ) then
        write(*,*) 'Negative weights!!!!'
        write(*,*) 'Pos: ',z,y
        write(*,*) 'Vel: ',uz,uy
        write(*,*) 'A: ',A1,A2

        stop
    end if
    ! add the charge
    edge_charge(zd1,yd1)  =  edge_charge(zd1,yd1) + A1*q
    edge_charge(zd2,yd2)  =  edge_charge(zd2,yd2) + A2*q


end subroutine



subroutine find_intersection (zo,yo,zn,yn,uz,uy,z_int,y_int,dz,dy,zd1,yd1,zd2,yd2,debug,A1,A2)
    implicit none
    real*8:: zo,yo,zn,yn,uz,uy,z_int,y_int,dz,dy,z_grid,y_grid,A1,A2
    integer:: zd1,yd1,zd2,yd2
    logical:: debug
    if (uz.eq.0.0.or.uy.eq.0.0) then
        ! error -  we can't handle this one for the moment
        write(*,*) 'Error edge charge calculation'
        write(*,*) 'Pos:', zo,yo

        write(*,*) 'Vel:', uz,uy
        stop
    end if
    if (uy.lt.0.0) then
        y_grid = int(yo/dy)*dy
    else
        y_grid = real(int(yo/dy)+1)*dy
    end if
    if (debug) then
        write(*,*) 'y_grid:', y_grid
    end if
    z_grid = (y_grid - yo)*(zn - zo)/(yn - yo) + zo
    if (debug) then
        write(*,*) 'z_grid:', z_grid
    end if

    if (int(z_grid/dz).eq.int(zo/dz)) then
        ! fall/thrust
        z_int = z_grid
        y_int = y_grid
        zd1 = int(zo/dz)
        zd2 = zd1 +1
        yd1 = int((y_grid + 1E-6)/dy) ! little extra thing to get the right rounding
        yd2 = yd1
        A1 = abs(z_int - zd2*dz)/dz
        A2 = abs(z_int - zd1*dz)/dz


    else if (int(z_grid/dz).gt.int(zo/dz)) then
        ! go right
        z_int = real(int(zo/dz) + 1)*dz
        y_int = (yn - yo)/(zn - zo)*(z_int - zo) + yo
        zd1 = int(zo/dz) + 1
        zd2 = zd1
        yd1 = int(yo/dy)
        yd2 = yd1 + 1
        A1 = abs(y_int - yd2*dy)/dy
        A2 = abs(y_int - yd1*dy)/dy

    else if (int(z_grid/dz).lt.int(zo/dz)) then
        ! go left
        z_int = int(zo/dz)*dz
        y_int = (yn - yo)/(zn - zo)*(z_int - zo) + yo
        zd1 = int(zo/dz)
        zd2 = zd1
        yd1 = int(yo/dy)
        yd2 = yd1 + 1
        A1 = abs(y_int - yd2*dy)/dy
        A2 = abs(y_int - yd1*dy)/dy


    end if
end subroutine


subroutine smooth_edge_charge(Nz,Ny,edge_charge,edges)
    implicit none
    ! simple routine to smooth the edge charge and avoid sawtooth profile formation
    ! the filter needs to be rather a soft one in order to avoid charge leakages
    ! better idea- don't apply every timestep
    integer:: Nz,Ny,i,j
    real*8:: s1,s2
    real*8, dimension(Nz,Ny):: edge_charge, new_edge_charge
    integer, dimension(Nz,Ny):: edges
    new_edge_charge = 0.0
    s1 = sum(edge_charge)
    do i=2,Nz-1
        do j=2,Ny-1
            new_edge_charge(i,j) = edges(i,j)*(0.5*edge_charge(i,j) + 0.25*edge_charge(i+1,j)+ 0.25*edge_charge(i-1,j)+ &
                0.25*edge_charge(i,j+1)+ 0.25*edge_charge(i,j-1))
        end do
    end do
    s2 = sum(new_edge_charge)
    if (abs(s1 - s1).gt.1E-3) then
        write(*,*) 'Edge charge conservation violated!!!',s1,s2
    end if
    edge_charge  =new_edge_charge
end subroutine


subroutine build_count_matrix(Nz,anp,iy1,iz1,max_cell_count,tot_cell_count,cell_count)
    implicit none
    integer:: anp,tot_cell_count,max_cell_count,Ny,y,Nz,z
    integer, dimension(Nz):: cell_count
    integer, dimension(anp):: iz1,iy1
    integer:: i,j
    cell_count = 0
    do i=1,anp
        z = iz1(i)
        cell_count(z) = cell_count(z) + 1
        tot_cell_count = tot_cell_count +1
        if (cell_count(z).gt.max_cell_count) then
            max_cell_count = cell_count(z)
        end if
    end do


end subroutine



subroutine sort_particles(anp,y,z,ux,uy,uz,vtot_init,use_vtot,max_cell_count,Nz,cell_count,tot_cell_count,iz1,iy1,stype,no_species,no_diag_reg, particle_no)
    implicit none
    integer:: anp,max_cell_count,Nz,tot_cell_count,no_species,sp,Ny,no_diag_reg
    real*8, dimension(anp):: y,z,ux,uy,uz,tmp,vtot_init
    integer, dimension(anp):: iy1,iz1,key
    integer, dimension(Nz,max_cell_count):: sort_matrix
    integer, dimension(Nz):: cell_count,cell_tmp
    integer*2, dimension(anp):: stype
    integer, dimension(no_diag_reg):: particle_no
    integer:: i,j,k, k_max,a,pos,iy,vac_pos,iz
    logical:: use_vtot
    cell_tmp = 0
    sort_matrix = 0
    key = 0
    ! build sort matrix
    do sp=1,no_species
        do i=1,anp
            if (stype(i).eq.sp) then
                iz = iz1(i)
                k = cell_tmp(iz) +1
                cell_tmp(iz)  = k
                sort_matrix(iz,k) = i
            end if
        end do
    end do
    ! sort the particles according to the sort matrix
    ! build the key
    pos = 1
    do j=1,Nz
        do a=1,cell_tmp(j)
            key(pos) = sort_matrix(j,a)
            pos = pos +1
        end do
    end do
    ! copy over vectors
    do i=1,anp
        tmp(i) = z(key(i))
    end do
    z = tmp

    do i=1,anp
        tmp(i) = y(key(i))
    end do
    y = tmp

    do i=1,anp
        tmp(i) = ux(key(i))
    end do
    ux = tmp

    do i=1,anp
        tmp(i) = uy(key(i))
    end do
    uy = tmp

    do i=1,anp
        tmp(i) = uz(key(i))
    end do
    uz = tmp
    if (use_vtot) then
        do i=1,anp
            tmp(i) = vtot_init(key(i))
        end do
        vtot_init = tmp
    end if

    do i=1,anp
        tmp(i) = stype(key(i))
    end do
    stype = tmp
    ! switch test particles
    do i=1,no_diag_reg
        if (particle_no(i).gt.0) then
            particle_no(i) = key(particle_no(i))
        end if
    end do

end subroutine


subroutine construct_bc_matrix(Nz,Ny,objects,bc_matrix)
    implicit none
    integer:: Nz,Ny,i,j,p
    integer, dimension(Nz,Ny):: objects,bc_matrix
    bc_matrix= 0
    do j=1,Nz-1
        do i=1,Ny-1
            p = objects(j,i)*objects(j,i+1)*objects(j+1,i+1)*objects(j+1,i)
            bc_matrix(j,i) = p
        end do
    end do
    ! Periodic BC
    bc_matrix(:,Ny) = bc_matrix(:,1)
end subroutine


subroutine find_bc_check_limits(Nz,Ny,objects,dy,dz,Lz_low_limit,Lz_high_limit)
    implicit none
    integer:: Nz,Ny,Nz_low_limit,Nz_high_limit,z,y,found_object
    real*8:: dz,dy,Lz_low_limit,Lz_high_limit
    integer, dimension(Nz,Ny):: objects
    Nz_low_limit = 1
    Nz_high_limit = Nz

    found_object = 0
    do z=1,Nz
        if (sum(objects(z,:)).eq.0.and.found_object.eq.0) then
            Nz_low_limit = z
        else
            found_object = 1
        end if
    end do

    found_object = 0

    do z=Nz,1,-1
        if (sum(objects(z,:)).eq.0.and.found_object.eq.0) then
            Nz_high_limit = z
        else
            found_object = 1
        end if
    end do

    ! safety check
    Nz_low_limit = max(Nz_low_limit - 3,1)
    Nz_high_limit = min(Nz_high_limit + 3,Nz)

    Lz_low_limit = float(Nz_low_limit - 1)*dz
    Lz_high_limit = float(Nz_high_limit -1)*dz

    write(*,*) 'Determined BC limits:',Lz_low_limit,Lz_high_limit

end subroutine


! subroutine to compute temperature - check for numerical heating
subroutine te_diag(no_part,y,z,ux,uy,uz,Nz,Ny,vx,vy,vz,no_species,stype,temperature,iy1,iz1,dz,dy)
    implicit none
    integer:: count,Nz,Ny,Nz_max,no_species,sp,i,no_part
    real*8 :: y_stop_large,z_start,z_stop,z_start_box,z_stop_box,vx_mean,vy_mean,vz_mean,v_mean,v_act,temp,dz,dy
    real*8, dimension(no_part):: y,z,ux,uy,uz
    integer*2, dimension(no_part):: stype
    real*8, dimension(no_species,Nz,Ny):: vx,vy,vz,temperature
    integer, dimension(no_part):: iz1,iy1
    real*8:: iz,iy,A11x,A12x,A21x,A22x
    ! reset the field
    temperature = 0.0

    do i=1,no_part
        iz1(i)=idint(z(i)/dz) + 1
        iz= z(i)/dz - iz1(i) +1

        iy1(i)=idint(y(i)/dy) + 1
        iy =y(i)/dy - iy1(i) + 1
        !write(*,*) 'Iz1',iz1(i)
        if (iz1(i).lt.Nz) then
            sp = stype(i)
            A11x=(1.d0-iz)*(1.d0-iy)
            A12x=(1.d0-iz)*iy
            A21x=iz*(1.d0-iy)
            A22x=iz*iy
            ! get the local mean velocity
            vx_mean = A11x*vx(sp,iz1(i),iy1(i)) + A12x*vx(sp,iz1(i),iy1(i) + 1) + A21x*vx(sp,iz1(i) +1,iy1(i)) + A22x*vx(sp,iz1(i)+1,iy1(i) +1)
            vy_mean = A11x*vy(sp,iz1(i),iy1(i)) + A12x*vy(sp,iz1(i),iy1(i) + 1) + A21x*vy(sp,iz1(i) +1,iy1(i)) + A22x*vy(sp,iz1(i)+1,iy1(i) +1)
            vz_mean = A11x*vz(sp,iz1(i),iy1(i)) + A12x*vz(sp,iz1(i),iy1(i) + 1) + A21x*vz(sp,iz1(i) +1,iy1(i)) + A22x*vz(sp,iz1(i)+1,iy1(i) +1)
            v_mean = sqrt(vx_mean**2 + vy_mean**2  + vz_mean**2)
            ! actual total velocity
            v_act = sqrt(ux(i)**2 + uy(i)**2  + uz(i)**2)
            temp = (v_act - v_mean)**2
            ! distribute it to the nodes
            temperature(sp,iz1(i),iy1(i)) = temperature(sp,iz1(i),iy1(i)) + temp*A11x
            temperature(sp,iz1(i)+1,iy1(i)) = temperature(sp,iz1(i)+1,iy1(i)) + temp*A21x
            temperature(sp,iz1(i),iy1(i)+1) = temperature(sp,iz1(i),iy1(i)+1) + temp*A12x
            temperature(sp,iz1(i)+1,iy1(i)+1) = temperature(sp,iz1(i)+1,iy1(i)+1) + temp*A22x
        end if
    end do
    !write(*,*) 'Temperatue csum',sum(temperature(1,:,:)),sum(temperature(2,:,:))

end subroutine

subroutine set_floating_objects_pot(Nz,Ny,Pot,equipot_m,objects_enum,rectangle_params,circle_params,triangle_params,rectangles,circles,triangles)
    use spice2_types
    implicit none

    integer:: Nz,Ny,rectangles,circles,triangles,i,j,k,no,otype,nr,sp
    integer, dimension(Nz,Ny):: objects_enum
    real*8, dimension(Nz,Ny):: Pot,equipot_m
    type(rectangle_t), dimension(rectangles):: rectangle_params
    type(circle_t), dimension(circles):: circle_params
    type(triangle_t), dimension(triangles):: triangle_params

    ! loop over the Pot grid and look for floating objects
    do i=1,Ny
        do j=1,Nz
            no = objects_enum(j,i)
            if (no.gt.0) then
                if (no.gt.(rectangles + circles)) then
                    ! check for triangles
                    nr = no  - (rectangles + circles)
                    if (triangle_params(nr)%param1.eq.2) then
                        triangle_params(nr)%Pot = Pot(j,i)
                    end if
                elseif (no.gt.rectangles) then
                    nr = no  - rectangles
                    if (circle_params(nr)%param1.eq.2) then
                        circle_params(nr)%Pot = Pot(j,i)
                    end if
                else
                    if (rectangle_params(no)%param1.eq.2) then
                        rectangle_params(no)%Pot = Pot(j,i)
                    end if

                end if
            end if
        end do
    end do
    ! now update the equpot matrix
    ! rectangles
    do sp=1,rectangles
        if (rectangle_params(sp)%param1.eq.2) then
            write(*,*) 'Updating rectangle floating potential',rectangle_params(sp)%Pot,sp
            do i=1,Nz
                do j=1,Ny
                    if (objects_enum(i,j).eq.sp) then
                        equipot_m(i,j) = rectangle_params(sp)%Pot
                    end if
                end do
            end do
        end if
    end do
    ! circles
    do sp=1,circles
        if (circle_params(sp)%param1.eq.2) then
            write(*,*) 'Updating circle floating potential',circle_params(sp)%Pot,sp
            do i=1,Nz
                do j=1,Ny
                    if (objects_enum(i,j).eq.sp) then
                        equipot_m(i,j) = circle_params(sp)%Pot
                    end if
                end do
            end do
        end if
    end do
    ! triangles
    do sp=1,triangles
        if (triangle_params(sp)%param1.eq.2) then
            write(*,*) 'Updating triangle floating potential',triangle_params(sp)%Pot,sp
            do i=1,Nz
                do j=1,Ny
                    if (objects_enum(i,j).eq.sp) then
                        equipot_m(i,j) = triangle_params(sp)%Pot
                    end if
                end do
            end do
        end if
    end do


end

subroutine find_surface_orientation(Nz,Ny,objects,orientation)
    implicit none
    integer:: iz,iy,Nz,Ny
    integer, dimension(Nz,Ny):: objects,orientation
    integer, dimension(0:(Nz+1),0:(Ny+2)):: bobjects

    bobjects(1:Nz,1:Ny) = objects
    bobjects(1:Nz,0) = objects(1:Nz,Ny)
    bobjects(1:Nz,Ny+1) = objects(1:Nz,1)
    bobjects(1:Nz,Ny+2) = objects(1:Nz,2)
    bobjects(1:Nz,0) = objects(1:Nz,Ny)

    bobjects(1,:) = 1
    bobjects(Nz+1,:) = 0


    orientation = 0
    orientation(1,:) = 1
    orientation(Nz,:) = 0

    do iz=2,(Nz-1)
        do iy=1,Ny
            if (objects(iz,iy).gt.0) then
                ! surface facing up
                ! special cases first

                if (bobjects(iz+1,iy).gt.0.and.bobjects(iz+1,iy+1).gt.0.and.(bobjects(iz+2,iy).eq.0.or.bobjects(iz+2,iy+1).eq.0)) then
                    orientation(iz,iy)  = 1

                    !    if ((iy+2).le.Ny) then
                    if (bobjects(iz+1,iy+2).eq.0) then
                        orientation(iz,iy)  = 5
                        write(*,*) 'Orientation 5',iz,iy
                    end if
                    !  end if
                    if ((iy-1).gt.0) then
                        if (bobjects(iz+1,iy-1).eq.0) then
                            orientation(iz,iy)  = 6
                            write(*,*) 'Orientation 6',iz,iy
                        end if
                    end if

                    ! surface facing down
                elseif ((bobjects(iz-1,iy).eq.0.or.bobjects(iz-1,iy+1)).eq.0.and.bobjects(iz,iy).gt.0.and.bobjects(iz,iy+1).gt.0) then
                    orientation(iz,iy) = 2
                    !  if ((iz+2).lt.Nz) then
                    if (bobjects(iz,iy+2).eq.0) then
                        orientation(iz,iy)  = 7
                        write(*,*) 'Orientation 7',iz,iy

                    end if
                    ! end if
                    if ((iy-1).gt.0) then
                        if (bobjects(iz,iy-1).eq.0) then
                            orientation(iz,iy)  = 8
                            write(*,*) 'Orientation 8',iz,iy

                        end if
                    end if

                    ! surface facing left
                elseif (bobjects(iz,iy+1).gt.0.and.bobjects(iz+1,iy+1).gt.0.and.(bobjects(iz+1,iy+2).eq.0.or.bobjects(iz,iy+2).eq.0)) then
                    orientation(iz,iy) = 3
                elseif (bobjects(iz,iy).gt.0.and.bobjects(iz+1,iy).gt.0.and.(bobjects(iz,iy-1).eq.0.or.bobjects(iz+1,iy-1).eq.0)) then
                    orientation(iz,iy) = 4

                    ! corner  - no emission, just keeping
                elseif(bobjects(iz+1,iy).gt.0.and.bobjects(iz+1,iy+1).gt.0.and.bobjects(iz+2,iy).gt.0.and.bobjects(iz+2,iy+1).gt.0.and.bobjects(iz+1,iy+2).gt.0..and.bobjects(iz+2,iy+2).eq.0.) then
                    orientation(iz,iy) = 9
                elseif(bobjects(iz+1,iy).gt.0.and.bobjects(iz+1,iy+1).gt.0.and.bobjects(iz+2,iy).gt.0.and.bobjects(iz+2,iy+1).gt.0.and.bobjects(iz+1,iy-1).gt.0..and.bobjects(iz+2,iy-1).gt.0.) then
                    orientation(iz,iy) = 10



                end if


            end if
        end do
    end do

end subroutine

