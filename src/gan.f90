!                       GAN (GAN Ain't NAG)
! This is poor man's NAG
! Free implementation of some of the NAG routines, which are used in the SPICE2 code.
! Note: NAG routines are much more sophisticated and also well-tested
! if you can, use NAG and sleep well.
! The low-level routines are taken from Numerical Recipes
! Written by Michael Komm, 06/10

! returns real random number between 0 and 1
! call with idum < 0 for initialization
function ran(idum)
    implicit none
    integer, parameter::k4b=selected_int_kind(9)
    integer(k4b), intent(INOUT)::idum
    real*8:: ran
    integer(k4b), parameter:: IA=16807, IM=2147483647, IQ=127773, IR=2836
    real*8, save:: am
    integer(k4b), save:: ix=-1,iy=-1,k
    if (idum <= 0 .or. iy < 0) then
        am = nearest(1.0, -1.0)/IM
        iy = ior(ieor(888889999,abs(idum)),1)
        ix = ieor(777755555, abs(idum))
        idum = abs(idum) + 1
    end if
    ix = ieor(ix,ishft(ix,13))
    ix = ieor(ix,ishft(ix,-17))
    ix = ieor(ix,ishft(ix,5))
    k = iy/IQ
    iy = IA*(iy - k*IQ) - IR*k
    if (iy < 0) iy=iy + IM
    ran = am*ior(iand(IM,ieor(ix,iy)),1)

end function ran


subroutine gauss(harvest)
    implicit none
    real*8, intent(out):: harvest
    real*8:: rsq, v1,v2
    real*8, save:: g
    integer, parameter::k4b=selected_int_kind(9)

    integer(k4b), save::idum

    logical, save:: gaus_stored=.false.
    if (gaus_stored) then
        harvest = g
        gaus_stored = .false.
    else
        do
            v1 = ran(idum)
            v2 = ran(idum)
            v1 = 2.0*v1 - 1.0
            v2 = 2.0*v2 - 1.0
            rsq = v1**2 + v2**2
            if (rsq > 0 .and. rsq < 1.0) exit
        end do
        rsq = sqrt(-2.0*log(rsq)/rsq)
        harvest = v1*rsq
        g = v2*rsq
        gaus_stored = .true.
    end if
end subroutine gauss

! GAN implementation of G05CCF
! This routine initializes the generator

subroutine G05CCF
    implicit none
    integer, parameter::k4b=selected_int_kind(9)
    integer(k4b), save::idum
    real*8:: ret
    idum = -1
    ret  = ran(idum)
end subroutine

! GAN implementation of G05CAF
! returns uniformly distributed number between A and B
! B should be bigger than A

subroutine G05CAF(a,b,r)
    implicit none
    real*8:: a,b,r
    integer, parameter::k4b=selected_int_kind(9)
    integer(k4b), save::idum
    r = ran(idum)
    r = r*(b-a) + a
end subroutine

! GAN implementation of G05DDF
! returns gaussian distributed number with mean A and std. dev B
! B should be positive

subroutine G05DDF(a,b,r)
    implicit none
    real*8:: a,b,r
    call gauss(r)
    r = r*B + A
end subroutine

! GAN implementation of G05FAF
! returns a vector of uniformly distributed N numbers between A and B
! B should be bigger than A
! N should be positive

subroutine G05FAF(a,b,n,r)
    implicit none
    real*8:: a,b,rs
    integer:: n,i
    real*8, dimension(n):: r
    do i=1,n
        call G05CAF(a,b,r(i))
    end do
end subroutine

! GAN implementation of G05FDF
! returns a vector of normal distributed N numbers with mean A and std. dev. B
! B should be positive
! N should be positive

subroutine G05FDF(a,b,n,r)
    implicit none
    real*8:: a,b,rs
    integer:: n,i
    real*8,dimension(n):: r
    do i=1,n
        call G05DDF(a,b,r(i))
    end do
end subroutine



