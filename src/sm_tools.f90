module sm_tools

	use ap_utils
    use sorting
    use mpi

    implicit none

    contains

    subroutine sm_count(nZ, nY, flagMatrix, elementCount)

        implicit none

        integer, intent(in) :: nZ, nY
        integer, dimension(nZ, nY), intent(in) :: flagMatrix
        integer, intent(out) :: elementCount

        integer :: z, y, flagCount

        elementCount = 0
        flagCount = 0

        do z = 1, nZ
            do y = 1, nY
                call sm_flagCount(flagMatrix(z, y), flagCount)
                elementCount = elementCount + flagCount
            enddo
        enddo

    end subroutine sm_count

    subroutine sm_countdist(zMin, zMax, yMin, yMax, flagMatrixLoc, elementCount)

        implicit none

        integer, intent(in) :: zMin, zMax, yMin, yMax
        integer, dimension(zMin:zMax, yMin:yMax), intent(in) :: flagMatrixLoc
        integer, intent(out) :: elementCount

        integer :: z, y, flagCount

        elementCount = 0
        flagCount = 0

        do z = zMin, zMax
            do y = yMin, yMax
                call sm_flagCount(flagMatrixLoc(z, y), flagCount)
                elementCount = elementCount + flagCount
            enddo
        enddo

    end subroutine sm_countdist

    subroutine sm_countdist2(nZ, nY, zMin, zMax, yMin, yMax, flagMatrix, elementCount)

        implicit none

        integer, intent(in) :: nZ, nY, zMin, zMax, yMin, yMax
        integer, dimension(nZ, nY), intent(in) :: flagMatrix
        integer, intent(out) :: elementCount

        integer :: z, y, flagCount

        elementCount = 0
        flagCount = 0

        do z = zMin, zMax
            do y = yMin, yMax
                call sm_flagCount(flagMatrix(z, y), flagCount)
                elementCount = elementCount + flagCount
            enddo
        enddo

    end subroutine sm_countdist2

    subroutine sm_flagCount(flag, flagCount)
        implicit none

        integer, intent(in) :: flag
        integer, intent(out) :: flagCount

        flagCount = 0

        if (flag == 0) then
            flagCount = 1
        else if (flag == 1) then
            flagCount = 5
        else if (flag == 2 .or. flag == 3) then
            flagCount = 4
        else
            flagCount = 5
        endif

    end subroutine sm_flagCount

    subroutine sm_resetflags(nZ, nY, flagMatrix)

        implicit none

        integer, intent(in) :: nZ, nY
        integer, dimension(nZ, nY), intent(inout) :: flagMatrix

        integer :: z, y

        do z = 1, nZ
            do y = 1, nY
                if (flagMatrix(z, y) .ne. 0) then
                    flagMatrix(z, y) = 1
                endif
            enddo
        enddo

    end subroutine sm_resetflags

    subroutine sm_ijx2csr(elementCount, nRows, Ai, Aj, Ax, Bi, Bp, Bx)

        implicit none

        integer, intent(in) :: elementCount, nRows
        integer, dimension(elementCount), intent(in) :: Ai, Aj
        real*8, dimension(elementCount), intent(in) :: Ax

        integer, dimension(elementCount), intent(out) :: Bi
        integer, dimension(nRows + 1), intent(out) :: Bp
        real*8, dimension(elementCount), intent(out) :: Bx

        integer :: i, previousRow, rowIndex
        integer, dimension(elementCount, 3) :: A

        A = 0;

        do i = 1, elementCount
            A(i, 1) = Ai(i);
            A(i, 2) = Aj(i);
            A(i, 3) = i;
        enddo

        Bi = 0
        Bp = 0
        Bx = 0.0

        Bp(1) = 1

        if (elementCount == 1) then
            return
        endif

        call mergesort_int(elementCount, 3, A, 2, (/1, 2/), 1)

        previousRow = A(1, 1);

        do i = 2, elementCount
            if (A(i, 1) .ne. previousRow) then
                rowIndex = rowIndex + 1
                Bp(rowIndex) = i - 1
                previousRow = A(i, 1);
            endif
        enddo

        Bp(nRows + 1) = elementCount + 1
        Bi = A(:,1);
        Bx = Ax(A(:,3));

    end subroutine sm_ijx2csr


    subroutine sm_create(nZ, nY, nZ_disp, flagMatrix, elementCount, dZ, dY, Ai, Aj, Ax)
        implicit none

        !integer sm_linearIndex

        integer, intent(in) :: nZ, nY, nZ_disp
        integer, dimension(nZ, nY), intent(in) :: flagMatrix
        integer, intent(in) :: elementCount
        real*8, intent(in) :: dY, dZ

        integer, dimension(elementCount), intent(out) :: Ai, Aj
        real*8, dimension(elementCount), intent(out) :: Ax

        integer :: z, y, i
        integer :: left, top, right, bottom, center
        real*8 :: hzy, hzySquared, centerValue, sideValue, sideValue2

        hzy = dZ / dY
        hzySquared = hzy * hzy
        i = 1

        centerValue = 2.0 * (1.0 + hzySquared)
        sideValue = -1.0 * hzySquared
        sideValue2 = - 2.0 * hzySquared


        Ai = 0
        Aj = 0
        Ax = 0.0

        do z = 1, nZ
            do y = 1, nY

                call sm_setijx(nZ, nY, nZ_disp, centerValue, sideValue, sideValue2, z, y, flagMatrix(z, y), elementCount, Ai, Aj, Ax, i)

            enddo
        enddo
    end subroutine sm_create

    subroutine sm_sliceflag(nZ, nY, commsize, zMinAll, zMaxAll, yMinAll, yMaxAll)
        implicit none
        integer, intent(in) :: nZ, nY, commsize
        integer, dimension(0:commsize - 1), intent(out) :: zMinAll, zMaxAll, yMinAll, yMaxAll

        integer :: rankindex, delta, z, y

        ! we do the slicing only one direction
        zMinAll = 1
        zMaxAll = nZ


        ! and now slice other
        delta = nY / commsize

        y = 0

        do rankindex = 0, commsize - 1

            yMinAll(rankindex) = y + 1
            yMaxAll(rankindex) = y + delta

            y = y + delta

        enddo

        yMaxAll(commsize - 1) = nY

    end subroutine sm_sliceflag

    subroutine sm_distint2d(nZ, nY, int2d, MPI_COMM, mpirank, transactionId, commsize, zMin, zMax, yMin, yMax, int2dLoc)

        implicit none

        integer, intent(in) :: nZ, nY
        integer, dimension(nZ, nY), intent(in) :: int2d
        integer, intent(in) :: MPI_COMM, mpirank, commsize, transactionId
        integer, dimension(0:commsize - 1), intent(in) :: zMin, zMax, yMin, yMax

        integer, dimension(zMin(mpirank):zMax(mpirank), yMin(mpirank):yMax(mpirank)), intent(inout) :: int2dLoc

        integer :: ierr
        integer, dimension(MPI_STATUS_SIZE) :: mpistatus
        integer :: dimZ, dimY, elementCount
        integer :: targetRank, mpiId

        if (mpirank == 0) then
            do targetRank = 1, commsize - 1
                dimZ = zMax(targetRank) - zMin(targetRank) + 1
                dimY = yMax(targetRank) - yMin(targetRank) + 1
                elementCount = dimZ * dimY
                call MPI_SEND(int2d(zMin(targetRank):zMax(targetRank), yMin(targetRank):yMax(targetRank)), elementCount, MPI_INTEGER, targetRank, transactionId, MPI_COMM, ierr)
            enddo
            int2dLoc(zMin(0):zMax(0), yMin(0):yMax(0)) = int2d(zMin(0):zMax(0), yMin(0):yMax(0))
        else
            dimZ = zMax(mpirank) - zMin(mpirank) + 1
            dimY = yMax(mpirank) - yMin(mpirank) + 1
            elementCount = dimZ * dimY

            call MPI_RECV(int2dLoc(zMin(mpirank):zMax(mpirank), yMin(mpirank):yMax(mpirank)), elementCount, MPI_INTEGER, 0, transactionId, MPI_COMM, mpistatus, ierr)

        endif

    end subroutine sm_distint2d

    subroutine sm_distreal2d(nZ, nY, real2d, MPI_COMM, mpirank, transactionId, commsize, zMin, zMax, yMin, yMax, real2dLoc)

        implicit none

        integer, intent(in) :: nZ, nY
        real*8, dimension(nZ, nY), intent(in) :: real2d
        integer, intent(in) :: MPI_COMM, mpirank, commsize, transactionId
        integer, dimension(0:commsize - 1), intent(in) :: zMin, zMax, yMin, yMax

        real*8, dimension(zMin(mpirank):zMax(mpirank), yMin(mpirank):yMax(mpirank)), intent(inout) :: real2dLoc

        integer :: ierr
        integer, dimension(MPI_STATUS_SIZE) :: mpistatus
        integer :: dimZ, dimY, elementCount
        integer :: targetRank, mpiId

        if (mpirank == 0) then
            do targetRank = 1, commsize - 1
                dimZ = zMax(targetRank) - zMin(targetRank) + 1
                dimY = yMax(targetRank) - yMin(targetRank) + 1
                elementCount = dimZ * dimY
                call MPI_SEND(real2d(zMin(targetRank):zMax(targetRank), yMin(targetRank):yMax(targetRank)), elementCount, MPI_DOUBLE_PRECISION, targetRank, transactionId, MPI_COMM, ierr)
            enddo
            real2dLoc(zMin(0):zMax(0), yMin(0):yMax(0)) = real2d(zMin(0):zMax(0), yMin(0):yMax(0))
        else
            dimZ = zMax(mpirank) - zMin(mpirank) + 1
            dimY = yMax(mpirank) - yMin(mpirank) + 1
            elementCount = dimZ * dimY

            call MPI_RECV(real2dLoc(zMin(mpirank):zMax(mpirank), yMin(mpirank):yMax(mpirank)), elementCount, MPI_DOUBLE_PRECISION, 0, transactionId, MPI_COMM, mpistatus, ierr)

        endif

    end subroutine sm_distreal2d

    subroutine sm_createdist(nZ, nY, nZ_disp, zMin, zMax, yMin, yMax, flagMatrixLoc, dZ, dY, elementCount, Ai, Aj, Ax)
        implicit none

        !integer sm_linearIndex

        integer, intent(in) :: nZ, nY, nZ_disp, zMin, zMax, yMin, yMax
        integer, dimension(zMin:zMax, yMin:yMax), intent(in) :: flagMatrixLoc
        real*8, intent(in) :: dY, dZ
        integer, intent(in) :: elementCount

        integer, dimension(elementCount), intent(out) :: Ai, Aj
        real*8, dimension(elementCount), intent(out) :: Ax

        integer :: z, y, i
        integer :: left, top, right, bottom, center
        real*8 :: hzy, hzySquared, centerValue, sideValue, sideValue2

        hzy = dZ / dY
        hzySquared = hzy * hzy
        i = 1

        centerValue = 2.0 * (1.0 + hzySquared)
        sideValue = -1.0 * hzySquared
        sideValue2 = - 2.0 * hzySquared


        Ai = 0
        Aj = 0
        Ax = 0.0

        do z = zMin, zMax
            do y = yMin, yMax

                call sm_setijx(nZ, nY, nZ_disp, centerValue, sideValue, sideValue2, z, y, flagMatrixLoc(z, y), elementCount, Ai, Aj, Ax, i)

            enddo
        enddo
    end subroutine sm_createdist

    subroutine sm_setijx(nZ, nY, nZ_disp, centerValue, sideValue, sideValue2, z, y, flagValue, elementCount, Ai, Aj, Ax, i)

        implicit none

        integer, intent(in) :: nZ, nY, nZ_disp
        real*8 :: centerValue, sideValue, sideValue2

        integer, intent(in) :: z, y, flagValue

        integer, intent(in) :: elementCount
        integer, dimension(elementCount), intent(inout) :: Ai, Aj
        real*8, dimension(elementCount), intent(inout) :: Ax
        integer, intent(inout) :: i

        integer :: left, top, right, bottom, center

        center = sm_linearIndex(nZ, nY, z, y)

        if (flagValue > 0) then

            bottom = sm_linearIndex(nZ, nY, z - 1, y)
            top = sm_linearIndex(nZ, nY, z + 1, y)
            left = sm_linearIndex(nZ, nY, z, y - 1)
            right = sm_linearIndex(nZ, nY, z, y + 1)

            if ((nZ_disp .gt. 0).and.(y == 1).and.(z .gt. nZ_disp)) then

                left = sm_linearIndex(nZ, nY, z - nZ_disp, y - 1)

            else if ((nZ_disp .gt. 0).and.(y == nY).and.(z .lt. nZ - nZ_disp)) then

                right = sm_linearIndex(nZ, nY, z + nZ_disp, y + 1)

            endif

            Ai(i) = center
            Aj(i) = left
            Ax(i) = sideValue
            i = i + 1

            Ai(i) = center
            Aj(i) = center
            Ax(i) = centerValue
            i = i + 1

            Ai(i) = center
            Aj(i) = right
            Ax(i) = sideValue
            i = i + 1


            if (flagValue == 2) then

                ! top point omitted

                Ai(i) = center
                Aj(i) = bottom
                Ax(i) = sideValue2
                i = i + 1

            else if (flagValue == 3) then

                ! bottom point omitted

                Ai(i) = center
                Aj(i) = top
                Ax(i) = sideValue2
                i = i + 1

            else
                ! flagMatrix(z, y) == 1 // or others

                ! both points are present

                Ai(i) = center
                Aj(i) = bottom
                Ax(i) = sideValue
                i = i + 1

                Ai(i) = center
                Aj(i) = top
                Ax(i) = sideValue
                i = i + 1

            endif
        else

            Ai(i) = center
            Aj(i) = center
            Ax(i) = 1
            i = i + 1

        endif


    end subroutine sm_setijx

    subroutine sm_createdist2(nZ, nY, nZ_disp, zMin, zMax, yMin, yMax, flagMatrix, dZ, dY, elementCount, Ai, Aj, Ax)
        implicit none

        !integer sm_linearIndex

        integer, intent(in) :: nZ, nY, nZ_disp, zMin, zMax, yMin, yMax
        integer, dimension(nZ, nY), intent(in) :: flagMatrix
        real*8, intent(in) :: dY, dZ
        integer, intent(in) :: elementCount

        integer, dimension(elementCount), intent(out) :: Ai, Aj
        real*8, dimension(elementCount), intent(out) :: Ax

        integer :: z, y, i
        integer :: left, top, right, bottom, center
        real*8 :: hzy, hzySquared, centerValue, sideValue, sideValue2

        hzy = dZ / dY
        hzySquared = hzy * hzy
        i = 1

        centerValue = 2.0 * (1.0 + hzySquared)
        sideValue = -1.0 * hzySquared
        sideValue2 = - 2.0 * hzySquared


        Ai = 0
        Aj = 0
        Ax = 0.0

        do z = zMin, zMax
            do y = yMin, yMax
                call sm_setijx(nZ, nY, nZ_disp, centerValue, sideValue, sideValue2, z, y, flagMatrix(z, y), elementCount, Ai, Aj, Ax, i)
            enddo
        enddo

    end subroutine sm_createdist2

    subroutine sm_rhs(nZ, nY, dZ, dY, n0, flagMatrix, potentialMatrix, densityMatrix, rightHandSide)

        implicit none

        ! input properties
        integer, intent(in) :: nZ, nY
        real*8, intent(in) :: dZ, dY
        integer, intent(in) :: n0
        integer, dimension(nZ, nY), intent(in):: flagMatrix
        real*8, dimension(nZ, nY), intent(in):: potentialMatrix, densityMatrix
        real*8, dimension(nZ * nY), intent(out) :: rightHandSide

        integer :: z, y, matrixIndex
        real*8 :: charge

        integer*8 :: timeStart, timeEnd, timeDiff

        rightHandSide = 0
        matrixIndex = 1

        !write(*,*) 'sm_rhs', nZ, nY, dZ, dY, n0, (real(n0) * dZ * dY)

        do z = 1, nZ
            do y = 1, nY
                charge = - dZ * dZ * densityMatrix(z, y) / (real(n0) * dZ * dY)

                rightHandSide(matrixIndex) = merge(potentialMatrix(z, y), charge, flagMatrix(z, y) .eq. 0)

                rightHandSide(matrixIndex) = real(flagMatrix(z, y))*charge + (1.0 - real(flagMatrix(z, y)))*potentialMatrix(z, y)

                !write(*,*) z, y, densityMatrix(z, y), charge, flagMatrix(z, y), rightHandSide(matrixIndex)


                matrixIndex = matrixIndex + 1
            enddo
        enddo


    end subroutine sm_rhs

    integer function sm_linearIndex(nZ, nY, z, y)

        implicit none
        integer, intent(in) :: nZ, nY, z, y
        integer :: z1, y1

        z1 = mod(z + nZ, nZ)
        y1 = mod(y + nY, nY)

        if (z1 == 0) z1 = nZ
        if (y1 == 0) y1 = nY

        sm_linearIndex = nY * (z1 - 1) + y1

        return

    end function sm_linearIndex

    subroutine sm_result(elementCount, rhs, nZ, nY, nZ_disp, newPotMatrix)
        implicit none

        integer, intent(in) :: elementCount
        real*8, dimension(elementCount), intent(in) :: rhs
        integer, intent(in) :: nZ, nY, nZ_disp
        real*8, dimension(nZ, nY), intent(inout) :: newPotMatrix

        integer :: z, y, matrixIndex

        matrixIndex = 1
        do z = 1, nZ
            do y = 1, nY - 1
                newPotMatrix(z, y) = rhs(matrixIndex)
                matrixIndex = matrixIndex + 1
            enddo
        enddo

        if (nZ_disp == 0) then
            newPotMatrix(1:nZ, nY) = newPotMatrix(1:nZ, 1)
        else
            newPotMatrix(1:nZ - nZ_disp, nY) = newPotMatrix(nZ_disp + 1:nZ, 1)
        endif

        if (nZ_disp .gt. 0) then
            newPotMatrix(nZ - nZ_disp:nZ, nY) = 0;
        endif

    end subroutine sm_result



end module
