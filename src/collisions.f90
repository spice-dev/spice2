module collisions

! use module_name

implicit none

! integer, parameter :: variables, go, here

contains

    ! subroutines for collision calculations
    subroutine coulomb_collisions(no_tot,y,z,ux,uy,uz,stype,no_species,m,q,dz,dt,Ny,Nz,Nz_max,rho,mks_info,Npc,collee,collie,collii,cell_count_max,z_start,z_stop,y_start,y_stop)
        use spice2_types
        implicit none
        ! include "struct.h"
        ! input-output parameters
        integer:: no_tot,no_species,Nx,Ny,Nz,iz1,iy1,ic,Npc,Nz_max,cell_count_max,new_cell_count_max,z_start,z_stop,y_start,y_stop,sp,sp1,sp2
        real*8:: dz,dt,collee,collie,collii
        real*8,dimension(no_tot):: y,z,ux,uy,uz
        integer*2, dimension(no_tot):: stype
        real*8,dimension(no_species,Nz_max,Ny):: rho
        type(mks_t):: mks_info
        real*8, dimension(no_species):: m,q
        ! internal variables
        integer:: i,j,k,no_cell,p1,p2
        real*8:: factor,n_L
        integer,dimension(cell_count_max):: indices,indices2
        integer, dimension(no_species,Nz_max,Ny,cell_count_max):: cell_indices
        integer, dimension(no_species,Nz_max,Ny):: cell_indices_count
        real*8, dimension(no_species):: rho_local
        logical:: debug
        integer, dimension(no_species):: sp_ind

        new_cell_count_max = 10*Npc
        debug = .true.
        cell_indices_count = 0
        ! step 1: loop over the particles and group them in cells
        do i=1,no_tot
            iz1 = idint(z(i)/dz) + 1.0
            iy1 = idint(y(i)/dz) + 1.0
            sp =stype(i)
            cell_indices_count(sp,iz1,iy1) = cell_indices_count(sp,iz1,iy1) +1
            if (cell_indices_count(sp,iz1,iy1).gt.new_cell_count_max) then
                new_cell_count_max = cell_indices_count(sp,iz1,iy1)
            end if
            if (cell_indices_count(sp,iz1,iy1).lt. (cell_count_max)) then
                cell_indices(sp,iz1,iy1,cell_indices_count(sp,iz1,iy1)) = i
            else
                write(*,*) 'Collisions: particle grouping error - array size exceeded',iz1,iy1,cell_count_max,i
            end if
        end do

        !!!!!!!!!!!!!!!!!
        ! same-species collisions!
        !!!!!!!!!!!!!!!!!
        if (collee.gt.0.0.or.collii.gt.0.0) then

            do sp=1,no_species
                ! step 2: loop over the indices and work with particle from each cell
                do i=z_start,z_stop
                    do j=y_start,y_stop
                        !    write(*,*) 'Running cell:',i,j

                        ! get local density - simply in one point for now
                        rho_local = rho(:,i,j)/real(Npc)
                        ic = cell_indices_count(sp,i,j)
                        indices(1:ic) = cell_indices(sp,i,j,1:ic)
                        ! randomize the positions
                        call randomize_vector(ic,indices(1:ic))
                        ! step 3: perform binary collisions
                        if (ic.gt.1) then
                            do k=1,(ic-1),2
                                if (k.eq.(ic-2)) then
                                    factor = 0.5
                                else
                                    factor = 1.0
                                end if

                                p1 = indices(k)
                                p2 = indices(k+1)
                                !    write(*,*) 'Chosen pair normal:',p1,p2,k,ic

                                if (q(stype(p1)).gt.0.0.and.q(stype(p2)).gt.0.0) then
                                    factor = factor*collii
                                elseif (q(stype(p1)).lt.0.0.and.q(stype(p2)).lt.0.0) then
                                    factor = factor*collee
                                else
                                    factor = factor*collie
                                end if

                                ! only perform those collision which are allowed
                                if (factor.gt.0.0) then
                                    n_L = min(rho_local(stype(p1)),rho_local(stype(p2)))
                                    if (n_L.eq.0.0) then
                                        !	write(*,*) 'Error, local density zero, picking the maximum density',i,j
                                        n_L = max(rho_local(stype(p1)),rho_local(stype(p2)))
                                    end if
                                    !if (debug) then
                                    !   write(*,*) 'Chosen pair:',p1,p2,k,ic
                                    !    write(*,*) 'Particle 1',z(p1),y(p1),stype(p1),n_L
                                    !    write(*,*) 'Particle 2',z(p2),y(p2),stype(p2)

                                    !end if


                                    call collide_pair(m(stype(p1)),q(stype(p1)),ux(p1),uy(p1),uz(p1),m(stype(p2)),q(stype(p2)),ux(p2),uy(p2),uz(p2),mks_info,n_L,dt,.false.,factor)
                                end if

                                ! handle the last pair if the number of particles is even
                                if (k.eq.(ic-2)) then
                                    p1 = indices(k)
                                    p2 = indices(k+2)
                                    !    write(*,*) 'Chosen pair final:',p1,p2,k,ic

                                    if (q(stype(p1)).gt.0.0.and.q(stype(p2)).gt.0.0) then
                                        factor = factor*collii
                                    elseif (q(stype(p1)).lt.0.0.and.q(stype(p2)).lt.0.0) then
                                        factor = factor*collee
                                    else
                                        factor = factor*collie
                                    end if
                                    if (factor.gt.0.0) then
                                        n_L = min(rho_local(stype(p1)),rho_local(stype(p2)))
                                        if (n_L.eq.0.0) then
                                            !	write(*,*) 'Error, local density zero, picking the maximum density',i,j
                                            n_L = max(rho_local(stype(p1)),rho_local(stype(p2)))
                                        end if
                                        call collide_pair(m(stype(p1)),q(stype(p1)),ux(p1),uy(p1),uz(p1),m(stype(p2)),q(stype(p2)),ux(p2),uy(p2),uz(p2),mks_info,n_L,dt,.false.,factor)
                                    end if
                                end if
                            end do
                        end if
                    end do
                end do
            end do ! specie count
        end if !  colii check

        !!!!!!!!!!!!!!!!!
        ! i-e collisions!
        !!!!!!!!!!!!!!!!!
        if (collie.gt.0.0) then
            do i=z_start,z_stop
                do j=y_start,y_stop
                    !    write(*,*) 'Running cell:',i,j

                    ! get local density - simply in one point for now
                    rho_local = rho(:,i,j)/real(Npc)


                    ! determine which species has this minimum
                    ic = 100000
                    do k=1,no_species
                        if (ic.ge.cell_indices_count(k,i,j)) then
                            sp1 = k
                            ic = cell_indices_count(k,i,j)

                        end if
                    end do
                    ! fancy trick which assumes we have only two species
                    sp2 = no_species - sp1 +1

                    indices(1:ic) = cell_indices(sp1,i,j,1:ic)
                    indices2(1:ic) = cell_indices(sp2,i,j,1:ic)

                    ! randomize the positions
                    call randomize_vector(ic,indices(1:ic))
                    call randomize_vector(ic,indices2(1:ic))

                    ! step 3: perform binary collisions
                    if (ic.gt.1) then
                        do k=1,ic
                            factor = 1.0

                            p1 = indices(k)
                            p2 = indices2(k)
                            !    write(*,*) 'Chosen pair normal:',p1,p2,k,ic

                            if (q(stype(p1)).gt.0.0.and.q(stype(p2)).gt.0.0) then
                                factor = factor*collii
                            elseif (q(stype(p1)).lt.0.0.and.q(stype(p2)).lt.0.0) then
                                factor = factor*collee
                            else
                                factor = factor*collie
                            end if

                            ! only perform those collision which are allowed
                            if (factor.gt.0.0) then
                                n_L = max(rho_local(stype(p1)),rho_local(stype(p2)))
                                if (n_L.eq.0.0) then
                                    !	write(*,*) 'Error, local density zero, picking the maximum density',i,j
                                    n_L = max(rho_local(stype(p1)),rho_local(stype(p2)))
                                end if
                                !if (debug) then
                                !   write(*,*) 'Chosen pair:',p1,p2,k,ic
                                !    write(*,*) 'Particle 1',z(p1),y(p1),stype(p1),n_L
                                !    write(*,*) 'Particle 2',z(p2),y(p2),stype(p2)

                                !end if
                                call collide_pair(m(stype(p1)),q(stype(p1)),ux(p1),uy(p1),uz(p1),m(stype(p2)),q(stype(p2)),ux(p2),uy(p2),uz(p2),mks_info,n_L,dt,.false.,factor)
                            end if

                        end do
                    end if
                end do
            end do
        end if !colie check
        cell_count_max = new_cell_count_max
    end subroutine



    subroutine collide_pair(m1,q1,ux1,uy1,uz1,m2,q2,ux2,uy2,uz2,mks_info,n_L,dt,debug,factor)
        use spice2_types
        implicit none
        ! include "struct.h"
        type(mks_t):: mks_info
        integer:: Npc
        real*8:: w_i,lambda_D,factor
        real*8::m1,q1,ux1,uy1,uz1,m2,q2,ux2,uy2,uz2,var_delta,n_L,coulomb_log,dt
        real*8::ux,uy,uz,phi,theta,rndx,m12,sin_phi,cos_phi,uperp,delta,sin_theta,u,delta_ux,delta_uy,delta_uz,cos_theta,cos_theta_min_one
        logical:: debug

        ! model taken from Takizuka & Abe 1977


        ! calculate the normalization factors
        lambda_D = sqrt(8.85E-12*mks_info%mks_Te/mks_info%mks_n0/1.602E-19)
        w_i = 1.602E-19*mks_info%mks_B/mks_info%mks_main_ion_m/1.67E-27
        rndx = 0.0
        ! first calculate the relative speed
        ux = ux1 - ux2
        uy = uy1 - uy2
        uz = uz1 - uz2
        u = dsqrt(ux*ux + uy*uy + uz*uz)
        ! uperp
        uperp = dsqrt(ux*ux + uy*uy)
        if (debug) then
            write(*,*) 'relative velocity',u,uperp
        end if
        ! combined mass
        m12 = m1*m2/(m1 + m2)
        coulomb_log = 24.0 - log(dsqrt(mks_info%mks_n0*1.0E-6)/mks_info%mks_Te)
        ! we combine e^4/epsilon^2/m_p^2 into one constant = 3.034 to avoid numerical issues
        ! variance of theta
        var_delta = abs(factor*3.034*q1**2*q2**2*n_L*mks_info%mks_n0*coulomb_log/(2.0*3.14159*m12**2*mks_info%mks_main_ion_m**2*u**3*w_i**3*lambda_D**3)*dt/w_i)
        if (debug) then

            write(*,*) 'Var delta',var_delta,coulomb_log,lambda_D,w_i,u,q1,q2,n_L,mks_info%mks_n0,m12,mks_info%mks_main_ion_m,factor

        end if
        ! determine the scattering phase theta

        call G05FAF(0.d0,1.d0,1,rndx)
        ! the real delta is chosen from a P(x) = x/var*exp(-x^2/(2*var))
        theta= sqrt(abs(-2.0*var_delta*log(1.0 - rndx)))

        if (theta.gt.3.14159) then
            !     write(*,*) 'Collision warning: theta too large',var_delta,n_L,u,theta/3.14*180.0
            call G05FAF(0.d0,1.d0,1,rndx)
            theta = rndx*3.14159
        end if

        ! fix the direction
        ! if (u.lt.0.0) then
        !	  theta = 2*3.14159 - theta
        !      end if
        sin_theta = dsin(theta)
        cos_theta_min_one = 1.0 - dcos(theta)



        ! determine the scattering phase phi
        call G05FAF(0.d0,1.d0,1,rndx)
        phi = rndx*2.0*3.14159
        ! fix phi to simplify
        !      phi = 0.0
        ! determine the scattering angle theta
        sin_phi = dsin(phi)
        cos_phi = dcos(phi)
        !  write(*,*) 'delta,Theta,phi',delta,asin(sin_theta)/3.14*180.0,phi/3.14*180.0
        if (debug) then
            write(*,*) 'Theta',theta/3.14*180.0,sin_theta,cos_theta_min_one
            write(*,*) 'phi',phi/3.14*180.0,sin_phi,cos_phi
        end if

        ! change of relative velocity
        if (uperp.eq.0.0) then
            delta_ux = u*sin_theta*cos_phi
            delta_uy = u*sin_theta*sin_phi
            delta_uz = -u*cos_theta_min_one
        else
            delta_ux = ux/uperp*uz*sin_theta*cos_phi - uy/uperp*u*sin_theta*sin_phi - ux*cos_theta_min_one
            delta_uy = uy/uperp*uz*sin_theta*cos_phi + ux/uperp*u*sin_theta*sin_phi - uy*cos_theta_min_one
            delta_uz = -uperp*sin_theta*cos_phi - uz*cos_theta_min_one
            !write(*,*) 'uz cross check',uperp,sin_theta,uz,cos_theta_min_one
        end if

        if (debug) then
            write(*,*) 'delta u',delta_ux,delta_uy,delta_uz
            ! check for the max deviation
            !write(*,*) 'scattering crosscheck',u*tan(theta),dsqrt(delta_ux**2 + delta_uy**2)

        end if

        ! looks like Takizuka had a mistake in his paper...
        ! ux =-ux
        ! uy = -uy
        ! uz = -uz

        ! update the velocities
        ! tempoerary hack - only update electrons
        ! if (q1.lt.0.0) then

        ux1 = ux1 + m12/m1*delta_ux
        uy1 = uy1 + m12/m1*delta_uy
        !   if (uz1.lt.0.0.and.delta_uz.lt.0.0) then
        !   write(*,*) 'q1: Accelerating electron!',ux1,uy1,uz1,m12/m1*delta_uz
        !   write(*,*) 'Theta',asin(sin_theta)/3.14*180.0,sin_theta,cos_theta_min_one
        !   write(*,*) 'phi',phi/3.14*180.0,sin_phi,cos_phi
        !   write(*,*) 'Var delta',var_delta,coulomb_log,lambda_D,w_i,u,q1,q2,n_L,mks_info%mks_n0,m12,mks_info%mks_main_ion_m,factor
        !   end if
        uz1 = uz1 + m12/m1*delta_uz
        !end if

        !  if (q2.lt.0.0) then
        !   if (delta_uz.gt.0.0.and.uz2.lt.0.0) then
        !   write(*,*) 'q2: Accelerating electron!',ux2,uy2,uz2, - m12/m2*delta_uz
        !   write(*,*) 'Theta',asin(sin_theta)/3.14*180.0,sin_theta,cos_theta_min_one
        !   write(*,*) 'phi',phi/3.14*180.0,sin_phi,cos_phi
        !   write(*,*) 'Var delta',var_delta,coulomb_log,lambda_D,w_i,u,q1,q2,n_L,mks_info%mks_n0,m12,mks_info%mks_main_ion_m,factor
        !   end if

        ux2 = ux2 - m12/m2*delta_ux
        uy2 = uy2 - m12/m2*delta_uy
        uz2 = uz2 - m12/m2*delta_uz
        !   end if
    end subroutine


    subroutine randomize_vector(no,a)
        integer:: no,no_loop,i,j,i_temp,j_temp,t
        integer, dimension(no):: a
        real*8:: rndx
        rndx = 0.0
        no_loop = no
        do t=1,no_loop
            call G05FAF(0.d0,1.d0,1,rndx)
            i = dnint(rndx*(no-1)) + 1
            call G05FAF(0.d0,1.d0,1,rndx)
            j = dnint(rndx*(no-1)) +1

            i_temp = a(i)
            j_temp = a(j)
            ! swap the pair
            a(i) = j_temp
            a(j) = i_temp
        end do
    end subroutine

end module
