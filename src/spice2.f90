! !!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!SHEATH is a 2D-1/2 quasineutral PIC simulation of collisionless sheath.
!2 space coordinates (z,y) and 3 velocity coordinates (vx,vy,vz)
!for the entire box (no symetry axis)
!This version uses first-order weighting.
!The integration method is leapfrog.
!
!Semi-Infinite case (PERIODIC conditions (see !PC and !BC comments) on the horizontal axes!!!
!                    and the WALL is FLOATING)
!This version uses arbitrary distrb. func. in all dir. to inject ions & electrons
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! MK's comments
!This code uses following normalization:
! t -> t*w_ci
! x -> x/debye_length
! u -> u/(w_ci*debye_length)
! phi -> phi*e/(k*Te)
! E -> E/(k*Te/(e*debye_length))
! n -> n/n_0

!Main simulation parameters
! tau = Ti/Te
!  mu = mi/me
!  ksi = r_l/debye_length
! r_l = c_i/w_ci
! c_i = sqrt(kTe/mi)


! requires the NAG library, libmat and getoptions module
! http://www.dominik-epple.de/getoptions/
! direct poisson solver requires GotoBLAS and UMFPack

! see man ifort for correct optimization!
! To compile on Sun cluster run `ifort -o pic-0.5 -O3 -xW -fp -fpic  getoptions.f90 pic-0.5.f90 !0.1: this version implements linked lists as a main data structure MK 11/08/2006
!0.2:  added command line argument parsingfdoma
!0.3: reversing back to static arrays but reducing the memory usage MK 14/08/06
! the trick is not to initialize parts of particle arrays unless it is really needed
!0.4: first attempt to implement direct poisson solver MK 17/08/06
! change in input file, added entry psolver, so far 2 options
!		1 - standard iterative solver
!		2 - new direct solver
!	solvers are placed in subroutines
! added intersection checking so that particles won't fly through corners of the tiles
! added random test particle tracking - pick a number in the ion list and it will have it's position and velocity saved
! 0.5: added macroparticles - controlled by k_macro MK 29/08/06
!changed input file format to allow input parameters in MKS RD 29/08/06
! successful implementation of the direct poisson solver
! 0.6: disabled automatic Lz calculation
! added parameter pot_chi to see the convergence of potential
! changed diagnostics sampling, code now takes diag_ntimes samples instead of sampling every time step
! 0.7 paralelization here!
! code now requires openMPI with fortran90 bindings to be installed
! parallelization is done for 1 or 2 processors
! program should be called  `mpirun -np 2 pic-0.6 -i ......`
!set -np 1 to run on single-processor machine
! compilation command is `mpif90 -o pic-0.8 -axN -arch SSE2 -ip -O3 -fp  getoptions.f90 pic-0.8.f90 /usr/lib/umf4_f77wrapper.o -lnagifort /usr/lib/libumfpack.a /usr/lib/libamd.a /usr/lib/libgoto.a /usr/lib/gcc/i686-pc-linux-gnu/3.4.6/libg2c.a libmatFile.a`
! restore function - using -r you can relaunch a simulation from a given temporary file
! this is in particular useful for particle tracking
! 0.8 MAJOR CHANGE OF THE CODE
! positions are now in z(spec_num,i),y(spec_num,i) 2D arraysm, velocities and other
! particle- related parameters accordingly
! completely removed k_macro
! time histories have fixed number of records and are being combed
! code split into several files
! new format of the input file, see README.input
!0.9 bugfix version
! fixed problem with vxav, vyav,etc.
! added diagnostic regions - see REDME.input file for changes
! other minor fixes of the code
! 0.9.3
! code can now be compiled in 64bit and even with PGI compiler
! finally resolved vxav bug
! added more diagnostics - 6 - particle tracking, 7 - resonance angle
! minor change to the time history mat file to enable full restore
! 1.0 - first "stable" release
! 1.1 - implementation of trully periodic BC so now it should work also for toroidal case
!       but it doesn't :(
! new type of poisson solver - 3 - first get's the solution by teh direct solver and uses it as a initial try for the iterative solver
! 1.3 - general BC subroutine that allows erosion modeling
! 2.0 - major uprgade of the code
! 	spice should now run with arbitrary geometry of equipotentials
!	inputs for version 2.0 are incompatible with inputs for version 1.x!!!
! 	testing mode of particle cross-field transport due to collisions
! 2.1 - dynamic load balances - enabled by setting mpi_rank = -1
! 2.2 - dielectric objects implemented, use param1 = 1 in the input, fixed (hopefuly) the dynamic load balance :)
!	param2 now adds fraction of utherm/sqrt(m)*dt in the x direction so there should be resulting drift in z
!	fixed bugs - negative velocities for electrons, sleep delay for more than 2 threads
!	dynamic balance off
!	-t mat file now contains actual density grids for all species named rhoXX, where XX is specie number
! 2.3 - bugfix release - fixed bug in leapfrog  - diffusion now disabled
! 2.4 - fixed bug in injection - code now works for mu other than 200
!     - fixed minor bug in collision detection
!     - first successful implementation of dielectrics
! 2.5 - major code cleanup -  no more memory leaks
!     - suprathermal electrons
!     - floating equipotentials
! 2.8 Bunch of new features:
! MatIO library used for storage in MAT files - so now we have MAT v5 files
! new namelist to store MKS parameters so they don't get lost and can be used in post process
! Guiding center electrons with motion_method=1
! 2 new scenarios using the SOL-like source-sink pairs
! lot of parameters is now passed into the t-file
! Reload functions now available with -c and -r

PROGRAM SHEATH
    ! we should push the code to get it working with impicit none!
    !implicit none
    use getoptions
    use hdf5_spice2
    use spice2_types

    !use IFPORT
    implicit none
    ! MK parallel
    include 'mpif.h'
    !data structure for simulation, grid and particle parameters
    !include "struct.h"

    ! MK 13/08/06 for command line arguments
    character :: okey
    !character(len=80) :: optarg
    character(len=2048) :: ifile='017-2.9-n2.inp'//char(0), ofile='001'//char(0), tfile='t-001'//char(0), pfile='pot'//char(0)
    logical:: verbose, debug, help,restore,cont, use_hdf5


    real*8:: dt,dt1,dt2,dt3,dz,dy,Lz,Ly,mu,P0,PL,P1,ta,tp,tau,ttrans,pi,Umonoi,Npc_real,enable_dump,ycentre,zcentre,radius,ya,za,yb,zb,yc,zc,rt,dt_ratio
    integer*8 :: t_max, step_max
    real*8:: Bfieldx,Bfieldy,Bfieldz,B,ksi,deltai,deltae,frelax,crit_conv
    real*8:: bx,by,bz,alpha_xz,alpha_yz,alpha_xy,cosalpha_xz,cosalpha_yz,Lzi,Lze
    real*8:: B_mks,ne_mks,Te_mks,lgap,Lz1_mks,spec,mi_mks,e_mks,l_debye,A
    integer:: N0,Na,Np,Npc,Npts,Nz,Ny,Nv,i,psolver,test_particle,diag_ntimes
    logical*1:: time_diag,monoi,relax,norm
    !MK parallel - actual processor number and maximum processor number
    integer:: proc_no,proc_max,ierr,par_size,status, threading
    !AP poisson solver parallelisation
    integer :: ps_mpigroup, ps_worldgroup, MPI_COMM_SOLVER, ps_rank, ps_commsize
    integer, dimension(:), allocatable   :: ps_ranks
    integer:: no_slices_z,no_slices_y
    ! for direct solver
    integer:: n,p
    real*8:: hzy
    type(grid_info):: g_i_s
    type(part_info)::	p_i_s
    type(sim_info)::	s_i_s

    ! domain decomposition
    integer, dimension(2):: no_slices
    integer:: y_start,y_stop,y_delta,j,Nz_max,z_start,z_stop,z_delta
    logical:: domain_decomp
    real*8:: Lz_inj


    ! namelist /norm_mks/ norm
    ! namelist /plasma/ tau,mu,ksi,P0,PL,Umonoi,frelax,alpha_xz,alpha_yz
    ! ! namelist /params/ ta,tp,Npc,Nv,dz,Lz,dy,Ly
    ! namelist /geom/ Ly1,Ly2,Lz1,crit_conv
    ! namelist /control/ time_diag,monoi,relax,psolver
    ! namelist /mks_test/ B_mks,lgap,spec
    ! ! ! namelist /plasma_mks/ tau,mu,B_mks,ne_mks,Te_mks,P0,PL,Umonoi,frelax,alpha_xz,alpha_yz,lgap,Lz1_mks,spec
    ! namelist /params_mks/ ta,tp,Npc,Nv,dz,dy,Lz,Ly1,crit_conv
    !MK new data structures
    integer:: no_species,sp,mpi_rank,motion_method,diag_nsamples,dump_period,pot_smoothing,injection_method,history_ntimes
    character*40:: name
    real*8:: T,m,q,w,Umono,injection_rate,surface_temperature
    integer:: material
    logical:: mono,injection_rate_relative,flow_diag,track_positions,track_velocity,ang_diag
    character*80:: injection_file
    real*8:: Npts_ratio,nr,ang_bin
    !diag regions
    integer:: no_diag_reg,record_property,nbins
    integer*2:: diag_specie
    real*8:: z_low,z_high,y_low,y_high,record_interval
    character*80:: diag_name
    logical:: record_only_steady_state
    integer:: fv_bin,fv_arrays,fv_perp_arrays,diag_histories,diag_full_histories
    ! max number of species fixed to 99
    type(pinfo), dimension(999):: spec_params
    type(diag_region), dimension(99):: diag_regions
    type(rectangle_t), dimension(99):: rectangle_params
    type(circle_t), dimension(99):: circle_params
    type(triangle_t), dimension(99):: triangle_params
    type(object_t), dimension(99):: object_params

    type(mks_t):: mks_info

    logical:: run_inversion,advanced_continue, enable_poisson,frozen_potvac,approximate_pot,have_thermionic,use_surface_heat_tweaks,coll_test
    ! version 2.0 parameters
    real*8:: Lz_low,Lz_high,tc
    integer:: automatic_ta,potdebug,no_objects
    real*8:: param1,param2,param3,param4
    character*120:: param5,pert_pot_file
    integer:: rectangles,triangles,circles,shape1,shape2,shape3,enable_erosion
    logical:: negative,impose_pot,use_vtot,have_impact_diag
    real*8:: zlow,zhigh,ylow,yhigh,pot,erode_prob,se_prob
    integer:: group,scenario,Nc,supra_buffer,used_gyroelectrons,regroup_runs
    real*8:: k_diff,driftz,sort_period,thermionic_A,thermionic_Wf
    ! duration parameters
    real*8:: complex_index,eta_days
    ! MKS vars
    real*8:: mks_n0,mks_Te,mks_B,mks_main_ion_q,mks_main_ion_m,mks_par1,mks_par2,mks_par3,init_disp,lambda_D,w_i
    ! Coulomb collisions control parameters
    real*8:: collii,collie,collee,see_factor,ebs_factor
    integer:: ns

    ! BC jump
    real*8:: Lz_disp
    integer:: Nz_disp
    ! v2.0 namelists
    integer, dimension(2,99):: slice_boundaries
    namelist /plasma/ ksi,tau,mu,P0,PL,alpha_xz,alpha_yz
    namelist /geom/ Ly,Lz,dy,dz,Lz_low,Lz_high,ta,tc,tp,Npc,Npts_ratio,scenario,automatic_ta,dt_ratio
    namelist /control/ time_diag, relax,psolver,ps_commsize,diag_ntimes,dump_period,pot_smoothing,history_ntimes,ang_diag,&
        ang_bin,potdebug,pert_pot_file,collii,collie,collee
    namelist /optional/ param1,param2,param3,param4,param5
    namelist /num_blocks/ rectangles,triangles,circles,shape1,shape2,shape3
    namelist /rectangle/ name,group,ylow,zlow,yhigh,zhigh,pot,enable_erosion,negative,surface_temperature,material,see_factor,ebs_factor,param1,param2,param3
    namelist /circle/ name,group,ycentre,zcentre,radius,pot,enable_erosion,negative,surface_temperature,material,see_factor,ebs_factor,param1,param2,param3,ylow,zlow,yhigh,zhigh
    namelist /triangle/ name,group,ya,za,yb,zb,yc,zc,pot,enable_erosion,negative,surface_temperature,material,see_factor,ebs_factor,param1,param2,param3

    namelist /num_spec/ no_species
    namelist /specie/ name,T,m,q,w,mpi_rank,motion_method,mono,Umono,injection_file,injection_method,injection_rate_relative,&
        injection_rate,flow_diag, track_positions,track_velocity,erode_prob,se_prob,init_disp
    namelist /num_diag_regions/ no_diag_reg
    namelist /diag_reg/ y_low,y_high,z_low,z_high,diag_name,record_property,record_only_steady_state,nbins,record_interval,diag_specie
    namelist /mks/ mks_n0,mks_Te,mks_B,mks_main_ion_q,mks_main_ion_m,mks_par1,mks_par2,mks_par3
    namelist /domaindecomp/ no_slices_z,no_slices_y

    ! partitioning for MG solver
    integer, dimension(1024,6) :: grid_partitioning
    integer :: i_y, i_z, i_r
    integer :: p_y, p_z, s_y, s_z, x_to_loc, y_from_loc, y_to_loc, z_from_loc, z_to_loc, y_to, y_from, z_to, z_from, n_loc, n_y_loc, n_z_loc


    !MK proper init of logical vars - needed for 64bit
    verbose = .true.
    debug = .false.
    restore = .false.
    run_inversion = .false.
    enable_poisson = .true.
    negative=.false.
    advanced_continue = .false.
    ! testing the collision models

    coll_test = .false.
    no_slices_z = 0
    no_slices_y = 0
    frozen_potvac = .false.
    cont = .false.
    ! enable the decomposition by default
    domain_decomp = .true.
    approximate_pot = .false.
    have_thermionic = .false.
    use_vtot = .false.
    use_surface_heat_tweaks = .true.
    have_impact_diag = .true.
    slice_boundaries = 0
    ! apparently it's good idea to initialiaze all variables in the namelists
    mks_n0 = 0.0
    mks_Te = 0.0
    mks_B = 0.0
    mks_main_ion_m = 0.0
    mks_main_ion_q = 0.0
    mks_par1 = 0.0
    mks_par2 = 0.0
    mks_par3 = 0.0
    regroup_runs = 0

    ! default thermionic constant
    thermionic_A = 60.0E4
    thermionic_Wf = 4.55
    Nv = 0.0
    used_gyroelectrons = 0
    frelax = 0.0
    crit_conv = 0.0
    ksi = 0.0
    tau = 0.0
    mu = 0.0
    P0 = 0.0
    PL = 0.0
    alpha_xz = 0.0
    alpha_yz = 0.0
    Ly = 0.0
    Lz = 0.0
    dy = 0.0
    dz = 0.0
    Lz_low = 0.0
    Lz_high = 0.0
    ta = 0.0
    tc = 0.0
    tp = 0.0
    Npc = 0
    Npts_ratio = 0.0
    dt_ratio = 0.0
    scenario = 0
    automatic_ta = 0
    time_diag = .false.
    relax = .false.
    psolver = 0
    diag_ntimes = 0
    dump_period = 0
    pot_smoothing = 0
    history_ntimes = 0
    ang_diag = .false.
    ang_bin = 0.0
    potdebug = 0
    pert_pot_file = ''
    t_max = 0
    step_max = 0
    material = 0
    ! different materials: 0=tunsten, 1=carbon, 2=boron-nitride
    impose_pot = .false.
    !TEMPORARY CONFIGURATION
    ! no_species = 2

    test_particle = 0
    verbose = .true.
    use_hdf5 = .false.
    !debug = .true.
    !MK number of samples taken for diagnostics
    ! diag_ntimes = 1000
    ! default values - no collisions
    collee= 0.0
    collie = 0.0
    collii = 0.0

    see_factor = 0.0
    ebs_factor = 0.0
    init_disp = 0.001
    ierr = 0
    proc_no = 0
    par_size  = 0
    threading = 0
    MPI_COMM_SOLVER = MPI_COMM_WORLD



    ! MK parallelization
    ! call MPI_INIT(ierr)
    ! AP switched to threading init call
    call MPI_INIT_THREAD(MPI_THREAD_MULTIPLE, threading, ierr)

    if (threading .ne. MPI_THREAD_MULTIPLE) then
        write(*,*) 'Warning: MPI threading was not provided, running non-threaded'
    endif
    write(*,*) 'MPI initalization returned status',ierr
    write(*,*) 'MPI initalization provided threading',threading


    !    call MPI_ERRHANDLER_SET(MPI_COMM_WORLD, MPI_ERRORS_RETURN, ierr);

    call MPI_COMM_RANK(MPI_COMM_WORLD,proc_no,ierr)
    ps_rank = proc_no

    call MPI_COMM_SIZE(MPI_COMM_WORLD,par_size,ierr)
    ps_commsize = par_size

    write(*,*) 'Parallelization initialized'
    write(*,*) 'Processors under command:',par_size
    proc_max = par_size -1
    if (proc_no.ne.0) then
        !put the process to sleep so the info is written in order
        call sleep(5*proc_no + 1)
    else
        ! read(*,*) ierr

    endif


    write(*,*) 'This is processor no',proc_no

#ifdef SPICE_TRACER
    write(*,*) 'THIS IS TRACER -- DO NOT USE FOR PLASMA SIMULATIONS'
    write(*,*) 'Main usecase of tracer is to trace particles upon existing potential'
    write(*,*) 'Tracer runs should be started with -j o-file.mat'
#endif

    ! parsing command line arguments
    do
        okey=getopt('i:t:o:l:vfabg:hp:rcxs:j:d5u')
        if(okey.eq.'>') exit
        if(okey.eq.'!') then
            write(6,*) 'unknown option: ', trim(optarg)
            call display_help()
            stop
        endif
        if(okey.eq.'v') then
            verbose = .true.
            write(6,*) 'Running in verbose mode'
        endif
        if(okey.eq.'f') then
            frozen_potvac = .true.
            write(6,*) 'Running in frozen potvac mode'
        endif

        if(okey.eq.'5') then
            use_hdf5 = .true.
            write(6,*) 'I/O will use HDF5'
        endif


        if(okey.eq.'b') then
            enable_poisson = .false.
            write(6,*) 'Running with E=0'
        endif
        if(okey.eq.'a') then
            approximate_pot = .true.
            write(6,*) 'Running with fixed approximate potential'
        endif
        if(okey.eq.'j') then
            pfile=trim(optarg)
            impose_pot = .true.
            frozen_potvac = .true.
            write(6,*) 'Potential will be loaded from ',trim(optarg)
        endif

        if(okey.eq.'c') then
            cont = .true.
            write(6,*) 'Running in continue mode'
        endif
        if(okey.eq.'x') then
            advanced_continue = .true.
            write(6,*) 'Running in advanced continue mode'
        endif

        if(okey.eq.'d') then
            debug = .true.
            write(6,*) 'Running in debug mode'
        endif
        if(okey.eq.'r') then
            restore = .true.
            write(6,*) 'Simulation will be restored'
        endif

        if(okey.eq.'h') then
            call display_help()
            stop
        endif

        if(okey.eq.'o') then
            ofile=trim(optarg)
            write(6,*) 'Final output will be written to ',trim(optarg)
        endif
        if(okey.eq.'i') then
            ifile=trim(optarg)
            write(6,*) 'Using input file ',trim(optarg)
        endif
        if(okey.eq.'t') then
            tfile=trim(optarg)
            write(6,*) 'Time diag will be written to ',trim(optarg)
        endif
        if(okey.eq.'p') then
            read(optarg,'(i)') p
            test_particle = p
            write(6,*) 'Storing test particle count.',test_particle
        endif
        if(okey.eq.'g') then
            read(optarg,'(i)') p
            regroup_runs = p
            advanced_continue = .true.
            write(6,*) 'Regrouping restore from runs.', regroup_runs
        endif

        if(okey.eq.'l') then
            read(optarg,'(i)') p
            t_max = int(p)
            write(6,*) 'Maximum runtime will be limited to (hours)',t_max
            ! convert to microseconds
            t_max = t_max * 3600e6;
        endif


        if(okey.eq.'s') then
            read(optarg,'(i)') p
            step_max = int(p)
            write(6,*) 'Maximum runtime will be limited to (steps)',step_max
        endif



    end do

    !!!!!!!!!!!!!!!!!!!!
    !!! DEBUG CONFIG !!!
    !!!!!!!!!!!!!!!!!!!!

    verbose=.true.
    !    debug = .true.
    !   tfile = 'tbv8'
    !   ofile = 'bv8'
    !   ifile = 'bench8-diag.inp'



    open(3,file = trim(ifile)//char(0),status='unknown')
    if (debug) then
        write(*,*) 'Input file opened'
    endif
    read(3,nml=plasma)
    if (debug) then
        write(*,*) 'Plasma loaded '
    endif

    read(3,nml=geom)
    if (debug) then
        write(*,*) 'Geom loaded'
    endif

    read(3,nml=control)
    if (debug) then
        write(*,*) 'Control loaded'
    endif

    read(3,nml=optional)
    if (debug) then
        write(*,*) 'Optional loaded'
    endif


    k_diff= 0.0
    !k_diff = param1
    !    write(*,*) 'Using diff coef.',k_diff
    Lz_disp = param1
    if (abs(Lz_disp).gt.0.0) then
        write(*,*) 'Using BC jump', Lz_disp
    endif


    driftz = param2

    write(*,*) 'Adding external drift',driftz
    ! disable grid dumping use this to load MKS namelist instead
    enable_dump = 0
    write(*,*) 'Grid dumping:',enable_dump
    if (param3.ne.0) then
        read(3,nml=mks)
        write(*,*) 'Loaded MKS info'
        write(*,*) 'density: ',mks_n0
        write(*,*) 'Te: ',mks_Te
        write(*,*) 'B: ',mks_B
        write(*,*) 'Main ion mass: ',mks_main_ion_m
        write(*,*) 'Main ion charge: ',mks_main_ion_q
        write(*,*) 'PAR1: ',mks_par1
        write(*,*) 'PAR2: ',mks_par2
        write(*,*) 'PAR3: ',mks_par3
        if (mks_par1.gt.0.0) then
            thermionic_Wf = mks_par1
        endif

        if (mks_par3.gt.0.0) then
            thermionic_A = mks_par3
        endif


        if (param3.gt.1) then
            read(3,nml=domaindecomp)
            write(*,*) 'Loaded domain decomp info'
            write(*,*) 'Slices:',no_slices_z,no_slices_y

            if (no_slices_z*no_slices_y .ne. par_size) then
                ! Forcing domain decomposition, if incorrect number of slices is specified
                no_slices_y = par_size / no_slices_z
                write(*,*) 'Recalculated slices:', no_slices_z, no_slices_y
            endif

        endif

    else
        write(*,*) 'Warning:::: Not loading MKS info'
        write(*,*) 'Are you trying to run old input file?'
        write(*,*) 'MKS namelist is optional, however recommended'
        write(*,*) 'Do not forget to set nonzero param3 in the optional namelist'
    endif

    !AP poisson solver (psolver == 4) can be configured to use less cores to optimize performance
    if ((psolver.eq.4) .or. (psolver.eq.5)) then
        write(*,*) ''
        if (ps_commsize <= 0 .or. ps_commsize > par_size) then
            ps_commsize = par_size
        endif

        allocate(ps_ranks(ps_commsize));

        write (*,*) '*** PES *** Communicator will be limited to', ps_commsize

        !AP this is a simple method of distribution -- if you are using more threads per core, this should be updated
        do i = 0, ps_commsize - 1
            write (*,*) '*** PES *** Adding rank to solver MPI communicator', i
            !ps_ranks(i + 1) = i * ompNumThreads;
            ps_ranks(i + 1) = i;
        enddo


        call MPI_COMM_GROUP(MPI_COMM_WORLD, ps_worldgroup, ierr);
        call MPI_GROUP_INCL(ps_worldgroup, ps_commsize, ps_ranks, ps_mpigroup, ierr)

        call MPI_COMM_CREATE(MPI_COMM_WORLD, ps_mpigroup, MPI_COMM_SOLVER, ierr)
        call MPI_COMM_RANK(MPI_COMM_SOLVER, ps_rank, ierr)

        write (*,*) '*** PES *** Ranks', ps_ranks
        write (*,*) '*** PES *** MPI_COMM_WORLD', MPI_COMM_WORLD
        write (*,*) '*** PES *** MPI_COMM_SOLVER', MPI_COMM_SOLVER

    else
        ! proxy not to have undefined variables
        allocate(ps_ranks(ps_commsize));

    endif


    sort_period = param4
#ifdef SPICE_TRACER
    sort_period = 0
    write(*,*) 'Particle sorting period overriden'
#endif
    write(*,*) 'Particle sorting period',sort_period
    surface_temperature = 0
    material = 0
    read(3,nml=num_blocks)
    if (verbose) then
        write(*,*) 'Geometry configuration:'
        write(*,*) '-----------------------'
        write(*,*) 'Rectangles:',rectangles
        write(*,*) 'Triangles:',triangles
        write(*,*) 'Circles:',circles
    endif

    no_objects = rectangles + circles + triangles
    if (rectangles.gt.0) then
        do i=1,rectangles
            read(3,nml=rectangle)
            rectangle_params(i)%name = name
            rectangle_params(i)%ylow = idint(ylow/dy) + 1
            rectangle_params(i)%zlow = idint(zlow/dz) + 1
            rectangle_params(i)%yhigh = idint(yhigh/dy) +1
            rectangle_params(i)%zhigh = idint(zhigh/dz) +1
            rectangle_params(i)%pot = pot
            rectangle_params(i)%negative = negative
            rectangle_params(i)%enable_erosion = enable_erosion
            rectangle_params(i)%surface_temperature = surface_temperature
            rectangle_params(i)%material = material

            rectangle_params(i)%see_factor = see_factor
            rectangle_params(i)%ebs_factor = ebs_factor
            rectangle_params(i)%param1 = param1
            rectangle_params(i)%param2 = param2
            rectangle_params(i)%param3 = param3
            rectangle_params(i)%group = group
            ! universal parameters
            object_params(i)%see_factor = see_factor
            object_params(i)%ebs_factor = ebs_factor
            object_params(i)%ts = surface_temperature
            object_params(i)%material = material





        enddo
    endif
    if (circles.gt.0) then
        do i=1,circles
            read(3,nml=circle)
            circle_params(i)%name = name
            circle_params(i)%ycentre = idint(ycentre/dy) + 1
            circle_params(i)%zcentre = idint(zcentre/dz) + 1
            circle_params(i)%radius = idint(radius/dy) +1
            circle_params(i)%pot = pot
            circle_params(i)%negative = negative
            circle_params(i)%enable_erosion = enable_erosion
            circle_params(i)%surface_temperature = surface_temperature
            circle_params(i)%material = material

            circle_params(i)%see_factor = see_factor
            circle_params(i)%ebs_factor = ebs_factor

            circle_params(i)%param1 = param1
            circle_params(i)%param2 = param2
            circle_params(i)%param3 = param3
            circle_params(i)%group = group
            circle_params(i)%ylow = 0

            ! universal parameters
            object_params(i + rectangles)%see_factor = see_factor
            object_params(i + rectangles)%ebs_factor = ebs_factor
            object_params(i + rectangles)%ts = surface_temperature
            object_params(i + rectangles)%material = material

            if (ylow > 0) then
                circle_params(i)%ylow = idint(ylow/dy) + 1
            endif
            circle_params(i)%zlow = 0
            if (zlow > 0) then
                circle_params(i)%zlow = idint(zlow/dz) + 1
            endif
            circle_params(i)%yhigh = 0
            if (yhigh > 0) then
                circle_params(i)%yhigh = idint(yhigh/dy) + 1
            endif
            circle_params(i)%zhigh = 0
            if (zhigh > 0) then
                circle_params(i)%zhigh = idint(zhigh/dz) + 1
            endif
        enddo
    endif
    if (triangles.gt.0) then
        do i=1,triangles
            read(3,nml=triangle)
            triangle_params(i)%name = name
            triangle_params(i)%ya = idint(ya/dy) + 1
            triangle_params(i)%za = idint(za/dz) + 1
            triangle_params(i)%yb = idint(yb/dy) + 1
            triangle_params(i)%zb = idint(zb/dz) + 1
            triangle_params(i)%yc = idint(yc/dy) + 1
            triangle_params(i)%zc = idint(zc/dz) + 1
            triangle_params(i)%pot = pot
            triangle_params(i)%negative = negative
            triangle_params(i)%enable_erosion = enable_erosion
            triangle_params(i)%surface_temperature = surface_temperature

            triangle_params(i)%material = material
            triangle_params(i)%see_factor = see_factor
            triangle_params(i)%ebs_factor = ebs_factor
            ! universal parameters
            object_params(i + rectangles + circles)%see_factor = see_factor
            object_params(i + rectangles + circles)%ebs_factor = ebs_factor
            object_params(i + rectangles + circles)%ts = surface_temperature
            object_params(i + rectangles + circles)%material = material

            triangle_params(i)%param1 = param1
            triangle_params(i)%param2 = param2
            triangle_params(i)%param3 = param3
            triangle_params(i)%group = group

        enddo
    endif



    read(3,nml=num_spec)
    ! we cannot loop over sp with internal increments!!!
    sp  = 0
    do i=1,no_species
        sp = sp +1
        read(3,nml=specie)
        !copy over the species paramters into the data structure
        spec_params(sp)%name = name
        spec_params(sp)%T = T
        spec_params(sp)%q = q
        spec_params(sp)%w = w
        spec_params(sp)%m = m
        spec_params(sp)%mpi_rank = mpi_rank
        spec_params(sp)%motion_method = motion_method
        if (motion_method.eq.1) then
            used_gyroelectrons = 1
        endif
        spec_params(sp)%mono = mono
        spec_params(sp)%Umono = Umono
        spec_params(sp)%injection_file = injection_file
        spec_params(sp)%injection_method = injection_method
        if (injection_method.eq.5) then
            have_thermionic = .true.
            use_vtot = .true.
            write(*,*) 'Detected thermionic injection!'
        endif
        spec_params(sp)%injection_rate_relative = injection_rate_relative
        spec_params(sp)%injection_rate = injection_rate
        spec_params(sp)%flow_diag = flow_diag
        spec_params(sp)%track_positions = track_positions
        spec_params(sp)%track_velocity = track_velocity
        spec_params(sp)%particle_group = sp
        spec_params(sp)%dynamic_balance = .false.
        spec_params(sp)%se_prob = se_prob
        spec_params(sp)%init_disp = init_disp

        if (domain_decomp) then
            spec_params(sp)%mpi_rank = proc_no

            ! create copy for each of the processors
            ! not anymore, just adjust the mpi_rank

            ! do j=1,par_size-1
            ! 	sp = sp +1
            ! !    read(3,nml=specie)
            ! !copy over the species paramters into the data structure
            ! 	spec_params(sp)%name = name
            ! 	spec_params(sp)%T = T
            ! 	spec_params(sp)%q = q
            ! 	spec_params(sp)%w = w
            ! 	spec_params(sp)%m = m
            ! 	spec_params(sp)%mpi_rank = j
            ! 	spec_params(sp)%motion_method = motion_method
            ! 	if (motion_method.eq.1) then
            ! 	      used_gyroelectrons = 1
            ! 	endif
            ! 	spec_params(sp)%mono = mono
            ! 	spec_params(sp)%Umono = Umono
            ! 	spec_params(sp)%injection_file = injection_file
            ! 	spec_params(sp)%injection_method = injection_method
            ! 	spec_params(sp)%injection_rate_relative = injection_rate_relative
            ! 	spec_params(sp)%injection_rate = injection_rate
            ! 	spec_params(sp)%flow_diag = flow_diag
            ! 	spec_params(sp)%track_positions = track_positions
            ! 	spec_params(sp)%track_velocity = track_velocity
            ! 	spec_params(sp)%particle_group = sp
            ! 	spec_params(sp)%dynamic_balance = .false.
            ! 	spec_params(sp)%se_prob = se_prob
            !
            !
            !
            ! enddo
            ! no_species = no_species + par_size -1

        endif
        ! dynamic load balance
        if (mpi_rank.eq.-1) then
            write(*,*) 'Enabled dynamic balance for: ', name
            !this specie will be distributed over all nodes - we add no_species
            no_species = no_species + par_size -1
            !we change the rank and copy over other characteristics
            spec_params(sp)%mpi_rank = 0
            ! divide the injection_rate by number of threads
            spec_params(sp)%injection_rate = injection_rate/real(par_size)
            write(*,*) 'Relative inj. rate:', injection_rate/real(par_size)
            ! set the balance on - debug we'd better leave that off for the moment..
            spec_params(sp)%dynamic_balance = .false.
            do mpi_rank = 0,proc_max
                spec_params(sp + mpi_rank)%mpi_rank = mpi_rank
                if (mpi_rank.gt.0) then
                    spec_params(sp + mpi_rank) = spec_params(sp)
                endif
                spec_params(sp + mpi_rank)%mpi_rank = mpi_rank

            enddo
            ! step in the sp
            sp = sp + par_size -1
        endif
    enddo
    no_species = sp
    write(*,*) 'Total no of species',no_species
    do sp=1,no_species
        write(*,*) 'Rank: ',spec_params(sp)%name, spec_params(sp)%mpi_rank
    enddo
    do sp=1,no_species
        write(*,*) 'Mass: ',sp, spec_params(sp)%m
    enddo

    ! diag_regions
    read(3,nml=num_diag_regions)
    write(*,*) 'Number of diag regions:',no_diag_reg
    do sp=1,no_diag_reg
        read(3,nml=diag_reg)
        diag_regions(sp)%name = diag_name
        diag_regions(sp)%z_low = z_low
        diag_regions(sp)%z_high = z_high
        diag_regions(sp)%y_low = y_low
        diag_regions(sp)%y_high = y_high
        diag_regions(sp)%nbins = nbins
        diag_regions(sp)%record_interval = record_interval
        diag_regions(sp)%record_property = record_property
        diag_regions(sp)%record_only_steady_state = record_only_steady_state
        diag_regions(sp)%specie = diag_specie
    enddo

    pi=dacos(-1.d0)


    !  mi_mks=1.6d-27*spec
    !  e_mks=1.6d-19
    !MPI debug
    if (proc_no.gt.0) then
        ! 	debug = .false.
        ! 	verbose = .false.
    endif

    fv_bin = 100
    !diag regions  - decide how many diag arrays should be allocated
    if (no_diag_reg.gt.0) then
        fv_bin = maxval(diag_regions(1:no_diag_reg)%nbins)
    endif

    fv_arrays = 0
    fv_perp_arrays  = 0
    diag_histories = 0
    diag_full_histories = 0
    write(*,*) 'Diag arrays for f(v) will have',fv_bin,'slots'
    do sp=1,no_diag_reg
        write(*,*) 'Checking diag',diag_regions(sp)%record_property
        !	if (spec_params(diag_regions(sp)%specie)%mpi_rank.eq.proc_no) then
        if ((diag_regions(sp)%record_property.eq.2).or.(diag_regions(sp)%record_property.eq.3)) then
            ! f(v) diagnostics
            fv_arrays = fv_arrays + 3
        else if (diag_regions(sp)%record_property.eq.5) then
            ! 1D + 2D f(v) diagnostics
            fv_arrays = fv_arrays +1
            fv_perp_arrays = fv_perp_arrays +1
        else if (diag_regions(sp)%record_property.eq.6) then
            ! particle tracking
            diag_histories = diag_histories + 5
        else if (diag_regions(sp)%record_property.eq.7) then
            ! resonant angle
            fv_arrays = fv_arrays +1
        else if (diag_regions(sp)%record_property.eq.8) then
            ! energy of impact vs angle of impact
            fv_perp_arrays = fv_perp_arrays +1
        else if (diag_regions(sp)%record_property.eq.4) then
            diag_histories = diag_histories + 3
        else if (diag_regions(sp)%record_only_steady_state) then
            ! other steady state time histories
            diag_histories = diag_histories + 1
        else
            ! full time histories (not implemented)
            diag_full_histories = diag_full_histories + 1

        endif
        !	endif !par
    enddo
    write(*,*) 'Diagnostics'
    write(*,*) 'Number of f(v)',fv_arrays
    write(*,*) 'Number of steady state histories',diag_histories
    write(*,*) 'Number of full time histories',diag_full_histories

    !!!!!!!!!normalisation of the MKS input!!!!!!!!!!!
    !MK disabled for a while
    ! if (norm) then
    !    ne_mks=1/mi_mks*7.43**2*ksi**2*B_mks**2*e_mks*1.d6   ![m-3]
    !    Te_mks=lgap**2*ne_mks*1.d-6/((Ly2-Ly1)*7.43)**2      ![eV]
    !    write(6,*) '=============Normalisation==========='
    !    write(6,*) 'normalized input?:',norm
    !    write(6,*) 'ne=', ne_mks,'m-3'
    !    write(6,*) 'Te=', Te_mks,'eV'
    !    write(6,*) '=============Normalisation==========='
    ! else
    ! !    ksi=1.d0/7.43/B_mks*dsqrt(mi_mks*ne_mks/e_mks)
    !    Ly2=Ly1+lgap/7.43*dsqrt(ne_mks/Te_mks)
    ! !    Ly=Ly2+Ly1
    !    l_debye=7.43*dsqrt(Te_mks/ne_mks)
    !    Lz1=Lz1_mks/l_debye
    !Npc = int(ne_mks*1000000*l_debye*l_debye)
    !    write(6,*) '=============MKS input==========='
    !    write(6,*) 'normalized input?:',norm
    !    write(6,*) 'Ly1=', Ly1
    !    write(6,*) 'Ly2=', Ly2
    !    write(6,*) 'Ly=', Ly
    !    write(6,*) 'Lz1=', Lz1
    !    write(6,*) 'Lz=', Lz
    ! !    write(6,*) 'ksi=', ksi
    !    write(6,*) 'l_debye=', l_debye,' m'
    !    write(*,*) 'Real Npc=',Npc
    ! !    write(6,*) '=============MKS input==========='
    ! endif
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



    write(6,*) 'time_diag=',time_diag
    do sp=1,no_species
        write(*,*) 'Specie: ',spec_params(sp)%name
        write(6,*) 'mono=',spec_params(sp)%mono
        if (spec_params(sp)%mono) then
            spec_params(sp)%Umono=spec_params(sp)%Umono*ksi*dsqrt(spec_params(sp)%T/spec_params(sp)%m)
            write(6,*) 'Umono=',spec_params(sp)%Umono          !NEW RD 12/12/05
        endif
    enddo

    write(6,*) 'relax=',relax
    if (relax) then
        write(6,*) 'frelax=',frelax
    endif
    write(6,*) 'tau=',tau
    write(6,*) 'mu=',mu
    write(6,*) 'ksi=',ksi
    write(6,*) 'P0=',P0
    write(6,*) 'PL=',PL
    crit_conv = 0.0
    write(6,*) 'crit_conv=',crit_conv

    !Fields and Constantes
    write(6,*) 'alpha_xz=',alpha_xz
    write(6,*) 'alpha_yz=',alpha_yz

    Bfieldz=-1.d0                         !fixed from the beginning
    if (abs(alpha_yz).eq.90.d0) then
        Bfieldy=0.d0
    else
        alpha_yz=alpha_yz*pi/180.d0     !transform degres in radians
        Bfieldy=-1.d0/tan(alpha_yz)
    endif
    if (abs(alpha_xz).eq.90.d0) then
        Bfieldx=0.d0
    else
        alpha_xz=alpha_xz*pi/180.d0     !transform degres in radians
        Bfieldx=-1.d0/tan(alpha_xz)
    endif

    write(6,*) 'Bfieldx=',Bfieldx
    write(6,*) 'Bfieldy=',Bfieldy
    write(6,*) 'Bfieldz=',Bfieldz
    B=dsqrt(Bfieldx**2+Bfieldy**2+Bfieldz**2)
    bz=Bfieldz/B   != cosalpha  NEW!!                  !for orientation of B
    by=Bfieldy/B   != costheta
    bx=Bfieldx/B   != cosphi
    cosalpha_xz=Bfieldx/dsqrt(Bfieldx**2+Bfieldz**2)   !NEW!!
    cosalpha_yz=Bfieldy/dsqrt(Bfieldy**2+Bfieldz**2)

    !dz is the grid spacing
    write(6,*) 'dz=',dz
    !dy is the grid spacing in y direction
    write(6,*) 'dy=',dy
    !Ny is the number of grid points in y direction
    Ny=idint(Ly/dy+1.d-10)+1              !+1.d-10 <- Pb avec compilateur Linux/Unix
    write(6,*) 'Ny=',Ny

    ! we don't add the 1 for this displacement
    if (abs(Lz_disp).gt.0.0) then
        Nz_disp = idint(Lz_disp/dy +1.d-10)
    else
        Nz_disp = 0
    endif
    !Lz is the length of the simulation region
    !-----------------------------------------
    ! MK disabled
    !if ((Lz-Lz1).gt.(10.d0*ksi*dsqrt(bx**2+by**2))) then
    !   Lz=Lz
    !else
    !   Lz=10.d0*ksi*dsqrt(bx**2+by**2)+Lz1   !NEW!! 20/3/06
    !endif
    write(6,*) 'Lz(before rounding)=',Lz
    !Nz is the number of grid points
    Nz=idint(Lz/dz+1.d-10)+1              !+1.d-10 <- Pb avec Linux
    write(6,*) 'Nz=',Nz
    !Lz is the length of the simulation region rounded!
    Lz=(Nz-1)*dz
    write(6,*) 'Lz=',Lz
    !-----------------------------------------
    !Ly is the length of the simulation region in y dir.
    Ly=dy*(Ny-1)
    write(6,*) 'Ly=',Ly
    !N0 is the particle density at infinity (n=1).
    N0=idint(Npc/(dz*dy)+1.d-10)          !+1.d-10 <- Pb avec compilateur Linux
    write(6,*) 'N0=',N0
    !Lz is the length of the simulation region
    !-----------------------------------------
    ! MK disabled
    !if ((Lz-Lz1).gt.(10.d0*ksi*dsqrt(bx**2+by**2))) then
    !   Lz=Lz
    !else
    !   Lz=10.d0*ksi*dsqrt(bx**2+by**2)+Lz1   !NEW!! 20/3/06
    !endif
    write(6,*) 'Lz(before rounding)=',Lz
    !Nz is the number of grid points
    Nz=idint(Lz/dz+1.d-10)+1              !+1.d-10 <- Pb avec Linux
    write(6,*) 'Nz=',Nz
    !Lz is the length of the simulation region rounded!
    Lz=(Nz-1)*dz
    write(6,*) 'Lz=',Lz
    !-----------------------------------------
    !Ly is the length of the simulation region in y dir.
    Ly=dy*(Ny-1)
    write(6,*) 'Ly=',Ly
    !N0 is the particle density at infinity (n=1).
    N0=idint(Npc/(dz*dy)+1.d-10)          !+1.d-10 <- Pb avec compilateur Linux
    write(6,*) 'N0=',N0

    !Set time step
    dt1=0.25*dsqrt((dz**2+dy**2)/mu/2.0)/ksi
    !   dt2=2.d0*pi/dsqrt(mu)/10.d0                 ! dt = 1/10*T for Larmor gyration

    if (used_gyroelectrons.eq.1)  then
        ! don't use this criterion for gyroelectrons
        dt2 = 10000.0
    else
        dt2=2.d0*pi/mu/10.d0

    endif
    if (dt1.le.dt2) then
        dt=dt1
    else
        dt=dt2
    endif

    ! collisions impose a new time step limit - at least 10x smaller than the e-beam slowing down time_diag
    if (collie.gt.0.0.or.collee.gt.0.0.or.collii.gt.0.0) then
        lambda_D = sqrt(8.85E-12*mks_Te/mks_n0/1.602E-19)
        w_i = 1.602E-19*mks_B/mks_main_ion_m/1.67E-27

        rt = mks_n0/w_i**4
        dt3 = 0.1/(3.034*11.0*mu**(1/2)/ksi**3/lambda_D**3*rt)
        write(*,*) 'dt3=',dt3
    else
        dt3 = 10000.0
    endif
    if (dt3.lt.dt) then
        dt = dt3
        write(*,*) 'Time step determined by collision limits'
    endif


    if (dt_ratio.gt.0.0) then
        write(*,*) 'dt ratio used to reduce time step',dt_ratio
        dt =  dt*dt_ratio
    endif
    ! safety factor - scale it down twice  (there were still electrons too fast)
    !dt = dt / 10.0
    write(6,*) 'dt=',dt
    !!!!MK obsolete, moved to mainloop, here only for Npts
    !length of the box for ion injection (Lz+5rL)   NEW!!
    if (alpha_yz.eq.90.d0.and.alpha_xz.eq.90.d0) then
        Lzi=Lz                                                  !sqrt(2)*ksi=1rLarmor and we take 5 as maximum
    else                                                       !in the Maxwll. distribution. the factor 2 is
        Lzi=Lz+2.d0*(5.d0*dsqrt(2.d0)*ksi)*dsqrt(bx**2+by**2)   !because we inject particles at 5rL after Lz.
    endif                                                      !All particles shud be inside
    write(6,*) 'Lzi=',Lzi

    !length of the box for electron injection (Lz+2rL)   NEW!!
    !    if (alpha_yz.eq.90.d0.and.alpha_xz.eq.90.d0) then
    !       Lze=Lz
    !    else
    !       Lze=Lz+2.d0*(5.d0*dsqrt(2.d0)*ksi/dsqrt(mu))*dsqrt(bx**2+by**2)
    !    endif
    !    write(6,*) 'Lze=',Lze

    !Npts is the total number of particles for n=1 everywhere.
    !Npts=idint(N0*Lzi*Ly*1.1)
    !Npts is now a number of all particles at one proc, hence 2.0* for ions + electrons
    nr = 0.0
    do sp=1,no_species
        if ((spec_params(sp)%mpi_rank.eq.proc_no).and.(spec_params(sp)%injection_rate_relative)) then
            nr = nr + spec_params(sp)%injection_rate
        endif
    enddo
    !fix - in most cases we have ions + electrons
    ! this gives nr = 1.5, while we want 2.0
    nr = nr/1.5*2.0
    !Npts_ratio is a input-specific tuning parameter
    if (domain_decomp) then
        Npts_ratio = Npts_ratio/float(par_size)
    endif
    Npts=idint(Npts_ratio*nr*N0*Lzi*Ly*1.1)   !NEW 4/7/06 (JG)
    write(6,*) 'Npts=',Npts,idint(N0*Lzi*Ly*Npts_ratio*nr)
    !ttrans is the ion transit time
    ttrans=Lz/dsqrt(tau)/ksi/abs(bz)
    write(6,*) 'ttrans=',ttrans
    !Np is the number of time steps to be made.
    Np=idint(ttrans*tp/dt)+1
    write(6,*) 'Np=',Np
    !Na is the number of time steps before averaging starts.
    Na=idint(ttrans*ta/dt)+1
    write(6,*) 'Na=',Na
    !Nc is optional to start some strange stuff in saturated case
    Nc=idint(ttrans*tc/dt)+1
    write(6,*) 'Nc=',Nc


    write(6,*) 'B-field =',B
    write(6,*) 'bx=',bx
    write(6,*) 'by=',by
    write(6,*) 'bz=',bz
    write(6,*) 'cosalpha_xz=',cosalpha_xz
    write(6,*) 'cosalpha_yz=',cosalpha_yz
    ! index estimating how demanding the simulation will be
    complex_index = real(Npts)/1.0E12*real(Np)
    ! estimation of duration at 2 AMD Opteron CPUs
    eta_days = complex_index/0.8524
    write(*,*) 'Initial duration estimation'
    write(*,*) 'Index of complex.:', complex_index
    write(*,*) 'Estimated ETA:',eta_days
    write(*,*) 'Poisson solver handle:', psolver

    if (psolver .eq. 1)  then
        write(*,*) 'Using old iterative Poisson solver (get a cup of cofee this will take time)'
    else if (psolver .eq. 2)  then
        write(*,*) 'Using new direct Poisson solver (good choice :)'
    else if(psolver .eq. 3)  then
        write(*,*) 'Deadly combo of both solvers!'
    else if(psolver .eq. 4)  then
        write(*,*) 'Using MUMPS parallel solver'
    else if(psolver .eq. 5)  then
        write(*,*) 'Using MUMPS parallel solver and forcing distribution of the matrix'


    endif
    Nz_max= 0
    do sp=1,no_species
        write(*,*) 'Pre-loop Calculating injection box for species',sp
        if (alpha_yz.eq.90.d0.and.alpha_xz.eq.90.d0) then
            Lz_inj=Lz !+ 5.0*ksi*dt                                                  !sqrt(2)*ksi=1rLarmor and we take 5 as maximum
        else                                                       !in the Maxwll. distribution. the factor 2 is
            !sqfety factor for electrons - 2.0
            ! changed this from 5 larmors to 3 - no need to have such a large box
            if (abs(Lz_disp).gt.0.0) then
                Lz_inj=Lz+2.d0*(3.d0*dsqrt(2.d0)*ksi*dsqrt(spec_params(sp)%T*spec_params(sp)%m))/abs(spec_params(sp)%q)*dsqrt(bx**2+by**2) + Ly*abs(bz/by) + abs(Lz_disp) +10   !because we inject particles at 5rL after Lz.
            else
                Lz_inj=Lz+2.d0*(3.d0*dsqrt(2.d0)*ksi*dsqrt(spec_params(sp)%T*spec_params(sp)%m))/abs(spec_params(sp)%q)*dsqrt(bx**2+by**2)   !because we inject particles at 5rL after Lz.

            endif

        endif                                                      !All particles shud be inside
        if ((int(Lz_inj/dz) + 1).gt.Nz_max) then
            Nz_max = int(Lz_inj/dz) + 1

        endif
        write(*,*) 'Lz_inj:',Lz_inj
        write(*,*) 'Nz_max:',Nz_max

    enddo

    !For lu decomposition
    !n=3*Nz*Ny
    ! domain decomposition
    if (par_size.gt.1.and.domain_decomp) then
        ! here we need to define the slices
        ! this can be a very smart algorithm but for now lets keep it simple
        ! 2 slices in z direction only and the rest in y direction
        if (no_slices_z.gt.0) then
            no_slices(1) = no_slices_z
        else
            no_slices(1) = 2
        endif
        no_slices(2) = par_size/no_slices(1)

        write(*,*) 'Domain decomposition grid:', no_slices(1),no_slices(2)

        y_delta = min(int(float(Ny)/float(no_slices(2))) ,Ny)
        z_delta = min((int(float(Nz_max)/float(no_slices(1))*1.0)),Nz_max)



        do sp=1,no_slices(1)
            slice_boundaries(1,sp) = (sp-1)*z_delta + 1
        enddo
        do sp=1,no_slices(2)
            slice_boundaries(2,sp) = (sp-1)*y_delta + 1
        enddo
        ! fix the last boundary
        slice_boundaries(1,no_slices(1) +1) = Nz_max
        slice_boundaries(2,no_slices(2) +1) = Ny



        write(*,*) 'Domain decomposition enabled'
        write(*,*) 'y_delta: ',y_delta
        write(*,*) 'z_delta: ',z_delta
        i = 0
        j = 1
        do sp=1,par_size

            i = i+1
            if ((sp-1).eq.proc_no) then
                y_start = slice_boundaries(2,i)
                y_stop = slice_boundaries(2,i+1)
                z_start = slice_boundaries(1,j)
                z_stop = slice_boundaries(1,j+1)

            endif
            if (i.eq.no_slices(2)) then
                j = j+1
                i = 0
            endif


        enddo

        write(*,*) proc_no,'y_delta range:',y_start,y_stop
        write(*,*) proc_no,'z_delta range:',z_start,z_stop

    else
        y_start = 1
        y_stop = Ny
        y_delta = Ny -1
        z_start = 1
        z_stop = Nz_max
        z_delta = Nz_max -1
        slice_boundaries(1,1) = 1
        slice_boundaries(1,2) = Nz_max
        slice_boundaries(2,1) = 1
        slice_boundaries(2,2) = Ny

    endif

    ! new periodic n
    n = Nz*Ny

    hzy=dz/dy
    !history combing
    if (history_ntimes.gt.(2*Np)) then
        !too many samples, no combing
        history_ntimes = Np +10
    else if (history_ntimes.eq.0) then
        !forced combing disabled
        history_ntimes = Np +10
    else
        !we need double space for combing to assure history_ntimes samples
        history_ntimes = history_ntimes*2

    endif
    supra_buffer = int(real(Npts)/10.0)


    ! solver init
    if (psolver .eq. 1)  then
        write(*,*) 'Using old iterative Poisson solver (get a cup of cofee this will take time)'
    else if (psolver .eq. 2)  then
        write(*,*) 'Using new direct Poisson solver (good choice :)'
    else if(psolver .eq. 3)  then
        write(*,*) 'Deadly combo of both solvers!'
    else if(psolver .eq. 4)  then
        write(*,*) 'Using MUMPS parallel solver'
    else if(psolver .eq. 5)  then
        write(*,*) 'Using MUMPS parallel solver and forcing distribution of the matrix'
    else  if (psolver.eq.6) then
        write(*,*) 'Using HYPRE parallel solver and forcing distribution of the matrix'
        p_y = no_slices_y
        p_z = no_slices_z

        s_y =  nY / p_y;
        s_z =  nZ / p_z;

        write (*,*) 'PES sizes.', nY, nZ
        write (*,*) 'PES partitioning.', p_y, p_z
        write (*,*) 'PES sizes.', s_y, s_z
        i_r = 1
        do i_z = 1, p_z
            do i_y = 1, p_y
                y_from = 1 + (i_y - 1) * s_y
                y_to = i_y * s_y
                if (i_y .eq. p_y) then
                    y_to = Ny
                endif

                z_from = 1 + (i_z - 1) * s_z
                z_to = i_z * s_z
                if (i_z .eq. p_z) then
                    z_to = Nz
                endif

                if (i_r .eq. proc_no + 1) then
                    y_from_loc = y_from
                    z_from_loc = z_from

                    y_to_loc = y_to
                    z_to_loc = z_to

                    n_y_loc = y_to_loc - y_from_loc + 1
                    n_z_loc = z_to_loc - z_from_loc + 1
                endif



                ! coordinates are switched in the solver
                grid_partitioning(i_r, 1) = 0
                grid_partitioning(i_r, 2) = 0
                grid_partitioning(i_r, 3) = z_from
                grid_partitioning(i_r, 4) = z_to
                grid_partitioning(i_r, 5) = y_from
                grid_partitioning(i_r, 6) = y_to

                i_r = i_r + 1
            enddo
        enddo
        write (*,*) 'Local dim z', z_from_loc, z_to_loc
        write (*,*) 'Spice z', z_start, z_stop
        write (*,*) 'Local dim y', y_from_loc, y_to_loc
        write (*,*) 'Spice y', y_start, y_stop

    endif
    !now fill the data structure

    g_i_s%dt  = dt
    g_i_s%dy  = dy
    g_i_s%dz  = dz
    g_i_s%Lz  = Lz
    g_i_s%Ly  = Ly
    g_i_s%rectangles = rectangles
    g_i_s%triangles = triangles
    g_i_s%circles = circles

    p_i_s%mu = mu
    p_i_s%ksi = ksi
    p_i_s%N0 = N0

    s_i_s%Na = Na
    s_i_s%Npc = Npc
    s_i_s%psolver = psolver
    s_i_s%test_particle = test_particle
    s_i_s%diag_ntimes = diag_ntimes
    s_i_s%proc_no = proc_no
    s_i_s%proc_max = proc_max
    s_i_s%pot_smoothing = pot_smoothing
    s_i_s%dump_period = dump_period
    s_i_s%ang_diag = ang_diag
    s_i_s%ang_bin = ang_bin
    s_i_s%domain_decomp = domain_decomp
    s_i_s%y_delta = y_delta
    s_i_s%y_start = y_start
    s_i_s%y_stop = y_stop
    s_i_s%z_delta = z_delta
    s_i_s%z_start = z_start
    s_i_s%z_stop = z_stop
    s_i_s%coll_test = coll_test




    mks_info%mks_n0 = mks_n0
    mks_info%mks_Te = mks_Te
    mks_info%mks_B = mks_B
    mks_info%mks_main_ion_m = mks_main_ion_m
    mks_info%mks_main_ion_q = mks_main_ion_q
    lambda_D = sqrt(8.85E-12*mks_info%mks_Te/mks_info%mks_n0/1.602E-19)
    w_i = 1.602E-19*mks_B/mks_main_ion_m/1.67E-27

    ! Param1 - Work function of surface. This is what surface receives when it is hit by an electron
    ! Param2 - Ionization energy - Work function + Binding energy - this is what surface receives when it is hit by an ions

    ! defalut value- tungsten
    ! mks_par1 = 4.55
    ! default value Hydrogen on tungsten, binding energy is ~1 eV
    !	mks_par2 = 13.6 -  4.55 + 1.0

    ! need to normalize the Work function given in eV to dimensionless units

    ! normalized by m_i lambda_D^2 w_i^2
    mks_par1 = mks_par1*1.602E-19/lambda_D/lambda_D/w_i/w_i/mks_main_ion_m/1.67E-27
    mks_par2 = mks_par2*1.602E-19/lambda_D/lambda_D/w_i/w_i/mks_main_ion_m/1.67E-27

    write(*,*) 'Normalized work function Wf',mks_par1,mks_par2
    mks_info%mks_par1 = mks_par1
    mks_info%mks_par2 = mks_par2
    mks_info%mks_par3 = mks_par3


    ns = 100
    write(*,*) 'EBS table dimension',ns

    if (debug) then

        write(*,*) 'Calling mainloop'
    endif

    call MAINLOOP(g_i_s,p_i_s,s_i_s,Nv,relax,frelax,tau,P0,PL,time_diag,&
        bx,by,bz,crit_conv,tfile,ofile,&
        debug,verbose,use_hdf5,n,hzy,Nz,Ny,Npts,Np,restore,no_species,proc_max,alpha_yz,alpha_xz,spec_params(1:no_species),history_ntimes,&
        ang_bin,fv_bin,fv_arrays,diag_histories,diag_full_histories,no_diag_reg,diag_regions(1:no_diag_reg),Na,fv_perp_arrays,cont,&
        rectangle_params,circle_params,triangle_params,scenario,k_diff,Nc,driftz,enable_dump,rectangles,circles,triangles,&
        sort_period,supra_buffer,cosalpha_yz,cosalpha_xz,mks_info,Nz_max,no_slices,slice_boundaries,advanced_continue,par_size,ifile,t_max, &
        step_max,Lz_disp,Nz_disp, enable_poisson, MPI_COMM_SOLVER, ps_commsize, ps_ranks, grid_partitioning(1:proc_max+1,:),frozen_potvac, &
        approximate_pot,regroup_runs,have_thermionic,impose_pot,pfile, use_surface_heat_tweaks,use_vtot,thermionic_A,thermionic_Wf, &
        have_impact_diag,collii,collie,collee,no_objects,object_params,n)

    if ((psolver .eq. 4) .or. (psolver.eq.5)) then
        deallocate(ps_ranks)
    endif

    STOP

END PROGRAM SHEATH

!******************************************************************************
!******************************************************************************

SUBROUTINE MAINLOOP(g_i_s,p_i_s,s_i_s,Nv,relax,frelax,tau,P0,PL,time_diag,&
        bx,by,bz,crit_conv,tfile,ofile,&
        debug,verbose,use_hdf5,n,hzy,Nz,Ny,Npts,Np,restore,no_species,proc_max,alpha_yz,alpha_xz,spec_params,&
        history_ntimes,ang_bin,fv_bin,fv_arrays,diag_histories,diag_full_histories,no_diag_reg,diag_regions,Na,&
        fv_perp_arrays,cont,rectangle_params,circle_params,triangle_params,scenario,k_diff,Nc,driftz,enable_dump,&
        rectangles,circles,triangles,sort_period,supra_buffer,cosalpha_yz,cosalpha_xz,mks_info,Nz_max,no_slices, &
        slice_boundaries,advanced_continue,par_size,ifile, t_max, step_max,Lz_disp,Nz_disp, &
        enable_poisson, MPI_COMM_SOLVER, ps_commsize, ps_ranks,grid_partitioning,frozen_potvac,approximate_pot,regroup_runs,have_thermionic,impose_pot,pfile, &
        use_surface_heat_tweaks,use_vtot,thermionic_A,thermionic_Wf,have_impact_diag,collii,collie,collee,no_objects,object_params,ns)

    use pes_mc
    use pes_md
    use pes2d
    use hdf5_spice2
    use spice2_types
    use collisions
    use injection
    !#endif
    use mpi

    implicit none
    !data structure for simulation, grid and particle parameters
    !include "struct.h"

    integer:: rectangles,triangles,circles,no_objects

    type(grid_info):: g_i_s
    type(part_info)::	p_i_s
    type(sim_info)::	s_i_s
    type(pinfo), dimension(no_species):: spec_params
    type(rectangle_t), dimension(rectangles):: rectangle_params
    type(circle_t), dimension(circles):: circle_params
    type(triangle_t), dimension(triangles):: triangle_params
    type(mks_t):: mks_info
    type(object_t), dimension(no_objects):: object_params

    ! Old data structures - what remained
    ! need to get this a bit into order!!!

    real*8:: k_diff,enable_dump,cosalpha_yz,cosalpha_xz
    integer:: N0,Na,Np,Nv,Nz,Ny,Npts,k,history_ntimes,fv_perp_arrays,Nc,highest_z,regroup_runs
    real*8:: dt,dtu,dtz,dty,dz,dy,Lz,Lz1,Ly,Ly1,Ly2,mu,pi,progress,rand,randn
    real*8:: ksi,cosalpha,costheta,cosphi,deltai,deltae
    real*8:: tau
    integer*8:: t1,t2
    real*8:: P0,PL
    real*8 bx,by,bz

    real*8:: sqbx2by2,dt0,cte_geo,cte_geo2
    real*8:: frelax,erreur,erreur2,crit_conv,erreur_max0
    integer:: Ncell
    !!!
    real*8:: dthalf
    integer:: nav
    integer:: count,i,j,jv,jy, loop_stat,l
    integer:: ind_Te
    integer qwallup,qwalldw,qslitup,qslitdw,qbp,nbit,psolver
    logical:: time_diag,relax,frozen_potvac,approximate_pot,impose_pot,use_surface_heat_tweaks

    !MK diag sampling
    integer:: diag_ntimes, diag_per,diag_count
    !RD le 13/01/05   Pour le calcul des fonctions de distibution arbitraires en -z !!
    !---------------------------------------------------------------------------------
    !---
    !End

    real*8, dimension(Nz):: z_g
    real*8, dimension(Ny):: y_g
    real*8, dimension(Ny):: jwtot
    real*8, dimension(Nz,Ny):: charge,Escz,Escy,Pot,Pot_old,Potav,Pot_b4, Potvac,Epar,Eperp
    real*8, dimension(history_ntimes):: t,pot_chi, Esct,dPHIqn
    logical:: verbose, debug,use_hdf5,h5_exists,take_diag,restore,cont,advanced_continue,have_impact_diag
    character(len=2048) :: ofile,tfile,ifile,pfile
    real*8::p_z
    integer*8:: t_start, t_fin, t_fin2, t_grid, t_poiss, t_wt, t_lp, t_inj, t_diag,t_bc, t_sum, t_gd, t_com1, t_com2, t_com3, t_init,t_coll
    integer:: c_rate, c_tmp
    integer*8:: timestamp, t_max, step_max
    integer*8:: t_intro,t_act
    real*8, dimension(12,Np):: time_history
    ! MK 19/08/06 test particle arrays
    integer:: pc,test_particle,count_start
#ifdef SPICE_TRACER
    integer, dimension(:,:), allocatable :: test_particle_i
    real*8, dimension(:,:,:,:), allocatable :: test_particle_data
#endif
    !!! direct poisson solver
    real*8:: hzy
    integer:: n,offset,symbolic,sys,status
    integer*8:: numeric
    type (DMUMPS_STRUC) :: mumpsPar

    integer, dimension(n+1):: Ap,Aprow
    integer, dimension(5*n):: Ai,Airow
    real*8, dimension(5*n)::  Ax_matrix,Axrow
    integer, dimension(n- Nz):: flag
    real*8, dimension(n- Nz):: equipot
    real*8, dimension(n):: chrg,xpot
    real*8, dimension(20):: controllu
    real*8, dimension(90):: info


    real*8:: alpha_yz,alpha_xz

    !MK new data structures
    ! we have now vectors z,y,ux,uy,uz,... containing all particles
    ! + we have vector stype, saying of which type is i-th particle
    ! this type works as an index for q,m,mpi_rank etc.
    !tot_no important - total number of all particles!
    integer :: sp, no_species,tot_no,Npc
    !coordinates and velocities
    real*8, dimension(Npts):: z,y,ux,uy,uz,vtot_init
    ! 256 types of particles should be fine
    integer*2, dimension(Npts):: stype
    !grid-related vars
    ! don't need these any more, just a waste of space
    !real*8, dimension(no_species,Npts):: iz,iy
    integer, dimension(Npts):: iy1,iz1
    !weighting-related vars
    ! we get rid of them, it's faster to re-calculate them
    ! real*8, dimension(Npts):: A11,A12,A21,A22
    real*8,dimension(no_species,Nz_max,Ny):: rho
    real*8, dimension(no_species,Nz,Ny):: vx,vy,vz,vx2,vy2,vz2,vx3,vy3,vz3,dens,dens_tot,temperature,temperature_ave,temperature_ave_tot
    real*8, dimension(Nz,Ny):: rho_tot,rhot
    integer, dimension(no_species,Np):: j_w
    real*8,dimension(no_species,Nv):: vpar
    real*8,dimension(no_species):: dv
    !E field
    ! don't think we need these either but let's go step by step
    !real*8, dimension(Npts):: Ez,Ey
    !species parameters
    real*8, dimension(no_species):: m,q,w,Temp
    integer, dimension(no_species):: mpi_rank,no_part
    integer:: no_tot
    character*120,dimension(no_species):: spec_name,inj_file
    integer:: anp
    !particle injection
    logical, dimension(no_species):: mono
    real*8, dimension(no_species):: Umono, Gammaz0,umin,umax, utherm,N_u_zreal
    integer, dimension(no_species):: N_u_z
    real*8, dimension(no_species,1024) ::  u_z, fu_z
    real*8, dimension(no_species,1024):: X_z,Xu_z
    real*8, dimension(no_species,1024):: ufu_z
    real*8, dimension(no_species):: Iinj,Ninj_fx,Ninj_r
    integer, dimension(no_species):: Ninj
    !MK what is vwall anyway?
    !real*8,dimension(no_species,history_ntimes):: vwall
    real*8, dimension(no_species):: inj_ratio
    integer:: npt
    !time histories
    integer, dimension(no_species,history_ntimes):: number
    ! diag
    real*8, dimension(no_species,Nz,Ny):: q_grid,T_g
    real*8, dimension(no_species,Nz,Ny):: vxav,vyav,vzav,vx2av,vy2av,vz2av,vx3av,vy3av,vz3av
    real*8, dimension(no_species,Nz,Ny):: q_grid_tot,T_g_tot
    real*8, dimension(no_species,Nz,Ny):: vxav_tot,vyav_tot,vzav_tot,vx2av_tot,vy2av_tot,vz2av_tot,vx3av_tot,vy3av_tot,vz3av_tot

    ! leapfrog
    real*8,dimension(no_species):: delta,delta_half, mu2,Lz_inj,Lz_inj_half,midLz
    real*8,dimension(no_species):: Ax_lf,Bx_lf,Cx_lf,Dx_lf,Ex_lf,Ay_lf,By_lf,Cy_lf,Dy_lf,Ey_lf,Az_lf,Bz_lf,Cz_lf,Dz_lf,Ez_lf,Fx_lf,Fy_lf,Fz_lf
    real*8,dimension(no_species):: Ax_half,Bx_half,Cx_half,Dx_half,Ex_half,Ay_half,By_half,Cy_half,Dy_half,Ey_half,Az_half,Bz_half, &
        Cz_half,Dz_half,Ez_half,Fx_half,Fy_half,Fz_half

    !parallelization
    integer par_status(MPI_STATUS_SIZE)
    integer:: proc_no,proc_max,ierr,par_size
    integer, dimension(proc_max +1,history_ntimes):: no_part_proc


    ! Parallel MG Poisson solver
    type (solver_info) :: pes_info

    real*8 :: n_0
    integer, dimension(proc_max+1,6) :: grid_partitioning
    integer, dimension(:,:), allocatable :: flag_m_loc
    real*8, dimension(:,:), allocatable :: pot_m_loc, dens_m_loc, newpot_m_loc, pot_mask_m_loc
    integer :: i_y, i_z, i_r
    integer :: x_to_loc, y_from_loc, y_to_loc, z_from_loc, z_to_loc, y_to, y_from, z_to, z_from, n_loc, n_y_loc, n_z_loc
    integer :: mympi_ierr
    integer :: mympi_status(MPI_STATUS_SIZE)

    !random
    real*8:: rndx
    integer:: dump_period
    !history combing
    integer:: delta_h,h_pos
    real*8:: act_pot_chi,act_dphiqn,act_esct
    !weighting
    integer:: weighting_type
    !angular diagnostics
    logical:: ang_diag
    real*8:: ang_bin
    ! diag regions
    integer:: fv_bin,fv_arrays,diag_histories,diag_full_histories,no_diag_reg
    real*8, dimension (fv_arrays,fv_bin):: fv_ar
    integer, dimension(fv_arrays):: fv_sum
    real*8, dimension (diag_histories,Np):: diag_full_hist
    !!!!!!!!!!HACK Change Nc to Na  -needs to be fixed properly
    real*8, dimension (diag_histories,Np):: diag_hist
    integer, dimension(diag_histories):: hist_limits
    !fv limits
    real*8, dimension(fv_arrays,2):: fv_limits
    !counter
    integer:: fv_count,d_count,hist_count,bin,fv_perp_array_count,bin1,bin2
    !regions
    type(diag_region), dimension(no_diag_reg):: diag_regions
    real*8:: rphi,ralpha,rbeta,u_perp,u_perp1,u_perp2,u_par
    real*8,dimension(fv_perp_arrays,fv_bin,fv_bin):: u_perp_a
    real*8:: fv_surf
    !particle tracking
    integer,dimension(no_diag_reg):: particle_no
    logical:: run_inversion,have_thermionic
    integer:: zi,yi
    real*8:: Eza,Eya,dzi,dyi,ux_temp,uy_temp,uz_temp

    !we record total incident angle and the angle in the z-y plane, normalizing over time 1
    !real*8, dimension(Nz1,int(180.0/ang_bin) +1,no_species):: left_tot_ang,right_tot_ang,left_part_ang,right_part_ang

    ! matrixes for equipotentials
    integer,dimension(Nz,Ny):: flag_m
    integer,dimension(Nz,3*Ny):: triple_flag_m

    real*8,dimension(Nz,Ny):: equipot_m
    real*8,dimension(Nz,3*Ny):: triple_equipot_m

    ! equipot structures
    integer:: no_of_rectangles

    !scenario vars
    integer:: scenario
    integer, dimension(no_species):: injection_method
    integer:: zlow,zhigh,ylow,yhigh

    !collisions
    real*8, dimension(no_species):: kdiff
    real*8, dimension(no_species):: collision_prob

    !timing history
    real*8, dimension(proc_max +1,history_ntimes):: iteration_time_hist
    real*8, dimension(proc_max +1):: proc_power
    real*8, dimension(proc_max +1):: new_injection_rate
    real*8:: irs

    real*8:: total_inj
    !injection rate history
    real*8, dimension(no_species,history_ntimes):: injection_rate_hist


    ! dielectric needed vars
    integer, dimension(Nz,Ny):: objects, edges,orientation
    real*8, dimension(Nz,Ny):: edge_charge
    ! averaging dumping
    real*8, dimension(Nz,Ny):: avepot,avedens
    integer:: scount,ave_buffer
    ! external drift values
    real*8:: driftz
    ! diagnostic vizualization
    integer, dimension(Nz,Ny):: diagm
    real*8:: tmz,tmy
    real*8:: yc,zc,r,ya,yb,za,zb
    integer:: result1,result2,result3
    !    real*8, dimension(Nz,Ny):: se
    ! boundary arrays
    integer, dimension(Nz,Ny):: objects_enum
    real*8, dimension(2,rectangles + circles + triangles,Np):: objects_current,objects_power_flux
    real*8, dimension(2,rectangles + circles + triangles,Np):: objects_current_tot,objects_power_flux_tot
    real*8:: i_rel,i_rel_max, i_rel_treshold
    real*8, dimension(rectangles + circles + triangles):: float_constant
    real*8, dimension(rectangles + circles + triangles,10):: i_rel_history

    ! impact diags
    real*8, dimension(no_species,rectangles + circles + triangles,fv_bin,fv_bin):: impact_diag,impact_diag_tot,emmit_diag,emmit_diag_tot

    ! particle sorting
    integer, dimension(2*Nz):: cell_count
    integer:: max_cell_count,tot_cell_count
    real*8:: sort_period
    !suprathermal particles
    integer, dimension(no_species):: motion_method
    integer:: supra_buffer,s_no,s_no_orig
    real*8, dimension(supra_buffer):: s_y,s_z,s_ux,s_uy,s_uz,s_A11,s_A12,s_A21,s_A22
    integer*2, dimension(supra_buffer):: s_stype
    integer, dimension(supra_buffer):: s_order,s_iy1,s_iz1
    real*8:: supra_dt
    integer:: no_supra_loops
    logical:: emit
    ! edge flux
    real*8, dimension(Nz,Ny,no_species):: edge_flux, edge_energy_flux, edge_velocity_x, edge_velocity_y, edge_velocity_z,edge_flux_ave, edge_energy_flux_ave, edge_velocity_x_ave, edge_velocity_y_ave, edge_velocity_z_ave
    real*8, dimension(Nz,Ny,no_species):: edge_flux_ave_tot, edge_energy_flux_ave_tot, edge_velocity_x_ave_tot, edge_velocity_y_ave_tot, edge_velocity_z_ave_tot

    ! biassing steps
    real*8:: vstart,vstop,v_act,q_therm
    integer:: nbsteps,bstep,bint
    integer:: count_iv_objects
    ! SOL injection
    ! width of the injection region
    integer:: Lsh
    real*8:: W0
    ! injection weight matrix
    real*8, dimension(no_species,Nz,Ny):: SOL_W
    ! number of injected particles
    real*8, dimension(no_species,Nz,Ny):: SOL_Ns,SOL_Nsr
    ! other stuff
    real*8, dimension(no_species,Nz,Ny):: SOL_a,SOL_b,SOL_asq,SOL_bsq,SOL_bminusa,SOL_dW
    integer:: nobjects
    ! use compression in MAT files
    integer:: use_compression

    ! domain decomposition
    logical:: domain_decomp,have_dielectrics,use_vtot
    integer:: y_start,y_stop,y_delta,def_sp,y_stop_large,z_delta,z_start,z_stop,z_start_box,z_stop_box,y_start1,y_stop1,z_start1,z_stop1
    real*8:: Ly_start,Ly_stop
    integer, dimension(no_species):: stype_transfer
    integer:: Nz_max
    real*8, dimension(Nz,Ny):: surface_matrix
    integer, dimension(Nz_max,Ny):: slice_proc
    logical:: enable_poisson, poisson_distributed, have_iv
    integer, intent(in) :: MPI_COMM_SOLVER
    integer, intent(in) :: ps_commsize
    integer, dimension(ps_commsize), intent(in) :: ps_ranks

    ! enhanced BC
    integer, dimension(Nz,Ny):: bc_matrix
    integer,dimension(2):: no_slices
    integer,dimension(2,99):: slice_boundaries
    integer, dimension(8):: neigh_slices

    ! SPICE MONITOR
    character*120:: machine,spice_user,perc_str,nproc_str,due_str

    real*8:: Lz_low_limit,Lz_high_limit,potential_lambda
    !BC displacement
    real*8:: Lz_disp
    integer:: Nz_disp
    ! AP Debug variables
    integer*8 :: exitAfter
    integer :: circylow, circzlow, circyhigh, circzhigh
    real*8, dimension(Nz,Ny):: approximate_potential
    real*8:: dist,nearest_dist,nearest_pot
    ! energy diagnostics
    real*8, dimension(no_species,4,Np):: total_energy
    real*8:: lambda_D,thermionic_A,thermionic_Wf,w_i
    real*8, dimension(Nz,Ny,no_species):: thermionic_prob
    ! surface temperature for thermionic emission
    real*8, dimension(Nz,Ny):: Ts
    ! Coulomb collisions
    real*8:: collii,collie,collee
    integer:: cell_count_max
    real*8:: E_tot_after, E_tot_before,px_before,py_before,pz_before,px_after,py_after,pz_after
    logical:: beam_switch
    integer:: ns
    ! expect up to 5 different materials
    real*8, dimension(5,ns):: f_ebs,cdf_ebs
    real*8, dimension(ns):: Eout

    external G05CAF,G05CCF,G05DDF,G05FAF,G05FDF
    call gettime(timestamp)

    write(*,*) 'Survived mainloop jump'
    no_objects =  rectangles + circles + triangles
    machine = '                     '
    spice_user = '                     '
    perc_str = '                     '
    due_str = '                     '
    nproc_str = '                     '
    t_intro = 0
    beam_switch = .false.
    if (enable_poisson.eq..false.) then
        write(*,*) '---------- WARNING ----------'
        write(*,*) 'Poisson equation solver is disabled!'
        write(*,*) '----------/WARNING/----------'
    endif

    call gettime(t_intro)

    write(*,*) 'Initial timestamp',t_intro

    ! AP debug
    exitAfter = 520
    h5_exists = .false.

    if (have_thermionic) then
        write(*,*) 'Thermionic injection will be used'
    endif

    ! SPICE
    neigh_slices = -1
    thermionic_prob = 0
    ! AP 03/17/2014: This is now set before the mainloop jump
    !enable_poisson = .true.
    poisson_distributed = .false.
    have_dielectrics = .false.
    have_iv = .false.

    t_init = timestamp
    ! Nz_max = 0
    surface_matrix = 0.0
    slice_proc = 0
    ! by default use compression
    use_compression = 0
    ! biasing steps definition
    nbsteps = 400
    vstart = -10.0
    vstop = 10.0
    ! AP update vstop to shorten time
    vstop = 1.0
    bstep = 0
    count_iv_objects = 0
    bint = int(real(Np - Na)/real(nbsteps))
    v_act = vstart
    !let's get the data out of data structures
    SOL_W = 0.0
    SOL_Ns = 0.0
    SOL_Nsr = 0.0
    t_com2 = 0
    SOL_a = 0.0
    SOL_b = 0.0
    SOL_asq = 0.0
    SOL_bsq = 0.0
    SOL_bminusa = 0.0
    SOL_dW = 0.0
    ! 10 cells for the source should be enough
    Lsh = 20
    ! This parameter is overwritten by w in species definition
    W0 = 1.0
    proc_no = 0
    act_pot_chi = 1.0

    edge_flux = 0.0
    edge_energy_flux = 0.0
    edge_velocity_x = 0.0
    edge_velocity_y = 0.0
    edge_velocity_z = 0.0
    edge_flux_ave = 0.0
    time_history = 0.0
    edge_energy_flux_ave = 0.0
    edge_velocity_x_ave = 0.0
    edge_velocity_y_ave = 0.0
    edge_velocity_z_ave = 0.0

    edge_energy_flux_ave_tot = 0.0
    edge_velocity_x_ave_tot = 0.0
    edge_velocity_y_ave_tot = 0.0
    edge_velocity_z_ave_tot = 0.0
    if (have_impact_diag) then
        impact_diag= 0.0
        impact_diag_tot = 0.0
        emmit_diag= 0.0
        emmit_diag_tot = 0.0

    endif

    Ninj_r = 0
    objects_enum = 0
    objects_current = 0.0
    objects_current_tot = 0.0
    objects_power_flux = 0.0
    objects_power_flux_tot = 0.0

    temperature = 0.0
    temperature_ave = 0.0
    temperature_ave_tot = 0.0

    i_rel_history = 0.0
    s_no = 0
    Ly1 = 0.0
    total_energy = 0.0
    dt =	g_i_s%dt
    dy =	g_i_s%dy
    dz =	g_i_s%dz
    Lz =	g_i_s%Lz
    Ly =	g_i_s%Ly
    rectangles = g_i_s%rectangles
    triangles = g_i_s%triangles
    circles = g_i_s%circles
    ! Lzi = 	g_i_s%Lzi
    ! Lze =	g_i_s%Lze
    mu =	p_i_s%mu
    ksi =	p_i_s%ksi
    N0 =	p_i_s%N0

    Na =	s_i_s%Na
    Npc =	s_i_s%Npc
    domain_decomp = s_i_s%domain_decomp
    y_delta = s_i_s%y_delta
    y_start = s_i_s%y_start
    y_stop = s_i_s%y_stop
    z_delta = s_i_s%z_delta
    z_start = s_i_s%z_start
    z_stop = s_i_s%z_stop
    z_start_box = min(z_start,Nz)
    z_stop_box = min(z_stop,Nz)
    y_stop_large = min(y_stop + 1,Ny)

    dump_period = s_i_s%dump_period
    psolver =	s_i_s%psolver
    test_particle =	s_i_s%test_particle
    diag_ntimes = s_i_s%diag_ntimes
    proc_no = s_i_s%proc_no
    proc_max = s_i_s%proc_max
    Ly_start = max(0.0,float(y_start -1)*dy)
    Ly_stop = min(float(y_stop - 1)*dy,Ly)
    write(*,*) 'Defined Ly int:',Ly_start,Ly_stop
    write(*,* ) 'Nz_max:', Nz_max

    cell_count_max = Npc*10
    ! construct the slice index matrix
    i = 0
    j = 1
    do sp=0,proc_max
        i = i+1
        y_start1 = slice_boundaries(2,i)
        y_stop1 = slice_boundaries(2,i+1)
        z_start1 = slice_boundaries(1,j)
        z_stop1 = slice_boundaries(1,j+1)
        write(*,*) 'Slice proc:',y_start1,y_stop1,z_start1,z_stop1
        slice_proc(z_start1:z_stop1,y_start1:y_stop1) = sp
        if (i.eq.no_slices(2)) then
            j = j+1
            i = 0
        endif

    enddo

#ifdef SPICE_TRACER
    if (test_particle.gt.0) then

        allocate(test_particle_data(Np,no_species,test_particle,7))
        allocate(test_particle_i(no_species,test_particle))

        !do i = 1,test_particle
        !    test_particle_i(i) = i
        !enddo

        test_particle_i = 0
        test_particle_data = 0


        ! overriding sort period to keep particles trackable
        sort_period = 0
    endif
#endif
    ! write some info
    ! write(*,*) 'lines',slice_proc(40,:)
    ! write(*,*) 'lines',slice_proc(90,:)

    ! find neighbors
    if (proc_max.gt.0) then
        if (z_stop.lt.Nz_max.and.y_start.gt.1) then
            neigh_slices(1) = slice_proc(z_stop+1,y_start-1)
        elseif     (z_stop.lt.Nz_max) then
            neigh_slices(1) = slice_proc(z_stop+1,Ny)
        endif

        if (z_stop.lt.Nz_max) then
            neigh_slices(2) = slice_proc(z_stop+1,y_start+1)
        endif

        if (z_stop.lt.Nz_max.and.y_stop.lt.Ny) then
            neigh_slices(3) = slice_proc(z_stop+1,y_stop+1)
        elseif (z_stop.lt.Nz_max) then
            neigh_slices(3) = slice_proc(z_stop+1,1)
        endif

        if (y_start.gt.1) then
            neigh_slices(4) = slice_proc(z_start+1,y_start-1)
        else
            neigh_slices(4) = slice_proc(z_start+1,Ny)

        endif
        if (y_stop.lt.Ny) then
            neigh_slices(5) = slice_proc(z_start+1,y_stop+1)
        else
            neigh_slices(5) = slice_proc(z_start+1,1)

        endif

        if (y_start.gt.1.and.z_start.gt.1) then
            neigh_slices(6) = slice_proc(z_start-1,y_start-1)
        elseif (z_start.gt.1) then
            neigh_slices(6) = slice_proc(z_start-1,Ny)

        endif
        if (z_start.gt.1) then
            neigh_slices(7) = slice_proc(z_start-1,y_start+1)
        endif
        if (z_start.gt.1.and.y_stop.lt.Ny) then
            neigh_slices(8) = slice_proc(z_start-1,y_stop+1)
        elseif (z_start.gt.1) then
            neigh_slices(8) = slice_proc(z_start-1,1)

        endif
        write(*,*) proc_no,'Neighborhooding cells:',neigh_slices(1),neigh_slices(2),neigh_slices(3)
        write(*,*) proc_no,'Neighborhooding cells:',neigh_slices(4),'---',neigh_slices(5)
        write(*,*) proc_no,'Neighborhooding cells:',neigh_slices(6),neigh_slices(7),neigh_slices(8)
    else
        neigh_slices = 0
    endif
    ! prepare stype transfers
    def_sp = proc_no + 1
    do sp=1,no_species
        !        stype_transfer(sp) = def_sp
        !       if (sp.eq.int(sp/(proc_max+1))*(proc_max + 1)) then
        !            def_sp = def_sp + proc_max + 1
        !       endif
        stype_transfer(sp) = sp
        write(*,*) proc_no,"stype transfer",sp ,stype_transfer(sp)
    enddo


    ! pre-calculate the table for EBS exit energy
    Eout = 0.0
    f_ebs = 0.0
    cdf_ebs = 0.0
    call  precalculate_f_ebs(ns,Eout,f_ebs(1,:),cdf_ebs(1,:),0)
    call  precalculate_f_ebs(ns,Eout,f_ebs(2,:),cdf_ebs(2,:),1)


    !ang_diag = s_i_s%ang_diag
    !ang_bin = s_i_s%ang_bin
    if (debug) then
        write(*,*) 'Survived var. declaration in mainloop'
    endif
    !write(*,*) 'This is proc', proc_no
    iteration_time_hist = 0.0
    injection_rate_hist = 0.0
    float_constant = 0.5
    ! reduce the constant for thermionic cases
    if (have_thermionic) then
        float_constant = 0.125
    endif

    diagm = 0
    weighting_type = 0
    rho_tot = 0.0
    rhot = 0.0
    number = 0
    delta_h = 1
    h_pos = 1
    u_perp_a = 0.0
    particle_no = -1
    run_inversion= .false.
    !MK diagnostic sampling
    diag_per = int((Np - Na)/diag_ntimes)
    !fix for small simulations
    if (diag_per < 1) then
        diag_per = 1
        diag_ntimes = Np - Na
    endif
    take_diag = .false.
    diag_count = 0
    no_part_proc = 0
    write(*,*)'Diagnostics will be taken every',diag_per,'step'
    write(*,*)'Total number of diagnostics samples:',diag_ntimes
    ! fill diagm to see where are the diags
    do d_count=1,no_diag_reg
        do i=1,Nz
            do j=1,Ny
                tmz = real(i - 1)*dz
                tmy = real(j - 1)*dy
                if (tmz.le.(diag_regions(d_count)%z_high).and.tmz.ge.(diag_regions(d_count)%z_low).and.tmy.le.&
                        (diag_regions(d_count)%y_high).and.tmy.ge.(diag_regions(d_count)%y_low)) then
                    ! thi is the right grid point
                    diagm(i,j) = d_count
                endif
            enddo
        enddo
    enddo

    no_supra_loops = 0
    ! copy out some specie params for faster access
    do sp=1,no_species
        spec_name(sp) = spec_params(sp)%name
        inj_file(sp) = spec_params(sp)%injection_file
        !make the old data structures map to the new ones
        m(sp) = spec_params(sp)%m
        mono(sp)  = spec_params(sp)%mono
        Umono(sp)  = spec_params(sp)%Umono

        Temp(sp) = spec_params(sp)%T
        write(*,*) 'Temperature::',sp,Temp(sp)
        q(sp) = spec_params(sp)%q
        mpi_rank(sp) = spec_params(sp)%mpi_rank
        inj_ratio(sp) = spec_params(sp)%injection_rate
        injection_method(sp) = spec_params(sp)%injection_method
        write(*,*) 'Injection method::',sp,injection_method(sp)
        if (spec_params(sp)%motion_method.eq.2.and.no_supra_loops.lt.int(dsqrt(Temp(sp)))) then
            no_supra_loops = int(dsqrt(Temp(sp)))
        endif
    enddo

    avepot = 0.0
    avedens = 0.0
    scount = 1
    ave_buffer = 0
    !Initiate RNGs:
    call G05CCF
    call G05FAF(0.d0,1.d0,1,rndx)
    write(*,*) 'First random number',rndx

    !debug print
    if (debug) then
        !writeout the g_i_s
        write(*,*)'g_i_s',g_i_s
        write(*,*)'p_i_s',p_i_s
        write(*,*)'s_i_s',s_i_s
    endif

    do sp=1,no_species
        delta(sp)=1+(1/m(sp))**2*dt**2/4.d0
    enddo
    !diffusion coefficient
    if (k_diff.ne.0.0) then
        do sp=1,no_species

            kdiff(sp) = m(sp)*10.0/k_diff
            write(*,*) 'Col. rate',sp,kdiff(sp)
            collision_prob(sp) =1.0 - exp( - dt/kdiff(sp))
            write(*,*) 'Col. prob',collision_prob(sp)
        enddo
    else
        collision_prob  = 0.0
    endif

    pi=dacos(-1.d0)
    ! calculate sizes of injection boxes
    do sp=1,no_species
        write(*,*) 'Calculating injection box for species',sp
        if (alpha_yz.eq.90.d0.and.alpha_xz.eq.90.d0) then
            Lz_inj(sp)=Lz !+ 5.0*ksi*dt                                                  !sqrt(2)*ksi=1rLarmor and we take 5 as maximum
        else                                                       !in the Maxwll. distribution. the factor 2 is
            !sqfety factor for electrons - 2.0
            if (abs(Lz_disp).gt.0.0) then
                Lz_inj(sp)=Lz+2.d0*(3.d0*dsqrt(2.d0)*ksi*dsqrt(Temp(sp)*m(sp)))/abs(q(sp))*dsqrt(bx**2+by**2) + Ly*abs(bz/by) + abs(Lz_disp) +10  !because we inject particles at 5rL after Lz.
                midLz(sp) = Lz + (3.d0*dsqrt(2.d0)*ksi*dsqrt(Temp(sp)*m(sp)))/abs(q(sp))*dsqrt(bx**2+by**2) + abs(Lz_disp) + 10
            else
                Lz_inj(sp)=Lz+2.d0*(3.d0*dsqrt(2.d0)*ksi*dsqrt(Temp(sp)*m(sp)))/abs(q(sp))*dsqrt(bx**2+by**2)   !because we inject particles at 5rL after Lz.
                midLz(sp) = Lz + (3.d0*dsqrt(2.d0)*ksi*dsqrt(Temp(sp)*m(sp)))/abs(q(sp))*dsqrt(bx**2+by**2)
            endif

        endif                                                      !All particles shud be inside
        write(6,*) 'Lz_inj=',Lz_inj(sp)
        write(6,*) 'Mid_Lz=',MidLz

        if ((int(Lz_inj(sp)/dz) + 1).gt.Nz_max) then
            ! Nz_max = int(Lz_inj(sp)/dz) + 1

        endif
    enddo

    !Define the 2D spatial grid.
    !===========================
    !BC - The left hand boundary between z=0 and z=Lz1 is a perfectly absorbing wall with a slit
    !BC   between y=Ly1 and y=Ly2.
    !BC - The right hand boundary at z=Lz is the unperturbed plasma.
    !z=0.d0
    do i=1,Nz
        z_g(i)=dz*(i-1)
    enddo
    !BC - The bottom boundary at y=0 is periodic.
    !BC - The upper boundary at y=Ly is periodic.
    !y=0.d0
    do i=1,Ny
        y_g(i)=dy*(i-1)
    enddo
    Pot = 0.0
    GAMMAz0 = 0.0
    u_z = 0.0
    N_u_z = 0.0
    fu_z = 0.0
    X_z = 0.0
    Xu_z = 0.0
    ufu_z = 0.0
    count_start = 1
    !Define vectors for ions.
    ! MK 14/08/06 don't define what we don't need

    ! Bbzi=0.d0     !also for LeapFrog
    ! Bbyi=0.d0     !also for LeapFrog
    ! Bbxi=0.d0     !also for LeapFrog

    !Define vectors for electrons.
    ! uzee=0.d0     !idem for subroutine VELOCITY
    ! Bbze=0.d0     !also for LeapFrog
    ! ! uyee=0.d0     !idem for subroutine VELOCITY
    ! Bbye=0.d0     !also for LeapFrog
    ! uxee=0.d0     !idem for subroutine VELOCITY
    ! Bbxe=0.d0     !also for LeapFrog

    !=====================================================================================================1
    !Flux calculation for particle injection
    !===================================
    do sp=1,no_species

        write(*,*) 'Injection of ', trim(spec_name(sp))
        umin(sp)=-5.d0*dsqrt(2*Temp(sp))*ksi/dsqrt(m(sp))   !-5.d0*maxi=5.d0*dsqrt(2*tau)*ksi
        utherm(sp)=dsqrt(Temp(sp))*ksi/dsqrt(m(sp))              !ion thermal speed
        write(6,*) 'Utherm=',utherm(sp)
        ! check against the work function
        write(*,*),'Wf check',0.5*utherm(sp)*utherm(sp)

        no_part(sp)=0                    !no particle in the cells at t=0
        write(6,*) 'N (initial)=',no_part(sp)
        !====================
        !in the z-direction =
        !====================
        if (mono(sp)) then          !NEW RD
            GAMMAz0(sp)=-Umono(sp)       ! bcz no=1
        else
            if (spec_params(sp)%injection_method.le.10) then
                open(10,file=trim(inj_file(sp))//char(0),FORM ="formatted")
                read(10,*) N_u_zreal(sp)
                write(6,*) ' N_u_zreal ',N_u_zreal(sp)
                N_u_z(sp)=idint(N_u_zreal(sp))
                write(6,*) ' N_u_z ',N_u_z(sp)
                read(10,*) u_z(sp,1:N_u_z(sp))
                u_z(sp,:)=u_z(sp,:)*ksi  !Velocities come from QPIC with diff. norm. !!!
                !!!! HARD ELECTRON HACK !!!
                ! NEEDS PROPER FIX, DEBUG SOLUTION
                if (q(sp).lt.0.0) then
                    u_z(sp,:)=u_z(sp,:)*dsqrt(mu/200.d0)
                else
                    ! ions can have different masses as well
                    !    u_z(sp,:)=u_z(sp,:)/dsqrt(m(sp))
                    !u_z(sp,:)=u_z(sp,:)*dsqrt(1.0/m(sp))


                endif
                read(10,*) fu_z(sp,1:N_u_z(sp))
                close(10)
                !debug - sflux
                ! 		if (debug) then
                !  			write(*,*) 'fu_z',sp,fu_z(sp,1:N_u_z(sp))
                ! 			write(*,*) 'u_z',sp,u_z(sp,1:N_u_z(sp))
                ! 		endif
                call SFLUX(u_z(sp,1:N_u_z(sp)),N_u_z(sp),fu_z(sp,1:N_u_z(sp)),X_z(sp,1:N_u_z(sp)),Xu_z(sp,1:N_u_z(sp)),&
                    ufu_z(sp,1:N_u_z(sp)),GAMMAz0(sp))
                !debug - sflux
                if (debug) then
                    ! 	 	write(*,*) 'fu_z',sp,fu_z(sp,1:N_u_z(sp))
                    ! 	write(*,*) 'X_z',sp,X_z(sp,1:N_u_z(sp))
                    ! 	write(*,*) 'Xu_z',sp,Xu_z(sp,1:N_u_z(sp))
                    ! 	write(*,*) 'ufu_z',sp,ufu_z(sp,1:N_u_z(sp))
                endif
            endif

        endif


    enddo

    ! SOL injection initialization
    write(*,*) 'Preparing the SOL injection matrixes'
    call prepare_SOL_injection(SOL_W,SOL_a,SOL_b,SOL_asq,SOL_bsq,SOL_bminusa,SOL_Ns,SOL_dW,Nz,Ny,no_species,ksi,dt,bz,dz,Lz,Lsh,N0,W0,inj_ratio,scenario,injection_method,spec_params)


    !====================================End of ions injection module=====================================2

    numeric = 0
    !Initialize diagnostic variables.
    Escz=0.d0   !electric field on the grid
    Escy=0.d0   !electric field on the grid
    Epar=0.d0   !time-averaged electric field on the grid
    Eperp=0.d0  !time-averaged electric field on the grid
    Pot=0.d0    !potential on the grid
    Potav=0.d0  !time-averaged potential on the grid
    Pot_b4=0.d0 !
    !MK diag sampling
    nav=0       !number of time steps in average
    qwallup=0         !wall charge up
    qwalldw=0         !wall charge down
    qslitup=0         !slit charge up
    qslitdw=0         !slit charge down
    qbp=0             !back plate charge
    Esct=0.d0         !Electric field at z=Lz-1
    dPHIqn=0.d0       !Delta_Phi in quasi-neutral area
    !angles
    ralpha = -alpha_yz/180.0*3.141591
    rbeta = (90.0 -alpha_xz)/180*3.14159
    write(*,*) 'Rotation angles:'
    write(*,*) 'ralpha:',ralpha
    write(*,*) 'rbeta:',rbeta,alpha_xz

    stype = 0

    do sp=1,no_species
        dens(sp,:,:)=0.d0  !ion density on the grid
        j_w(sp,:)=0.d0
        vz(sp,:,:)=0.d0    !instantaneous <vi> on the grid
        vy(sp,:,:)=0.d0    !instantaneous <vi> on the grid
        vx(sp,:,:)=0.d0    !instantaneous <vi> on the grid
        vzav(sp,:,:)=0.d0  !time-averaged <vi> on the grid
        vyav(sp,:,:)=0.d0  !time-averaged <vi> on the grid
        vxav(sp,:,:)=0.d0  !time-averaged <vi> on the grid
        vzav_tot(sp,:,:)=0.d0  !time-averaged <vi> on the grid
        vyav_tot(sp,:,:)=0.d0  !time-averaged <vi> on the grid
        vxav_tot(sp,:,:)=0.d0  !time-averaged <vi> on the grid

        vz2(sp,:,:)=0.d0   !instantaneous <vi^2> on the grid
        vy2(sp,:,:)=0.d0   !instantaneous <vi^2> on the grid
        vx2(sp,:,:)=0.d0   !instantaneous <vi^2> on the grid
        vz2av(sp,:,:)=0.d0 !time-averaged <vi^2> on the grid
        vy2av(sp,:,:)=0.d0 !time-averaged <vi^2> on the grid
        vx2av(sp,:,:)=0.d0 !time-averaged <vi^2> on the grid
        vz2av_tot(sp,:,:)=0.d0 !time-averaged <vi^2> on the grid
        vy2av_tot(sp,:,:)=0.d0 !time-averaged <vi^2> on the grid
        vx2av_tot(sp,:,:)=0.d0 !time-averaged <vi^2> on the grid

        vz3(sp,:,:)=0.d0   !instantaneous <vi^3> on the grid
        vy3(sp,:,:)=0.d0   !instantaneous <vi^3> on the grid
        vx3(sp,:,:)=0.d0   !instantaneous <vi^3> on the grid
        vz3av(sp,:,:)=0.d0 !time-averaged <vi^3> on the grid
        vy3av(sp,:,:)=0.d0 !time-averaged <vi^3> on the grid
        vx3av(sp,:,:)=0.d0 !time-averaged <vi^3> on the grid
        vz3av_tot(sp,:,:)=0.d0 !time-averaged <vi^3> on the grid
        vy3av_tot(sp,:,:)=0.d0 !time-averaged <vi^3> on the grid
        vx3av_tot(sp,:,:)=0.d0 !time-averaged <vi^3> on the grid

        dv(sp)=(umax(sp)-umin(sp))/Nv    !ion speed bin size for distribution function
        do i=1,Nv
            vpar(sp,i)=umin(sp)-dv(sp)/2+dv(sp)*i
        enddo
        number(sp,:)=0     !total number of ions on the grid
        Iinj(sp)=0.d0        !ion current injected from plasma at z=Lz
        ! 	vwall(sp,:)=0.d0       !???
        Ninj(sp)=0       !number of injected ions (real!)

    enddo

    t=0.d0
    if (abs(Lz_disp).gt.0.0) then
        Ly1 = abs(Lz_disp)/abs(bz/by)
        ! if (Ly1.gt.Ly) then
        !      write(*,*) 'Error in BC jump configuration'
        !      write(*,*) 'Ly',Ly
        !      write(*,*) 'Ly1',Ly1
        !      write(*,*) 'Ly has to be larger than Ly1, increase the box width'
        ! stop
        !      endif
    else
        Ly1 = 0.0
    endif
    write(*,*) 'Ly1: ',Ly1
    lambda_D = sqrt(8.85E-12*mks_info%mks_Te/mks_info%mks_n0/1.602E-19)
    w_i  = 1.602E-19*mks_info%mks_B/mks_info%mks_main_ion_m/1.67E-27
    do sp=1,no_species

        write(*,*) 'Injection ratio for ',spec_name(sp), sp

        if (spec_params(sp)%injection_method.eq.5) then
            ! thermionic emission - breaks the normalization
            ! calculating the emitted flux based on MKS parameters

            ! following the richardson's formula j = A*T^2*exp(-Wf/kT)
            ! A = 60E4 A/m2/K2 effective Richardson constant
            ! Wf = 4.55 eV based on experiment for tungsten
            write(*,*) 'Thermionic factor A*T^2*exp(-Wf/kT)',thermionic_A*(spec_params(sp)%T*mks_info%mks_Te*11600)**2*exp(-thermionic_Wf/(spec_params(sp)%T*mks_info%mks_Te))
            Ninj_fx(sp)=thermionic_A/1.602E-19*(spec_params(sp)%T*mks_info%mks_Te*11600)**2*exp(-thermionic_Wf/(spec_params(sp)%T*mks_info%mks_Te))*inj_ratio(sp)*dt*Ly*N0/(mks_info%mks_n0*lambda_D*(1.602E-19*mks_info%mks_B/mks_info%mks_main_ion_m/1.67E-27 ))

        else
            ! the usual injection
            Ninj_fx(sp)=inj_ratio(sp)*abs(GAMMAz0(sp)*N0*dt*Ly*bz)
        endif
        ! fix for BC jump
        if (Ly1.gt.0.0.and.by.lt.0.0) then
            Ninj_fx(sp) = Ninj_fx(sp)* (Ly + Ly1)/Ly
        elseif (Ly1.gt.0.0.and.by.gt.0.0) then
            Ninj_fx(sp) = Ninj_fx(sp)* (Ly-Ly1)/Ly
        endif
        write(6,*) 'Ninj_fx',Ninj_fx(sp),'   GAMMAz0',GAMMAz0(sp)
        write(*,*) 'Inj ratio',inj_ratio(sp)
        !	write(*,*) 'Check:',inj_ratio(sp),abs(GAMMAz0(sp)),N0,dt,Ly,bz
        if (spec_params(sp)%injection_method.eq.3) then
            Ninj_fx(sp) = 0.0
        endif

    enddo


    !Time-step for leapfrog        !NEW
    dt0=-dt/2.d0
    dthalf=dt0
    dtu=dt
    dtz=dt

    !Number of cells in the simul. region
    Ncell=Nz*Ny
    if (debug) then
        write(*,*) 'Total number of cells',Ncell
    endif
    !For LF optimization
    ! diag regions - set limits for fvs and init histories
    ! we take -10*utherm..10*utherm
    fv_count =0
    hist_count = 0
    fv_ar = 0.0
    do d_count=1,no_diag_reg
        if ((diag_regions(d_count)%record_property.eq.2).or.(diag_regions(d_count)%record_property.eq.3)) then
            fv_count = fv_count + 1
            fv_limits(fv_count,1) = -10.0*abs(utherm(diag_regions(d_count)%specie))
            fv_limits(fv_count,2) = 10.0*abs(utherm(diag_regions(d_count)%specie))
            fv_count = fv_count + 1
            fv_limits(fv_count,1) = -10.0*abs(utherm(diag_regions(d_count)%specie))
            fv_limits(fv_count,2) = 10.0*abs(utherm(diag_regions(d_count)%specie))
            fv_count = fv_count + 1
            fv_limits(fv_count,1) = -10.0*abs(utherm(diag_regions(d_count)%specie))
            fv_limits(fv_count,2) = 10.0*abs(utherm(diag_regions(d_count)%specie))
        else if (diag_regions(d_count)%record_property.eq.5) then
            fv_count = fv_count + 1
            fv_limits(fv_count,1) = -10.0*abs(utherm(diag_regions(d_count)%specie))
            fv_limits(fv_count,2) = 10.0*abs(utherm(diag_regions(d_count)%specie))
        else if (diag_regions(d_count)%record_property.eq.7) then
            !resonant angle
            fv_count = fv_count + 1
            fv_limits(fv_count,1) = -3.5/2.0
            fv_limits(fv_count,2) = 3.5/2.0
        else if (diag_regions(d_count)%record_property.eq.8) then
            ! set directly in diag module
        else if (diag_regions(d_count)%record_property.eq.4) then
            diag_hist(hist_count+1,:) =  0.0
            diag_hist(hist_count+2,:) =  0.0
            diag_hist(hist_count+3,:) =  0.0


            hist_count = hist_count + 3
        else
            !zero out the history
            hist_count = hist_count + 1
            diag_hist(hist_count,:) =  0.0
        endif
    enddo
    hist_limits = 1
    !write(*,*) 'f(v) limit check',fv_limits(1,1),fv_limits(1,2)

    !!!!!!!!a virer une fois le changement fait dans le LF!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    sqbx2by2=dsqrt(bx**2+by**2)
    cte_geo=dz**2*dy**2/2.d0/(dz**2+dy**2)
    cte_geo2=0.5d0/(dz**2+dy**2)
    delta = 0.0
    delta_half = 0.0
    write(*,*) 'Supra loops:', no_supra_loops
    do sp=1,no_species
        if (spec_params(sp)%motion_method.eq.0.or.spec_params(sp)%motion_method.eq.1.or.spec_params(sp)%motion_method.eq.3) then
            call leapfrog_init(mu2(sp),m(sp),delta(sp),dt0,dtu,bx,by,bz,midLz(sp),q(sp),ksi,Lz_inj(sp),Lz,Ax_lf(sp),&
                Bx_lf(sp),Cx_lf(sp),Dx_lf(sp),Ex_lf(sp),Ay_lf(sp),By_lf(sp),Cy_lf(sp),Dy_lf(sp),Ey_lf(sp),Az_lf(sp),Bz_lf(sp),Cz_lf(sp),&
                Dz_lf(sp),Ez_lf(sp),Fx_lf(sp),Fy_lf(sp),Fz_lf(sp))
            !------------INJ---------------------------------

            call leapfrog_init(mu2(sp),m(sp),delta_half(sp),dt0,dthalf,bx,by,bz,midLz(sp),q(sp),ksi,Lz_inj(sp),Lz,Ax_half(sp)&
                ,Bx_half(sp),Cx_half(sp),Dx_half(sp),Ex_half(sp),Ay_half(sp),By_half(sp),Cy_half(sp),Dy_half(sp),Ey_half(sp),Az_half(sp)&
                ,Bz_half(sp),Cz_half(sp),Dz_half(sp),Ez_half(sp),Fx_half(sp),Fy_half(sp),Fz_half(sp))
        else if (spec_params(sp)%motion_method.eq.2) then
            call leapfrog_init(mu2(sp),m(sp),delta(sp),dt0/real(no_supra_loops),dtu/real(no_supra_loops),bx,by,bz,midLz(sp), &
                q(sp),ksi,Lz_inj(sp),Lz,Ax_lf(sp),&
                Bx_lf(sp),Cx_lf(sp),Dx_lf(sp),Ex_lf(sp),Ay_lf(sp),By_lf(sp),Cy_lf(sp),Dy_lf(sp),Ey_lf(sp),Az_lf(sp),Bz_lf(sp),Cz_lf(sp),&
                Dz_lf(sp),Ez_lf(sp),Fx_lf(sp),Fy_lf(sp),Fz_lf(sp))
            !------------INJ---------------------------------

            call leapfrog_init(mu2(sp),m(sp),delta_half(sp),dt0/real(no_supra_loops),dthalf/real(no_supra_loops),bx,by,bz,&
                midLz(sp),q(sp),ksi,Lz_inj(sp),Lz,Ax_half(sp)&
                ,Bx_half(sp),Cx_half(sp),Dx_half(sp),Ex_half(sp),Ay_half(sp),By_half(sp),Cy_half(sp),Dy_half(sp),Ey_half(sp),Az_half(sp)&
                ,Bz_half(sp),Cz_half(sp),Dz_half(sp),Ez_half(sp),Fx_half(sp),Fy_half(sp),Fz_half(sp))


        endif

    enddo



    if (debug) then
        !printout leapfrog coeficients
        write(*,*) 'LeapFrog coeficients: ions'
        write(*,*) Ax_lf(1),Bx_lf(1),Cx_lf(1),Dx_lf(1),Ex_lf(1)
        write(*,*) Ay_lf(1),By_lf(1),Cy_lf(1),Dy_lf(1),Ey_lf(1)
        write(*,*) Az_lf(1),Bz_lf(1),Cz_lf(1),Dz_lf(1),Ez_lf(1)
        write(*,*) 'Half coeficients - for wind back'
        write(*,*) Ax_half(1),Bx_half(1),Cx_half(1),Dx_half(1),Ex_half(1)
        write(*,*) Ay_half(1),By_half(1),Cy_half(1),Dy_half(1),Ey_half(1)
        write(*,*) Az_half(1),Bz_half(1),Cz_half(1),Dz_half(1),Ez_half(1)
        write(*,*) 'LeapFrog coeficients: electrons'
        write(*,*) Ax_lf(2),Bx_lf(2),Cx_lf(2),Dx_lf(2),Ex_lf(2)
        write(*,*) Ay_lf(2),By_lf(2),Cy_lf(2),Dy_lf(2),Ey_lf(2)
        write(*,*) Az_lf(2),Bz_lf(2),Cz_lf(2),Dz_lf(2),Ez_lf(2)
        write(*,*) 'Half coeficients - for wind back'
        write(*,*) Ax_half(2),Bx_half(2),Cx_half(2),Dx_half(2),Ex_half(2)
        write(*,*) Ay_half(2),By_half(2),Cy_half(2),Dy_half(2),Ey_half(2)
        write(*,*) Az_half(2),Bz_half(2),Cz_half(2),Dz_half(2),Ez_half(2)
    endif

    !Injection length checking
    if (debug) then
        write(*,*) "Lzi (new)",Lz_inj(1)
        write(*,*) "Lze (new)",Lz_inj(2)
    endif

    !------------------------------------------------ equipotentials configuration --------------------!
    write(*,*) 'Configuring geometry of equipotentials'

    ! temporary equipot. structures
    ! scenario = 2


    !zero flag and equipot array
    ! at the beginning, everywhere was plasma...
    flag = 1
    flag_m = 1

    equipot = 0.0
    equipot_m = 0.0
    if (scenario.eq.3.or.scenario.eq.4) then
        ! zero E field condition
        flag_m(Nz,:) = 2

    else
        ! top wall - zero potential
        flag_m(Nz,:) = 0
        equipot_m(Nz,:) = PL
    endif
    ! bottom wall - depends on the scenario

    if (scenario.eq.1) then
        ! particles injected from the top wall at the bottom
        flag_m(1,:) = 0
        equipot_m(1,:) = P0
    else if (scenario.eq.2) then
        ! particles injected from both sides

        flag_m(1,:) = 0
        equipot_m(1,:) = PL
    else if (scenario.eq.3) then
        ! SOL source&sink at the top, wall at the bottom

        flag_m(1,:) = 0
        equipot_m(1,:) = P0
    else if (scenario.eq.4) then
        ! SOL source&sink on both sides
        flag_m(1,:) = 3
    endif

    ! arbitrary tile on the left
    ! flag_m(1:5,1:Ny) = 0
    ! equipot_m(1:5,1:Ny) = P0


    !we keep the configuration - 2 tiles
    ! flag_m(1:Nz1,1:Ny1) = 0
    ! equipot_m(1:Nz1,1:Ny1) = P0
    !
    !
    !  flag_m(1:Nz1,Ny2:Ny) = 0
    !  equipot_m(1:Nz1,Ny2:Ny) = P0
    ! let's try one block in the middle
    ! flag_m(Nz1 -2:Nz1 +2,1:Ny) = 0
    !   equipot_m(Nz1 -2:Nz1 +2,1:Ny) = P0
    ! make a hole in the middle to simulate RFA slit
    ! flag_m(Nz1 -2:Nz1 +2,11:21) = 1
    !   equipot_m(Nz1 -2:Nz1 +2,11:21) = PL
    !Ball-pen probe test
    ! flag_m(50:53,10:30) = 0
    ! equipot_m(50:53,10:30) = P0
    !
    ! flag_m(67:70,10:30) = 0
    ! equipot_m(67:70,10:30) = P0
    !
    ! flag_m(50:70,10:13) = 0
    ! equipot_m(50:70,10:13) = P0
    ! rectangular shapes
    objects = 0
    edges = 0
    edge_charge = 0.0
    Ts = 0.0
    do i=1,rectangles
        write(*,*) 'Building rectangle',i,rectangle_params(i)%param3
        zlow = rectangle_params(i)%zlow
        zhigh = rectangle_params(i)%zhigh
        ylow = rectangle_params(i)%ylow
        yhigh = rectangle_params(i)%yhigh
        write(*,*) 'Dims:',zlow,ylow,zhigh,yhigh
        if (rectangle_params(i)%negative.eq.0) then
            if (rectangle_params(i)%see_factor.gt.0.0) then
                write(*,*) 'Enabling secondary electron emission for this block',rectangle_params(i)%see_factor
            endif
            !        se(zlow:zhigh ,ylow:yhigh) = rectangle_params(i)%see_factor
            objects_enum(zlow:zhigh ,ylow:yhigh) = i
            Ts(zlow:zhigh ,ylow:yhigh) = rectangle_params(i)%surface_temperature
            if (rectangle_params(i)%param1.eq.3)then
                count_iv_objects = count_iv_objects + 1
            endif
            if (rectangle_params(i)%param1.eq.0.or.rectangle_params(i)%param1.eq.2.or.rectangle_params(i)%param1.eq.3) then
                ! conductor
                write(*,*) 'Rectangle is conductor'
                flag_m(zlow:zhigh ,ylow:yhigh) =0
                ! this will change - for dielectric there will be different values
                equipot_m(zlow:zhigh ,ylow:yhigh) = rectangle_params(i)%pot
                objects(zlow:zhigh ,ylow:yhigh) = 1

            else if (rectangle_params(i)%param1.eq.1) then
                !dielectric
                objects(zlow:zhigh ,ylow:yhigh) = 2
                have_dielectrics = .true.
            endif
        else             !negative object
            !       se(zlow:zhigh ,ylow:yhigh) = 0.0
            objects_enum(zlow:zhigh ,ylow:yhigh) = 0
            flag_m(zlow:zhigh ,ylow:yhigh) =1
            equipot_m(zlow:zhigh ,ylow:yhigh) = 0
            objects(zlow:zhigh ,ylow:yhigh) = 0
            Ts(zlow:zhigh ,ylow:yhigh) = 0.0


        endif

    enddo

    ! circles definition
    ! special parameter param2 means elongation in the z direction
    ! this way we can draw elipses!
    ! value 0 means no elongation applied (default)

    do sp=1,circles
        write(*,*) 'Building circle',sp
        yc = circle_params(sp)%ycentre
        zc = circle_params(sp)%zcentre
        r =  circle_params(sp)%radius
        circylow = 1
        circzlow = 1
        circyhigh = Ny
        circzhigh = Nz
        if (circle_params(sp)%ylow > 0) then
            circylow = circle_params(sp)%ylow
        endif
        if (circle_params(sp)%zlow > 0) then
            circzlow = circle_params(sp)%zlow
        endif
        if (circle_params(sp)%yhigh > 0) then
            circyhigh = circle_params(sp)%yhigh
        endif
        if (circle_params(sp)%zhigh > 0) then
            circzhigh = circle_params(sp)%zhigh
        endif
        !if elongation has value 0, change it to 1 so we get no elongation

        write(*,*) 'Circle is limited:',circzlow,circzhigh,circylow,circyhigh
        if  (circle_params(sp)%param2.eq.0.0) then
            circle_params(sp)%param2 = 1.0
        endif
        if (circle_params(sp)%param1.eq.3)then
            count_iv_objects = count_iv_objects + 1
        endif

        do i=1,Nz
            do j=1,Ny
                if ((sqrt(real((i - zc)*(i - zc)/circle_params(sp)%param2/circle_params(sp)%param2 + (j - yc)*(j - yc))).le.r).and.(i >= circzlow).and.(i <= circzhigh).and.(j >= circylow).and.(j <= circyhigh)) then
                    !if ((sqrt(real((i - zc)*(i - zc)/circle_params(sp)%param2/circle_params(sp)%param2 + (j - yc)*(j - yc))).le.r)) then
                    ! our point
                    if (circle_params(sp)%negative.eq.0) then
                        !                  se(i ,j) = circle_params(sp)%param3
                        objects_enum(i ,j) = rectangles + sp
                        Ts(i,j) = circle_params(sp)%surface_temperature


                        if (circle_params(sp)%param1.eq.0.or.circle_params(sp)%param1.eq.2.or.circle_params(sp)%param1.eq.3) then
                            flag_m(i,j) = 0
                            objects(i,j) = 1
                            equipot_m(i,j) = circle_params(sp)%pot
                        else if (circle_params(sp)%param1.eq.1) then
                            objects(i,j) = 2
                            have_dielectrics = .true.
                        endif
                    else ! negative object
                        !                 se(i,j) = 0.0
                        objects_enum(i,j) = 0
                        flag_m(i,j) =1
                        equipot_m(i,j) = 0
                        objects(i,j) = 0
                        Ts(i,j) = 0.0


                    endif


                endif


            enddo
        enddo

    enddo

    ! triangles definition
    do sp=1,triangles
        write(*,*) 'Building triangle',sp
        ya = triangle_params(sp)%ya
        za = triangle_params(sp)%za
        yb = triangle_params(sp)%yb
        zb = triangle_params(sp)%zb
        yc = triangle_params(sp)%yc
        zc = triangle_params(sp)%zc
        write(*,*) 'A: ',za,ya
        write(*,*) 'B: ',zb,yb
        write(*,*) 'C: ',zc,yc


        do i=1,Nz
            do j=1,Ny
                result1 = 0
                result2 = 0
                result3 = 0
                call check_halfplane(ya,za,yb,zb,yc,zc,real(j),real(i),result1)
                call check_halfplane(yc,zc,ya,za,yb,zb,real(j),real(i),result2)
                call check_halfplane(yb,zb,yc,zc,ya,za,real(j),real(i),result3)
                if (triangle_params(sp)%param1.eq.3)then
                    count_iv_objects = count_iv_objects + 1
                endif


                if (result1 .eq. 1 .and. result2 .eq. 1 .and. result3.eq.1) then
                    !  				if (result2 .eq. 1) then
                    ! our point
                    if (triangle_params(sp)%negative.eq.0) then
                        !               se(i ,j) = triangle_params(sp)%param3
                        objects_enum(i ,j) = rectangles + circles + sp
                        Ts(i,j) = triangle_params(sp)%surface_temperature

                        if (triangle_params(sp)%param1.eq.0.or.triangle_params(sp)%param1.eq.2.or.triangle_params(sp)%param1.eq.3) then
                            flag_m(i,j) = 0
                            objects(i,j) = 1
                            equipot_m(i,j) = triangle_params(sp)%pot
                        else if (triangle_params(sp)%param1.eq.1) then
                            objects(i,j) = 2

                            have_dielectrics = .true.
                        endif
                    else ! negative object
                        !                se(i,j) = 0.0
                        objects_enum(i,j) = 0
                        flag_m(i,j) =1
                        equipot_m(i,j) = 0
                        objects(i,j) = 0
                        Ts(i,j) = 0.0


                    endif

                endif


            enddo
        enddo

    enddo


    write(*,*) 'IV objects count:',count_iv_objects
    nobjects = rectangles+triangles+circles
    ! find the edges
    write(*,*) 'Detecting edges'
    call find_edges (Nz,Ny,objects,edges)
    write(*,*) 'Determining the surface orientation'
    orientation = 0
    call find_surface_orientation(Nz,Ny,objects,orientation)
    ! populate the thermionic probability matrix
    do sp=1,no_species
        if (spec_params(sp)%motion_method.eq.5) then
            do i=1,Nz
                do j=1,Ny
                    if (edges(i,j).gt.0) then
                        thermionic_prob(i,j,sp) = Ninj_fx(sp)/Ly
                    endif
                enddo
            enddo
        endif
    enddo
    write(*,*) 'Converting equipot. matrixes to vectors'

    call matrix2vector_i(flag_m,Nz,Ny,n,flag)
    call matrix2vector_r(equipot_m,Nz,Ny,n,equipot)


    if (approximate_pot) then
        write(*,*) 'Calculating approximate potential'
        ! first copy over the fixed potentials
        approximate_potential = equipot_m
        triple_flag_m(:,1:Ny) = flag_m
        triple_flag_m(:,(Ny+1):2*Ny) = flag_m
        triple_flag_m(:,(2*Ny+1):3*Ny) = flag_m
        triple_equipot_m(:,1:Ny) = equipot_m
        triple_equipot_m(:,(Ny+1):2*Ny) = equipot_m
        triple_equipot_m(:,(2*Ny+1):3*Ny) = equipot_m
        !do i=1,Nz-1
        !    if (min(float(flag_m(i,:))).lt.1) then
        !		  highest_z = i
        !    endif
        !enddo
        highest_z = Nz -1
        if (driftz.gt.0.0) then
            potential_lambda = driftz
        else
            potential_lambda = 1.0
        endif
        write(*,*) 'Using potential lambda (param2)',potential_lambda

        do i=(Ny+1),2*Ny
            do j=1,Nz
                if (triple_flag_m(j,i).eq.1) then
                    ! this is the plasma point
                    nearest_dist= 9E9;
                    do k=1,3*Ny
                        do l=1,highest_z
                            if (triple_flag_m(l,k).ne.1) then
                                dist = sqrt(float(i-k)**2 + float(j-l)**2)
                                if (dist .lt. nearest_dist) then
                                    nearest_dist = dist
                                    nearest_pot = triple_equipot_m(l,k)
                                endif
                            endif
                        enddo
                    enddo
                    ! write(*,*) j,i,nearest_dist,exp(-nearest_dist*dz/(1.0 + ksi*max(abs(cosalpha_yz),abs(cosalpha_xz))))
                    !write(*,*) ksi,dz,max(abs(cosalpha_yz),abs(cosalpha_xz))
                    approximate_potential(j,i - Ny) = nearest_pot*exp(-nearest_dist*dz/(1.0 + potential_lambda*sqrt(2.0)*ksi*max(abs(cosalpha_yz),abs(cosalpha_xz))))
                endif
            enddo
        enddo

        ! hard hack for parabolic potential
        !  do j=1,Nz
        !	approximate_potential(j,:) = -3.0/Lz/Lz*real(j - Lz)**2 + 3.0
        !   enddo


    endif



    write(*,*) 'Creating surface matrix'
    call make_surface_matrix(Nz,Ny,flag,surface_matrix,debug,Nz_max)
    ! surface_matrix = 1.0
    write(*,*) 'Creating BC matrix'
    call construct_bc_matrix(Nz,Ny,objects,bc_matrix)

    write(*,*) 'Searching for BC z limits at', proc_no
    call find_bc_check_limits(Nz,Ny,objects,dy,dz,Lz_low_limit,Lz_high_limit)
    !Matrix inversion +  parallelization (only 1 proc does the poisson)
    write(*,*) 'Searching for BC z limits completed at', proc_no

    if (enable_poisson) then
        if ((psolver.eq.2).or.(psolver.eq.3)) then
            if (proc_no.eq.0) then
                write (*,*) 'Umfpack initialisation'
                call psolver_direct_periodic(Ny,Nz,P0,PL,n,hzy,numeric,flag,equipot,Nz_disp)
            endif
        else if ((psolver.eq.4) .or. (psolver.eq.5)) then
            write (*,*) '*** PES *** MUMPS initialisation', proc_no
            if (any(ps_ranks == proc_no)) then
                write (*,*) '*** PES *** This rank is in solver communicator'
                if (psolver.eq.5) then
                    write (*,*) 'Using distributed solver', poisson_distributed
                    write (*,*) 'Debug', proc_no, par_size, nZ, nY, dZ, dY, sum(flag_m)
                    !call PESSync()
                    call pes_md_init(mumpsPar, MPI_COMM_SOLVER, nZ, nY, nZ_disp, dZ, dY, flag_m)
                    !call PESInitSPICE2Dist(mumpsPar, debug, MPI_COMM_SOLVER, nZ, nY, dZ, dY, flag_m)
                else
                    write (*,*) 'Using centralized solver', proc_no, poisson_distributed
                    !call PESSync()
                    call pes_mc_init(mumpsPar, MPI_COMM_SOLVER, nZ, nY, nZ_disp, dZ, dY, flag_m)
                    !call PESInitSPICE2(mumpsPar, debug, MPI_COMM_SOLVER, nZ, nY, Nz_disp, dZ, dY, flag_m)
                endif
            else
                write (*,*) '*** PES *** This rank does not take part in PE solver'
            endif
        else if (psolver.eq.6) then
            i_r = proc_no + 1
            write (*,*) '*** PES2D *** Initializing PES at processor', i_r
            write (*,*) grid_partitioning
            y_from_loc = grid_partitioning(i_r, 5)
            z_from_loc = grid_partitioning(i_r, 3)

            y_to_loc = grid_partitioning(i_r, 6)
            z_to_loc = grid_partitioning(i_r, 4)

            n_y_loc = y_to_loc - y_from_loc + 1
            n_z_loc = z_to_loc - z_from_loc + 1

            allocate(flag_m_loc(n_z_loc,n_y_loc))
            allocate(pot_m_loc(n_z_loc,n_y_loc))
            allocate(newpot_m_loc(n_z_loc,n_y_loc))
            allocate(dens_m_loc(n_z_loc,n_y_loc))


            flag_m_loc = flag_m(z_from_loc:z_to_loc,y_from_loc:y_to_loc)
            pot_m_loc = equipot_m(z_from_loc:z_to_loc,y_from_loc:y_to_loc)
            dens_m_loc = rho_tot(z_from_loc:z_to_loc,y_from_loc:y_to_loc)
            newpot_m_loc = 0
            n_0 = 1.0*N0;

            call pes2d_init(pes_info, MPI_COMM_WORLD, debug, Nz, Ny, dZ, dy, n_0, proc_max + 1, grid_partitioning, &
                n_z_loc, n_y_loc, z_from_loc, y_from_loc, flag_m_loc)

            write(*,*) 'Pes2d initialized'
            !#endif
        endif
    endif

    if (debug) then
        write(*,*) 'Entering main time loop'
    endif
    no_tot  =0
    !partial restore - to use particles in another simulation
    if (restore) then
        !reload particles

        if (use_hdf5) then
            call h5_restore_run_9_particles (tfile,verbose,debug,y,z,ux,uy,uz,Nz,Ny,Npts,no_species,stype,no_tot,proc_no,count)
        else
            call restore_run_9_particles (tfile,verbose,debug,y,z,ux,uy,uz,Nz,Ny,Npts,no_species,stype,no_tot,proc_no,count)
        endif

        ! return to a couple of iterations before Nc to get all the data for floating potentials ready
        count_start = MIN(Nc - 100,count)
        if (impose_pot) then
            if (use_hdf5) then
                h5_exists = .false.
                inquire(file=trim(pfile)//'.h5'//char(0), exist=h5_exists)
                if (h5_exists) then
                    call h5_restore_pot(pfile, Nz,Ny,Pot,verbose)
                else
                    write (*,*) 'HDF5 not found, falling back to matlab!'
                    call restore_pot(pfile, Nz,Ny,Pot,verbose)
                endif
            else
                call restore_pot(pfile, Nz,Ny,Pot,verbose)
            endif
            Potvac = Pot
        endif

    endif
    if (advanced_continue) then
        if (use_hdf5) then
            call h5_advanced_restore_run_9_particles (tfile,verbose,debug,y,z,ux,uy,uz,Nz,Ny,Npts,no_species,stype,no_tot,proc_no,count,par_size,dz,dy,Nz_max,slice_proc,regroup_runs)
        else
            call advanced_restore_run_9_particles (tfile,verbose,debug,y,z,ux,uy,uz,Nz,Ny,Npts,no_species,stype,no_tot,proc_no,count,par_size,dz,dy,Nz_max,slice_proc,regroup_runs)
        endif
        ! return to a couple of iterations before Nc to get all the data for floating potentials ready
        count_start = MIN(Nc - 100,count)
        if (impose_pot) then
            if (use_hdf5) then
                h5_exists = .false.
                inquire(file=trim(pfile)//'.h5'//char(0), exist=h5_exists)
                if (h5_exists) then
                    call h5_restore_pot(pfile, Nz,Ny,Pot,verbose)
                else
                    write (*,*) 'HDF5 not found, falling back to matlab!'
                    call restore_pot(pfile, Nz,Ny,Pot,verbose)
                endif
            else
                call restore_pot(pfile, Nz,Ny,Pot,verbose)
            endif
            Potvac = Pot
        endif


    endif

    !complete restore - to continue halted run
    if (cont) then
        !reload particles
        if (use_hdf5) then
            call h5_restore_run_9_particles (tfile,verbose,debug,y,z,ux,uy,uz,Nz,Ny,Npts,no_species,stype,no_tot,proc_no,count)
        else
            call restore_run_9_particles (tfile,verbose,debug,y,z,ux,uy,uz,Nz,Ny,Npts,no_species,stype,no_tot,proc_no,count)
        endif

        write(*,*) 'Particles restored'
        !reload time histories

        if (use_hdf5) then
            call h5_restore_run_9_histories(no_species,number,Nz,Ny,Nv,z_g,y_g,rho_tot,Escz,Escy,Pot,count,t,Esct,dPHIqn,bx,by,bz,&
                tfile,debug,Np,pot_chi,&
                Potvac,no_part_proc,proc_max,h_pos,no_diag_reg,diag_regions,diag_histories,diag_hist,fv_arrays,fv_ar,hist_limits,Np,Na,&
                fv_bin,fv_sum,fv_limits,u_perp_a,fv_perp_array_count,N0,diag_ntimes,verbose,edge_charge,flag_m,equipot_m,flag, &
                iteration_time_hist,injection_rate_hist,objects,edges,diagm,objects_enum,nobjects,objects_current,objects_power_flux,history_ntimes, &
                rho,i_rel_history,float_constant,n,Nc,Nz_max)
        else
            call restore_run_9_histories(no_species,number,Nz,Ny,Nv,z_g,y_g,rho_tot,Escz,Escy,Pot,count,t,Esct,dPHIqn,bx,by,bz,&
                tfile,debug,Np,pot_chi,&
                Potvac,no_part_proc,proc_max,h_pos,no_diag_reg,diag_regions,diag_histories,diag_hist,fv_arrays,fv_ar,hist_limits,Np,Na,&
                fv_bin,fv_sum,fv_limits,u_perp_a,fv_perp_array_count,N0,diag_ntimes,verbose,edge_charge,flag_m,equipot_m,flag, &
                iteration_time_hist,injection_rate_hist,objects,edges,diagm,objects_enum,nobjects,objects_current_tot,objects_power_flux_tot,history_ntimes, &
                rho,i_rel_history,float_constant,n,Nc,Nz_max,dz)
        endif
        ! for the first processor copy the stuff to objects current
        if (proc_no.eq.0) then
            objects_current = objects_current_tot
            objects_power_flux = objects_power_flux_tot
        endif

        ! check if the simulation isn't finished already
        if (count.ge.Np) then
            write(*,*) 'Simulation is completed already, exiting'
            stop
        endif

        ! reset the floating potentials
        call set_floating_objects_pot(Nz,Ny,Pot,equipot_m,objects_enum,rectangle_params,circle_params,triangle_params,rectangles,circles,triangles)
        ! recalculate the equipot matrix
        call matrix2vector_r(equipot_m,Nz,Ny,n,equipot)

        if (frozen_potvac) then
            Pot = Potvac
        endif
        if (impose_pot) then
            if (use_hdf5) then
                h5_exists = .false.
                inquire(file=trim(pfile)//'.h5'//char(0), exist=h5_exists)
                if (h5_exists) then
                    call h5_restore_pot(pfile, Nz,Ny,Pot,verbose)
                else
                    write (*,*) 'HDF5 not found, falling back to matlab!'
                    call restore_pot(pfile, Nz,Ny,Pot,verbose)
                endif
            else
                call restore_pot(pfile, Nz,Ny,Pot,verbose)
            endif
            Potvac = Pot
        endif
        if (count.gt.Na) then

            if (use_hdf5) then
                call h5_restore_9_avdiag(dt,Nz,Ny,dens,Epar,Eperp,Potav,vzav,vyav,vxav,vz2av,vy2av,vx2av,vz3av,vy3av,&
                    vx3av,ofile,debug,no_species,nav,verbose,edge_flux,edge_energy_flux)
            else
                call restore_9_avdiag(dt,Nz,Ny,dens,Epar,Eperp,Potav,vzav,vyav,vxav,vz2av,vy2av,vx2av,vz3av,vy3av,&
                    vx3av,ofile,debug,no_species,nav,verbose,edge_flux,edge_energy_flux)
            endif

            if (proc_no.gt.0) then
                ! the edgeflux diagnostics are saved agregated over all threads. Therefore they need to be zero'd for all processes except for the first one
                edge_flux_ave = 0.0
                edge_energy_flux_ave = 0.0
                ! the same applies to averaged velocity components
                vxav = 0.0
                vyav = 0.0
                vzav = 0.0
                vx2av = 0.0
                vy2av = 0.0
                vz2av = 0.0
                vx3av = 0.0
                vy3av = 0.0
                vz3av = 0.0
            endif

        endif
        ! fill no_part
        no_part = number(:,h_pos-1)
        count_start = count
        write(*,*) 'Reseting count to',count_start
        write(*,*) 'Restored h_pos', h_pos
        !rollup of the combing
        h_pos = 0
        bstep = 0
        do i=1,(count_start-1)

            if (real(count - Na).gt.bstep*bint.and.count_iv_objects.gt.0.and.count_start.gt.Nc) then
                bstep = bstep + 1
            endif
            if (i.eq.delta_h*int(real(i)/real(delta_h))) then
                h_pos = h_pos + 1
            endif
            if (h_pos.eq.history_ntimes) then
                delta_h = delta_h*2
                !set the position to the middle - very unwise to use odd number of samples ;)
                h_pos = int(history_ntimes/2)
            endif
        enddo
        write(*,*) 'Recounted h_pos',h_pos
        write(*,*) 'Recounted delta_h',delta_h
        ! now prepare the updated potentials
        if (proc_no.eq.0.and.count_iv_objects.gt.0) then
            v_act  = vstart + (vstop - vstart)/real(nbsteps)*real(bstep)
            write(*,*) 'New bias voltage ',v_act

            do sp=1,rectangles
                if (rectangle_params(sp)%param1.eq.3) then
                    do i=1,Nz
                        do j=1,Ny
                            if (objects_enum(i,j).eq.sp) then
                                equipot_m(i,j) = v_act
                            endif
                        enddo
                    enddo
                endif
            enddo
            !circles
            do sp=1,circles
                if (circle_params(sp)%param1.eq.3) then
                    do i=1,Nz
                        do j=1,Ny
                            if (objects_enum(i,j).eq.(rectangles + sp)) then
                                equipot_m(i,j) = v_act
                            endif
                        enddo
                    enddo

                endif
            enddo
            !triangles
            do sp=1,triangles
                if (triangle_params(sp)%param1.eq.3) then

                    do i=1,Nz
                        do j=1,Ny
                            if (objects_enum(i,j).eq.(rectangles + circles + sp)) then
                                equipot_m(i,j) = v_act
                            endif
                        enddo
                    enddo

                endif
            enddo
            call matrix2vector_r(equipot_m,Nz,Ny,n,equipot)
            !Matrix inversion +  parallelization (only 1 proc does the poisson)

            if (enable_poisson) then
                write (*,*) 'Poisson solver enabled', psolver
                if ((psolver.eq.2).or.(psolver.eq.3)) then
                    if (proc_no.eq.0) then
                        write (*,*) 'Umfpack inversion'
                        call psolver_direct_periodic(Ny,Nz,P0,PL,n,hzy,numeric,flag,equipot,Nz_disp)
                    endif
                else if ((psolver.eq.4) .or. (psolver.eq.5)) then
                    if (proc_no.eq.0) then
                        write (*,*) 'MUMPS inversion'
                    endif

                    if (any(ps_ranks == proc_no)) then

                        if (psolver.eq.5) then
                            call pes_md_stop(mumpsPar)
                            call pes_md_init(mumpsPar, MPI_COMM_SOLVER, nZ, nY, Nz_disp, dZ, dY, flag_m)
                        else
                            call pes_mc_stop(mumpsPar)
                            call pes_mc_init(mumpsPar, MPI_COMM_SOLVER, nZ, nY, Nz_disp, dZ, dY, flag_m)
                        endif
                    endif

                    call pes_sync()

                    !if (any(ps_ranks == proc_no)) then
                    !    if (poisson_distributed) then
                    !        call PESSync()
                    !        call PESStopMumpsDist(mumpsPar, debug, proc_no, par_size)
                    !        call PESInitSPICE2Dist(mumpsPar, debug, proc_no, par_size, nZ, nY, dZ, dY, flag_m)
                    !    else
                    !        call PESSync()
                    !        call PESStopMumps(mumpsPar, debug)
                    !        call PESInitSPICE2(mumpsPar, debug, nZ, nY, Nz_disp, dZ, dY, flag_m)
                    !    endif
                    !endif

                endif
            endif






        endif ! end of potential update

    endif
    !adjust count if is already in the diag time
    ! if (count_start.gt.Na) then
    ! 	count_start = Na
    ! endif
    ! endif

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!MAIN LOOP ON TIME-STEP!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    do count=count_start,Np
        !write(6,*) '%%%%%%%%%%%%'
        if (debug) then
            write(6,*) '% Count=',count,' %'
        endif
        ! when reaching Nc, the injection is turned off and collisions are turned on
        if (count.eq.Nc.and.s_i_s%coll_test) then
            write(*,*) 'performing beam switch'
            beam_switch = .true.
            Ninj_fx = 0.0
            collie = 1.0
            ! stop the ions
            do i=1,no_tot
                if (stype(i).eq.1.0) then
                    ux(i) = ux(i)/10000.0;
                    uy(i) = uy(i)/10000.0;
                    uz(i) = uz(i)/10000.0;
                endif

            enddo


        endif

        !write(6,*) '%%%%%%%%%%%%'
        timestamp = 0
        call gettime(timestamp)
        t_start = timestamp
        t_sum = 0
        t1=t_start
        loop_stat = 0
        anp = no_tot
        !if (proc_no.eq.0) then
        !	write(*,*) 'scount - ',scount
        !endif
        !----------------------------- GRID COORDINATES -------------------------------------!

        ! griding of particles done once for all since it's the same for all species
        !  call grid_particles(count,z(1:anp),y(1:anp),iy1(1:anp),iz1(1:anp),dz,dy,anp,A11(1:anp),A12(1:anp),A21(1:anp),A22(1:anp))


        call gettime(t_act)

        if (t_act - t_intro.gt.t_max .and. t_max.gt.0) then
            ! halt the run
            write(*,*) 'Maximum run time reached, exiting',timestamp - t_intro,t_max
            call exit(1)
        endif


        call gettime(timestamp)

        t_grid = timestamp - t_start

        if (abs(t_grid).gt.1000e6) then
            t_grid = 0
        endif
        t_sum = t_sum + t_grid
        loop_stat = loop_stat + 1
        if (debug) then
            write(*,*) count, loop_stat
        endif
        if (debug) then
            write(*,*) 'After gridding',no_tot,sum(z(1:no_tot)),sum(y(1:no_tot))
        endif



        anp = no_tot
        ! rho_tot=0.d0
        ! rho = 0.0


        !---------------------------------------------weighting-------------------------------1

        !Weighting
        !now again done for all particles at once, using apropriate q(stype(i))
        !so resulting rho_tot includes combined charge densities

        !the last parameter should be reference weighting
        ! since we don't use it, I put there mass

#ifdef SPICE_TRACER
        ! AP: Turn off weighting particles if a tracer modification is compiled. If so, potential should be superimposed, since this
        ! disables density calculation!
#else

        if (debug) then
            write (*,*) 'Weighting count, no_tot', count, no_tot
        endif
        rho_tot = 0.0
        rho = 0.0

        do sp=1,no_species
            if (mpi_rank(sp).eq.proc_no) then
                !rho(sp,z_start:z_stop,y_start:y_stop) = 0.0
                ! rho(sp,:,:) = 0.0

            endif
        enddo

        call weight_particles(count,no_tot,y(1:anp),z(1:anp),ux(1:anp),uy(1:anp),uz(1:anp),Lz,Nz,Ny,Na,Nv,iz1(1:anp),iy1(1:anp),no_species,q,stype(1:anp),rho_tot,&
            rho,weighting_type,debug,flag_m,domain_decomp,y_start,y_stop,mpi_rank,proc_no,surface_matrix,dy,dz,&
            take_diag,jv,vx,vy,vz,vx2,vy2,vz2,vx3,vy3,vz3,dv,vpar,y_stop_large,Nz_max,z_start,z_stop,z_start_box,z_stop_box)

        ! rho_tot = 24.0
        ! do sp=1,no_species
        ! if (mpi_rank(sp).eq.proc_no) then
        ! rho(sp,z_start:z_stop,y_start:y_stop) = 14.0
        ! ! rho(sp,:,:) = 0.0
        !
        ! endif
        ! end do

        ! rho_tot(:,y_start:y_stop) = 0.0
        ! rho(:,:,y_start:y_stop) = 0.0
        if (take_diag) then
            call te_diag(no_tot,y(1:anp),z(1:anp),ux(1:anp),uy(1:anp),uz(1:anp),Nz,Ny,vx,vy,vz,no_species,stype(1:anp),temperature,iy1(1:anp),iz1(1:anp),dz,dy)

        endif


        ! run the edge charge smooting only once every 100 steps to avoid charge leakaget
        ! 120 better than 100 so we see in the output how it looks like BEFORE the filtering
        if (int(real(count)/120.0)*120.eq.count.and.have_dielectrics) then
            call smooth_edge_charge(Nz,Ny,edge_charge,edges)
        endif
        ! dielectric - add edge charge density
        if (have_dielectrics) then
            rho_tot = rho_tot + edge_charge
        endif
#endif
        call gettime(timestamp)

        t_wt = timestamp - t_sum - t_start

        if (abs(t_wt).gt.1000e6) then
            t_wt = 0
        endif

        t_sum = t_sum + t_wt

        if (debug) then
            write(*,*)'Rho finished',sum(rho_tot)
        endif

        if (debug) then
            write(*,*) 'After weighting',no_tot,sum(z(1:no_tot)),sum(y(1:no_tot))
        endif

        ! ---------------------------- GRID DIAGNOSTICS --------------------------------------!
        ! this got moved inside the weight particles routine

        ! if ((count >= Na).and.take_diag) then
        ! vz=0.d0
        ! vy=0.d0
        ! vx=0.d0
        ! vz2=0.d0
        ! vy2=0.d0
        ! vx2=0.d0
        ! vz3=0.d0
        ! vy3=0.d0
        ! vx3=0.d0
        ! ! write(*,*) 'Ion rho pre-check',sum(rho(1,:,:))
        !  call grid_diag(no_species,no_tot,jv,Nv,Nz,Ny,y(1:anp),z(1:anp),ux(1:anp),uy(1:anp),uz(1:anp),A11(1:anp),A12(1:anp),A21(1:anp),&
            !  A22(1:anp),q(1:no_species),stype(1:anp),iz1(1:anp),iy1(1:anp),vx,vy,vz,vx2,vy2,vz2,vx3,vy3,vz3,rho,dv,vpar,Lz,n,flag,flag_m,y_start,y_stop,surface_matrix(:,y_start:y_stop),mpi_rank, proc_no)
        !
        ! call gettime(timestamp)
        !
        ! t_gd = timestamp - t_sum - t_start
        ! 	if (abs(t_gd).gt.1000e6) then
        ! 	t_gd = 0
        ! 	endif
        !
        ! 	t_sum = t_sum + t_gd
        ! else
        !  	t_gd = 0
        ! endif
        t_gd = 0
        !----test of particle sorting
        ! idea is to sort particle arrays according to the position in grid
        ! this should increase the probability of cache hit and hence speed-up leap-frog and other routines
        ! we try to sort only once in few iterations (50)
#ifndef SPICE_TRACER
        ! do not sort when tracing particles
        if (sort_period .ge. 0 .and. count.eq.int(float(count)/sort_period)*int(sort_period)) then

            ! if (count.eq.int(float(count)/sort_period)*int(sort_period)) then
            max_cell_count = 0
            tot_cell_count = 0
            ! build count matrix - we mainly need to know the maximal number of particles in one cell
            call build_count_matrix(Nz_max,anp,iy1(1:anp),iz1(1:anp),max_cell_count,tot_cell_count,cell_count(1:Nz_max))

            ! write(*,*) 'Cell count',max_cell_count,tot_cell_count
            ! build the sorting sequence and do sorting
            call sort_particles(anp,y(1:anp),z(1:anp),ux(1:anp),uy(1:anp),uz(1:anp),vtot_init(1:anp),use_vtot,max_cell_count,Nz_max,cell_count(1:Nz_max),tot_cell_count,iz1(1:anp),&
                iy1(1:anp),stype(1:anp),no_species,no_diag_reg,particle_no)
            ! run gridding again - I guess it gets faster
            call grid_particles(count,z(1:anp),y(1:anp),iy1(1:anp),iz1(1:anp),dz,dy,anp)
        endif
#else
        if (debug) then
            write(*,*) 'Skipping gridding and weighting, tracer is on.'
        endif
#endif
        !---------------------- Rho MPI transfer -----------------------------------------!
        !MK parallelization
        ! proc 0 does the poisson solving, still it needs rho from other procs

        if (proc_max.gt.0.and.count.gt.0.and.enable_poisson) then
            rhot = 0.0
            !we use blocking I/O scheme
            if (proc_no.eq.0) then
                !receiver
                ! loop over other procs and add-up final rho_tot
                if (debug) then
                    write(*,*) 'Rhot before MPI',sum(rho_tot)
                endif

                i = 0
                j = 1
                do sp=0,proc_max
                    i = i+1
                    y_start1 = slice_boundaries(2,i)
                    y_stop1 = slice_boundaries(2,i+1)
                    z_start1 = slice_boundaries(1,j)
                    z_stop1 = min(slice_boundaries(1,j+1),Nz)
                    if (i.eq.no_slices(2)) then
                        j = j+1
                        i = 0
                    endif
                    if (sp.gt.0.and.z_start1.lt.Nz) then
                        if (debug) then
                            write(*,*) count,'Expecting rhot from proc',17*sp*mod(count,1000),sp
                        endif
                        ! 			call MPI_RECV(rhot(z_start1:z_stop1,y_start1:y_stop1),(z_stop1 - z_start1)*(y_stop1 - y_start1),MPI_DOUBLE_PRECISION,sp,17*sp,MPI_COMM_WORLD,par_status,ierr)
                        call MPI_RECV(rhot,Nz*Ny,MPI_DOUBLE_PRECISION,sp,17*sp*mod(count,1000),MPI_COMM_WORLD,par_status,ierr)

                        if (debug) then
                            write(*,*) count,'Received rhot',sum(rhot),sp,Nz*Ny
                            write(*,*) count,'Received rhot',y_start1,y_stop1,z_start1,z_stop1
                            write(*,*) count,'Received rhot',y_stop1-y_start1,z_stop1 - z_start1

                        endif
                        ! 			rho_tot(z_start1:z_stop1,y_start1:y_stop1) = rho_tot(z_start1:z_stop1,y_start1:y_stop1) + rhot(z_start1:z_stop1,y_start1:y_stop1)
                        rho_tot = rho_tot + rhot

                    endif
                enddo
                if (debug) then
                    write(*,*) 'Rhot after MPI',sum(rho_tot)
                endif
            endif

            if (proc_no.gt.0) then
                !sender
                if (z_start.lt.Nz) then
                    if (debug) then

                        write(*,*) count,'Sending rhot',sum(rho_tot),proc_no,Nz*Ny,17*proc_no*mod(count,1000)

                        !			write(*,*) count,'Sending rhot',sum(rho_tot(z_start:min(z_stop,Nz),y_start:y_stop)),proc_no,(y_stop - y_start)*(min(z_stop,Nz) - z_start)
                        write(*,*) count,'Sending rhot',y_start,y_stop,z_start,min(z_stop,Nz)
                        write(*,*) count,'Sending rhot',y_stop - y_start,min(z_stop,Nz) - z_start


                    endif
                    !  		call MPI_SEND(rho_tot(z_start:min(z_stop,Nz),y_start:y_stop),(y_stop - y_start)*(min(z_stop,Nz) - z_start),MPI_DOUBLE_PRECISION,0,proc_no*17,MPI_COMM_WORLD,ierr)
                    call MPI_SEND(rho_tot,Nz*Ny,MPI_DOUBLE_PRECISION,0,proc_no*17*mod(count,1000),MPI_COMM_WORLD,ierr)

                endif
            endif

        endif !par comm



        !! DEBUG printout

        ! ---------------------- POISSON SOLVER ----------------------------------------!
        ! now it's time to fix the periodic conditions
        if (abs(Lz_disp).gt.0.0) then

            if (Nz_disp.gt.0) then

                do j=Nz_disp+1,Nz
                    rho_tot(j,1)=(rho_tot(j,1)+rho_tot(j-Nz_disp,Ny))/2.0   !PC
                    ! rho_tot(j,Ny)=rho_tot(j,1)
                enddo
                do j=1,Nz-Nz_disp
                    ! rho_tot(j,1)=(rho_tot(j,1)+rho_tot(j-Nz_disp,Ny))/2.0   !PC
                    rho_tot(j,Ny)=rho_tot(j + Nz_disp,1)
                enddo

            else
                ! negative Nz_disp
                do j=abs(Nz_disp)+1,Nz
                    rho_tot(j,Ny)=(rho_tot(j,Ny)+rho_tot(j-abs(Nz_disp),1))/2.0   !PC
                    ! rho_tot(j,Ny)=rho_tot(j,1)
                enddo
                do j=1,Nz-abs(Nz_disp)
                    ! rho_tot(j,1)=(rho_tot(j,1)+rho_tot(j-Nz_disp,Ny))/2.0   !PC
                    rho_tot(j,1)=rho_tot(j + abs(Nz_disp),Ny)
                enddo
            endif


        else
            do j=1,Nz
                rho_tot(j,1)=(rho_tot(j,1)+rho_tot(j,Ny))/2.0   !PC
                rho_tot(j,Ny)=rho_tot(j,1)
            enddo
        endif







        ! implement an artificial rho

        !do j=1,Ny
        !do i=1,Nz
        !if (objects(i,j).eq.0) then
        !rho_tot(i,j) = 1.0
        !else
        !rho_tot(i,j) = 0.0
        !endif
        !enddo
        !enddo


        call gettime(timestamp)

        t_com1 = timestamp - t_sum - t_start
        if ((abs(t_com1)).gt.1000e6) then
            t_com1 = 0
        endif
        t_sum = t_sum + t_com1

        !poisson is handled only by proc 0

        if (proc_no.eq.0) then
            loop_stat = loop_stat + 1
            if (debug) then
                write(*,*) count, loop_stat
            endif
        endif

        if (enable_poisson) then
            if(frozen_potvac.and.count.gt.1) then
                ! no need to run poisson anymore

            else
                if (psolver.eq.1) then
                    if (proc_no.eq.0) then
                        call psolver_gs(Pot,Pot_old,Pot_b4,crit_conv,Ny,Nz,dy,dz,cte_geo,cte_geo2,Ncell,Np,frelax,relax,Escz,Escy,&
                            rho_tot,N0,debug,PL)
                    endif
                elseif (psolver.eq.2) then
                    if (proc_no.eq.0) then
                        call psolver_direct2_periodic(Ny,Nz,numeric,dz,dy,Pot,debug,rho_tot,N0,n,flag,equipot,Nz_disp)
                    endif
                elseif (psolver.eq.3) then
                    ! first we run the direct solver
                    if (proc_no.eq.0) then
                        call psolver_direct2_periodic(Ny,Nz,numeric,dz,dy,Pot,debug,rho_tot,N0,n,flag,equipot,Nz_disp)
                        ! if (count.eq.(10*int(real(count)/10.0))) then
                        ! 		call dump_grid(Pot,Nz,Ny,2*count,0)
                        ! endif
                        ! now we use the output fot the direct solver
                        ! 	Pot_old = Pot
                        call psolver_gs(Pot,Pot_old,Pot_b4,crit_conv,Ny,Nz,dy,dz,cte_geo,cte_geo2,Ncell,Np,frelax,relax,Escz,Escy,&
                            rho_tot,N0,debug,PL)
                        ! if (count.eq.(10*int(real(count)/10.0))) then
                        ! 		call dump_grid(Pot,Nz,Ny,2*count+1,0)
                        ! endif
                    endif
                elseif ((psolver.eq.4) .or. (psolver.eq.5)) then
                    if (any(ps_ranks == proc_no)) then

                        call pes_solve(mumpsPar, nZ, nY, nZ_disp, dZ, dY, n0, flag_m, equipot_m, rho_tot, Pot)


                        !if (poisson_distributed) then
                        !call PESPrepareRhsSPICE2Dist(mumpsPar, debug, proc_no, par_size, nZ, nY, dZ, dY, n0, flag_m, equipot_m, rho_tot)
                        !call PESSolveSPICE2Dist(mumpsPar, debug, proc_no, par_size, nZ, nY, Pot)
                        !else
                        !call PESPrepareRhsSPICE2(mumpsPar, debug, nZ, nY, dZ, dY, n0, flag_m, equipot_m, rho_tot)
                        !call PESSolveSPICE2(mumpsPar, debug, nZ, nY, Nz_disp, Pot)
                        !endif
                    endif
                    call pes_sync()
                elseif (psolver.eq.6) then
                    !#ifndef NOHYPES
                    dens_m_loc = 0
                    call pes2d_scatter_r(pes_info, nZ, nY, rho_tot, n_z_loc, n_y_loc, dens_m_loc)

                    if (debug) then
                        write(*,*) 'z_from_loc:z_to_loc,y_from_loc:y_to_loc'
                        write(*,*) z_from_loc, z_to_loc, y_from_loc, y_to_loc
                        write(*,*) 'y_start,y_stop,z_start,min(z_stop,Nz)'
                        write(*,*) y_start,y_stop,z_start,min(z_stop,Nz)
                        write(*,*) 'sum dens', sum(dens_m_loc), sum(rho_tot)
                    endif
                    pot_m_loc = equipot_m(z_from_loc:z_to_loc,y_from_loc:y_to_loc)

                    call pes2d_solve(pes_info, n_z_loc, n_y_loc, flag_m_loc, pot_m_loc, dens_m_loc, newpot_m_loc)

                    call pes2d_gather_r(pes_info, nZ, nY, Pot, n_z_loc, n_y_loc, newpot_m_loc)
                    !#endif
                endif
            endif
        endif

        if (proc_no.eq.0) then
            act_pot_chi = sum(abs(Pot - Pot_old))/(Ny*Nz)
            if (count.eq.1) then
                ! vacuum potential copy
                if (impose_pot) then
                    if (use_hdf5) then
                        h5_exists = .false.
                        inquire(file=trim(pfile)//'.h5'//char(0), exist=h5_exists)
                        if (h5_exists) then
                            call h5_restore_pot(pfile, Nz,Ny,Pot,verbose)
                        else
                            write (*,*) 'HDF5 not found, falling back to matlab!'
                            call restore_pot(pfile, Nz,Ny,Pot,verbose)
                        endif
                    else
                        call restore_pot(pfile, Nz,Ny,Pot,verbose)
                    endif
                endif
                Potvac = Pot
            endif
            ! frozen vacuum potential

            if (frozen_potvac) then
                Pot = Potvac
            endif
            if (approximate_pot) then
                Pot = approximate_potential
            endif
        endif


        !       if (count .ge. 50 .and. count .le. 100) then
        !
        !           write (*,*) '------ rho --------------------', count
        !           call printRealMatrix(nZ, nY, rho_tot)
        !           write (*,*) '------ Pot --------------------', count
        !          call printRealMatrix(nZ, nY, Pot)

        !      endif


        ! debug - imposing vacuum potential
        ! Pot = Potvac
        ! do i=1,Nz
        !   Pot(i,:) = i
        ! enddo
        ! construct artificial potential
        !call construct_pot(Pot,Nz,Ny,PL)
        ! quick test - impose artifical potential to test demagnetization

        !         do i=1,Nz
        !           Pot(i,:) = 0.0003*real(i*i) - 0.0003*real(Nz*Nz)
        !         enddo


        ! ----------------------------- Potential MPI transfer ---------------------------!

        !Paralelization - sending of the Pot
        ! proc 0 sends new Pot to proc 1
        if (proc_max.gt.0.and.enable_poisson) then
            !we use blocking I/O scheme
            if (proc_no.gt.0) then
                !receiver
                Pot = 0.0
                call MPI_RECV(Pot(1:Nz,1:Ny),Ny*Nz,MPI_DOUBLE_PRECISION,0,151*proc_no,MPI_COMM_WORLD,par_status,ierr)
                if (debug) then
                    write(*,*) 'Received Pot',sum(Pot),proc_no,ierr
                endif
            endif
            if (proc_no.eq.0) then
                !sender
                if (debug) then
                    write(*,*) 'Sending Pot',sum(Pot)
                endif
                do i=1,proc_max
                    call MPI_SEND(Pot(1:Nz,1:Ny),Ny*Nz,MPI_DOUBLE_PRECISION,i,151*i,MPI_COMM_WORLD,ierr)
                enddo
            endif
        endif !par comm

        ! char. dumping - for videos
        if (count.gt.Nc.and.enable_dump.gt.0.0.and.proc_no.eq.0) then
            ! we fill the buffer
            avepot = avepot + Pot
            if (no_species.eq.4) then
                avedens = avedens + rho(2,1:Nz,:) + rho(4,1:Nz,:)
            else
                avedens = avedens + rho(1,1:Nz,:)
            endif
            ave_buffer = ave_buffer + 1
            ! we dump every 100 frames averaged
            if (ave_buffer.eq.int(enable_dump)) then
                avepot = avepot/enable_dump
                avedens = avedens/enable_dump
                call dump_grid(avepot,Nz,Ny,scount,0)
                call dump_grid(avedens,Nz,Ny,scount,1)
                avepot = 0.0
                avedens = 0.0
                ave_buffer = 0
                scount = scount +1

            endif


        endif


        ! paralelization - E field calculated by both processors - not excessive and saves
        ! traffic
        !ELECTRIC FIELD
        !==============
        !call calc_E_field (Nz,Ny,Ny1,Ny2,Nz1,Escz,Escy,Pot,dz,dy)
        !subroutine calc_E_field (Nz,Ny,Ny1,Ny2,Nz1,Escz,Escy,Pot,dz,dy)
        if (enable_poisson) then
            if(frozen_potvac.and.count.gt.1) then
                ! no need to calculate E field each step, since potential does not change
                ! frozen potential means frozen E field
            else
                if (debug) then
                    write(*,*) 'Calculating electric field'
                endif
                call calc_E_field_general (Nz,Ny,Escz,Escy,Pot,dz,dy,objects,edges,Nz_disp)
            endif

            ! calculate paralel and perpendicular components
            !     Escy= 0.0
            !    Escz = 0.0


            ! Epar = Escy*cosalpha_yz + Escz*sqrt(1.0 - cosalpha_yz**2)
            ! Eperp = -Escy*sqrt(1.0 - cosalpha_yz**2) + Escz*cosalpha_yz
        else
            Escy= 0.0
            Escz = 0.0
        endif


        ! hack to test gyration
        !  if (frozen_potvac) then
        !     Escy = 1.0
        ! endif

        !zero E field debug
        !     do i=1,Nz
        ! 	Escz(i,:) = -abs(i -Nz)**2*0.01
        !     enddo
        call gettime(timestamp)

        t_poiss = timestamp - t_sum - t_start
        if (abs(t_poiss).gt.1000e6) then
            t_poiss = 0
        endif

        t_sum = t_sum + t_poiss
        loop_stat = loop_stat + 1
        if (debug) then
            write(*,*) count, loop_stat
        endif



        !---------------------------------------------leapfrog--------------------------------1

        anp = no_tot
        sp = no_species
        !PERFORM LEAPFROG STEP.
        !===============================
        if (s_i_s%coll_test)  then
            call leapfrog_nofield(anp,Nz,Ny,Ax_lf,Bx_lf,Cx_lf,Dx_lf,Ex_lf,Ay_lf,By_lf,Cy_lf,Dy_lf,Ey_lf,Az_lf,Bz_lf,Cz_lf,Dz_lf,Ez_lf,Lz,dtz,&
                z(1:anp),y(1:anp),ux(1:anp),uy(1:anp),uz(1:anp),iz1(1:anp),iy1(1:anp),Escz,&
                Escy,q,stype(1:anp),m,no_species,collision_prob,driftz,Nc,count,dt,utherm,0,spec_params,cosalpha_yz,ksi,cosalpha_xz,&
                alpha_yz,alpha_xz,y_start,y_stop,dz,dy,z_start,z_stop,z_start_box,z_stop_box,total_energy(:,:,count),approximate_pot,Fx_lf,Fy_lf,Fz_lf)

        else
            call leapfrog(anp,Nz,Ny,Ax_lf,Bx_lf,Cx_lf,Dx_lf,Ex_lf,Ay_lf,By_lf,Cy_lf,Dy_lf,Ey_lf,Az_lf,Bz_lf,Cz_lf,Dz_lf,Ez_lf,Lz,dtz,&
                z(1:anp),y(1:anp),ux(1:anp),uy(1:anp),uz(1:anp),iz1(1:anp),iy1(1:anp),Escz,&
                Escy,q,stype(1:anp),m,no_species,collision_prob,driftz,Nc,count,dt,utherm,0,spec_params,cosalpha_yz,ksi,cosalpha_xz,&
                alpha_yz,alpha_xz,y_start,y_stop,dz,dy,z_start,z_stop,z_start_box,z_stop_box,total_energy(:,:,count),approximate_pot,Fx_lf,Fy_lf,Fz_lf,bx,by,bz,sqbx2by2)


        endif

        ! suprathermal electrons only

        !move them to buffer
        s_no = 0
        ! suprathermal particles disabled now
        ! do i=1,no_tot
        ! 	if (spec_params(stype(i))%motion_method.eq.2) then
        ! 		s_no = s_no+1
        ! 		s_order(s_no) = i
        ! 		s_y(s_no) = y(i)
        ! 		s_z(s_no) = z(i)
        ! 		s_ux(s_no) = ux(i)
        ! 		s_uy(s_no) = uy(i)
        ! 		s_uz(s_no) = uz(i)
        ! 		s_stype(s_no) = stype(i)
        ! 		s_A11(s_no) = A11(i)
        ! 		s_A12(s_no) = A12(i)
        ! 		s_A21(s_no) = A21(i)
        ! 		s_A22(s_no) = A22(i)
        ! 		s_iy1(s_no) = iy1(i)
        ! 		s_iz1(s_no) = iz1(i)
        !
        ! 		if (s_no.ge.supra_buffer) then
        ! 			write(*,*) 'Supra buffer overflow!!!',s_no
        ! 		endif
        ! 	endif
        !
        ! enddo
        s_no_orig = s_no
        ! write(*,*) 'No of supra particles:', s_no
        !make a number of supra loops
        ! if (s_no.gt.0) then
        !
        ! do i=1,no_supra_loops
        ! anp = s_no
        ! !grid them
        ! call grid_particles(count,s_z(1:s_no),s_y(1:s_no),s_iy1(1:s_no),s_iz1(1:anp),dz,dy,anp,s_A11(1:anp),s_A12(1:anp),s_A21(1:anp),&
            ! s_A22(1:anp))
        !
        ! !move them
        ! call leapfrog(anp,Nz,Ny,Ax_lf,Bx_lf,Cx_lf,Dx_lf,Ex_lf,Ay_lf,By_lf,Cy_lf,Dy_lf,Ey_lf,Az_lf,Bz_lf,Cz_lf,Dz_lf,Ez_lf,Lz,&
            ! dtz/real(no_supra_loops),&
            ! s_z(1:anp),s_y(1:anp),s_A11(1:anp),s_A12(1:anp),s_A21(1:anp),s_A22(1:anp),s_ux(1:anp),s_uy(1:anp),s_uz(1:anp),s_iz1(1:anp),&
            ! s_iy1(1:anp),Escz,&
            ! Escy,q,s_stype(1:anp),m,no_species,collision_prob,driftz,Nc,count,dt/real(no_supra_loops),utherm,1,spec_params)
        ! ! check boundaries
        ! call boundary_check_general(s_no,Lz,Ly,Lz,Lz_inj,s_z,s_y,verbose,debug,s_ux,s_uy,s_uz,&
            ! no_part,no_species,s_stype,dz,Na,count,restore,cont,flag,equipot,n,Nz,m,run_inversion,scenario,Ny,flag_m,dy,&
            ! objects,edges,edge_charge,q,dt/real(no_supra_loops),se,spec_params,proc_no,utherm,objects_enum,&
            ! objects_current(proc_no+1,:,:,h_pos),rectangles+triangles+circles,edge_flux,edge_energy_flux,edge_velocity_x,edge_velocity_y,edge_velocity_z,mks_info,Ly_start,Ly_stop,proc_max,domain_decomp,stype_transfer,npts,t_com3,Nz_max,slice_proc,bc_matrix,y_start,y_stop)
        !
        !
        !
        ! enddo
        !
        ! endif

        !move them back
        !run down!
        ! do i=s_no_orig,1,-1
        ! 	if (i.le.s_no) then
        ! 	!move it back
        ! 		y(s_order(i)) = s_y(i)
        ! 		z(s_order(i)) = s_z(i)
        ! 		ux(s_order(i)) = s_ux(i)
        ! 		uy(s_order(i)) = s_uy(i)
        ! 		uz(s_order(i)) = s_uz(i)
        ! ! 		write(*,*)"UZ:",s_uz(i)
        ! 		A11(s_order(i)) = s_A11(i)
        ! 		A12(s_order(i)) = s_A12(i)
        ! 		A21(s_order(i)) = s_A21(i)
        ! 		A22(s_order(i)) = s_A22(i)
        ! 		iy1(s_order(i)) = s_iy1(i)
        ! 		iz1(s_order(i)) = s_iz1(i)
        ! 	else
        ! 		!discsrd
        ! ! 		number(stype(s_order(i)),h_pos) = number(stype(s_order(i)),h_pos) -1
        !    	       z(s_order(i))=z(no_tot)
        ! 	       y(s_order(i))=y(no_tot)
        !                uz(s_order(i))=uz(no_tot)
        ! 	       uy(s_order(i))=uy(no_tot)
        ! 	       ux(s_order(i))=ux(no_tot)
        ! 		stype(s_order(i)) = stype(no_tot)
        !             	no_tot=no_tot-1
        !
        ! 	endif
        !
        ! enddo

        anp = no_tot
        call gettime(timestamp)

        t_lp = timestamp - t_sum - t_start

        if (abs(t_lp).gt.1000e6) then
            t_lp = 0
        endif

        t_sum = t_sum + t_lp





        ! -------------- Boundary checking ---------------------------------------------!
        anp = no_tot
        !MK - new general boundary checking routine
        !Lz1 works only as arough limit for tile checking
        if (debug) then
            write(*,*) 'Sum of stype 1',sum(int(stype))
        endif
        !      if (sum(int(stype)).lt.0) then
        !         write(*,*) 'Error, sum of stype less than 0!!!'
        !        stop
        !   endif

        call boundary_check_general (no_tot,Lz,Ly,Lz_low_limit,Lz_high_limit,Lz_inj,z,y,verbose,debug,ux,uy,uz,vtot_init,use_vtot,&
            no_part,no_species,stype,dz,Na,count,restore,cont,flag,equipot,n,Nz,m,run_inversion,scenario,Ny,flag_m,dy,&
            objects,edges,edge_charge,q,dt,spec_params,proc_no,utherm,objects_enum,objects_current(:,:,count),objects_power_flux(:,:,count),&
            rectangles+triangles+circles,edge_flux,edge_energy_flux,edge_velocity_x,edge_velocity_y,edge_velocity_z,take_diag,&
            mks_info,Ly_start,Ly_stop,proc_max,domain_decomp,stype_transfer,npts,t_com3,Nz_max,slice_proc,bc_matrix,&
            y_start,y_stop,z_start,z_stop,z_start_box,z_stop_box,Lz_disp,total_energy(:,:,count),P0,Pot,use_surface_heat_tweaks,&
            impact_diag,emmit_diag,fv_bin,have_impact_diag,beam_switch,no_diag_reg,particle_no,orientation,lambda_D,w_i,&
            no_objects,object_params,ns,Eout,f_ebs,cdf_ebs,Ax_half,Ay_half,Az_half,Bx_half,By_half,Bz_half,Cx_half,Cy_half,Cz_half,cosalpha_yz)

        ! write(*,*) 'lines',slice_proc(40,:)
        ! write(*,*) 'lines',slice_proc(90,:)

        call gettime(timestamp)

        t_bc = timestamp - t_sum - t_start
        if (abs(t_bc).gt.1000e6) then
            t_bc = 0
        endif

        t_sum = t_sum + t_bc
        ! Coulomb collisions
        if (collie.eq.0.0.and.collee.eq.0.0.and.collii.eq.0.0) then
            ! do nothing
        else
            if (debug) then
                write(*,*) 'Running Coulomb collisions',cell_count_max
            endif

            if (count.gt.Nc) then
                if (debug) then
                    ! check for the conservation of energy and momentum
                    E_tot_before = 0
                    px_before = 0
                    py_before = 0
                    pz_before = 0
                    do i=1,no_tot
                        E_tot_before = E_tot_before + m(stype(i))*(ux(i)*ux(i) + uy(i)*uy(i) + uz(i)*uz(i))
                        px_before = px_before + m(stype(i))*ux(i)
                        py_before = py_before + m(stype(i))*uy(i)
                        pz_before = pz_before + m(stype(i))*uz(i)
                    enddo
                endif ! debug




                call   coulomb_collisions(no_tot,y(1:no_tot),z(1:no_tot),ux(1:no_tot),uy(1:no_tot),uz(1:no_tot),stype(1:no_tot),no_species,m,q,dz,dt,Ny,Nz,Nz_max,rho,mks_info,Npc,collee,collie,collii,cell_count_max,z_start,z_stop,y_start,y_stop)
                ! check for the conservation of energy and momentum
                if (debug) then
                    E_tot_after = 0
                    px_after = 0
                    py_after = 0
                    pz_after = 0

                    do i=1,no_tot
                        E_tot_after = E_tot_after + m(stype(i))*(ux(i)*ux(i) + uy(i)*uy(i) + uz(i)*uz(i))
                        px_after = px_after + m(stype(i))*ux(i)
                        py_after = py_after + m(stype(i))*uy(i)
                        pz_after = pz_after + m(stype(i))*uz(i)

                    enddo
                    write(*,*) 'Energy conservation', E_tot_after/E_tot_before
                    write(*,*) 'px conservation', px_after/px_before
                    write(*,*) 'py conservation', py_after/py_before
                    write(*,*) 'pz conservation', pz_after/pz_before
                endif ! debug
            endif ! count
        endif ! do coulomb

        call gettime(timestamp)

        t_coll = timestamp - t_sum - t_start
        if (abs(t_coll).gt.1000e6) then
            t_coll = 0
        endif

        t_sum = t_sum + t_coll

        ! if (sum(edge_flux(:,:,1)).lt.0.0) then
        !  write(*,*) proc_no,'Wrong edge_flux (after BC)',sum(edge_flux(:,:,1))
        !  stop
        !endif
        !    no_tot = 0
        !  t_com3 = 100
        if (scenario.eq.3.or.scenario.eq.4) then
            anp = no_tot

            call sink_SOL(Nz,Ny,no_species,SOL_W,SOL_dW,no_tot,z(1:anp),y(1:anp),ux(1:anp),uy(1:anp),uz(1:anp),stype(1:anp),iz1(1:anp),iy1(1:anp),no_part,spec_params,dz)
        endif
        anp = no_tot

        call gettime(timestamp)

        t_bc = timestamp - t_sum - t_start
        if (abs(t_bc).gt.1000e6) then
            t_bc = 0
        endif

        t_sum = t_sum + t_bc

        if (domain_decomp.and.proc_max.gt.0) then
            ! call transfer_particles_1(Npts,no_tot,y,z,ux,uy,uz,stype,Ny,Nz_max,slice_proc,verbose,debug,proc_no,proc_max,dy,dz,no_species,stype_transfer)
            ! call  transfer_particles_2(Npts,no_tot,y,z,ux,uy,uz,stype,Ny,Nz_max,slice_proc,verbose,debug,proc_no,proc_max,dy,dz,no_species,stype_transfer,t_sum,t_grid,t_start)
            call transfer_particles_2(Npts,no_tot,y,z,ux,uy,uz,vtot_init,use_vtot,stype,Ny,Nz_max,slice_proc,verbose,debug,proc_no,proc_max,dy,dz,no_species,stype_transfer,t_sum,t_grid,t_start,no_part)

            ! call      transfer_particles_3(Npts,no_tot,y,z,ux,uy,uz,stype,Ny,Nz_max,slice_proc,verbose,debug,proc_no,proc_max,&
                ! dy,dz,no_species,stype_transfer,neigh_slices,t_sum,t_grid,t_start,no_part)


        endif
        call gettime(timestamp)

        t_com3 = timestamp - t_sum - t_start

        if (abs(t_com3).gt.1000e6) then
            t_com3 = 0
        endif

        t_sum = t_sum + t_com3


        !if some grid points got eroded, run inversiona again
        if (run_inversion.and.enable_poisson) then
            if ((psolver.eq.2).or.(psolver.eq.3)) then
                if (proc_no.eq.0) then
                    write (*,*) 'Umfpack inversion'
                    call psolver_direct_periodic(Ny,Nz,P0,PL,n,hzy,numeric,flag,equipot,Nz_disp)
                endif
            else if ((psolver.eq.4) .or. (psolver.eq.5)) then

                if (any(ps_ranks == proc_no)) then

                    if (psolver.eq.5) then
                        call pes_md_stop(mumpsPar)
                        call pes_md_init(mumpsPar, MPI_COMM_SOLVER, nZ, nY, Nz_disp, dZ, dY, flag_m)
                    else
                        call pes_mc_stop(mumpsPar)
                        call pes_mc_init(mumpsPar, MPI_COMM_SOLVER, nZ, nY, Nz_disp, dZ, dY, flag_m)
                    endif
                endif

                call pes_sync()

            else if (psolver.eq.6) then
                write (*,*) "Erosion not yet supported "
                ! not supported yet
            endif

        endif


        loop_stat = loop_stat + 1
        if (debug) then
            write(*,*) count, loop_stat
        endif

        !!!!!!!!!!!!!!!!!!!!!!!!!
        ! diagnostic regions !!!!
        !!!!!!!!!!!!!!!!!!!!!!!!!
        hist_count = 0
        fv_count = 0
        fv_perp_array_count = 0
        do d_count=1,no_diag_reg
            !go one by one and perform diag
            !check par
            !!!! HACK  - REPLACE Nc to Na in case u want properly averaged diagnostics
            !disable

            if (((diag_regions(d_count)%record_only_steady_state).and.(count.gt.Nc)).or.&
                    (.not.diag_regions(d_count)%record_only_steady_state)) then

                ! recording the potential
                if (diag_regions(d_count)%record_property.eq.1) then
                    !record potential
                    k =0
                    hist_count = hist_count + 1
                    diag_hist(hist_count,count) = 0.0
                    !  if (mpi_rank(diag_regions(d_count)%specie).eq.proc_no) then

                    do i=int(diag_regions(d_count)%z_low/dz +1),int(diag_regions(d_count)%z_high/dz +1)
                        do j = int(diag_regions(d_count)%y_low/dy+1),int(diag_regions(d_count)%y_high/dy+1)
                            k = k +1
                            diag_hist(hist_count,count) = diag_hist(hist_count,count) + Pot(i,j)
                            !   write(*,*) 'Pot diag',d_count,Pot(i,j)
                        enddo
                    enddo
                    !  write(*,*) 'Pot diag',d_count,diag_hist(hist_count,count)/k
                    if (k.ne.0) then
                        diag_hist(hist_count,count) = diag_hist(hist_count,count)/k
                    else
                        write(*,*) 'Diag error, no potential grids encountered',hist_count
                    endif
                    hist_limits(hist_count) = count
                    !   endif

                    ! cartesian f(v)
                else if (diag_regions(d_count)%record_property.eq.2) then
                    if (mpi_rank(diag_regions(d_count)%specie).eq.proc_no) then
                        fv_surf = (diag_regions(d_count)%z_high -diag_regions(d_count)%z_low)/(diag_regions(d_count)%y_high -diag_regions(d_count)%y_low)
                        !cartesian f(v)
                        do i=1,no_tot
                            if (stype(i).eq.diag_regions(d_count)%specie) then
                                if (z(i).lt.(diag_regions(d_count)%z_high).and.z(i).gt.(diag_regions(d_count)%z_low).and.y(i).lt.&
                                        (diag_regions(d_count)%y_high).and.y(i).gt.(diag_regions(d_count)%y_low)) then
                                    !x direction
                                    bin = abs(int((ux(i) - fv_limits(fv_count+1,1))*real(fv_bin)/(fv_limits(fv_count+1,2) - &
                                        fv_limits(fv_count+1,1))))
                                    if (bin.gt.0.and.bin.lt.fv_bin) then
                                        fv_ar(fv_count+1,bin) = fv_ar(fv_count+1,bin) +1.0/fv_surf
                                        fv_sum(fv_count+1) = fv_sum(fv_count+1) +1/fv_surf

                                    else
                                        write(*,*) 'xBin out of range',bin,ux(i)
                                        write(*,*) 'check',fv_count,fv_bin,fv_limits(fv_count+1,1),fv_limits(fv_count+1,2)
                                    endif
                                    !y direction
                                    bin = abs(int((uy(i) -fv_limits(fv_count+2,1))*real(fv_bin)/(fv_limits(fv_count+2,2) - &
                                        fv_limits(fv_count+2,1))))
                                    if (bin.gt.0.and.bin.lt.fv_bin) then
                                        fv_ar(fv_count+2,bin) = fv_ar(fv_count+2,bin) +1.0/fv_surf
                                        fv_sum(fv_count+2) = fv_sum(fv_count+2) +1.0/fv_surf
                                    else
                                        write(*,*) 'yBin out of range',bin,uy(i)
                                        write(*,*) 'check',fv_count,fv_bin,fv_limits(fv_count+2,1),fv_limits(fv_count+2,2)
                                    endif

                                    !z direction
                                    bin = abs(int((uz(i) - fv_limits(fv_count+3,1))*real(fv_bin)/(fv_limits(fv_count+3,2) - &
                                        fv_limits(fv_count+3,1))))
                                    if (bin.lt.1) then
                                        bin = 1
                                    endif
                                    if (bin.gt.fv_bin) then
                                        bin = fv_bin
                                    endif

                                    fv_ar(fv_count+3,bin) = fv_ar(fv_count+3,bin) +1.0/fv_surf
                                    fv_sum(fv_count+3) = fv_sum(fv_count+3) +1.0/fv_surf
                                endif !rectangle
                            endif ! specie
                        enddo
                    endif
                    !push fv_count
                    fv_count = fv_count + 3

                    ! B field coords f(v) - v_par, v_perp,phi
                else if (diag_regions(d_count)%record_property.eq.3) then
                    if (mpi_rank(diag_regions(d_count)%specie).eq.proc_no) then
                        fv_surf = (diag_regions(d_count)%z_high -diag_regions(d_count)%z_low)/(diag_regions(d_count)%y_high -diag_regions(d_count)%y_low)

                        do i=1,no_tot
                            if (stype(i).eq.diag_regions(d_count)%specie) then
                                if (z(i).lt.(diag_regions(d_count)%z_high).and.z(i).gt.(diag_regions(d_count)%z_low).and.y(i).lt.&
                                        (diag_regions(d_count)%y_high).and.y(i).gt.(diag_regions(d_count)%y_low)) then
                                    !transformate velicites
                                    u_perp1 =  ux(i)*cos(rbeta) - uy(i)*sin(rbeta)
                                    u_par = ux(i)*sin(rbeta)*cos(ralpha) + uy(i)*cos(ralpha)*cos(rbeta) - uz(i)*sin(ralpha)
                                    u_perp2 = ux(i)*sin(rbeta)*sin(ralpha) + uy(i)*sin(ralpha)*cos(rbeta) + uz(i)*cos(ralpha)
                                    u_perp   = dsqrt(u_perp1*u_perp1 + u_perp2*u_perp2)
                                    rphi = atan(u_perp2/u_perp1)
                                    ! 			write(*,*) 'Trans:',u_par,u_perp1,u_perp2
                                    ! 			write(*,*) 'Dia:',u_par,u_perp,rphi
                                    !u parallel
                                    bin = abs(int((u_par - fv_limits(fv_count+1,1))*real(fv_bin)/(fv_limits(fv_count+1,2) - &
                                        fv_limits(fv_count+1,1))))
                                    if (bin.gt.0.and.bin.lt.fv_bin) then
                                        fv_ar(fv_count+1,bin) = fv_ar(fv_count+1,bin) +1.0/fv_surf
                                        fv_sum(fv_count+1) = fv_sum(fv_count+1) +1

                                    else
                                        write(*,*) 'upar out of range',bin,u_par
                                        write(*,*) 'check',fv_count,fv_bin,fv_limits(fv_count+1,1),fv_limits(fv_count+1,2)
                                    endif
                                    !u perpendicular
                                    bin = abs(int((u_perp -fv_limits(fv_count+2,1))*real(fv_bin)/(fv_limits(fv_count+2,2) -&
                                        fv_limits(fv_count+2,1))))
                                    if (bin.gt.0.and.bin.lt.fv_bin) then
                                        fv_ar(fv_count+2,bin) = fv_ar(fv_count+2,bin) +1.0/fv_surf
                                        fv_sum(fv_count+2) = fv_sum(fv_count+2) +1
                                    else
                                        write(*,*) 'u_perp out of range',bin,u_perp
                                        write(*,*) 'check',fv_count,fv_bin,fv_limits(fv_count+2,1),fv_limits(fv_count+2,2)
                                    endif

                                    !phase
                                    fv_limits(fv_count+3,1) = -3.5/2.0
                                    fv_limits(fv_count+3,2) = 3.5/2.0
                                    bin = abs(int((rphi - fv_limits(fv_count+3,1))*real(fv_bin)/(fv_limits(fv_count+3,2) -&
                                        fv_limits(fv_count+3,1))))
                                    if (bin.lt.1) then
                                        bin = 1
                                    endif
                                    if (bin.gt.fv_bin) then
                                        bin = fv_bin
                                    endif

                                    fv_ar(fv_count+3,bin) = fv_ar(fv_count+3,bin) +1.0/fv_surf
                                    fv_sum(fv_count+3) = fv_sum(fv_count+3) +1
                                endif !rectangle
                            endif ! specie
                        enddo
                    endif
                    !push fv_count
                    fv_count = fv_count + 3
                    ! mean velocity components
                else if (diag_regions(d_count)%record_property.eq.4) then

                    k =0
                    do i=1,no_tot
                        if (diag_regions(d_count)%specie.eq.stype(i)) then
                            if (z(i).lt.(diag_regions(d_count)%z_high).and.z(i).gt.(diag_regions(d_count)%z_low).and.y(i).lt.&
                                    (diag_regions(d_count)%y_high).and.y(i).gt.(diag_regions(d_count)%y_low)) then

                                k = k +1
                                diag_hist(hist_count+1,count) = diag_hist(hist_count+1,count) + ux(i)
                                diag_hist(hist_count+2,count) = diag_hist(hist_count+2,count) + uy(i)
                                diag_hist(hist_count+3,count) = diag_hist(hist_count+3,count) + uz(i)

                            endif
                        endif

                    enddo
                    if (k.ne.0) then
                        diag_hist(hist_count+1,count) = diag_hist(hist_count+1,count)/real(k)
                        diag_hist(hist_count+2,count) = diag_hist(hist_count+2,count)/real(k)
                        diag_hist(hist_count+3,count) = diag_hist(hist_count+3,count)/real(k)
                        !           write(*,*) 'Diag check',diag_hist(hist_count+3,count)

                    endif



                    hist_limits(hist_count+1) = count
                    hist_limits(hist_count+2) = count
                    hist_limits(hist_count+3) = count
                    hist_count = hist_count+3


                    !B field coords f(v) - f(v_par), f(v_perp1,v_perp2)
                else if (diag_regions(d_count)%record_property.eq.5) then
                    if (mpi_rank(diag_regions(d_count)%specie).eq.proc_no) then
                        fv_surf = (diag_regions(d_count)%z_high -diag_regions(d_count)%z_low)/(diag_regions(d_count)%y_high -diag_regions(d_count)%y_low)

                        do i=1,no_tot
                            if (stype(i).eq.diag_regions(d_count)%specie) then
                                if (z(i).lt.(diag_regions(d_count)%z_high).and.z(i).gt.(diag_regions(d_count)%z_low).and.y(i).lt.&
                                        (diag_regions(d_count)%y_high).and.y(i).gt.(diag_regions(d_count)%y_low)) then
                                    !transformate velicites
                                    u_perp1 =  ux(i)*cos(rbeta) - uy(i)*sin(rbeta)
                                    u_par = ux(i)*sin(rbeta)*cos(ralpha) - uy(i)*cos(ralpha)*cos(rbeta) - uz(i)*sin(ralpha)
                                    u_perp2 = ux(i)*sin(rbeta)*sin(ralpha) + uy(i)*sin(ralpha)*cos(rbeta) + uz(i)*cos(ralpha)
                                    !                      write(*,*) 'Zcheck:',u_par,uz(i)
                                    !                      write(*,*) 'Ycheck:',u_perp2,uy(i),ralpha,rbeta
                                    !                      write(*,*) 'Xcheck:',u_perp1,ux(i),cos(ralpha),cos(rbeta)

                                    ! 			u_perp   = dsqrt(u_perp1*u_perp1 + u_perp2*u_perp2)
                                    ! 			rphi = atan(u_perp2/u_perp1)
                                    ! 			write(*,*) 'Trans:',u_par,u_perp1,u_perp2
                                    ! 			write(*,*) 'Dia:',u_par,u_perp,rphi
                                    !u parallel
                                    bin = abs(int((u_par - fv_limits(fv_count+1,1))*real(fv_bin)/(fv_limits(fv_count+1,2) - &
                                        fv_limits(fv_count+1,1))))
                                    if (bin.lt.1) then
                                        bin = 1
                                    endif
                                    if (bin.gt.fv_bin) then
                                        bin = fv_bin
                                    endif


                                    fv_ar(fv_count+1,bin) = fv_ar(fv_count+1,bin) +1.0/fv_surf
                                    fv_sum(fv_count+1) = fv_sum(fv_count+1) +1

                                    !u perpendicular1
                                    bin1 = abs(int((u_perp1 -fv_limits(fv_count+1,1))*real(fv_bin)/(fv_limits(fv_count+1,2) - &
                                        fv_limits(fv_count+1,1))))


                                    !u perpendicular2
                                    bin2 = abs(int((u_perp2 -fv_limits(fv_count+1,1))*real(fv_bin)/(fv_limits(fv_count+1,2) - &
                                        fv_limits(fv_count+1,1))))

                                    if (bin1.lt.1) then
                                        bin1 = 1
                                    endif
                                    if (bin1.gt.fv_bin) then
                                        bin1 = fv_bin
                                    endif
                                    if (bin2.lt.1) then
                                        bin2 = 1
                                    endif
                                    if (bin2.gt.fv_bin) then
                                        bin2 = fv_bin
                                    endif

                                    u_perp_a(fv_perp_array_count+1,bin1,bin2) = u_perp_a(fv_perp_array_count+1,bin1,bin2) +1.0/fv_surf



                                endif !rectangle
                            endif ! specie
                        enddo
                    endif
                    !push fv_count
                    fv_count = fv_count + 1
                    fv_perp_array_count =fv_perp_array_count +1



                else if (diag_regions(d_count)%record_property.eq.8) then
                    if (mpi_rank(diag_regions(d_count)%specie).eq.proc_no) then
                        fv_surf = (diag_regions(d_count)%z_high -diag_regions(d_count)%z_low)*(diag_regions(d_count)%y_high -diag_regions(d_count)%y_low)
                        !B field coords f(v) - f(v_par), f(v_perp1,v_perp2)
                        do i=1,no_tot
                            if (stype(i).eq.diag_regions(d_count)%specie) then
                                if (z(i).lt.(diag_regions(d_count)%z_high).and.z(i).gt.(diag_regions(d_count)%z_low).and.y(i).lt.&
                                        (diag_regions(d_count)%y_high).and.y(i).gt.(diag_regions(d_count)%y_low)) then
                                    !transformate velicites
                                    !u_perp1 =  ux(i)*cos(rbeta) - uy(i)*sin(rbeta)
                                    !u_par = ux(i)*sin(rbeta)*cos(ralpha) - uy(i)*cos(ralpha)*cos(rbeta) - uz(i)*sin(ralpha)
                                    !u_perp2 = ux(i)*sin(rbeta)*sin(ralpha) + uy(i)*sin(ralpha)*cos(rbeta) + uz(i)*cos(ralpha)

                                    ! interpolate E field to the position of the particle
                                    zi = int(z(i)/dz)
                                    yi = int(y(i)/dy)
                                    dzi = (z(i) - zi*dz)/dz
                                    dyi = (y(i) - yi*dy)/dy
                                    sp = stype(i)
                                    Eza = Escz(zi,yi)*(1-dzi)*(1-dyi) + Escz(zi+1,yi)*(dzi)*(1-dyi) + Escz(zi,yi+1)*(1-dzi)*(dyi) + Escz(zi+1,yi+1)*(dzi)*(dyi)
                                    Eya = Escy(zi,yi)*(1-dzi)*(1-dyi) + Escy(zi+1,yi)*(dzi)*(1-dyi) + Escy(zi,yi+1)*(1-dzi)*(dyi) + Escy(zi+1,yi+1)*(dzi)*(dyi)

                                    ! peform a half turn forward for the velocities to get velocity at the position of the particle
                                    ux_temp=ux(i)*Ax_lf(sp) + uy(i)*Bx_lf(sp) + uz(i)*Cx_lf(sp) + Eya*Dx_lf(sp) - Eza*Ex_lf(sp)

                                    uy_temp=ux(i)*Ay_lf(sp) + uy(i)*By_lf(sp) + uz(i)*Cy_lf(sp) + q(sp)*Eya*Dy_lf(sp) + Eza*Ey_lf(sp)

                                    uz_temp=ux(i)*Az_lf(sp) + uy(i)*Bz_lf(sp) + uz(i)*Cz_lf(sp) + Eya*Dz_lf(sp) + q(sp)*Eza*Ez_lf(sp)
                                    ! check the energy difference
                                    !   write(*,*) 'Energy check',i,(ux(i)*ux(i) + uy(i)*uy(i) + uz(i)*uz(i))/(ux_temp*ux_temp + uy_temp*uy_temp + uz_temp*uz_temp)
                                    ! get mean with the old velocity
                                    ux_temp = (ux_temp + ux(i))/2.0
                                    uy_temp = (uy_temp + uy(i))/2.0
                                    uz_temp = (uz_temp + uz(i))/2.0
                                    ! tot al energy
                                    bin1 = abs(int((ux_temp*ux_temp + uy_temp*uy_temp + uz_temp*uz_temp )*real(fv_bin)/(400.0*utherm(stype(i))*utherm(stype(i)))))
                                    if (bin1.lt.1) then
                                        bin1 = 1
                                    endif
                                    if (bin1.gt.fv_bin) then
                                        bin1 = fv_bin
                                    endif

                                    !u perpendicular2
                                    rphi = asin(-uz_temp/sqrt(ux_temp*ux_temp + uy_temp*uy_temp + uz_temp*uz_temp))/3.14159*180
                                    bin2 = abs(int((rphi)*real(fv_bin)/100.0))
                                    if (bin2.lt.1) then
                                        bin2 = 1
                                    endif
                                    if (bin2.gt.fv_bin) then
                                        bin2 = fv_bin
                                    endif



                                    u_perp_a(fv_perp_array_count+1,bin1,bin2) = u_perp_a(fv_perp_array_count+1,bin1,bin2) +1.0/fv_surf


                                endif !rectangle
                            endif ! specie
                        enddo
                    endif
                    !push fv_count
                    fv_perp_array_count =fv_perp_array_count +1







                else if (diag_regions(d_count)%record_property.eq.7) then
                    fv_surf = (diag_regions(d_count)%z_high -diag_regions(d_count)%z_low)/(diag_regions(d_count)%y_high -diag_regions(d_count)%y_low)

                    if (mpi_rank(diag_regions(d_count)%specie).eq.proc_no) then
                        do i=1,no_tot
                            if (stype(i).eq.diag_regions(d_count)%specie) then
                                if ((z(i).gt.diag_regions(d_count)%z_low).and.(z(i).lt.diag_regions(d_count)%z_high).and.(y(i).gt.&
                                        diag_regions(d_count)%y_low).and.(y(i).lt.diag_regions(d_count)%y_high)) then
                                    rphi = atan(uy(i)/ux(i))
                                    ! 			fv_limits(fv_count+1,1) = -3.5/2.0
                                    ! 			fv_limits(fv_count+1,2) = 3.5/2.0
                                    bin = abs(int((rphi - fv_limits(fv_count+1,1))*real(fv_bin)/(fv_limits(fv_count+1,2) - &
                                        fv_limits(fv_count+1,1))))
                                    if (bin.gt.0.and.bin.lt.fv_bin) then
                                        fv_ar(fv_count+1,bin) = fv_ar(fv_count+1,bin) +1.0/fv_surf
                                    else
                                        write(*,*) 'phi out of range',bin,rphi
                                        write(*,*) 'check',fv_count,fv_bin,fv_limits(fv_count+1,1),fv_limits(fv_count+1,2)

                                    endif

                                    !  exit
                                endif
                            endif
                        enddo

                    endif
                    fv_count = fv_count + 1
                else if (diag_regions(d_count)%record_property.eq.6) then
                    if (mpi_rank(diag_regions(d_count)%specie).eq.proc_no) then
                        if ((count.gt.2).and.(particle_no(d_count).eq.-1)) then
                            ! if the bondary was reached, mark it by zeros
                            diag_hist(hist_count +1,count-1) = 0
                            diag_hist(hist_count +2,count-1) = 0
                            diag_hist(hist_count +3,count-1) = 0
                            diag_hist(hist_count +4,count-1) = 0
                            diag_hist(hist_count +5,count-1) = 0
                        endif
                        if (((count.gt.Na).or.(.not.diag_regions(d_count)%record_only_steady_state)).and.(particle_no(d_count).eq.-1)) then
                            !we need to choose a particle from the given rectangle
                            !let's pick first one
                            do i=1,no_tot
                                if (stype(i).eq.diag_regions(d_count)%specie) then
                                    if ((z(i).gt.diag_regions(d_count)%z_low).and.(z(i).lt.diag_regions(d_count)%z_high).and.(y(i).gt.&
                                            diag_regions(d_count)%y_low).and.(y(i).lt.diag_regions(d_count)%y_high)) then
                                        particle_no(d_count) = i
                                        exit
                                    endif
                                endif
                            enddo
                            !if there wasn't any, choose particle no 1

                            if (verbose.and.particle_no(d_count).gt.0) then
                                write(*,*) 'Selected particle',particle_no(d_count),stype(particle_no(d_count)),count
                                write(*,*) 'Selected particle pos',y(particle_no(d_count)),z(particle_no(d_count))
                                write(*,*) 'Selected particle vel',ux(particle_no(d_count)),uy(particle_no(d_count)),uz(particle_no(d_count))


                            endif
                        endif !choose particle
                        !if the particle has reached the boundary, we start tracking a new one.
                        ! having one sample of all zeros shall serve as a mark
                        if (hist_limits(hist_count+2).gt.1.and.particle_no(d_count).gt.0) then
                            if ((abs(diag_hist(hist_count +2,count-1) - z(particle_no(d_count))).gt.10.0).and.(particle_no(d_count)&
                                    .ne.-1).and.(diag_hist(hist_count +2,&
                                    count-1).ne.0.0)) then
                                if (verbose) then
                                    write(*,*) 'Particle reached boundary, restart tracking'
                                    write(*,*) 'Check',diag_hist(hist_count +2,hist_limits(hist_count+2)-1),particle_no(d_count)
                                endif
                                particle_no(d_count) = -1
                                !jump one step forward
                                hist_limits(hist_count + 1) = count
                                hist_limits(hist_count + 2) = count
                                hist_limits(hist_count + 3) = count
                                hist_limits(hist_count + 4) = count
                                hist_limits(hist_count + 5) = count

                            endif
                        endif

                        !otherwise store particle position and velocities
                        if ((particle_no(d_count).ne.-1).and.(particle_no(d_count).lt.no_tot)) then
                            !y coord
                            diag_hist(hist_count +1,count) = y(particle_no(d_count))
                            hist_limits(hist_count + 1) = count
                            !z coord
                            diag_hist(hist_count +2,count) = z(particle_no(d_count))
                            hist_limits(hist_count + 2) = count
                            !ux coord
                            diag_hist(hist_count +3,count) = ux(particle_no(d_count))
                            hist_limits(hist_count + 3) = count
                            !uy coord
                            diag_hist(hist_count +4,count) = uy(particle_no(d_count))
                            hist_limits(hist_count + 4) = count
                            !uz coord
                            diag_hist(hist_count +5,count) = uz(particle_no(d_count))
                            hist_limits(hist_count + 5) = count
                        else
                            ! just fill the vectors with zeros
                            !y coord
                            diag_hist(hist_count +1,count) = 0
                            hist_limits(hist_count + 1) = count
                            !z coord
                            diag_hist(hist_count +2,count) = 0
                            hist_limits(hist_count + 2) = count
                            !ux coord
                            diag_hist(hist_count +3,count) = 0
                            hist_limits(hist_count + 3) = count
                            !uy coord
                            diag_hist(hist_count +4,count) = 0
                            hist_limits(hist_count + 4) = count
                            !uz coord
                            diag_hist(hist_count +5,count) = 0
                            hist_limits(hist_count + 5) = count

                        endif

                        if (particle_no(d_count).ge.no_tot) then
                            if (verbose) then
                                write(*,*) 'Warning! Lost track of particle', particle_no(d_count)
                                !                            write(*,*) 'Warning! Now tracking particle', particle_no(d_count)
                            endif
                            particle_no(d_count) =  -1
                            ! particle_no(d_count) = int(real(no_tot)/2.0)

                        endif

                    endif
                    hist_count = hist_count + 5
                    !           else if (diag_regions(d_count)%record_property.eq.4) then
                    !                    if (mpi_rank(diag_regions(d_count)%specie).eq.proc_no) then
                    !                        !MHD time histories
                    !                        bin = 0
                    !                        hist_count = hist_count + 1
                    !                        do i=1,no_tot
                    !                            if (stype(i).eq.diag_regions(d_count)%specie) then
                    !                                if ((z(i).gt.diag_regions(d_count)%z_low).and.(z(i).lt.diag_regions(d_count)%z_high).and.&
                        !                                        (y(i).gt.diag_regions(d_count)%y_low).and.(y(i).lt.diag_regions(d_count)%y_high)) then
                    !                                    if (diag_regions(d_count)%record_interval.eq.1.0) then
                    !                                        ! density
                    !                                        bin = bin +1
                    !                                    else if (diag_regions(d_count)%record_interval.eq.2.0) then
                    !                                        ! jx
                    !                                        bin = bin + ux(i)
                    !                                    else if (diag_regions(d_count)%record_interval.eq.3.0) then
                    !                                        ! jy
                    !                                        bin = bin + uy(i)
                    !                                    else if (diag_regions(d_count)%record_interval.eq.4.0) then
                    !                                        ! jz
                    !                                        bin = bin + uz(i)
                    !                                    else if (diag_regions(d_count)%record_interval.eq.5.0) then
                    !                                        ! T
                    !                                        bin = bin + ux(i)*ux(i) + uy(i)*uy(i) + uz(i)*uz(i)
                    !
                    !                                    endif
                    !                                endif
                    !                            endif
                    !                        enddo
                    !                        diag_hist(hist_count,count) = diag_hist(hist_count,count) + bin
                    !                        !divide by the region
                    !                        diag_hist(hist_count,count) = diag_hist(hist_count,count)/(diag_regions(d_count)%z_high -&
                        !                            diag_regions(d_count)%z_low)/(diag_regions(d_count)%y_high - diag_regions(d_count)%y_low)/real(N0)
                    !                        hist_limits(hist_count) = count

                    !                   endif
                endif ! do diag
            endif !par
        enddo
        call gettime(timestamp)

        t_diag = timestamp - t_sum - t_start

        if (abs(t_diag).gt.1000e6) then
            t_diag = 0
        endif

        t_sum  = t_sum + t_diag


        !-------------------------------------injection-------------------------------------1
        do sp=1,no_species
            if (proc_no.eq.mpi_rank(sp)) then
                !    write(*,*) sp,'injection',Ninj_fx(sp),Ninj_r(sp)
                if (injection_method(sp).lt.5) then
                    if (count.eq.1) then
                        Ninj(sp)=idint(Ninj_fx(sp))
                        Ninj_r(sp)=Ninj_fx(sp)-idint(Ninj_fx(sp))
                    else
                        Ninj(sp)=idint((Ninj_fx(sp))+Ninj_r(sp))
                        Ninj_r(sp)=(Ninj_fx(sp))+Ninj_r(sp)-Ninj(sp)
                    endif
                    npt = no_tot + Ninj(sp)
                else ! injection of thermoionic electrons
                    Ninj(sp) = 0
                    Ninj_r(sp)=Ninj_fx(sp) + Ninj_r(sp)
                endif
                if(npt.gt.Npts) then
                    write(*,*) proc_no,'Error, particle vector exhausted (injection)',npt,Npts,Ninj(sp)
                    write(*,*) proc_no,'Increase the npts_ratio parameter in the input file and restart the simulation with -c flag!'
                    write(*,*) proc_no, 'Emergency halt'
                    stop
                endif
                if (debug) then
                    write(*,*) count,'Species',sp, 'injection',Ninj(sp),no_tot
                endif
                if (injection_method(sp).eq.0) then
                    call inject_top(Ninj(sp),no_tot,npt,bx,by,bz,z(1:npt),y(1:npt),ux(1:npt),uy(1:npt),uz(1:npt),mono(sp),Lz,Ly,dt,&
                        utherm(sp),sqbx2by2,N_u_z(sp),u_z(sp,1:N_u_z(sp)),ufu_z(sp,1:N_u_z(sp)),Xu_z(sp,1:N_u_z(sp)),&
                        Ax_half(sp),Bx_half(sp),Cx_half(sp),Ay_half(sp),By_half(sp),Cy_half(sp),Az_half(sp),Bz_half(sp),Cz_half(sp),&
                        midLz(sp),stype(1:npt),sp,m(sp),q(sp),1.0d0,Umono(sp),Temp(sp),&
                        spec_params,no_species,no_supra_loops,Ly_start,Ly_stop,z_start,z_stop,dz,Lz_disp,debug,proc_no,total_energy(:,:,count),s_i_s%coll_test)
                    
                    !	no_tot = npt
                else if (injection_method(sp).eq.1) then
                    call inject_bottom(Ninj(sp),no_tot,npt,bx,by,bz,z(1:npt),y(1:npt),ux(1:npt),uy(1:npt),uz(1:npt),mono(sp),Lz,Ly,dt,&
                        utherm(sp),sqbx2by2,N_u_z(sp),u_z(sp,1:N_u_z(sp)),ufu_z(sp,1:N_u_z(sp)),Xu_z(sp,1:N_u_z(sp)),&
                        Ax_half(sp),Bx_half(sp),Cx_half(sp),Ay_half(sp),By_half(sp),Cy_half(sp),Az_half(sp),Bz_half(sp),Cz_half(sp),&
                        midLz(sp),stype(1:npt),sp,m(sp),q(sp),1.0d0,Umono(sp),Temp(sp),spec_params,no_species,no_supra_loops,Ly_start,Ly_stop,z_start,z_stop,dz,proc_no)
                    !        no_tot = npt
                else if (injection_method(sp).eq.3.or.injection_method(sp).eq.4) then
                    SOL_Nsr(sp,:,:)=SOL_Nsr(sp,:,:)+SOL_Ns(sp,:,:)
                    !  write(*,*) 'Ninj SOL',sp,sum(idint(SOL_Nsr(sp,:,1)))
                    !  stop
                    npt = no_tot + sum(idint(SOL_Nsr(sp,:,1)))
                    ! write(*,*) 'SOL injeciton',sp,sum(idint(SOL_Nsr(sp,:,:)))
                    call source_sol(sp,no_part(sp),Nz,Ny,Ly,Lz,SOL_Nsr(sp,:,:),SOL_Ns(sp,:,:),no_tot,SOL_a(sp,:,:),&
                        SOL_b(sp,:,:),SOL_asq(sp,:,:),SOL_bsq(sp,:,:),SOL_bminusa(sp,:,:),npt,dz,dy,bx,by,bz,utherm(sp),sqbx2by2,Ax_half(sp),Bx_half(sp),Cx_half(sp),Ay_half(sp),&
                        By_half(sp),Cy_half(sp),Az_half(sp),Bz_half(sp),Cz_half(sp),y(1:npt),z(1:npt),ux(1:npt),uy(1:npt),uz(1:npt),stype(1:npt))
                    no_tot = npt

                else if (injection_method(sp).eq.5) then
                    ! thermionic emission
                    npt = Npts
                    i = no_tot
                    ! write(*,*) count,'Injecting',Ninj_r(sp)
                    if (mks_info%mks_main_ion_m.gt.0) then
                        q_therm =  q(sp)*dsqrt(m(sp)*1837.0*mks_info%mks_main_ion_m)
                    else
                        q_therm =  q(sp)*dsqrt(m(sp)*1837.0*2)
                    endif
                    !   write(*,*) 'use_vtot',use_vtot
                    call inject_thermionic(Ninj(sp),no_tot,npt,bx,by,bz,z(1:npt),y(1:npt),ux(1:npt),uy(1:npt),uz(1:npt),vtot_init(1:npt),use_vtot,mono(sp),Lz,Ly,dt,&
                        utherm(sp),sqbx2by2,N_u_z(sp),u_z(sp,1:N_u_z(sp)),ufu_z(sp,1:N_u_z(sp)),Xu_z(sp,1:N_u_z(sp)),&
                        Ax_half(sp),Bx_half(sp),Cx_half(sp),Ay_half(sp),By_half(sp),Cy_half(sp),Az_half(sp),Bz_half(sp),Cz_half(sp),&
                        midLz(sp),stype(1:npt),sp,m(sp),q(sp),1.0d0,Umono(sp),Temp(sp),&
                        spec_params(sp),no_species,no_supra_loops,Ly_start,Ly_stop,z_start,z_stop,dz,Lz_disp,debug,proc_no,Nz,Ny,edges,objects,&
                        dy,no_part(sp),Ninj_r(sp),edge_flux(:,:,sp),edge_energy_flux(:,:,sp),take_diag,objects_current(:,:,count),no_objects,&
                        q_therm,use_surface_heat_tweaks,mks_info,Ts,N0, have_impact_diag,fv_bin,emmit_diag,count,Na,cosalpha_xz,cosalpha_yz,alpha_yz,alpha_xz,ksi)

                    npt = no_tot
                endif
                no_part(sp) = no_part(sp) + npt - no_tot

            endif !par
        enddo
        ! control check
        if (debug) then
            write(*,*) 'After injection',no_tot,sum(z(1:no_tot)),sum(y(1:no_tot))
        endif


        call gettime(timestamp)

        t_inj = timestamp - t_sum - t_start

        if (abs(t_inj).gt.1000e6) then
            t_inj = 0
        endif

        t_sum = t_sum + t_inj

        loop_stat = loop_stat + 1
        if (debug) then
            write(*,*) count, loop_stat
        endif


        ! debug - track first ion's position and velocities
        ! write(*,*) 'Pos',z(1),y(1)
        ! write(*,*) 'Vel',ux(1),uy(1),ux(1)
        !number of particles per processor


        !time histories
        if (count.eq.delta_h*int(real(count)/real(delta_h))) then
            no_part_proc(proc_no +1,h_pos) = no_tot
            ! construct number
            no_part = 0
            do i=1,no_tot
                no_part(stype(i)) =no_part(stype(i))+ 1
            enddo

            do sp=1,no_species
                number(sp,h_pos)=no_part(sp)
            enddo
            Esct(h_pos)=Escz(idint((Lz-1)*Nz/Lz)+1,idint(Ny/2.d0))
            dPHIqn(h_pos)=Pot(idint((Lz-10)*Nz/Lz)+1,1)-Pot(idint(15/dz+1.d-10)+1,idint(Ny/2.d0))
            pot_chi(h_pos) = act_pot_chi
            ! iteration time
            iteration_time_hist(proc_no +1,h_pos) = t_grid + t_wt + t_gd + t_lp + t_inj + t_com3
            !	iteration_time_hist(proc_no +1,h_pos) = t_grid + t_wt + t_lp + t_inj
            ! species injection rate
            do sp=1,no_species
                injection_rate_hist(sp,h_pos) = inj_ratio(sp)
            enddo
            ! we want object currents to be normalized to 1 timestep
            ! objects currents are now without combing
            !          objects_current(:,:,h_pos) = objects_current(:,:,h_pos)/delta_h
            !	objects_power_flux(:,:,h_pos) = objects_power_flux(:,:,h_pos)/delta_h

            h_pos = h_pos + 1
        endif

        !combing
        if (h_pos.eq.history_ntimes) then

            call icomb_history(no_part_proc(proc_no+1,:),history_ntimes)
            do sp=1,no_species
                call icomb_history(number(sp,:),history_ntimes)
            enddo
            call rcomb_history(Esct,history_ntimes)
            call rcomb_history(dPhiqn,history_ntimes)
            call rcomb_history(pot_chi,history_ntimes)
            do i=1,proc_max+1
                call rcomb_history(iteration_time_hist(i,:),history_ntimes)
            enddo
            !     do i=1,rectangles+triangles+circles
            !         do j=1,2
            !             call rcomb_history(objects_current(j,i,:),history_ntimes)
            !     call rcomb_history(objects_power_flux(j,i,:),history_ntimes)
            !
            !               enddo
            !          enddo

            do sp=1,no_species
                call rcomb_history(injection_rate_hist(sp,:),history_ntimes)
            enddo
            !double the  interval between measurements
            delta_h = delta_h*2
            !set the position to the middle - very unwise to use odd number of samples ;)
            h_pos = int(history_ntimes/2)
            ! rewrite t with new timestep
            do i=1,history_ntimes
                t(i) = i*dtz*delta_h
            enddo
        endif


        !Make diagnostic measurements
        !============================
        if (count.eq.1) then
            t(count) = 0.0
        endif
        do sp=1,no_species
            !    do k=1,Ny
            !       vwall(sp,count)=vwall(sp,count)+vz(sp,1,k)   ! a verifier
            !    enddo
            !disable
            if ((count >= Na).and.(take_diag).and.(mpi_rank(sp).eq.proc_no)) then
                if (nav.gt.0) then
                    dens(sp,:,:)=dens(sp,:,:)*real(nav)/(real(nav)+1.0)+rho(sp,1:Nz,:)/(real(nav)+1.0)/(N0*dz*dy)
                    vzav(sp,:,:)=vzav(sp,:,:)*real(nav)/(real(nav)+1.0)+vz(sp,:,:)/(real(nav)+1.0)
                    vyav(sp,:,:)=vyav(sp,:,:)*real(nav)/(real(nav)+1.0)+vy(sp,:,:)/(real(nav)+1.0)
                    vxav(sp,:,:)=vxav(sp,:,:)*real(nav)/(real(nav)+1.0)+vx(sp,:,:)/(real(nav)+1.0)
                    vz2av(sp,:,:)=vz2av(sp,:,:)*real(nav)/(real(nav)+1.0)+vz2(sp,:,:)/(real(nav)+1.0)
                    vz3av(sp,:,:)=vz3av(sp,:,:)*real(nav)/(real(nav)+1.0)+vz3(sp,:,:)/(real(nav)+1.0)
                    vy2av(sp,:,:)=vy2av(sp,:,:)*real(nav)/(real(nav)+1.0)+vy2(sp,:,:)/(real(nav)+1.0)
                    vy3av(sp,:,:)=vy3av(sp,:,:)*real(nav)/(real(nav)+1.0)+vy3(sp,:,:)/(real(nav)+1.0)
                    vx2av(sp,:,:)=vx2av(sp,:,:)*real(nav)/(real(nav)+1.0)+vx2(sp,:,:)/(real(nav)+1.0)
                    vx3av(sp,:,:)=vx3av(sp,:,:)*real(nav)/(real(nav)+1.0)+vx3(sp,:,:)/(real(nav)+1.0)
                    temperature_ave(sp,:,:)=temperature_ave(sp,:,:)*real(nav)/(real(nav)+1.0)+temperature(sp,:,:)/(real(nav)+1.0)

                    edge_flux(:,1,sp) = edge_flux(:,Ny,sp) + edge_flux(:,1,sp)
                    edge_flux(:,Ny,sp) = edge_flux(:,1,sp)

                    edge_energy_flux(:,1,sp) = edge_energy_flux(:,Ny,sp) + edge_energy_flux(:,1,sp)
                    edge_energy_flux(:,Ny,sp) = edge_energy_flux(:,1,sp)

                    edge_velocity_x(:,1,sp) = edge_velocity_x(:,Ny,sp) + edge_velocity_x(:,1,sp)
                    edge_velocity_x(:,Ny,sp) = edge_velocity_x(:,1,sp)

                    edge_velocity_y(:,1,sp) = edge_velocity_y(:,Ny,sp) + edge_velocity_y(:,1,sp)
                    edge_velocity_y(:,Ny,sp) = edge_velocity_y(:,1,sp)


                    edge_velocity_z(:,1,sp) = edge_velocity_z(:,Ny,sp) + edge_velocity_z(:,1,sp)
                    edge_velocity_z(:,Ny,sp) = edge_velocity_z(:,1,sp)

                    edge_flux_ave(:,:,sp) = edge_flux_ave(:,:,sp)*real(nav)/(real(nav)+1.0)+edge_flux(:,:,sp)/(real(nav)+1.0)*dz/dt/Npc
                    edge_energy_flux_ave(:,:,sp) = edge_energy_flux_ave(:,:,sp)*real(nav)/(real(nav)+1.0)+edge_energy_flux(:,:,sp)/(real(nav)+1.0)*dz/dt/Npc*0.5*m(sp)
                    edge_velocity_x_ave(:,:,sp) = edge_velocity_x_ave(:,:,sp)*real(nav)/(real(nav)+1.0)+edge_velocity_x(:,:,sp)/(real(nav)+1.0)*dz/dt/Npc
                    edge_velocity_y_ave(:,:,sp) = edge_velocity_y_ave(:,:,sp)*real(nav)/(real(nav)+1.0)+edge_velocity_y(:,:,sp)/(real(nav)+1.0)*dz/dt/Npc
                    edge_velocity_z_ave(:,:,sp) = edge_velocity_z_ave(:,:,sp)*real(nav)/(real(nav)+1.0)+edge_velocity_z(:,:,sp)/(real(nav)+1.0)*dz/dt/Npc

                else
                    edge_flux(:,1,sp) = edge_flux(:,Ny,sp) + edge_flux(:,1,sp)
                    edge_flux(:,Ny,sp) = edge_flux(:,1,sp)

                    edge_energy_flux(:,1,sp) = edge_energy_flux(:,Ny,sp) + edge_energy_flux(:,1,sp)
                    edge_energy_flux(:,Ny,sp) = edge_energy_flux(:,1,sp)

                    edge_velocity_x(:,1,sp) = edge_velocity_x(:,Ny,sp) + edge_velocity_x(:,1,sp)
                    edge_velocity_x(:,Ny,sp) = edge_velocity_x(:,1,sp)

                    edge_velocity_y(:,1,sp) = edge_velocity_y(:,Ny,sp) + edge_velocity_y(:,1,sp)
                    edge_velocity_y(:,Ny,sp) = edge_velocity_y(:,1,sp)


                    edge_velocity_z(:,1,sp) = edge_velocity_z(:,Ny,sp) + edge_velocity_z(:,1,sp)
                    edge_velocity_z(:,Ny,sp) = edge_velocity_z(:,1,sp)

                    edge_flux_ave(:,:,sp) = edge_flux(:,:,sp)*dz/dt/Npc
                    edge_energy_flux_ave(:,:,sp) = edge_energy_flux(:,:,sp)*dz/dt/Npc*0.5*m(sp)
                    edge_velocity_x_ave(:,:,sp) = edge_velocity_x(:,:,sp)*dz/dt/Npc
                    edge_velocity_y_ave(:,:,sp) = edge_velocity_y(:,:,sp)*dz/dt/Npc
                    edge_velocity_z_ave(:,:,sp) = edge_velocity_z(:,:,sp)*dz/dt/Npc


                    dens(sp,:,:)=rho(sp,1:Nz,:)/(N0*dz*dy)
                    vzav(sp,:,:)=vz(sp,:,:)
                    vyav(sp,:,:)=vy(sp,:,:)
                    vxav(sp,:,:)=vx(sp,:,:)
                    vz2av(sp,:,:)=vz2(sp,:,:)
                    vz3av(sp,:,:)=vz3(sp,:,:)
                    vy2av(sp,:,:)=vy2(sp,:,:)
                    vy3av(sp,:,:)=vy3(sp,:,:)
                    vx2av(sp,:,:)=vx2(sp,:,:)
                    vx3av(sp,:,:)=vx3(sp,:,:)
                    temperature_ave(sp,:,:) = temperature(sp,:,:)
                endif
                !write(*,*) 'EFC',nav,dz/dt/Npc,sum(edge_flux(:,:,sp)),sum(edge_flux_ave(:,:,sp))
            endif
        enddo

        !disable

        if ((count >= Na).and.(take_diag)) then
            if (nav.gt.0) then
                !    Epar=Epar*real(nav)/(real(nav)+1.0)+Escz/(real(nav)+1.0)
                !    Eperp=Eperp*real(nav)/(real(nav)+1.0)+Escy/(real(nav)+1.0)
                Potav=Potav*real(nav)/(real(nav)+1.0)+Pot/(real(nav)+1.0)
            else
                !  Epar=Escz
                !   Eperp=Escy
                Potav=Pot

            endif

            nav=nav+1
        endif
        !RD le 31/05/05 - diagnostic: temporal evolution of E-field at 1 Lambda_D from plasma and on the middle in -y!

        !RD le 31/05/05 - diagnostic: temporal e volution of dPHI in quasi-neutral area (between 15LD after wall
        !                             to 10LD before plasma) and in the middle in -y!
        call gettime(timestamp)

        t2 = timestamp - t1
        t_fin = t2
        ! if ((test_particle.gt.0).and.(no_part(1).gt.test_particle)) then
        !MK 19/08/06 test particle
        !write(*,*) 'keeping test particle info'

#ifdef SPICE_TRACER
        if (test_particle.gt.0) then

            !do k = 1,no_species
            !    write(*,*) 'Saving test particle data: sp, total ', k, maxval(test_particle_i(k,:))
            !enddo

            do i = 1,test_particle
                do k = 1,no_species
                    if (test_particle_i(k,i).eq.0) then
                        do j = maxval(test_particle_i(k,:))+1,Npts
                            ! tracing only particles moving to left (should be fixed for other B orientations)
                            if (stype(j).eq.k.and.uy(j).lt.0) then
                                test_particle_i(k,i) = j
                                exit
                            endif
                        enddo
                    endif
                enddo
            enddo

            do k = 1,no_species
                do i=1,test_particle
                    if (test_particle_i(k, i).gt.0) then
                        test_particle_data(count,k,i,1) = 0
                        test_particle_data(count,k,i,2) = y(test_particle_i(k, i))
                        test_particle_data(count,k,i,3) = z(test_particle_i(k, i))
                        test_particle_data(count,k,i,4) = ux(test_particle_i(k, i))
                        test_particle_data(count,k,i,5) = uy(test_particle_i(k, i))
                        test_particle_data(count,k,i,6) = uz(test_particle_i(k, i))
                        test_particle_data(count,k,i,7) = int(stype(test_particle_i(k, i)))
                    endif
                enddo
            enddo
        endif
#endif
        ! test_z(count) = z(1,test_particle)
        ! test_y(count) = y(1,test_particle)
        ! test_uz(count) = uz(1,test_particle)
        ! test_uy(count) = uy(1,test_particle)
        ! test_ux(count) = ux(1,test_particle)
        ! endif
        ! time diagnostics
        time_history(1,count) = real(t_grid)
        time_history(2,count) = real(t_wt)
        time_history(3,count) = real(t_poiss)
        time_history(4,count) = real(t_com1)
        time_history(5,count) = real(t_com2)
        time_history(6,count) = real(t_lp)
        time_history(7,count) = real(t_bc)
        time_history(8,count) = real(t_inj)
        time_history(9,count) = real(t_diag)
        time_history(10,count) = real(t_com3)
        time_history(11,count) = real(t_coll)

        t_fin2 = t_grid + t_wt + t_poiss + t_com1 + t_com2 + t_lp + t_bc + t_inj + t_diag +t_com3  + t_coll






        !MK diag sampling
        if (count.gt. (Na + diag_per*diag_count)) then
            diag_count = diag_count + 1
            take_diag = .true.
            ! zero out the fluxes
            edge_flux = 0.0
            edge_energy_flux = 0.0
        else
            take_diag = .false.
        endif
        !!! bias sweeping
        !!! look for all objects of type 3 and alter their potential
        if (count.gt.Na-1) then
            !  write(*,*) 'Bias sweep check',bint
            if (real(count - Na).gt.bstep*bint.and.count_iv_objects.gt.0) then
                bstep = bstep + 1
                write(*,*) 'Bias sweeping step ',bstep
                v_act  = vstart + (vstop - vstart)/real(nbsteps)*real(bstep)
                write(*,*) 'New bias voltage ',v_act

                do sp=1,rectangles
                    if (rectangle_params(sp)%param1.eq.3) then
                        do i=1,Nz
                            do j=1,Ny
                                if (objects_enum(i,j).eq.sp) then
                                    equipot_m(i,j) = v_act
                                endif
                            enddo
                        enddo
                    endif
                enddo
                !circles
                do sp=1,circles
                    if (circle_params(sp)%param1.eq.3) then
                        do i=1,Nz
                            do j=1,Ny
                                if (objects_enum(i,j).eq.(rectangles + sp)) then
                                    equipot_m(i,j) = v_act
                                endif
                            enddo
                        enddo

                    endif
                enddo
                !triangles
                do sp=1,triangles
                    if (triangle_params(sp)%param1.eq.3) then

                        do i=1,Nz
                            do j=1,Ny
                                if (objects_enum(i,j).eq.(rectangles + circles + sp)) then
                                    equipot_m(i,j) = v_act
                                endif
                            enddo
                        enddo

                    endif
                enddo
                call matrix2vector_r(equipot_m,Nz,Ny,n,equipot)
                !Matrix inversion +  parallelization (only 1 proc does the poisson)

                if (enable_poisson) then
                    write (*,*) 'Poisson solver enabled', psolver
                    if ((psolver.eq.2).or.(psolver.eq.3)) then
                        if (proc_no.eq.0) then
                            write (*,*) 'Umfpack inversion'
                            call psolver_direct_periodic(Ny,Nz,P0,PL,n,hzy,numeric,flag,equipot,Nz_disp)
                        endif
                    else if ((psolver.eq.4) .or. (psolver.eq.5)) then
                        if (proc_no.eq.0) then
                            write (*,*) 'MUMPS inversion'
                        endif

                        if (any(ps_ranks == proc_no)) then

                            if (psolver.eq.5) then
                                call pes_md_stop(mumpsPar)
                                call pes_md_init(mumpsPar, MPI_COMM_SOLVER, nZ, nY, Nz_disp, dZ, dY, flag_m)
                            else
                                call pes_mc_stop(mumpsPar)
                                call pes_mc_init(mumpsPar, MPI_COMM_SOLVER, nZ, nY, Nz_disp, dZ, dY, flag_m)
                            endif
                        endif

                        call pes_sync()

                        !if (any(ps_ranks == proc_no)) then
                        !    if (poisson_distributed) then
                        !        call PESSync()
                        !        call PESStopMumpsDist(mumpsPar, debug, proc_no, par_size)
                        !        call PESInitSPICE2Dist(mumpsPar, debug, proc_no, par_size, nZ, nY, dZ, dY, flag_m)
                        !    else
                        !        call PESSync()
                        !        call PESStopMumps(mumpsPar, debug)
                        !        call PESInitSPICE2(mumpsPar, debug, nZ, nY, Nz_disp, dZ, dY, flag_m)
                        !    endif
                        !endif

                    endif
                endif

            endif
        endif


        progress = 100.*count/dfloat(Np)

        ! ---------------- Time histories MPI Transfer ----------------------------!

        if (count==dump_period*int(real(count)/real(dump_period))+1) then
            ! count for data dumping
            if (proc_max.gt.0) then
                if (debug) then
                    write(*,*) 'Parallelization communication'
                    write(*,*) '------------------------------'
                endif
                !we use blocking I/O scheme
                if (proc_no.eq.0) then
                    ! receive number of particles and use it to store particle characteristics
                    !call subroutine to handle the data
                    if (debug) then
                        write(*,*) 'Running receive_diag',proc_no
                    endif
                    impact_diag_tot = impact_diag
                    emmit_diag_tot = emmit_diag

                    call receive_diag(rho,no_part_proc(:,1:h_pos),mpi_rank,count,number(:,1:h_pos),debug,no_species,&
                        proc_max,h_pos,iteration_time_hist(:,1:h_pos),Nz,Ny,objects_current(:,:,1:count),objects_power_flux(:,:,1:count),objects_current_tot(:,:,1:count),&
                        objects_power_flux_tot(:,:,1:count),rectangles+circles+triangles,Nz_max,have_impact_diag,emmit_diag,impact_diag_tot,fv_bin,dz,dt,Npc)

                endif
                if (proc_no.gt.0) then
                    !call sender subroutine
                    if (debug) then
                        write(*,*) 'Running send_diag',proc_no
                    endif

                    call send_diag(rho,no_species,mpi_rank,no_tot,count,number(:,1:h_pos),debug,proc_no,no_part_proc(proc_no &
                        +1,1:h_pos),h_pos,iteration_time_hist(proc_no+1,1:h_pos),Nz,Ny,objects_current(:,:,1:count),objects_power_flux(:,:,1:count),&
                        rectangles+circles+triangles,Nz_max,have_impact_diag,impact_diag,emmit_diag,fv_bin)
                endif
            else
                ! serial run
                impact_diag_tot = impact_diag
                emmit_diag_tot = emmit_diag

            endif !par comm
            ! fix edge_flux

            ! total objects current
            !    objects_current_tot = 0.0
            !    objects_power_flux_tot = 0.0

            ! this is now moved to the receive_diag
            !     do i=1,proc_max+1
            !         objects_current_tot = objects_current_tot + objects_current(i,:,:,:)
            ! here we renormalize it to make it compatible with edge energy flux
            !objects_power_flux_tot = objects_power_flux_tot + objects_power_flux(i,:,:,:)/dt/Npc*dz

            !           enddo



            ! ----------------------- MAT FILE SAVING --------------------------!

            !first save the proc-file
            if (use_hdf5) then
                call h5_save_temp_proc_file(tfile,proc_no,no_tot,z(1:no_tot),y(1:no_tot),ux(1:no_tot),uy(1:no_tot),uz(1:no_tot),stype(1:no_tot),count,use_compression)
            else
                call save_temp_proc_file(tfile,proc_no,no_tot,z(1:no_tot),y(1:no_tot),ux(1:no_tot),uy(1:no_tot),uz(1:no_tot),vtot_init(1:no_tot),use_vtot,stype(1:no_tot),count,use_compression)
            endif

#ifdef SPICE_TRACER
            if (use_hdf5) then
                call h5_save_test_particle(tfile,proc_no,no_species,test_particle,count,test_particle_i,test_particle_data(1:count,:,:,:),use_compression)
            else
                call save_test_particle(tfile,proc_no,no_species,test_particle,count,test_particle_i,test_particle_data(1:count,:,:,:),use_compression)
            endif
#endif

            !only proc 0 writes any stuff and writes output
            if (proc_no.eq.0.and.count==dump_period*int(real(count)/real(dump_period))+1) then
                write(*,*) '---------------------------------------------------------'
                write(6,*) idint(progress), '% done. ', (Np-count)*t2/(3600.d0*1.0e6),' h remaining'
                write(6,*) 'N=',no_part(1),' - count=',count
                if (verbose) then
                    write(*,*) 'Profiling results'
                    write(*,*) '================================'
                    write(*,*) 'Weighting:',int(t_wt*100.0/t_fin),'%, full time',t_wt
                    write(*,*) 'G. Diag:',int(t_gd*100.0/t_fin),'%, full time',t_gd
                    if(proc_max.gt.0) then
                        write(*,*) 'MPI Comm1:',int(t_com1*100.0/t_fin),'%, full time',t_com1
                    endif
                    write(*,*) 'Poisson:',int(t_poiss*100.0/t_fin),'%, full time',t_poiss
                    write(*,*) 'LeapFrog:',int(t_lp*100.0/t_fin),'%, full time',t_lp
                    write(*,*) 'BC:',int(t_bc*100.0/t_fin),'%, full time',t_bc
                    write(*,*) 'Collisions:',int(t_coll*100.0/t_fin),'%, full time',t_coll
                    write(*,*) 'Particle Transfer:',int(100.0*t_grid/t_fin),'%, full time',t_grid
                    write(*,*) 'MPI Comm3:',int(t_com3*100.0/t_fin),'%, full time',t_com3

                    write(*,*) 'Injection:',int(t_inj*100.0/t_fin),'%, full time',t_inj
                    if (no_diag_reg.gt.0) then
                        write(*,*) 'Diag. regs.',int(t_diag*100.0/t_fin),'%, full time',t_diag
                    endif

                    write(*,*) 't_fin', t_fin, t_fin2
                    write(*,*) '================================'
                    call gettime(timestamp)




                    if (t_max.gt.0) then
                        call gettime(t_act)
                        write(*,*) 'Time limit (h)', (t_max - real(t_act - t_intro))/3600e6,t_max/3600e6, t_intro, t_act
                    endif


                    write(*,*) 'Timestamp:', timestamp
                endif
                if (count > Na) then
                    write(*,*)'number of taken diagnostics:',diag_count
                    write(*,*) 'Check: Potav',sum(Potav)
                    write(*,*) 'Check: vxav',sum(vxav(1,:,:)),sum(vx(1,:,:))

                endif
                ! SPICE MONITOR
                write(perc_str,'(F7.2)') progress
                !ap 150216 - time is 1000x upscaled now in the whole code
                write(due_str,'(F7.2)') (Np-count)*t2/3600000.d0
                write(nproc_str,'(I4.2)') proc_max+1
                ! read ENV
                call getenv("MACHINE",machine)
                call getenv("SPICE_USER",spice_user)

                ! send the stats
                call system('wget -q -O /dev/null "http://spice.rdx.cz/recv.php?perc=' //trim(perc_str) // '&due=' // trim(due_str) // '&input=' // trim(ifile) // '&machine=' // trim(machine) //'&user=' // trim(spice_user) // '&nproc='// trim(nproc_str) // '&code=spice2-2.11'//'"')
                !write(*,*) 'SPICE monitor data sent'



                ! dynamic load balance - calculate optimized injection ratios
                ! first check the particle groups
                if (h_pos.gt.10.and.proc_max.gt.0) then
                    if (proc_no.eq.0) then
                        do sp=1,no_species
                            if ((spec_params(sp)%dynamic_balance).and.(spec_params(sp)%particle_group.eq.sp)) then
                                ! this is one group
                                ! let's count the total injection ratio
                                write(*,*) 'Particle group ',sp,spec_params(sp)%name
                                total_inj = 0.0
                                do i=1,no_species
                                    if (spec_params(i)%particle_group.eq.sp) then
                                        total_inj = total_inj +  spec_params(sp)%injection_rate
                                    endif
                                enddo
                                write(*,*) 'Total injection',total_inj

                                !compute the CPU power
                                do i=1,proc_max +1
                                    !				proc_power(i)  = spec_params(sp + i -1)%injection_rate/sum(iteration_time_hist(i,(h_pos -10):h_pos))
                                    proc_power(i)  = sum(no_part_proc(i,(h_pos -10):h_pos))/sum(iteration_time_hist(i,(h_pos -10):h_pos))

                                enddo
                                ! one more loop for nice stats
                                do i=1,proc_max +1
                                    write(*,*) 'CPU power:', i, proc_power(i)/sum(proc_power)
                                enddo
                                ! calculate new injection rates
                                do i=1,proc_max +1
                                    new_injection_rate(i)  = proc_power(i)
                                enddo
                                irs = sum(new_injection_rate)
                                do i=1,proc_max +1
                                    new_injection_rate(i)  = new_injection_rate(i)/irs*total_inj
                                    inj_ratio(sp + i -1) = new_injection_rate(i)
                                    write(*,*) 'New ratio',i,new_injection_rate(i)
                                enddo
                            endif
                        enddo
                        ! send the
                        call send_rates(proc_max,no_species,inj_ratio,debug)
                    else
                        !receive new rates
                        call receive_rates(proc_max,no_species,inj_ratio,proc_no,debug)

                    endif
                    ! recalculate the Ninj stuff

                    do sp=1,no_species
                        !	write(*,*) 'Injection ratio for',spec_name(sp), sp
                        !valeur du nb de particule a injecter en fontion du flux pour avoir n=1
                        !Ninj_fx(sp)=inj_ratio(sp)*abs(GAMMAz0(sp)*N0*dt*Ly*bz)!RD - for PARTICLE INJECTION with an angle   NEW!!
                        !     Ninj_fx(sp)=inj_ratio(sp)*abs(GAMMAz0(sp)*N0*dt*Ly*bz)!RD - for PARTICLE INJECTION with an angle   NEW!!
                        !	write(6,*) 'Ninj_fx',Ninj_fx(sp),'   GAMMAz0',GAMMAz0(sp)
                    enddo

                endif

                !!!!!!! FLOATING POTENTIALS - THERE THEY GO
                ! make revision of currents flowing to them
                ! and adjust the potential if needed
                ! we have no unified  interface for objects so we go through all rectangles,m circles and triangkes
                i = 0
                i_rel_max = 0.0
                ! better for the calculation if we only adapt to some accuracy, inversion of the matrix takes a lot of time
                i_rel_treshold = 0.01
                if (count.gt.1.and.(.not.impose_pot).and.(.not.frozen_potvac)) then
                    do sp=1,rectangles
                        if (rectangle_params(sp)%param1.eq.2) then
                            if (max(abs(objects_current_tot(1,sp,count -1)),abs(objects_current_tot(2,sp,count -1))).gt.0.0) then
                                i_rel = (sum(objects_current_tot(1,sp,(count -11):(count -1))) + sum(objects_current_tot(2,sp,&
                                    (count -11):(count -1))))/max(sum(abs(objects_current_tot(1,sp,(count -11):(count -1)))),&
                                    sum(abs(objects_current_tot(2,sp,(count -11):(count -1)))))
                            else
                                i_rel = 0.0
                            endif
                            if (i_rel_max.lt.abs(i_rel)) then
                                i_rel_max = abs(i_rel)
                            endif
                            if (abs(i_rel).gt.i_rel_treshold.and.count.gt.Nc) then
                                rectangle_params(sp)%Pot = rectangle_params(sp)%Pot + float_constant(sp)*i_rel
                                ! add to history
                                call add_to_i_rel_history(i_rel,i_rel_history(sp,:))
                                ! addapt the constant
                                call adapt_float_constant(float_constant(sp),i_rel_history(sp,:))
                                ! run through the area and fix the values
                                if (verbose) then
                                    write(*,*) 'Changing Pot to ',rectangle_params(sp)%Pot
                                endif
                                do i=1,Nz
                                    do j=1,Ny
                                        if (objects_enum(i,j).eq.sp) then
                                            equipot_m(i,j) = rectangle_params(sp)%Pot
                                        endif
                                    enddo
                                enddo
                            endif
                        endif
                    enddo
                    !circles
                    do sp=1,circles
                        if (circle_params(sp)%param1.eq.2) then
                            if (max(abs(objects_current_tot(1,rectangles +sp,count -1)),abs(objects_current_tot(2,&
                                    rectangles +sp,count -1))).gt.0.0) then

                                i_rel = (objects_current_tot(1,rectangles + sp,count -1) + objects_current_tot(2,rectangles + sp,count -1))&
                                    /max(abs(objects_current_tot(1,rectangles + sp,count -1)),abs(objects_current_tot(2,&
                                    rectangles + sp,count -1)))
                                ! run through the area and fix the values
                            else
                                i_rel = 0.0
                            endif

                            if (i_rel_max.lt.abs(i_rel)) then
                                i_rel_max = abs(i_rel)
                            endif
                            if (abs(i_rel).gt.i_rel_treshold.and.count.gt.Nc) then
                                circle_params(sp)%Pot = circle_params(sp)%Pot + float_constant(rectangles + sp)*i_rel

                                call add_to_i_rel_history(i_rel,i_rel_history(rectangles +sp,:))
                                ! addapt the constant
                                call adapt_float_constant(float_constant(rectangles +sp),i_rel_history(rectangles +sp,:))

                                do i=1,Nz
                                    do j=1,Ny
                                        if (objects_enum(i,j).eq.(rectangles + sp)) then
                                            equipot_m(i,j) = circle_params(sp)%Pot
                                        endif
                                    enddo
                                enddo
                            endif
                        endif
                    enddo
                    !triangles
                    do sp=1,triangles
                        if (triangle_params(sp)%param1.eq.2) then
                            if (max(abs(objects_current_tot(1,rectangles + circles +sp,count -1)),abs(objects_current_tot(2,&
                                    rectangles +circles +sp,count -1))).gt.0.0) then

                                i_rel = (objects_current_tot(1,rectangles + circles + sp,count -1) + objects_current_tot(2,&
                                    rectangles +circles + sp,count -1))/max(abs(objects_current_tot(1,rectangles + circles +sp,count&
                                    -1)),abs(objects_current_tot(2,rectangles + circles +sp,count -1)))
                                ! run through the area and fix the values
                            else
                                i_rel = 0.0
                            endif

                            if (i_rel_max.lt.abs(i_rel)) then
                                i_rel_max = abs(i_rel)
                            endif
                            if (abs(i_rel).gt.i_rel_treshold.and.count.gt.Nc) then
                                triangle_params(sp)%Pot = triangle_params(sp)%Pot + float_constant(rectangles + circles +sp)*i_rel

                                call add_to_i_rel_history(i_rel,i_rel_history(rectangles +circles +sp,:))
                                ! addapt the constant
                                call adapt_float_constant(float_constant(rectangles +sp),i_rel_history(rectangles +circles +sp,:))

                                do i=1,Nz
                                    do j=1,Ny
                                        if (objects_enum(i,j).eq.(rectangles + circles +sp)) then
                                            equipot_m(i,j) = triangle_params(sp)%Pot
                                        endif
                                    enddo
                                enddo
                            endif
                        endif
                    enddo


                    if (i_rel_max.gt.i_rel_treshold) then
                        call matrix2vector_r(equipot_m,Nz,Ny,n,equipot)
                        !Matrix inversion +  parallelization (only 1 proc does the poisson)

                        if (enable_poisson) then
                            if ((psolver.eq.2).or.(psolver.eq.3)) then
                                if (proc_no.eq.0) then
                                    write (*,*) 'Umfpack inversion'

                                    ! subroutine psolver_direct1(Ny,Nz,P0,PL,n,hzy,Ny1,Ny2,Nz1,numeric,flag,equipot)
                                    ! first get rid of the old one
                                    !LU DECOMPOSITION (3/3):
                                    !=======================
                                    ! 		call umf4fnum(numeric)    !free the numeric object
                                    ! create new poisson solving object
                                    ! 	call psolver_direct_periodic(Ny,Nz,P0,PL,n,hzy,numeric,flag,equipot,Nz_disp)
                                endif
                            else if ((psolver.eq.4) .or. (psolver.eq.5)) then
                                if (proc_no.eq.0) then
                                    write (*,*) 'MUMPS inversion'
                                endif
                            endif
                        endif

                    endif

                endif
                !MK - there are 2 types of temporary matlab files
                ! first is named after the -t option + .mat
                ! it contains time histories, potentials etc
                ! second is created by each processor, it is named -t option + proc_no + .mat
                ! it contains positions and velocities of particles
                !

                if (use_hdf5) then
                    call h5_LANG_MONITOR(no_species,number(1:no_species,1:h_pos),Nz,Ny,Nv,z_g,y_g,rho_tot,Escz,Escy,Pot,count,t(1:h_pos),&
                        Esct(1:h_pos),dPHIqn(1:h_pos),bx,by,bz,tfile,debug,Np,pot_chi(1:h_pos),Potvac,no_part_proc(:,1:h_pos),proc_max,h_pos,&
                        no_diag_reg,diag_regions,diag_histories,diag_hist,fv_arrays,fv_ar,hist_limits,Np,Na,fv_bin,fv_sum,fv_limits,u_perp_a,&
                        fv_perp_arrays,N0,flag_m,equipot_m,n,flag,iteration_time_hist(:,1:h_pos),injection_rate_hist(:,1:h_pos),objects,edges,&
                        edge_charge,rho,diagm,circles+triangles+rectangles,objects_enum,objects_current_tot,objects_power_flux_tot,ksi,tau,SOL_W,&
                        SOL_Ns,use_compression,i_rel_history,float_constant,Nc,mks_info,alpha_yz,alpha_xz,mu,dz,dt,m,q,Temp,time_history(:,1:count),&
                        Nz_max,slice_proc,domain_decomp,surface_matrix,delta_h,Npc,total_energy)
                else
                    call LANG_MONITOR(no_species,number(1:no_species,1:h_pos),Nz,Ny,Nv,z_g,y_g,rho_tot,Escz,Escy,Pot,count,t(1:h_pos),&
                        Esct(1:h_pos),dPHIqn(1:h_pos),bx,by,bz,tfile,debug,Np,pot_chi(1:h_pos),Potvac,no_part_proc(:,1:h_pos),proc_max,h_pos,&
                        no_diag_reg,diag_regions,diag_histories,diag_hist,fv_arrays,fv_ar,hist_limits,Np,Na,fv_bin,fv_sum,fv_limits,u_perp_a,&
                        fv_perp_arrays,N0,flag_m,equipot_m,n,flag,iteration_time_hist(:,1:h_pos),injection_rate_hist(:,1:h_pos),objects,edges,&
                        edge_charge,rho,diagm,circles+triangles+rectangles,objects_enum,objects_current_tot,objects_power_flux_tot,ksi,tau,SOL_W,&
                        SOL_Ns,use_compression,i_rel_history,float_constant,Nc,mks_info,alpha_yz,alpha_xz,mu,dz,dt,m,q,Temp,time_history(:,1:count),&
                        Nz_max,slice_proc,domain_decomp,surface_matrix,delta_h,Npc,total_energy,have_impact_diag,impact_diag,emmit_diag,orientation)
                endif
                !write(*,*) 'f(v) limit check',fv_limits(1,1),fv_limits(1,2)
                ! zero out rho
                !rho = 0.0

            endif !par
            !now for count >Na we also store the averaged quantities - needed for full restore
            if (count.gt.Na) then
                !parallelization - proc 1 sending ion diag to proc 0
                dens_tot =  dens
                vzav_tot =  vzav
                vyav_tot =  vyav
                vxav_tot =  vxav
                vz2av_tot = vz2av
                vy2av_tot =  vy2av
                vx2av_tot =  vx2av
                vz3av_tot =  vz3av
                vy3av_tot =  vy3av
                vx3av_tot =  vx3av
                edge_flux_ave_tot =  edge_flux_ave
                edge_energy_flux_ave_tot =  edge_energy_flux_ave
                edge_velocity_x_ave_tot =  edge_velocity_x_ave
                edge_velocity_y_ave_tot =  edge_velocity_y_ave
                edge_velocity_z_ave_tot =  edge_velocity_z_ave
                temperature_ave_tot = temperature_ave
                ! write(*,*) proc_no,'Edge flux before comm',sum(edge_flux_ave(:,:,1))

                if (proc_max.gt.0) then


                    ! turn on debug
                    ! 	debug = .true.
                    if (debug) then
                        write(*,*) 'Averaged diagnostics par. comm.'
                        write(*,*) '------------------------------'
                    endif
                    !we use blocking I/O scheme
                    if (proc_no.eq.0) then
                        do sp=1,no_species
                            ! 			if (mpi_rank(sp).ne.0) then
                            ! write(*,*) 'RECV EF',sp,sum(edge_flux(:,:,sp))

                            call receive_final_diag(edge_flux_ave_tot(:,:,sp),count,sp,Nz,Ny,mpi_rank(sp),dens_tot(sp,:,:),&
                                vxav_tot(sp,:,:),vyav_tot(sp,:,:),vzav_tot(sp,:,:),vx2av_tot(sp,:,:),vy2av_tot(sp,:,:),vz2av_tot(sp,:,:),vx3av_tot(sp,:,:),vy3av_tot(sp,:,:),&
                                vz3av_tot(sp,:,:),debug,edge_energy_flux_ave_tot(:,:,sp),edge_velocity_x_ave_tot(:,:,sp),edge_velocity_y_ave_tot(:,:,sp),&
                                edge_velocity_z_ave_tot(:,:,sp),proc_max)

                            ! 			endif
                        enddo
                    else if (proc_no.gt.0) then
                        do sp=1,no_species
                            if (mpi_rank(sp).eq.proc_no) then
                                ! write(*,*) 'SEND EF',sp,sum(edge_flux(:,:,sp))

                                call send_final_diag(edge_flux_ave_tot(:,:,sp),count,sp,Nz,Ny,dens_tot(sp,:,:),vxav_tot(sp,:,:),&
                                    vyav_tot(sp,:,:),vzav_tot(sp,:,:),vx2av_tot(sp,:,:),vy2av_tot(sp,:,:),vz2av_tot(sp,:,:),vx3av_tot(sp,:,:),vy3av_tot(sp,:,:),vz3av_tot(sp,:,:),&
                                    debug,edge_energy_flux_ave_tot(:,:,sp),edge_velocity_x_ave_tot(:,:,sp),edge_velocity_y_ave_tot(:,:,sp),edge_velocity_z_ave_tot(:,:,sp),&
                                    proc_no)
                            endif
                        enddo
                    endif
                endif

                !debug
                ! call dump_grid(dens(1,:,:),Nz,Ny,1,1)
                ! call dump_grid(dens(2,:,:),Nz,Ny,1,2)
                ! call dump_grid(dens(3,:,:),Nz,Ny,1,3)
                ! call dump_grid(dens(4,:,:),Nz,Ny,1,4)

#ifdef SPICE_TRACER
                if (test_particle.gt.0) then
                    if (use_hdf5) then
                        call h5_save_test_particle(tfile,proc_no,no_species,test_particle,count,test_particle_i,test_particle_data(1:count,:,:,:),use_compression)
                    else
                        call save_test_particle(tfile,proc_no,no_species,test_particle,count,test_particle_i,test_particle_data(1:count,:,:,:),use_compression)
                    endif
                endif
#endif


                if (proc_no.eq.0) then
                    if (use_hdf5) then
                        call h5_SAVE_RESULTS2(dt,N0,Nv,Nz,Ny,z_g,y_g,dens_tot,Epar,Eperp,Potav,q_grid,T_g,vzav_tot,vyav_tot,vxav_tot,vz2av_tot,vy2av_tot,vx2av_tot,&
                            vz3av_tot,vy3av_tot,vx3av_tot,count,t(1:count),dz,Iinj,ofile,debug,no_species,nav,edge_flux_ave_tot,edge_energy_flux_ave_tot,&
                            edge_velocity_x_ave_tot,edge_velocity_y_ave_tot,edge_velocity_z_ave_tot,use_compression,domain_decomp,proc_max,temperature_ave_tot)
                    else
                        call SAVE_RESULTS2(dt,N0,Nv,Nz,Ny,z_g,y_g,dens_tot,Epar,Eperp,Potav,q_grid,T_g,vzav_tot,vyav_tot,vxav_tot,vz2av_tot,vy2av_tot,vx2av_tot,&
                            vz3av_tot,vy3av_tot,vx3av_tot,count,t(1:count),dz,Iinj,ofile,debug,no_species,nav,edge_flux_ave_tot,edge_energy_flux_ave_tot,&
                            edge_velocity_x_ave_tot,edge_velocity_y_ave_tot,edge_velocity_z_ave_tot,use_compression,domain_decomp,proc_max,temperature_ave_tot)
                    endif

                    if (verbose) then
                        write(*,*) 'NAV:',nav
                    endif
                endif!par




            endif !count > Na
        endif ! count

        !-------------------------------------storage------------------------------------1

        ! 	z_old(1:no_tot) = z(1:no_tot)
        ! 	y_old(1:no_tot) = y(1:no_tot)



        if (step_max .gt. 0) then
            if (count >= step_max) then
                write (*,*) 'SPICE run was terminated, maximum step count was reached'
                exit
            endif
        endif
        if (debug) then
            write(*,*) 'Step', count, 'done'
        endif
        !exit
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    enddo ! do count=1,Np
    !!!!!!!!!!!!!!!!End of Main Loop!!!!!!!!!!!!!!!



    if (enable_poisson) then
        if ((psolver.eq.2).or.(psolver.eq.3)) then
            if (proc_no.eq.0) then
                !LU DECOMPOSITION (3/3):
                !=======================
                write (*,*) 'Umfpack disposal'
                call umf4fnum(numeric)    !free the numeric object
            endif
        else if ((psolver.eq.4) .or. (psolver.eq.5)) then
            call pes_sync()
            if (any(ps_ranks == proc_no)) then

                if (proc_no.eq.0) then
                    ! output on host
                    write (*,*) 'MUMPS disposal'
                endif

                if (psolver.eq.5) then
                    call pes_md_stop(mumpsPar)
                    !call PESStopMumpsDist(mumpsPar, debug, proc_no, par_size)
                else
                    !call PESSync()
                    !call PESStopMumps(mumpsPar, debug)
                    call pes_mc_stop(mumpsPar)
                endif
            endif
        else if (psolver.eq.6) then
            !#ifndef NOHYPES
            deallocate(flag_m_loc)
            deallocate(pot_m_loc)
            deallocate(newpot_m_loc)
            deallocate(dens_m_loc)
            call pes2d_stop(pes_info)
            !#endif
        endif
    endif

    ! final diag calculation
    do sp=1,no_species
        if (mpi_rank(sp).eq.proc_no)then
            ! 		dens(sp,:,:)=dens(sp,:,:)/(N0*dz*dy)
            T_g(sp,:,:)=vz2av(sp,:,:)-vzav(sp,:,:)*vzav(sp,:,:) !Ecart-type de la fonction vziav
            T_g(sp,:,:)=T_g(sp,:,:)*m(sp)/ksi/ksi
            q_grid(sp,:,:)=(vzav(sp,:,:)-3*vzav(sp,:,:)*vz2av(sp,:,:)+2*vzav(sp,:,:)*vzav(sp,:,:)*vzav(sp,:,:))/2
            do k=1,Ny
                j_w(sp,k)=-1.d0*vzav(sp,1,k)*dens(sp,1,k) !RD le 31/05/05   -1.d0* car vitesse<0 ???
            enddo
        endif

    enddo


    !parallelization - proc 1 sending ion diag to proc 0

    ! turn on debug
    dens_tot =  dens
    vzav_tot =  vzav
    vyav_tot =  vyav
    vxav_tot =  vxav
    vz2av_tot =  vz2av
    vy2av_tot =  vy2av
    vx2av_tot =  vx2av
    vz3av_tot =  vz3av
    vy3av_tot =  vy3av
    vx3av_tot =  vx3av
    edge_flux_ave_tot =  edge_flux_ave
    edge_energy_flux_ave_tot =  edge_energy_flux_ave
    edge_velocity_x_ave_tot =  edge_velocity_x_ave
    edge_velocity_y_ave_tot =  edge_velocity_y_ave
    edge_velocity_z_ave_tot =  edge_velocity_z_ave
    temperature_ave_tot = temperature_ave
    if (proc_max.gt.0) then
        debug = .true.
        if (debug) then
            write(*,*) 'Final parallelization communication'
            write(*,*) '------------------------------'
        endif
        !we use blocking I/O scheme
        if (proc_no.eq.0) then
            do sp=1,no_species
                ! 			if (mpi_rank(sp).ne.0) then
                call receive_final_diag(edge_flux_ave_tot(:,:,sp),count,sp,Nz,Ny,mpi_rank(sp),dens_tot(sp,:,:),&
                    vxav_tot(sp,:,:),vyav_tot(sp,:,:),vzav_tot(sp,:,:),vx2av_tot(sp,:,:),vy2av_tot(sp,:,:),vz2av_tot(sp,:,:),vx3av_tot(sp,:,:),vy3av_tot(sp,:,:),&
                    vz3av_tot(sp,:,:),debug,edge_energy_flux_ave_tot(:,:,sp),edge_velocity_x_ave_tot(:,:,sp),edge_velocity_y_ave_tot(:,:,sp),edge_velocity_z_ave_tot(:,:,sp),proc_max,temperature_ave_tot(sp,:,:))

                ! 			endif
            enddo
        else if (proc_no.gt.0) then
            do sp=1,no_species
                ! 			if (mpi_rank(sp).eq.proc_no) then
                call send_final_diag(edge_flux_ave_tot(:,:,sp),count,sp,Nz,Ny,dens_tot(sp,:,:),vxav_tot(sp,:,:),&
                    vyav_tot(sp,:,:),vzav_tot(sp,:,:),vx2av_tot(sp,:,:),vy2av_tot(sp,:,:),vz2av_tot(sp,:,:),vx3av_tot(sp,:,:),vy3av_tot(sp,:,:),vz3av_tot(sp,:,:),&
                    debug,edge_energy_flux_ave_tot(:,:,sp),edge_velocity_x_ave_tot(:,:,sp),edge_velocity_y_ave_tot(:,:,sp),edge_velocity_z_ave_tot(:,:,sp),proc_no,temperature_ave_tot(sp,:,:))
                !  			endif
            enddo
        endif
    endif

    !debug
    ! call dump_grid(dens(1,:,:),Nz,Ny,1,1)
    ! call dump_grid(dens(2,:,:),Nz,Ny,1,2)
    ! call dump_grid(dens(3,:,:),Nz,Ny,1,3)
    ! call dump_grid(dens(4,:,:),Nz,Ny,1,4)

#ifdef SPICE_TRACER
    if (test_particle.gt.0) then
        if (use_hdf5) then
            call h5_save_test_particle(tfile,proc_no,no_species,test_particle,count,test_particle_i,test_particle_data(1:count,:,:,:),use_compression)
        else
            call save_test_particle(tfile,proc_no,no_species,test_particle,count,test_particle_i,test_particle_data(1:count,:,:,:),use_compression)
        endif
    endif
#endif

    if (debug) then
        write (*,*) 'Storing final output at proc 0, this proc is ', proc_no
    endif

    if (proc_no.eq.0) then
        ! 	if (time_diag) then
        !      		call SAVE_RESULTS2(dt,N0,Nv,Nz,Ny,z_g,y_g,dens,Epar,Eperp,Potav,q_grid,T_g,vzav,vyav,vxav,vz2av,vy2av,&
            ! vx2av,vz3av,vy3av,vx3av,count,t(1:count),dz,Iinj,ofile,debug,no_species,nav,edge_flux_ave,edge_energy_flux_ave,edge_velocity_x_ave,edge_velocity_y_ave,edge_velocity_z_ave,use_compression,domain_decomp,proc_max)


        if (use_hdf5) then
            call h5_SAVE_RESULTS2(dt,N0,Nv,Nz,Ny,z_g,y_g,dens_tot,Epar,Eperp,Potav,q_grid,T_g,vzav_tot,vyav_tot,vxav_tot,vz2av_tot,vy2av_tot,vx2av_tot,&
                vz3av_tot,vy3av_tot,vx3av_tot,count,t(1:count),dz,Iinj,ofile,debug,no_species,nav,edge_flux_ave_tot,edge_energy_flux_ave_tot,&
                edge_velocity_x_ave_tot,edge_velocity_y_ave_tot,edge_velocity_z_ave_tot,use_compression,domain_decomp,proc_max,temperature_ave_tot)
        else
            call SAVE_RESULTS2(dt,N0,Nv,Nz,Ny,z_g,y_g,dens_tot,Epar,Eperp,Potav,q_grid,T_g,vzav_tot,vyav_tot,vxav_tot,vz2av_tot,vy2av_tot,vx2av_tot,&
                vz3av_tot,vy3av_tot,vx3av_tot,count,t(1:count),dz,Iinj,ofile,debug,no_species,nav,edge_flux_ave_tot,edge_energy_flux_ave_tot,&
                edge_velocity_x_ave_tot,edge_velocity_y_ave_tot,edge_velocity_z_ave_tot,use_compression,domain_decomp,proc_max,temperature_ave_tot)
        endif

        if (use_hdf5) then
            call h5_LANG_MONITOR(no_species,number(1:no_species,1:history_ntimes),Nz,Ny,Nv,z_g,y_g,rho_tot,Escz,Escy,Pot,count,t(1:history_ntimes),&
                Esct(1:history_ntimes),dPHIqn(1:history_ntimes),bx,by,bz,tfile,debug,Np,pot_chi(1:history_ntimes),Potvac,no_part_proc(:,1:history_ntimes),proc_max,history_ntimes,&
                no_diag_reg,diag_regions,diag_histories,diag_hist,fv_arrays,fv_ar,hist_limits,Np,Na,fv_bin,fv_sum,fv_limits,u_perp_a,&
                fv_perp_arrays,N0,flag_m,equipot_m,n,flag,iteration_time_hist(:,1:history_ntimes),injection_rate_hist(:,1:history_ntimes),objects,edges,&
                edge_charge,rho,diagm,circles+triangles+rectangles,objects_enum,objects_current_tot,objects_power_flux_tot,ksi,tau,SOL_W,&
                SOL_Ns,use_compression,i_rel_history,float_constant,Nc,mks_info,alpha_yz,alpha_xz,mu,dz,dt,m,q,Temp,time_history(:,1:count),&
                Nz_max,slice_proc,domain_decomp,surface_matrix,delta_h,Npc,total_energy)
        else
            call LANG_MONITOR(no_species,number(1:no_species,1:history_ntimes),Nz,Ny,Nv,z_g,y_g,rho_tot,Escz,Escy,Pot,count,t(1:history_ntimes),&
                Esct(1:history_ntimes),dPHIqn(1:history_ntimes),bx,by,bz,tfile,debug,Np,pot_chi(1:history_ntimes),Potvac,no_part_proc(:,1:history_ntimes),proc_max,history_ntimes,&
                no_diag_reg,diag_regions,diag_histories,diag_hist,fv_arrays,fv_ar,hist_limits,Np,Na,fv_bin,fv_sum,fv_limits,u_perp_a,&
                fv_perp_arrays,N0,flag_m,equipot_m,n,flag,iteration_time_hist(:,1:history_ntimes),injection_rate_hist(:,1:history_ntimes),objects,edges,&
                edge_charge,rho,diagm,circles+triangles+rectangles,objects_enum,objects_current_tot,objects_power_flux_tot,ksi,tau,SOL_W,&
                SOL_Ns,use_compression,i_rel_history,float_constant,Nc,mks_info,alpha_yz,alpha_xz,mu,dz,dt,m,q,Temp,time_history(:,1:count),&
                Nz_max,slice_proc,domain_decomp,surface_matrix,delta_h,Npc,total_energy,have_impact_diag,impact_diag,emmit_diag,orientation)

        endif



        !    		write(6,*) '----------------------------------------------------------'
        !    		write(6,*) 'La densite de courant arrivant a la plaque: '
        !  		!  write(6,*) 'j_w= ',j_w(1,:),' ET j_w=',j_w(2,:)
        ! 		!   write(6,*) 'Total: jwtot=',j_w(1,:)-j_w(2,:)
        !    		write(6,*) 'P0 = ',P0
        ! ! 	endif     !if (time_diag) then
        write(6,*) 'indice de moyenne pour dPHIqn',idint(15/dz+1.d-10)+1,' et',idint((Lz-10)*Nz/Lz)+1
        write (*,*) 'Everything done.'
    endif!par
    ! finalize MPI communication

#ifdef SPICE_TRACER
    if (test_particle.gt.0) then
        deallocate(test_particle_data)
        deallocate(test_particle_i)
    endif
#endif



    write (*,*) 'Finalizing MPI communication.'

    call MPI_FINALIZE(IERR)

    write (*,*) 'Exiting.'


    return


END


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

subroutine display_help()
    write(*,*) '2D edge plasma simulation code'
    write(*,*) '=============================='
    write(*,*) 'usage: ./pic [-vhd] [-i INPUT_FILE] [-o OUTPUT_FILE] [-t TDIAG_FILE] [-p p_num]'
    write(*,*) '	-v verbose mode'
    write(*,*) '	-d debug mode'
    write(*,*) '	-h display this help'
    write(*,*) '	-i input file (default sheatRD_tilegap.inp)'
    write(*,*) '	-o output file (default shRDarbitr_tile.mat)'
    write(*,*) '	-t time diag file (default shRDt_tile.mat)'
    write(*,*) '	-p track ion number p_num'
    write(*,*) '	-r restore run (requires valid  -t option )'
    write(*,*) '	-c continue run (requires valid  -t and -o option )'
    write(*,*) '	-f run will be perfomed in frozen Vacuum potential'
    write(*,*) '	-b run will be perfomed without E field (ballistic)'
    write(*,*) '	-l N stop run after N hours'
    write(*,*) '	-s N stop run after N steps'
    write(*,*) '	-5 save HDF5 files instead of Matlab files'



end subroutine


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




subroutine boundary_check_general (no_tot,Lz,Ly,Lz_low_limit,Lz_high_limit,Lz_inj,z,y,verbose,debug,ux,uy,uz,vtot_init,use_vtot,&
        no_part,no_species,stype,dz,Na,count,restore,cont,flag,equipot,n,Nz,m,run_inversion,scenario,Ny,flag_m,dy,&
        objects,edges,edge_charge,q,dt,spec_params,proc_no,utherm,objects_enum,objects_current,objects_power_flux,&
        nobjects,edge_flux,edge_energy_flux,edge_velocity_x,edge_velocity_y,edge_velocity_z,take_diag,mks_info,&
        Ly_start,Ly_stop,proc_max,domain_decomp,stype_transfer,npts,t_com3,Nz_max,slice_proc,bc_matrix,y_start,&
        y_stop,z_start,z_stop,z_start_box,z_stop_box,Lz_disp,total_energy,P0,Pot,use_surface_heat_tweaks,&
        impact_diag,emmit_diag,fv_bin,have_impact_diag,beam_switch,no_diag_reg,particle_no,orientation,lambda_D,w_i,&
        no_objects,object_params,ns,Eout,f_ebs,cdf_ebs,Ax_half,Ay_half,Az_half,Bx_half,By_half,Bz_half,Cx_half,Cy_half,Cz_half,cosalpha_yz)
        
    use spice2_types
    implicit none
    !include 'struct.h'
    include 'mpif.h'

    !checks for particles crossing borders of the simulation region and tiles
    !MK complete rewrite for enhanced effectivity + simple check
    !MK general routine that doesn't require fixed geometry, uses information from flag array instead
    type(pinfo), dimension(no_species):: spec_params
    type(object_t), dimension(no_objects):: object_params

    type(mks_t):: mks_info
    integer:: i,no_tot,N_temp,no_species,Na,count,n,Nz,Ny,proc_no,sp,nobjects,npts,Nz_max,y_start,y_stop,z_start,z_stop,z_start_box,z_stop_box,no_diag_reg,k,j,no_objects,ns
    real*8:: Lz,Ly,Ly1,Ly2,Lz1,p_z,dz,Lz_limit,dy,Lz_low_limit,Lz_high_limit,P0,vtot_sum,delta_se
    real*8, dimension(Npts):: z,y,ux,uy,uz,vtot_init
    integer, dimension(no_species):: no_part
    real*8, dimension(no_species):: Lz_inj,utherm,real_Lz_low_inj
    logical, dimension(no_species):: scenario_flag
    integer*2, dimension(Npts):: stype
    logical:: verbose,debug,restore,ang_diag,cont
    integer, dimension(n - Nz):: flag
    real*8, dimension(n - Nz):: equipot
    real*8, dimension(no_species)::m,T
    integer, dimension(Nz,Ny):: flag_m,flag_m_big,objects_enum
    integer, dimension(Nz+1,Ny+1):: objects_enum_big

    real*8, dimension(2,nobjects):: objects_current,objects_power_flux
    real*8, dimension(Nz,Ny,no_species):: edge_flux,edge_energy_flux,edge_velocity_x,edge_velocity_y,edge_velocity_z
    real*8, dimension(Nz+1,Ny+1,no_species):: edge_flux_b,edge_energy_flux_b,edge_velocity_x_b,edge_velocity_y_b,edge_velocity_z_b
    integer,dimension(no_species):: stype_transfer
    !boundary localation
    integer:: N_z,N_y, ngp_z, ngp_y,br,se_stype
    !erosion parameters
    real*8:: erode_prob,rndx
    logical:: run_inversion, take_diag
    ! scenario related vars
    integer:: scenario
    real*8:: Lz_low_inj
    integer, dimension(Nz,Ny):: objects, edges,orientation
    integer, dimension(Nz+1,Ny+1):: edges_b

    real*8, dimension(Nz,Ny):: edge_charge,se
    integer iz, iy
    real*8::A11, A12, A21, A22,A1,A2
    real*8, dimension(no_species):: q
    real*8:: q_sum
    integer:: nz1,ny1,nz2,ny2
    real*8:: dt,izx,iyx,vtot
    real*8:: chargei, chargee

    real*8:: edge_tot
    ! domain decomposition
    real*8:: Ly_start,Ly_stop,Lz_disp
    integer:: proc_max,enum
    logical:: domain_decomp

    integer:: left_i,right_i,left_proc_no,right_proc_no,ierr
    integer par_status(MPI_STATUS_SIZE)
    integer*8:: t_com3,t_start
    integer*8:: t_s,t_r,t_f
    integer:: incoming_left_i,incoming_right_i
    integer, dimension(Nz_max,Ny):: slice_proc
    integer, dimension(Nz,Ny):: bc_matrix
    logical:: enable_electron_reflection,two_side_injection,use_vtot,use_surface_heat_tweaks,have_impact_diag
    logical, dimension(no_species):: reflect_bottom,reflect_top
    real*8, dimension(no_species,4):: total_energy
    real*8:: mid_z,mid_y,interp_pot
    real*8, dimension(Nz,Ny):: Pot
    logical:: beam_switch,did_ebs

    ! impact diag
    integer:: fv_bin
    real*8, dimension(no_species,nobjects,fv_bin,fv_bin):: impact_diag,emmit_diag
    integer,dimension(no_diag_reg):: particle_no

    ! SEE stuff
    real*8:: E_inc,theta_inc,lambda_D,w_i,delta_see,nu_ebs,Eout_ebs,theta,psi,zo,yo,cosalpha_yz
    integer:: n_zo,n_yo,act_orientation,landing
    real*8, dimension(5,ns):: f_ebs,cdf_ebs
    real*8, dimension(ns):: Eout

    real*8, dimension(no_species):: Ax_half,Ay_half,Az_half,Bx_half,By_half,Bz_half,Cx_half,Cy_half,Cz_half
    delta_see = 0.0
    did_ebs = .false.
    nu_ebs = 0.0
    if (debug) then
        write(*,*) 'Survived decl. in collision detection'
    endif
    left_i = 0
    right_i = 0
    ierr = 0
    vtot_sum = 0.0
    ! zero out the objects currents
    objects_current = 0.0
    objects_power_flux = 0.0
    ! dumr = 0.0
    ! if (sum(edge_flux(:,:,1)).lt.0.0) then
    ! write(*,*) proc_no,'Wrong edge_flux (inside BC - beginning)',sum(edge_flux(:,:,1)),sum(edge_flux_b(1:Nz,1:Ny,1))
    ! stop
    ! endif

    ! enable reflection of electrons for 2 side particle injection. This should fix the artificial sheath problem
    enable_electron_reflection = .true.
    two_side_injection = .false.
    reflect_bottom = .false.
    reflect_top = .false.

    !write(*,*) 'Electron reflection',enable_electron_reflection
    if (enable_electron_reflection) then
        do sp=1,no_species
            !  write(*,*) 'injection method',sp,spec_params(sp)%injection_method

            if (spec_params(sp)%injection_method.eq.1) then
                ! write(*,*) 'Detected two side injection',sp
                two_side_injection = .true.
            endif

        enddo

        if (two_side_injection) then
            do sp=1,no_species
                if (spec_params(sp)%injection_method.eq.0.and.q(sp).lt.0.0) then
                    reflect_bottom(sp) = .true.
                    !  write(*,*) 'Setting bottom reflection for species',sp
                endif
                if (spec_params(sp)%injection_method.eq.1.and.q(sp).lt.0.0) then
                    reflect_top(sp) = .true.
                    !  write(*,*) 'Setting top reflection for species',sp

                endif

            enddo
        endif

    endif
    ! Lz_limit = min(Lz_limit,Lz)
    !Check if the ion has left the simulation region -> the outer box is divided into 12 boxes!
    !===============================================
    N_temp = no_tot
    run_inversion = .false.
    call gettime(t_s)

    ! flag_m_big = 1
    ! flag_m_big(1:Nz,1:Ny) = objects
    rndx = 0.0
    ! edge_flux = 0.0
    ! edge_energy_flux = 0.0
    ! edge_velocity_x  = 0.0
    ! edge_velocity_y  = 0.0
    ! edge_velocity_z  = 0.0
    if (take_diag) then

        !      edge_flux_b(1:Nz,1:Ny,:)  = 0.0
        !      edge_energy_flux_b(1:Nz,1:Ny,:)  = 0.0
        edge_flux_b  = 0.0
        edge_energy_flux_b  = 0.0

        do sp=1,no_species
            if (spec_params(sp)%mpi_rank.eq.proc_no) then

                ! edge_flux_b(z_start_box:(z_stop_box +1),y_start:(y_stop+1),:) = 0.0
                ! edge_energy_flux_b(z_start_box:(z_stop_box +1),y_start:(y_stop+1),:) = 0.0
                ! edge_velocity_x_b(z_start_box:(z_stop_box +1),y_start:(y_stop+1),:)  = 0.0
                ! edge_velocity_y_b(z_start_box:(z_stop_box +1),y_start:(y_stop+1),:)  = 0.0
                ! edge_velocity_z_b(z_start_box:(z_stop_box +1),y_start:(y_stop+1),:)  = 0.0
                ! edges_b(z_start_box:(z_stop_box +1),y_start:(y_stop+1)) = 0
                ! write(*,*) 'Creating edges_b',Nz,Ny
                !  edge_flux_b(:,:,sp) = 0.0
                !  edge_energy_flux_b(:,:,sp) = 0.0
                edge_velocity_x_b(:,:,sp) = 0.0
                edge_velocity_y_b(:,:,sp) = 0.0
                edge_velocity_z_b(:,:,sp) = 0.0

            endif
        enddo

    endif
    edges_b = 0.0
    edges_b(1:Nz,1:Ny) = edges(1:Nz,1:Ny)
    edges_b(1:Nz,Ny+1)= edges(:,1)
    objects_enum_big(1:Nz,1:Ny) = objects_enum
    objects_enum_big(1:Nz,Ny+1) = objects_enum(1:Nz,1)
    objects_enum_big(Nz+1,:) = 0


    ! write(*,*) 'DONE'
    chargei = 0.0
    chargee = 0.0
    ! prepare some flags
    if (proc_no.eq.0) then
        left_proc_no = proc_max
    else
        left_proc_no = proc_no - 1
    endif

    if (proc_no.eq.proc_max) then
        right_proc_no = 0
    else
        right_proc_no = proc_no + 1
    endif

    do sp=1,no_species
        if (scenario.eq.1.or.scenario.eq.3.or.scenario.eq.4) then
            real_Lz_low_inj(sp) = 0.0
        else if (scenario.eq.2) then
            real_Lz_low_inj(sp) = Lz - Lz_inj(sp)
            if (real_Lz_low_inj(sp).gt.0.0) then
                write(*,*) 'Error, lower limit higher than 0',real_Lz_low_inj(sp)
                write(*,*) 'Params', sp
            endif
        endif
        scenario_flag(sp) = scenario.eq.4.and.(spec_params(sp)%injection_method.eq.3.or.spec_params(sp)%injection_method.eq.4)

    enddo
    ! set erosion probability
    !erode_prob = 1.0/10000.0
    erode_prob = 0.0

    ! debug check
    ! do i=no_tot,1,-1
    ! if (stype(i).eq.4.and.z(i).lt.0.0) then
    ! write(*,*) 'bottom reflection candidate',i,z(i),stype(i)
    ! endif
    !
    ! enddo


    call gettime(t_f)

    if (take_diag) then
        ! write(*,*) 'BC INIT:',float(t_f - t_s)/1.0E6
    endif
    ! write(*,*) 'Low inj',real_Lz_low_inj
    ! write(*,*) 'High inj', Lz_inj


    do i=no_tot,1,-1

        did_ebs = .false.
        !  write (*,*) 'Lenght of the injection box',Lz_inj(stype(i)),stype(i),i,Lz_inj
        !MK fix  - do per. conditions only outside the tile
        Lz_low_inj = real_Lz_low_inj(stype(i))
        ! if (z(i) > Lz1) then
        !first check for periodic boundaries at y=0 and y=Ly

        if (y(i)>=Ly) then   ! = box #2           ! and they continue to live thanks
            y(i)=y(i)-idint(y(i)/Ly)*Ly                           ! to the PC
            if (abs(Lz_disp).gt.0.0) then
                z(i) = z(i) + Lz_disp
            endif
            ! = box #7
        else if (y(i) < 0.0) then
            y(i)=y(i)+(1 +idint(-y(i)/Ly))*Ly
            if (abs(Lz_disp).gt.0.0) then
                z(i) = z(i) - Lz_disp
            endif

        endif
        if (beam_switch.and.z(i).lt.0.0) then
            z(i) = z(i) + Lz_inj(stype(i))
        endif

        if (beam_switch.and.z(i).gt.Lz_inj(stype(i))) then
            z(i) = z(i) - Lz_inj(stype(i))
        endif

        ! endif
        !now the sofisticated if structure to eliminate particles
        if (z(i) < Lz_low_inj) then
            !Eliminate the neutralized ion from the inventory.
            !debug printoutans
            ! if (stype(i).eq.2) then
            ! 		write(*,*) 'Eliminating electron at the bottom!!!'
            !  		write(*,*) 'Eliminated',z(i),y(i)
            ! endif
            ! if (stype(i).eq.4) then
            ! write(*,*) 'bottom boundary',z(i),stype(i),reflect_bottom(stype(i))
            ! endif
            if (scenario_flag(stype(i)).or.reflect_bottom(stype(i))) then
                !     write(*,*) 'Bottom electron reflection',z(i),stype(i)
                ! reverse uz
                uz(i) = -uz(i)
                ! place it on a slightly different z
                !   write(*,*) 'Particle_reflection',y(i),z(i),uz(i)
                z(i) = Lz_low_inj + abs(z(i) - Lz_low_inj)

            else
                !	write(*,*) 'Eliminating particle - z=0',y(i),z(i),stype(i)
                no_part(stype(i)) = no_part(stype(i)) -1
                do k=1,no_diag_reg
                    if (particle_no(k).eq.i) then
                        write (*,*) 'Particle reached bottom boundary'
                        particle_no(k) = -1
                    endif
                enddo

                if (i<N_temp) then
                    !Replace the neutralized ion by the last one in the list.
                    z(i)=z(N_temp)
                    y(i)=y(N_temp)
                    uz(i)=uz(N_temp)
                    uy(i)=uy(N_temp)
                    ux(i)=ux(N_temp)
                    if (use_vtot) then
                        vtot_init(i) = vtot_init(N_temp)
                    endif
                    ! particle tracking algorithm
                    stype(i) = stype(N_temp)
                endif  !if (i<N_itemp) then

                N_temp=N_temp-1
            endif

        else if (z(i) > Lz_inj(stype(i))) then
            !debug printout
            ! if (stype(i).eq.1) then
            ! 		write(*,*) 'Eliminating top injected ion!!!'
            !  		write(*,*) 'Eliminated',z(i),y(i)
            ! endif

            ! 		if (stype(i).eq.1) then
            ! 		write(*,*) 'Eliminated',z(i),y(i)
            ! 		endif
            !Eliminate the neutralized ion from the inventory.
            ! dont eliminate SOL particles but reflect them (easy for now)
            if (spec_params(stype(i))%injection_method.eq.3.or.spec_params(stype(i))%injection_method.eq.4.or.reflect_top(stype(i))) then
                !     write(*,*) 'Bottom electron reflection',z(i),stype(i)

                ! reverse uz
                uz(i) = -uz(i)
                ! place it on a slightly different z
                !   write(*,*) 'Particle_reflection',y(i),z(i),uz(i)
                z(i) = Lz_inj(stype(i)) - (z(i) - Lz_inj(stype(i)) )

            else
                no_part(stype(i)) = no_part(stype(i)) -1
                total_energy(stype(i),3) = total_energy(stype(i),3) + ux(i)*ux(i) + uy(i)*uy(i) + uz(i)*uz(i)
                do k=1,no_diag_reg
                    if (particle_no(k).eq.i) then
                        write (*,*) 'Particle reached upper boundary'
                        particle_no(k) = -1
                    endif
                enddo
                if (i<N_temp) then

                    !Replace the neutralized ion by the last one in the list.
                    z(i)=z(N_temp)
                    y(i)=y(N_temp)
                    uz(i)=uz(N_temp)
                    uy(i)=uy(N_temp)
                    ux(i)=ux(N_temp)
                    if (use_vtot) then
                        vtot_init(i) = vtot_init(N_temp)
                    endif
                    stype(i) = stype(N_temp)


                endif  !if (i<N_itemp) then


                N_temp=N_temp-1
            endif
        else if ((z(i).le.Lz_high_limit).and.(z(i).ge.Lz_low_limit)) then
            !	else if(1.gt.2) then
            !find the nearest 4 grid points
            n_z   = idint(z(i)/dz) + 1
            n_y = idint(y(i)/dy) + 1
            ! 	if ((n_y.lt.1).or.(n_y.gt.Ny).or.(n_z.lt.1).or.(n_z.gt.Nz)) then
            ! 		write(*,*) 'Boundary error',n_z,n_y
            ! 	endif
            !check the flag array - only all 4 grid points as boundaries force elimination
            !       	br = flag_m_big(n_z,n_y)*flag_m_big(n_z + 1,n_y)*flag_m_big(n_z,n_y+1)*flag_m_big(n_z + 1,n_y + 1)
            ! add the edge flux
            !call add_edge_charge(z(i),y(i),uz(i),uy(i),1.0,dz,dy,Nz,Ny,edges,edge_flux(:,:,stype(i)),debug,dt)
            ! write(*,*) 'MATR',n_z,n_y
            ! write(*,*) 'MATR',n_z,n_y,bc_matrix(n_z,n_y)
            !
            if (bc_matrix(n_z,n_y).ne.0) then
                ! construct the old position
                zo = z(i) - uz(i)*dt
                yo = y(i) - uy(i)*dt
                ! get the coordinates of the old position
                n_zo = idint(zo/dz) + 1
                n_yo = idint(yo/dz) + 1
                landing=0
                ! determine the orientation of the landing
                !only for the proper particle coming from the plasma
                !            if (bc_matrix(n_zo,n_yo).eq.0) then
                !            if (n_zo.gt.n_z) then
                !                act_orientation = 1
                !            elseif (n_zo.lt.n_z) then
                !                act_orientation = 2
                !            elseif (n_yo.gt.n_y) then
                !                act_orientation = 4
                !            elseif (n_yo.le.n_y) then
                !                act_orientation = 3
                !            endif
                !            else
                !                ! something weird happened, set the most probable orientation
                !                act_orientation = 1
                !                write(*,*) 'Impact error',z(i),y(i),zo,yo
                !            endif
                ! decide corner cases
                if (orientation(n_z,n_y).gt.4) then
                    call G05FAF(0.d0,1.d0,1,rndx)
                    if (rndx.lt.dacos(abs(cosalpha_yz))/3.14159*2.0) then
                        landing=1
                    else
                        landing=2
                    endif
                    !  write(*,*) 'Landing',landing,dacos(abs(cosalpha_yz))/3.14159*2.0

                endif
                ! add particle to impact diag
                if (have_impact_diag.and.count.gt.Na) then
                    enum = max(objects_enum_big(n_z,n_y),objects_enum_big(n_z+1,n_y),objects_enum_big(n_z,n_y+1),objects_enum_big(n_z+1,n_y+1))
                    if (enum.gt. 0) then
                        sp =stype(i)
                        !		  add_to_impact_diag(ux,uy,uz,object,sp,fv_bin,no_species,no_objects,utherm,no,impact_diag)
                        call   add_to_impact_diag(ux(i),uy(i),uz(i),sp,fv_bin,no_species,nobjects,utherm(sp),enum,impact_diag(sp,enum,:,:))

                    endif
                endif
                if (use_surface_heat_tweaks) then
                    if (q(stype(i)).gt.0.0) then
                        ! ions: additional energy = Ionization energy - Wf + binding energy
                        vtot = ux(i)*ux(i) + uy(i)*uy(i) + uz(i)*uz(i) + 2.0*mks_info%mks_par2/m(stype(i))
                        !  write(*,*) 'Ion energy check',ux(i)*ux(i) + uy(i)*uy(i) + uz(i)*uz(i),2.0*mks_info%mks_par2/m(stype(i))
                    else
                        ! electrons: addition energy: Wf
                        vtot = ux(i)*ux(i) + uy(i)*uy(i) + uz(i)*uz(i)+ 2.0*mks_info%mks_par1/m(stype(i))
                        !			  vtot =  uz(i)*uz(i)+ 2*mks_info%mks_par1/m(stype(i))
                    endif
                else
                    vtot = ux(i)*ux(i) + uy(i)*uy(i) + uz(i)*uz(i)
                endif

                ! vtot = ux(i)*ux(i) + uy(i)*uy(i) + uz(i)*uz(i)
                ! adding acceleratin/deceleration in the sheath - this hack should be replaced by real potential of the object
                total_energy(stype(i),4) = total_energy(stype(i),4) + vtot  +q(stype(i))*P0*2*utherm(stype(i))*utherm(stype(i))

                if (count.gt.Na.and.take_diag) then



                    if (use_vtot) then
                        ! fix small error in vtot calculation
                        ! first interpolate y an z position to mid time step_max

                        !  mid_z = z(i) - uz(i)*dt/2
                        !       mid_y = y(i) - uy(i)*dt/2
                        !        iyx =mid_y/dy - idint(mid_y/dy) +1
                        !        izx =mid_z/dz - idint(mid_z/dz) +1

                        ! the calculate the potential interpolated to this position
                        !       interp_pot = Pot(idint(mid_z/dz) + 1,idint(mid_y/dz) + 1)*(1.d0-izx)*(1.d0-iyx) + Pot(idint(mid_z/dz) + 2,idint(mid_y/dz) + 1)*(izx)*(1.d0-iyx) + Pot(idint(mid_z/dz) + 1,idint(mid_y/dz) + 2)*(1.d0-izx)*(iyx) + Pot(idint(mid_z/dz) + 2,idint(mid_y/dz) + 2)*(izx)*(iyx)

                        ! next - subtract the potential difference from this potential and final potental from vtot
                        !	   write(*,*) 'EEfix',stype(i),Pot(n_z,n_y) - interp_pot,(vtot +q(stype(i))*(Pot(n_z,n_y) - interp_pot)*2*utherm(stype(i))*utherm(stype(i)))/vtot
                        !           vtot = vtot +q(stype(i))*(Pot(n_z,n_y) - interp_pot)*2*utherm(stype(i))*utherm(stype(i))
                        ! For thermionic electrons replace vtot by the initial energy
                        if (spec_params(stype(i))%injection_method.eq.5.and.vtot_init(i).gt.0.0) then
                            !   write(*,*) 'E balance check',vtot/vtot_init(i)
                            !			write(*,*) 'Impact using vinit',vtot/vtot_init(i)
                            vtot = vtot_init(i)

                        elseif (spec_params(stype(i))%injection_method.eq.5.and.vtot_init(i).eq.0.0) then
                            write(*,*) 'Vtot_init empty',vtot
                        endif
                    endif

                    iyx =y(i)/dy - n_y +1
                    izx =z(i)/dz - n_z +1

                    if (spec_params(stype(i))%injection_method.eq.5) then
                        vtot_sum = vtot_sum + vtot
                    endif

                    ! this is the mask sum. if all 4 grid points would lie on a surface, this would be = 1.0
                    ! if not, we multiply the final charge by 1/edge_tot to conserve charge
                    ! get rid of N/As
                    edge_tot = 1.0e-6
                    edge_tot = edge_tot + edges_b(n_z,n_y)*(1.d0-izx)*(1.d0-iyx) +  edges_b(n_z,n_y+1)*(1.d0-izx)*(iyx) + edges_b(n_z +1,n_y)*(izx)*(1.d0-iyx) +edges_b(n_z+1,n_y+1)*(izx)*(iyx)
                    ! write(*,*) 'EDGE DIAG'
                    ! write(*,*) 'edge_tot',edge_tot
                    ! write(*,*) 'edges',edges(n_z,n_y), edges(n_z,n_y+1),edges(n_z+1,n_y),edges(n_z+1,n_y+1)
                    ! if (edge_tot.le.0.0.or.edge_tot.gt.1.0.or. iyx.gt.1.0.or.izx.gt.1.0.or.iyx.lt.0.0.or.izx.lt.0.0) then
                    ! write(*,*) proc_no,'Error in edge_tot:',edge_tot,z(i),y(i),izx,iyx
                    ! stop
                    !
                    ! endif
                    !    write(*,*) 'DGETOT',(1.d0-izx)*(1.d0-iyx)*edges_b(n_z,n_y)/edge_tot,(1.d0-izx)*iyx*edges_b(n_z,n_y+1)/edge_tot,izx*(1.d0-iyx)*edges_b(n_z+1,n_y)/edge_tot,izx*iyx*edges_b(n_z+1,n_y+1)/edge_tot
                    !
                    !   if ((1.d0-izx)*(1.d0-iyx)*edges_b(n_z,n_y)/edge_tot.gt.1.0.or.(1.d0-izx)*(1.d0-iyx)*edges_b(n_z,n_y)/edge_tot.lt.0.0)    then
                    !       write(*,*) proc_no,'Wrong edge flux increment',(1.d0-izx)*(1.d0-iyx)*edges_b(n_z,n_y)/edge_tot,z(i),y(i)
                    !       write(*,*) proc_no,'Comp',izx,iyx,edge_tot,edges_b(n_z,n_y),n_z,n_y
                    ! stop
                    ! endif


                    edge_flux_b(n_z,n_y,stype(i)) = edge_flux_b(n_z,n_y,stype(i)) + (1.d0-izx)*(1.d0-iyx)*edges_b(n_z,n_y)/edge_tot
                    edge_energy_flux_b(n_z,n_y,stype(i)) = edge_energy_flux_b(n_z,n_y,stype(i)) + (1.d0-izx)*(1.d0-iyx)*vtot*edges_b(n_z,n_y)/edge_tot
                    edge_velocity_x_b(n_z,n_y,stype(i)) = edge_velocity_x_b(n_z,n_y,stype(i)) + (1.d0-izx)*(1.d0-iyx)*ux(i)*edges_b(n_z,n_y)/edge_tot
                    edge_velocity_y_b(n_z,n_y,stype(i)) = edge_velocity_y_b(n_z,n_y,stype(i)) + (1.d0-izx)*(1.d0-iyx)*uy(i)*edges_b(n_z,n_y)/edge_tot
                    edge_velocity_z_b(n_z,n_y,stype(i)) = edge_velocity_z_b(n_z,n_y,stype(i)) + (1.d0-izx)*(1.d0-iyx)*uz(i)*edges_b(n_z,n_y)/edge_tot


                    !       if ((1.d0-izx)*iyx*edges_b(n_z,n_y+1)/edge_tot.gt.1.0.or.(1.d0-izx)*iyx*edges_b(n_z,n_y+1)/edge_tot.lt.0.0)    then
                    !       write(*,*) proc_no,'Wrong edge flux increment',(1.d0-izx)*iyx*edges_b(n_z,n_y+1)/edge_tot
                    !       write(*,*) proc_no,'Comp',izx,iyx,edge_tot,edges_b(n_z,n_y+1),n_z,n_y+1
                    ! stop
                    ! endif

                    edge_flux_b(n_z,n_y+1,stype(i)) = edge_flux_b(n_z,n_y+1,stype(i)) + (1.d0-izx)*iyx*edges_b(n_z,n_y+1)/edge_tot
                    edge_energy_flux_b(n_z,n_y+1,stype(i)) = edge_energy_flux_b(n_z,n_y+1,stype(i)) + (1.d0-izx)*iyx*vtot*edges_b(n_z,n_y+1)/edge_tot
                    edge_velocity_x_b(n_z,n_y+1,stype(i)) = edge_velocity_x_b(n_z,n_y+1,stype(i)) + (1.d0-izx)*(iyx)*ux(i)*edges_b(n_z,n_y+1)/edge_tot
                    edge_velocity_y_b(n_z,n_y+1,stype(i)) = edge_velocity_y_b(n_z,n_y+1,stype(i)) + (1.d0-izx)*(iyx)*uy(i)*edges_b(n_z,n_y+1)/edge_tot
                    edge_velocity_z_b(n_z,n_y+1,stype(i)) = edge_velocity_z_b(n_z,n_y+1,stype(i)) + (1.d0-izx)*(iyx)*uz(i)*edges_b(n_z,n_y+1)/edge_tot
                    !
                    !      if (izx*(1.d0-iyx)*edges_b(n_z+1,n_y)/edge_tot.gt.1.0.or.izx*(1.d0-iyx)*edges_b(n_z+1,n_y)/edge_tot.lt.0.0)    then
                    !       write(*,*) proc_no,'Wrong edge flux increment',izx*(1.d0-iyx)*edges_b(n_z+1,n_y)/edge_tot
                    !       write(*,*) proc_no,'Comp',izx,iyx,edge_tot,edges_b(n_z+1,n_y),n_z+1,n_y
                    ! stop
                    ! endif

                    edge_flux_b(n_z+1,n_y,stype(i)) = edge_flux_b(n_z+1,n_y,stype(i)) + izx*(1.d0-iyx)*edges_b(n_z+1,n_y)/edge_tot
                    edge_energy_flux_b(n_z+1,n_y,stype(i)) = edge_energy_flux_b(n_z+1,n_y,stype(i)) + izx*(1.d0-iyx)*vtot*edges_b(n_z+1,n_y)/edge_tot
                    edge_velocity_x_b(n_z+1,n_y,stype(i)) = edge_velocity_x_b(n_z+1,n_y,stype(i)) + (izx)*(1.d0-iyx)*ux(i)*edges_b(n_z+1,n_y)/edge_tot
                    edge_velocity_y_b(n_z+1,n_y,stype(i)) = edge_velocity_y_b(n_z+1,n_y,stype(i)) + (izx)*(1.d0-iyx)*uy(i)*edges_b(n_z+1,n_y)/edge_tot
                    edge_velocity_z_b(n_z+1,n_y,stype(i)) = edge_velocity_z_b(n_z+1,n_y,stype(i)) + (izx)*(1.d0-iyx)*uz(i)*edges_b(n_z+1,n_y)/edge_tot

                    !      if (izx*iyx*edges_b(n_z+1,n_y+1)/edge_tot.gt.1.0.or.izx*iyx*edges_b(n_z+1,n_y+1)/edge_tot.lt.0.0)    then
                    !       write(*,*) proc_no,'Wrong edge flux increment',izx*iyx*edges_b(n_z+1,n_y+1)/edge_tot
                    !       write(*,*) proc_no,'Comp',izx,iyx,edge_tot,edges_b(n_z+1,n_y+1),n_z+1,n_y+1
                    ! stop
                    ! endif

                    edge_flux_b(n_z+1,n_y+1,stype(i)) = edge_flux_b(n_z+1,n_y+1,stype(i)) + izx*iyx*edges_b(n_z+1,n_y+1)/edge_tot
                    edge_energy_flux_b(n_z+1,n_y+1,stype(i)) = edge_energy_flux_b(n_z+1,n_y+1,stype(i)) + izx*iyx*vtot*edges_b(n_z+1,n_y+1)/edge_tot
                    edge_velocity_x_b(n_z+1,n_y+1,stype(i)) = edge_velocity_x_b(n_z+1,n_y+1,stype(i)) + (izx)*(iyx)*ux(i)*edges_b(n_z+1,n_y+1)/edge_tot
                    edge_velocity_y_b(n_z+1,n_y+1,stype(i)) = edge_velocity_y_b(n_z+1,n_y+1,stype(i)) + (izx)*(iyx)*uy(i)*edges_b(n_z+1,n_y+1)/edge_tot
                    edge_velocity_z_b(n_z+1,n_y+1,stype(i)) = edge_velocity_z_b(n_z+1,n_y+1,stype(i)) + (izx)*(iyx)*uz(i)*edges_b(n_z+1,n_y+1)/edge_tot

                endif
                !         if (z(i).lt.10.0.and.stype(i).eq.1) then
                !            write(*,*) 'BCD',z(i),y(i)
                !            write(*,*) w11(i),w12(i),w21(i),w22(i)
                !         endif
                !   vtot = ux(i)*ux(i) + uy(i)*uy(i) + uz(i)*uz(i)

                ! find the right objects- approximative only
                enum = max(objects_enum_big(n_z,n_y),objects_enum_big(n_z+1,n_y),objects_enum_big(n_z,n_y+1),objects_enum_big(n_z+1,n_y+1))
                ! add the particle charge to the appropriate current
                if (q(stype(i)).gt.0.0) then
                    ! ion current
                    objects_current(1,enum) = objects_current(1,enum) + q(stype(i))
                    objects_power_flux(1,enum) = objects_power_flux(1,enum) + 0.5*m(stype(i))*vtot

                else
                    ! electron current
                    ! trick for mu = 200
                    ! we use the MKS variables to determine real mu. if this info ain't available
                    ! we multiply current by sqrt(3670.0/mu) to get realistic floating potential
                    ! this means we imply Deuterium plasma!!!
                    if (mks_info%mks_main_ion_m.gt.0) then
                        objects_current(2,enum) = objects_current(2,enum) + q(stype(i))*dsqrt(m(stype(i))*1837.0*mks_info%mks_main_ion_m)
                        objects_power_flux(2,enum) = objects_power_flux(2,enum) + 0.5*m(stype(i))*dsqrt(m(stype(i))*1837.0*mks_info%mks_main_ion_m)*vtot

                    else

                        objects_current(2,enum) = objects_current(2,enum) + q(stype(i))*dsqrt(m(stype(i))*3674)
                        objects_power_flux(2,enum) = objects_power_flux(2,enum) + 0.5*m(stype(i))*dsqrt(m(stype(i))*3674)*vtot

                    endif
                endif
                !erosion procedure - only ions cause erosion - only non-steady state erosion
                ! disable erosion for now, we never use it

                ! 		if (count.lt.Na) then
                ! 			if (m(stype(i)).eq.1.0) then
                ! 				call G05FAF(0.d0,1.d0,1,rndx)
                ! ! 				write(*,*) 'Random',rndx
                ! 				if (rndx.le.erode_prob) then
                ! 					! find NGP and erode it
                ! 					flag(nint(z(i)) +1 + Nz*(nint(y(i)) +1 - 1)) = 1
                ! 					write(*,*) 'Particle::',z(i),y(i)
                ! 					write(*,*) 'Eroding point: ',nint(z(i)) +1,nint(y(i)) +1
                ! 					!set the inversion run
                ! 					run_inversion = .true.
                ! 				endif
                ! 			endif
                ! 		endif
                !debug printout
                ! dielectric check
                if (objects(n_z,n_y).eq.2) then
                    ! we add the charge to the nearest neighboroughs, if these are edge nodes
                    call add_edge_charge(z(i),y(i),uz(i),uy(i),q(stype(i)),dz,dy,Nz,Ny,edges,edge_charge,debug,dt)
                    ! 			if (n_z.gt.158) then
                    if (q(stype(i)).lt.0.0) then
                        chargee = chargee + q(stype(i))
                    else
                        chargei = chargei + q(stype(i))
                    endif
                    ! 			endif
                endif
                did_ebs = .false.


                ! electron back-scattering
                ! calculate the EBS probability
                if (orientation(n_z,n_y).eq.0) then
                    write(*,*) i,'Warning, orientation not set',n_z,n_y,edges(n_z,n_y)
                    write(*,*) i,'Warning, orientation not set N',z(i),y(i),stype(i)
                    write(*,*) i,'Warning, orientation not set O',z(i) - dt*uz(i),y(i) - dt*uy(i)
                    write(*,*) i,'Warning, orientation not set',ux(i),uy(i),uz(i)
                endif

                if (object_params(enum)%ebs_factor.gt.0.0.and.q(stype(i)).lt.0.0.and.orientation(n_z,n_y).gt.0.and.orientation(n_z,n_y).lt.9) then

                    vtot = dsqrt(ux(i)*ux(i) + uy(i)*uy(i) + uz(i)*uz(i))
                    theta_inc = 0.0
                    !                            if (orientation(n_z,n_y).eq.1.or.orientation(n_z,n_y).eq.2) then
                    if (orientation(n_z,n_y).eq.1.or.orientation(n_z,n_y).eq.2.or.landing.eq.1) then
                        ! impact angle
                        theta_inc = dacos(abs(uz(i)/vtot))
                    else
                        ! impact angle
                        theta_inc = dacos(abs(uy(i)/vtot))
                    endif
                    ! impact energy (convert to eV)
                    E_inc = 1/1.602E-19*0.5*vtot*vtot*m(stype(i))*mks_info%mks_main_ion_m*1.67E-27*lambda_D*lambda_D*w_i*w_i

                    call calculate_ebs_yield(E_inc,theta_inc,nu_ebs,object_params(enum)%material)
                    ! rescale  by the factor given in the input
                    nu_ebs = nu_ebs*object_params(objects_enum(n_z,n_y))%ebs_factor
                    if (orientation(n_z,n_y).gt.4) then
                        nu_ebs = nu_ebs/2.0
                    endif

                    !      write(*,*) 'EBS Theta',theta_inc,E_inc,nu_ebs
                    !write(*,'("6666666, ",1x,F,3(",",F))') E_inc,theta_inc,nu_ebs
                    ! get the random number
                    did_ebs = .false.
                    call G05FAF(0.d0,1.d0,1,rndx)
                    if (rndx.lt.nu_ebs) then
                        ! we will back-scatter
                        !     write(*,*) i,'EBS ON',nu_ebs
                        did_ebs = .true.
                        do sp=1,no_species
                            if (spec_params(sp)%mpi_rank.eq.proc_no.and.spec_params(sp)%injection_method.eq.7) then

                                ! calculate the exit energy and angles
                                ! we need to be careful to capture the whole angular interval


                                !if (ux(i).gt.0.0) then
                                psi = atan2(uy(i),ux(i)) + 3.14159
                                !    else
                                !         psi = atan2(uy(i),-ux(i)) + 3.14159
                                !    endif

                                !                                 write(*,'("11111111, ",1x,F,3(",",F))') E_inc,theta_inc,psi

                                theta = theta_inc
                                Eout_ebs = 0.0
                                call generate_energy_ebs(ns,Eout,f_ebs(object_params(enum)%material+1,:),cdf_ebs(object_params(enum)%material+1,:),Eout_ebs)
                                call generate_angular_ebs(theta,psi,object_params(enum)%material)


                                !   write(*,'("22222222, ",1x,F,3(",",F))') Eout_ebs,theta,psi
                                ! convert the energy to the energy of incident electron
                                Eout_ebs = Eout_ebs*E_inc
                                enum = max(objects_enum_big(n_z,n_y),objects_enum_big(n_z+1,n_y),objects_enum_big(n_z,n_y+1),objects_enum_big(n_z+1,n_y+1))

                                ! include the new particle
                                call inject_ebs(y,z,ux,uy,uz,stype,N_temp,Npts,sp,objects_current,objects_power_flux,edge_flux_b,edge_energy_flux_b,Nz,Ny,no_species,nobjects,Eout_ebs,theta,psi,orientation(n_z,n_y),lambda_D,w_i,enum,q,m,mks_info,edges_b,edge_velocity_x_b,edge_velocity_y_b,edge_velocity_z_b,use_surface_heat_tweaks,y(i),z(i),dz,spec_params(sp)%init_disp,n_z,n_y,proc_no,i,Ax_half(sp),Ay_half(sp),Az_half(sp),Bx_half(sp),By_half(sp),Bz_half(sp),Cx_half(sp),Cy_half(sp),Cz_half(sp),landing)
                                ! check that we haven't injected the particle inside a surface_temperature
                                if (bc_matrix(idint(z(N_temp)/dz) + 1,idint(y(N_temp)/dz) + 1).gt.0) then
                                    write(*,*) 'Warning: EBS particle injected in the surface!!!'
                                    write(*,*) 'EBS O',z(i),y(i),orientation(n_z,n_y),landing
                                    write(*,*) 'EBS N',z(N_temp),y(N_temp),idint(z(N_temp)/dz)+1,idint(y(N_temp)/dz)+1
                                endif


                                no_part(stype(N_temp)) = no_part(stype(N_temp)) +1
                                ! add the particle to the impact diag
                                if (have_impact_diag.and.count.gt.Na) then
                                    call  add_to_impact_diag(ux(N_temp),uy(N_temp),uz(N_temp),sp,fv_bin,no_species,no_objects,utherm(sp),enum,emmit_diag(sp,enum,:,:))
                                endif

                            endif
                        enddo
                    endif
                    !   write(*,*) i,'EBS OFF',nu_ebs
                    ! SEE
                endif
                if (object_params(enum)%see_factor.gt.0.0.and.q(stype(i)).lt.0.0.and.(.not.did_ebs).and.orientation(n_z,n_y).gt.0.and.orientation(n_z,n_y).lt.9) then
                    !      write(*,*) 'Electron impact',z(i),y(i),n_z,n_y
                    !secondary electron emission
                    do sp=1,no_species

                        if (spec_params(sp)%mpi_rank.eq.proc_no.and.spec_params(sp)%injection_method.eq.6) then
                            ! determine the surface orientation
                            vtot = dsqrt(ux(i)*ux(i) + uy(i)*uy(i) + uz(i)*uz(i))
                            theta_inc = 0.0
                            if (orientation(n_z,n_y).eq.1.or.orientation(n_z,n_y).eq.2.or.landing.eq.1) then
                                ! impact angle
                                theta_inc = dacos(abs(uz(i)/vtot))
                            else
                                ! impact angle
                                theta_inc = dacos(abs(uy(i)/vtot))
                            endif
                            ! impact energy (convert to eV)
                            E_inc = 1/1.602E-19*0.5*vtot*vtot*m(stype(i))*mks_info%mks_main_ion_m*1.67E-27*lambda_D*lambda_D*w_i*w_i
                            ! calculate the SEE yield
                            call generate_see_yield(E_inc,theta_inc,delta_see,object_params(enum)%material)
                            ! write(*,*) 'SEEC',E_inc,theta_inc,delta_see
                            ! write(*,'("55555555, ",1x,F,3(",",F))') E_inc,theta_inc,delta_see
                            ! adjust by the se from input
                            delta_see = delta_see*object_params(objects_enum(n_z,n_y))%see_factor
                            !     write(*,*) i,'SEE TRY',delta_see
                            ! fix border cells
                            if (orientation(n_z,n_y).gt.4) then
                                delta_see = delta_see/2.0
                            endif

                            if (debug) then
                                write(*,*) 'SIM',vtot,orientation(n_z,n_y),y(i),z(i)
                                write(*,*) 'IMP',ux(i),uy(i),uz(i),stype(i)
                                write(*,*) 'SEE',E_inc,theta_inc/3.14*180,delta_see
                            endif
                            ! if larger then 1, inject the number of electrons
                            enum = max(objects_enum_big(n_z,n_y),objects_enum_big(n_z+1,n_y),objects_enum_big(n_z,n_y+1),objects_enum_big(n_z+1,n_y+1))
                            do j=1,delta_see
                                call inject_see(y,z,ux,uy,uz,stype,N_temp,Npts,sp,objects_current,objects_power_flux,edge_flux_b,edge_energy_flux_b,Nz,Ny,no_species,nobjects,E_inc,theta_inc,orientation(n_z,n_y),lambda_D,w_i,enum,q,m,mks_info,edges_b,edge_velocity_x_b,edge_velocity_y_b,edge_velocity_z_b,use_surface_heat_tweaks,y(i),z(i),dz,spec_params(sp)%init_disp,n_z,n_y,proc_no,Ax_half(sp),Ay_half(sp),Az_half(sp),Bx_half(sp),By_half(sp),Bz_half(sp),Cx_half(sp),Cy_half(sp),Cz_half(sp),object_params(enum)%material,landing)

                                if (bc_matrix(idint(z(N_temp)/dz) + 1,idint(y(N_temp)/dz) + 1).gt.0) then
                                    write(*,*) 'Warning: SEE particle injected in the surface!!!'
                                    write(*,*) 'SEE O',z(i),y(i),orientation(n_z,n_y)
                                    write(*,*) 'SEE OO',zo,yo
                                    write(*,*) 'SEE N',z(N_temp),y(N_temp),idint(z(N_temp)/dz)+1,idint(y(N_temp)/dz)+1
                                endif

                                no_part(stype(N_temp)) = no_part(stype(N_temp)) +1
                                if (have_impact_diag.and.count.gt.Na) then

                                    call  add_to_impact_diag(ux(N_temp),uy(N_temp),uz(N_temp),sp,fv_bin,no_species,no_objects,utherm(sp),enum,emmit_diag(sp,enum,:,:))
                                endif

                            enddo
                            call G05FAF(0.d0,1.d0,1,rndx)
                            ! the last particle - check if the probability is in favour
                            if (rndx.le.(delta_see - floor(delta_see))) then
                                call inject_see(y,z,ux,uy,uz,stype,N_temp,Npts,sp,objects_current,objects_power_flux,edge_flux_b,edge_energy_flux_b,Nz,Ny,no_species,nobjects,E_inc,theta_inc,orientation(n_z,n_y),lambda_D,w_i,enum,q,m,mks_info,edges_b,edge_velocity_x_b,edge_velocity_y_b,edge_velocity_z_b,use_surface_heat_tweaks,y(i),z(i),dz,spec_params(sp)%init_disp,n_z,n_y,proc_no,Ax_half(sp),Ay_half(sp),Az_half(sp),Bx_half(sp),By_half(sp),Bz_half(sp),Cx_half(sp),Cy_half(sp),Cz_half(sp),object_params(enum)%material,landing)
                                if (bc_matrix(idint(z(N_temp)/dz) + 1,idint(y(N_temp)/dz) + 1).gt.0) then
                                    write(*,*) 'Warning: SEE particle injected in the surface!!!'
                                    write(*,*) 'SEE O',z(i),y(i),orientation(n_z,n_y)
                                    write(*,*) 'SEE OO',zo,yo
                                    write(*,*) 'SEE N',z(N_temp),y(N_temp),idint(z(N_temp)/dz)+1,idint(y(N_temp)/dz)+1
                                endif

                                no_part(stype(N_temp)) = no_part(stype(N_temp)) +1
                                if (have_impact_diag.and.count.gt.Na) then
                                    enum = max(objects_enum_big(n_z,n_y),objects_enum_big(n_z+1,n_y),objects_enum_big(n_z,n_y+1),objects_enum_big(n_z+1,n_y+1))
                                    call  add_to_impact_diag(ux(N_temp),uy(N_temp),uz(N_temp),sp,fv_bin,no_species,no_objects,utherm(sp),enum,emmit_diag(sp,enum,:,:))
                                endif


                            endif
                        endif
                    enddo
                endif
                ! 		write(*,*) 'Eliminated',z(i),y(i)
                no_part(stype(i)) = no_part(stype(i)) -1
                !left block, anglular diag
                !Eliminate the neutralized ion from the inventory.
                do k=1,no_diag_reg
                    if (particle_no(k).eq.i) then
                        write (*,*) 'Particle hit an object',i,stype(i),z(i),count
                        particle_no(k) = -1
                    endif
                enddo

                if (i<N_temp) then
                    !Replace the neutralized ion by the last one in the list.
                    z(i)=z(N_temp)
                    y(i)=y(N_temp)
                    uz(i)=uz(N_temp)
                    uy(i)=uy(N_temp)
                    ux(i)=ux(N_temp)
                    if (use_vtot) then
                        vtot_init(i) = vtot_init(N_temp)
                    endif
                    stype(i) = stype(N_temp)
                endif  !if (i<N_itemp) then

                N_temp=N_temp-1
            endif !br = 1
        endif ! border checking



    enddo  !do i=N_i,1,-1
    !if (debug) then
    ! 	do i=1,N_temp
    !check whether all particles are inside the box
    ! 	if ((z(i) < 0.0).or.(z(i) >Lz).or.(y(i) < 0.0).or.(y(i) > Ly)) then
    ! 		write(*,*) 'Error, particle out of the box!!!',i,y(i),z(i)
    ! 		stop
    ! 	endif
    ! 	enddo
    !endif
    !write(*,*) 'Q:',chargei,chargee
    !if (chargei.ne.0.0) then
    !	write(*,*) 'QR:',chargee/chargei
    !endif
    if (debug) then
        write(*,*)'Discarded',no_tot - N_temp, 'particles out of',no_tot
    endif
    no_tot=N_temp   !New number of particle after lost by neutralization or escaping (here: ions)

    ! edge diagnostics
    if (count.gt.Na.and.take_diag) then


        edge_flux_b(1:Nz,1,:) =edge_flux_b(1:Nz,1,:) + edge_flux_b(1:Nz,Ny + 1,:)
        edge_flux_b(1:Nz,Ny,:) =edge_flux_b(1:Nz,1,:)
        edge_flux = edge_flux + edge_flux_b(1:Nz,1:Ny,:)
        ! croscheck
        !     write(*,*) proc_no,'Edgeflux',sum(edge_flux(:,1:20,3))

        ! if (sum(edge_flux(:,:,1)).lt.0.0) then
        ! write(*,*) proc_no,'Wrong edge_flux (inside BC)',sum(edge_flux(:,:,1)),sum(edge_flux_b(1:Nz,1:Ny,1))
        ! stop
        ! endif

        edge_energy_flux_b(1:Nz,1,:) =edge_energy_flux_b(1:Nz,1,:) + edge_energy_flux_b(1:Nz,Ny + 1,:)
        edge_energy_flux_b(1:Nz,Ny,:) =edge_energy_flux_b(1:Nz,1,:)
        edge_energy_flux = edge_energy_flux + edge_energy_flux_b(1:Nz,1:Ny,:)

        edge_velocity_x = edge_velocity_x_b(1:Nz,1:Ny,:)
        edge_velocity_x(1:Nz,1,:) =edge_velocity_x(1:Nz,1,:) + edge_velocity_x_b(1:Nz,Ny + 1,:)
        edge_velocity_y = edge_velocity_y_b(1:Nz,1:Ny,:)
        edge_velocity_y(1:Nz,1,:) =edge_velocity_y(1:Nz,1,:) + edge_velocity_y_b(1:Nz,Ny + 1,:)
        edge_velocity_z = edge_velocity_z_b(1:Nz,1:Ny,:)
        edge_velocity_z(1:Nz,1,:) =edge_velocity_z(1:Nz,1,:) + edge_velocity_z_b(1:Nz,Ny + 1,:)
    endif

    if (debug) then
        write(*,*) 'Finished in BC'
    endif
    !if (vtot_sum.gt.0.0) then
    !    write(*,*) 'BC Vtot sum',vtot_sum
    !endif


    !      if (take_diag.and.proc_no.eq.0) then
    !	  write(*,*) 'BC: Edge energy flux sum',sum(edge_energy_flux(:,:,3))

    !   endif
end subroutine

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


subroutine calc_E_field (Nz,Ny,Ny1,Ny2,Nz1,Escz,Escy,Pot,dz,dy)
    implicit none
    !calculates E field at the grid points
    !2-points difference form (inside the region)
    integer:: Nz,Ny,j,k,Ny1,Ny2,Nz1
    real*8, dimension(Nz,Ny):: Escz,Escy,Pot
    real*8:: dz,dy
    do j=1,Nz
        do k=1,Ny
            if (j.eq.1.AND.k.ge.Ny1.AND.k.le.Ny2) then
                Escz(1,k)=(Pot(1,k)-Pot(2,k))/dz
            elseif (j.eq.Nz1.AND.k.ge.1.AND.k.le.Ny1) then
                Escz(Nz1,k)=(Pot(Nz1,k)-Pot(Nz1+1,k))/dz
            elseif (j.eq.Nz1.AND.k.ge.Ny2.AND.k.le.Ny) then
                Escz(Nz1,k)=(Pot(Nz1,k)-Pot(Nz1+1,k))/dz
            elseif (j.eq.Nz.AND.k.ge.1.AND.k.le.Ny) then
                Escz(Nz,k)=(Pot(Nz-1,k)-Pot(Nz,k))/dz
            elseif (k.eq.Ny1.AND.j.ge.2.AND.j.le.Nz1-1) then
                Escy(j,Ny1)=(Pot(j,Ny1)-Pot(j,Ny1+1))/dy
            elseif (k.eq.Ny2.AND.j.ge.2.AND.j.le.Nz1-1) then
                Escy(j,Ny2)=(Pot(j,Ny2-1)-Pot(j,Ny2))/dy
            elseif (k.eq.1.AND.j.ge.Nz1+1.AND.j.le.Nz-1) then
                Escz(j,1)=(Pot(j-1,1)-Pot(j+1,1))/2.d0/dz
                Escy(j,1)=(Pot(j,Ny-1)-Pot(j,2))/2.d0/dy        !PC - Periodic cond.
            elseif (k.eq.Ny.AND.j.ge.Nz1+1.AND.j.le.Nz-1) then
                Escz(j,Ny)=(Pot(j-1,Ny)-Pot(j+1,Ny))/2.d0/dz
                Escy(j,Ny)=(Pot(j,Ny-1)-Pot(j,2))/2.d0/dy       !PC - Periodic cond.
            elseif (j.ge.2.AND.j.le.Nz1.AND.k.ge.Ny1+1.AND.k.le.Ny2-1) then
                Escz(j,k)=(Pot(j-1,k)-Pot(j+1,k))/2.d0/dz
                Escy(j,k)=(Pot(j,k-1)-Pot(j,k+1))/2.d0/dy
            elseif (j.ge.Nz1+1.AND.j.le.Nz-1.AND.k.ge.2.AND.k.le.Ny-1) then
                Escz(j,k)=(Pot(j-1,k)-Pot(j+1,k))/2.d0/dz
                Escy(j,k)=(Pot(j,k-1)-Pot(j,k+1))/2.d0/dy
            endif
        enddo
    enddo
    !----------------------------------------------------------------------------------------

end subroutine
subroutine calc_E_field_general (Nz,Ny,Escz,Escy,Pot,dz,dy,objects,edges,Nz_disp)
    implicit none
    !calculates E field at the grid points
    !2-points difference form (inside the region)
    integer:: Nz,Ny,j,k,Ny1,Ny2,Nz1,Nz_disp
    real*8, dimension(Nz,Ny):: Escz,Escy,Pot
    integer, dimension(Nz,Ny):: objects
    integer, dimension(Nz,Ny):: edges

    real*8:: dz,dy
    do j=1,Nz
        do k=1,Ny
            if (j.eq.1) then
                Escz(1,k)=(Pot(1,k)-Pot(2,k))/dz
            elseif (j.eq.Nz) then
                Escz(Nz,k)=(Pot(Nz-1,k)-Pot(Nz,k))/dz
                ! field along y=1
            elseif (k.eq.1.AND.j.ge.2.AND.j.le.Nz-1) then

                if (abs(Nz_disp).gt.0) then
                    if (j.gt.abs(Nz_disp)) then
                        if (Nz_disp.gt.0) then
                            Escz(j,1)=(Pot(j-1,1)-Pot(j+1,1))/2.d0/dz
                            Escy(j,1)=(Pot(j-Nz_disp,Ny-1)-Pot(j,2))/2.d0/dy        !PC - Periodic cond.
                        else
                            Escz(j,1)=(Pot(j-1,1)-Pot(j+1,1))/2.d0/dz
                            Escy(j,1)=(Pot(j,Ny-1)-Pot(j-abs(Nz_disp),2))/2.d0/dy        !PC - Periodic cond.

                        endif

                    else
                        ! standard field for y=1
                        if (objects(j,k).gt.0.and.objects(j+1,k).eq.0) then
                            Escz(j,1)=(Pot(j,1)-Pot(j+1,1))/dz
                        else
                            Escz(j,1)=(Pot(j-1,1)-Pot(j+1,1))/2.d0/dz
                        endif

                        Escy(j,1)=(Pot(j,Ny-1)-Pot(j,2))/2.d0/dy        !PC - Periodic cond.
                    endif

                else
                    Escz(j,1)=(Pot(j-1,1)-Pot(j+1,1))/2.d0/dz
                    Escy(j,1)=(Pot(j,Ny-1)-Pot(j,2))/2.d0/dy        !PC - Periodic cond.
                endif
            elseif (k.eq.Ny.AND.j.ge.2.AND.j.le.Nz-1) then

                if (abs(Nz_disp).gt.0) then
                    if (j.lt.(Nz-abs(Nz_disp))) then

                        if (Nz_disp.gt.0) then
                            Escz(j,Ny)=(Pot(j-1,Ny)-Pot(j+1,Ny))/2.d0/dz
                            Escy(j,Ny)=(Pot(j,Ny-1)-Pot(j+Nz_disp,2))/2.d0/dy       !PC - Periodic cond.
                        else
                            Escz(j,Ny)=(Pot(j-1,Ny)-Pot(j+1,Ny))/2.d0/dz
                            Escy(j,Ny)=(Pot(j+abs(Nz_disp),Ny-1)-Pot(j,2))/2.d0/dy       !PC - Periodic cond.

                        endif


                    else
                        ! standard field for y=1
                        if (objects(j,k).gt.0.and.objects(j+1,k).eq.0) then
                            Escz(j,k)=(Pot(k,1)-Pot(j+1,k))/dz
                        else
                            Escz(j,k)=(Pot(j-1,k)-Pot(j+1,k))/2.d0/dz
                        endif
                        Escy(j,Ny)=(Pot(j,Ny-1)-Pot(j,2))/2.d0/dy       !PC - Periodic cond.

                    endif

                else
                    Escz(j,Ny)=(Pot(j-1,Ny)-Pot(j+1,Ny))/2.d0/dz
                    Escy(j,Ny)=(Pot(j,Ny-1)-Pot(j,2))/2.d0/dy       !PC - Periodic cond.
                endif

            elseif (j.ge.2.AND.j.le.Nz.AND.k.ge.2.AND.k.lt.Ny) then
                ! special treatement is needed for surface E fields
                if (edges(j,k).eq.0) then

                    Escz(j,k)=(Pot(j-1,k)-Pot(j+1,k))/2.d0/dz
                    !	    Escz(j,k)=(Pot(j -1,k)-Pot(j,k))/1.d0/dz
                    Escy(j,k)=(Pot(j,k-1)-Pot(j,k+1))/2.d0/dy
                else
                    ! edge E field, need to be more localized
                    if (objects(j-1,k).gt.0.and.objects(j+1,k).eq.0) then
                        ! perpendicular to the surface
                        Escz(j,k)=(Pot(j,k)-Pot(j+1,k))/1.d0/dz

                    else if (objects(j-1,k).eq.0.and.objects(j+1,k).gt.0) then
                        ! perpendicular to the surface
                        Escz(j,k)=(Pot(j-1,k)-Pot(j,k))/1.d0/dz
                    else
                        ! parallel to the surface - cant use middle derivitive to avoid sawtooth profile
                        ! we take the right derivitive
                        Escz(j,k)=(Pot(j,k)-Pot(j+1,k))/1.d0/dz
                    endif

                    if (objects(j,k-1).gt.0.and.objects(j,k+1).eq.0) then
                        ! perpendicular to the surface
                        Escy(j,k)=(Pot(j,k)-Pot(j,k+1))/1.d0/dy

                    else if (objects(j,k-1).eq.0.and.objects(j,k+1).gt.0) then
                        ! perpendicular to the surface
                        Escy(j,k)=(Pot(j,k-1)-Pot(j,k))/1.d0/dy

                    else
                        ! parallel to the surface - cant use middle derivitive to avoid sawtooth profile
                        ! we take the left derivitive
                        Escy(j,k)=(Pot(j,k)-Pot(j,k+1))/1.d0/dy
                    endif


                endif
            endif
        enddo
    enddo
    !----------------------------------------------------------------------------------------

end subroutine


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


subroutine rcomb_history(a,history_ntimes)
    !for combing of real*8 arrays
    integer:: k,history_ntimes
    real*8, dimension(history_ntimes):: a
    !get the averages
    do k=1,history_ntimes-1,2
        !average
        a(k) = (a(k) + a(k+1))/2.0
    enddo
    !half the array
    do k=1,int(history_ntimes/2)
        !average
        a(k) = a(2*k)
    enddo
    !zero the rest
    do k=int(history_ntimes/2),history_ntimes
        !average
        a(k) = 0.0
    enddo


end subroutine


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


subroutine icomb_history(a,history_ntimes)
    !for combing of integer arrays
    integer:: k,history_ntimes
    integer, dimension(history_ntimes):: a
    !get the averages
    do k=1,history_ntimes-1,2
        !average
        a(k) = (a(k) + a(k+1))/2
    enddo
    !half the array
    do k=1,int(history_ntimes/2)
        !shift array
        a(k) = a(2*k)
    enddo
    !zero the rest
    do k=int(history_ntimes/2),history_ntimes
        a(k) = 0
    enddo


end subroutine
subroutine dump_grid(a,Nz,Ny,scount,id)

    integer:: Nz,Ny,scount,id,i,k
    real*8, dimension(Nz,Ny):: a
    character*40:: grid_str
    character*5:: count_str

    if (id.eq.0) then
        grid_str = 'pot/pot'
    else if (id.eq.1) then
        grid_str = 'dens/dens'
    endif
    write(*,*) 'Received scount',scount
    count_str = '     '
    write(count_str(1:5),'(I5.5)') scount
    grid_str = trim(grid_str)//count_str//'.dat'
    write (*,*) 'SAWriting output to ',trim(grid_str)
    open(3,file = trim(grid_Str)//char(0),status='unknown')
    do i=1,Nz
        do k=1,Ny
            write(3,*) i,k,a(i,k)
        enddo
    enddo
    close(3)
end subroutine


subroutine matrix2vector_r (m,Nz,Ny,n,v)
    implicit none
    ! converts real*8 matrix array to a vector - needed for flag and equipot vectors - matrixes are easier to maintain
    integer:: Nz,Ny,n
    real*8, dimension(Nz,Ny):: m
    real*8, dimension(n - Nz):: v
    !other vars
    integer:: k,j
    do k=1,Ny-1
        do j=1,Nz
            v(j+Nz*(k-1))=m(j,k)

        enddo
    enddo
end subroutine

subroutine matrix2vector_i (m,Nz,Ny,n,v)
    implicit none
    ! converts integer matrix array to a vector - needed for flag and equipot vectors - matrixes are easier to maintain
    integer:: Nz,Ny,n
    integer, dimension(Nz,Ny):: m
    integer, dimension(n - Nz):: v
    !other vars
    integer:: k,j
    do k=1,Ny-1
        do j=1,Nz
            v(j+Nz*(k-1))=m(j,k)

        enddo
    enddo
end subroutine

subroutine check_halfplane(ya,za,yb,zb,yc,zc,y1,z1,result1)
    implicit none
    real*8:: ya,za,yb,zb,yc,zc,yt,ytc,a,b,bc
    real:: y1,z1
    integer:: result1
    logical:: debug


    debug = .false.
    if (debug) then
        write(*,*) 'A: ', za,ya
        write(*,*) 'B: ', zb,yb
        write(*,*) 'C: ', zc,yc
        write(*,*) 'X: ', z1,y1





    endif


    if (zb.ne.za) then
        !regular line
        a = (yb - ya)/(zb - za)
        b = - (yb - ya)/(zb - za)*za + ya
        bc = -a*zc + yc
        yt = a*z1  + b
        ytc = a*z1 + bc
        if (debug) then
            write(*,*) 'a: ',a
            write(*,*) 'b: ',b
            write(*,*) 'bc: ',bc

        endif

        if (abs(yt - y1) .le. abs(yt - ytc).and.((yt.ge.y1.and.yt.ge.ytc).or.(yt.le.y1.and.yt.le.ytc))) then
            result1 = 1
        else
            result1 = 0
        endif
    else
        ! straight line
        if (sign(1.0,real(zc - za)) .eq. sign(1.0,z1 - real(za)).and.abs(zc - za) .ge. abs(z1 - za)) then
            result1 = 1
        else
            result1 = 0
        endif



    endif




end subroutine
subroutine add_to_i_rel_history(i_rel,history)
    implicit none
    real*8:: i_rel
    real*8, dimension(10):: history
    integer::i
    do i=2,10
        history(i -1) = history(i)
    enddo
    history(10) = i_rel
end subroutine

subroutine adapt_float_constant(float_constant,history)
    implicit none
    real*8:: float_constant,prod
    real*8, dimension(10):: history
    integer:: i,test
    ! check if there are any zero members
    prod  =1.0
    do i=1,10
        prod = prod*history(i)
    enddo

    if (prod.eq.0.0) then
        ! too few samples, no change now
    else
        ! check for monotone series
        test = 0
        do i=1,9
            if (history(i)*history(i+1) .lt. 0.0) then
                test = 1
            endif
        enddo
        if (test.eq.0) then
            ! double the constant
            ! float_constant = float_constant*2.0
            write(*,*) 'Changing float_constant to', float_constant
        endif

        ! check for socilating series
        test = 0
        do i=1,9
            if (history(i)*history(i+1) .gt. 0.0) then
                test = 1
            endif
        enddo
        if (test.eq.0) then
            ! half the constant
            float_constant = float_constant/2.0
            write(*,*) 'Changing float_constant to', float_constant

        endif





    endif


end subroutine
subroutine dummysub ()
    ! write(*,*) 'Dummy'


end subroutine

subroutine add_to_impact_diag(ux,uy,uz,sp,fv_bin,no_species,no_objects,utherm,no,impact_diag)
    integer:: no_species,no_objects,fv_bin,bin1,bin2,no,sp
    real*8:: ux,uy,uz,object,rphi,utherm
    real*8,dimension(fv_bin,fv_bin):: impact_diag


    ! total energy
    ! bins allow to capture up to 4*utherm
    bin1 = abs(int((ux*ux + uy*uy + uz*uz)*real(fv_bin)/(16.0*utherm*utherm)))
    if (bin1.lt.1) then
        bin1 = 1
    endif
    if (bin1.gt.fv_bin) then
        bin1 = fv_bin
    endif

    !u perpendicular2
    rphi = asin(-uz/sqrt(ux*ux + uy*uy + uz*uz))/3.14159*180
    bin2 = abs(int((rphi)*real(fv_bin)/100.0))
    if (bin2.lt.1) then
        bin2 = 1
    endif
    if (bin2.gt.fv_bin) then
        bin2 = fv_bin
    endif

    !                                         write(*,*) 'Impact diag',sp,bin1,bin2,ux,uy,uz,400.0*utherm*utherm
    !				    if (bin1.gt.0.and.bin1.lt.fv_bin.and.bin2.gt.0.and.bin2.lt.fv_bin) then

    impact_diag(bin1,bin2) = impact_diag(bin1,bin2) +1.0

    !				    endif


end subroutine
