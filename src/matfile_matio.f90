! Matlab firholes related subroutines
! These routines use new library MATIO
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!MK subroutine to restore simulation from particle mat file
subroutine restore_pot(pfile,Nz,Ny,Pot,verbose)
    USE MATIO
    implicit none
    integer:: Nz,Ny,ierr
    real*8, dimension(Nz,Ny):: Pot
    logical:: verbose
    character*2048:: pfile
    integer*8:: ifile
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    if (verbose) then
        write(*,*) 'Restoring run from ',trim(pfile)//'.mat'
    end if
    CALL FMAT_LOGINIT('particle')
    ierr = FMat_Open(trim(pfile)//'.mat'//char(0),MAT_ACC_RDONLY,MAT)
    ! extract potential
    ierr = FMat_VarReadInfo(MAT,'Potav',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,Pot)
    IERR = FMAT_CLOSE(MAT)
    write(*,*) 'Fixed potential restored',sum(Pot)

end subroutine

subroutine advanced_restore_run_9_particles (tfile,verbose,debug,y,z,ux,uy,uz,Nz,Ny,Npts,no_species,stype,no_tot,proc_no,count,par_size,dz,dy,Nz_max,slice_proc,regroup_runs)
    USE MATIO
    implicit none
    integer:: Npts,ierr,length,version,proc_no,Nz,Ny,no_species,count,i,regroup_runs
    real*8, dimension(Npts):: y,z,ux,uy,uz
    integer*2, dimension(Npts):: stype
    character*2048:: tfile
    logical:: verbose,debug
    integer*8:: ifile
    integer:: dumi,no_tot
    real*8:: dumr,dz,dy
    character*2:: sp_str
    integer::par_size,global_ntot,n_limit,k,iyx,izx,Nz_max
    integer, dimension(Nz_max,Ny):: slice_proc
    real*8, dimension(Npts*par_size):: y_large,z_large,ux_large,uy_large,uz_large
    integer*2, dimension(Npts*par_size):: stype_large
    logical:: found_end,file_exists
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    version = 0
    dumi = 0
    sp_str = '  '
    write(sp_str(1:2),'(I2.2)') proc_no
    global_ntot = 0
    if (regroup_runs.gt.0) then
        n_limit = regroup_runs
    else
        n_limit = 256
    end if
    found_end = .false.
    do i=0,n_limit
        if (found_end) then
            ! do nothing
        else
            ! check for existing file
            write(sp_str(1:2),'(I2.2)') i
            INQUIRE(FILE=trim(tfile)//sp_str//'.mat'//char(0), EXIST=file_exists)
            if (file_exists) then
                ! load particles from tile
                write(*,*) proc_no,'Loading particles from processor ',i
                call restore_run_9_particles (tfile,verbose,debug,y,z,ux,uy,uz,Nz,Ny,Npts,no_species,stype,no_tot,i,count)
                ! copy the particles to the large vectors
                do k=1,no_tot
                    z_large(k + global_ntot) = z(k)
                    y_large(k + global_ntot) = y(k)
                    ux_large(k + global_ntot) = ux(k)
                    uy_large(k + global_ntot) = uy(k)
                    uz_large(k + global_ntot) = uz(k)
                    stype_large(k + global_ntot) = stype(k)
                end do
                global_ntot = global_ntot + no_tot
            else
                found_end = .true.
            end if
        end if
    end do

    write(*,*) proc_no,'Discarding irrelevant particles'
    ! keep only the particles which are within our slice
    no_tot = 0
    do i=1,global_ntot
        iyx = int(y_large(i)/dy) + 1
        izx = int(z_large(i)/dz) + 1
        if (slice_proc(izx,iyx).eq.proc_no) then
            no_tot = no_tot +1
            z(no_tot) = z_large(i)
            y(no_tot) = y_large(i)
            ux(no_tot) = ux_large(i)
            uy(no_tot) = uy_large(i)
            uz(no_tot) = uz_large(i)
            stype(no_tot) = stype_large(i)
        end if
    end do

    write(*,*) proc_no,'Loaded particles',no_tot
end subroutine

subroutine restore_run_9_particles (tfile,verbose,debug,y,z,ux,uy,uz,Nz,Ny,Npts,no_species,stype,no_tot,proc_no,count)
    USE MATIO
    implicit none
    integer:: Npts,ierr,length,version,proc_no,Nz,Ny,no_species,count,i
    real*8, dimension(Npts):: y,z,ux,uy,uz
    integer*2, dimension(Npts):: stype
    character*2048:: tfile
    logical:: verbose,debug
    integer*8:: ifile
    integer:: dumi,no_tot
    real*8:: dumr
    character*2:: sp_str
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    version = 0
    dumi = 0
    sp_str = '  '
    write(sp_str(1:2),'(I2.2)') proc_no


    if (verbose) then
        write(*,*) 'Restoring run from ',trim(tfile)//sp_str//'.mat'
    end if
    CALL FMAT_LOGINIT('particle')
    ierr = FMat_Open(trim(tfile)//sp_str//'.mat'//char(0),MAT_ACC_RDONLY,MAT)

    ! extract version
    ierr = FMat_VarReadInfo(MAT,'version',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,version)

    if (verbose) then
        write(*,*) 'Restoring from data file version',version
    end if
    if (version.lt.50) then
        write(*,*) '--------- ERROR ---------'
        write(*,*) 'This MAT file is too old'
        write(*,*) 'Restore function only works with MAT v5 files generated by matIO library'
        stop
    end if
    ! extract no_tot
    ierr = FMat_VarReadInfo(MAT,'ntot',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,no_tot)

    if (verbose) then
        write(*,*) 'Restoring ',no_tot, 'particles'
    end if

    ! extract proc rank
    ierr = FMat_VarReadInfo(MAT,'nproc',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,proc_no)

    if (verbose) then
        write(*,*) 'Processor rank: ',proc_no
    end if

    ! extract count
    ierr = FMat_VarReadInfo(MAT,'count',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,count)

    if (verbose) then
        write(*,*) 'Count: ',count
    end if

    ! extract particle vectors

    ! y vector
    ierr = FMat_VarReadInfo(MAT,'y',MATVAR)
    length  = PRODUCT(MATVAR%dims(1:MATVAR%rank))
    if (length.ne.no_tot) then
        write (*,*) 'Error, vector length does not match: y',length, no_tot
    end if
    ierr = FMat_VarReadData(MAT,MATVAR,y(1:length))
    IERR = FMAT_VARFREE(MATVAR)

    if (verbose) then
        write(*,*) 'Loaded y vector, check',sum(y(1:no_tot))
    end if

    ! z vector
    ierr = FMat_VarReadInfo(MAT,'z',MATVAR)
    length  = PRODUCT(MATVAR%dims(1:MATVAR%rank))
    if (length.ne.no_tot) then
        write (*,*) 'Error, vector length does not match: z',length, no_tot
    end if
    ierr = FMat_VarReadData(MAT,MATVAR,z(1:length))
    IERR = FMAT_VARFREE(MATVAR)

    if (verbose) then
        write(*,*) 'Loaded z vector, check',sum(z(1:no_tot))
    end if

    ! ux vector
    ierr = FMat_VarReadInfo(MAT,'ux',MATVAR)
    length  = PRODUCT(MATVAR%dims(1:MATVAR%rank))
    if (length.ne.no_tot) then
        write (*,*) 'Error, vector length does not match: ux',length, no_tot
    end if
    ierr = FMat_VarReadData(MAT,MATVAR,ux(1:length))
    IERR = FMAT_VARFREE(MATVAR)

    if (verbose) then
        write(*,*) 'Loaded ux vector, check',sum(ux(1:no_tot))
    end if

    ! uy vector
    ierr = FMat_VarReadInfo(MAT,'uy',MATVAR)
    length  = PRODUCT(MATVAR%dims(1:MATVAR%rank))
    if (length.ne.no_tot) then
        write (*,*) 'Error, vector length does not match: uy',length, no_tot
    end if
    ierr = FMat_VarReadData(MAT,MATVAR,uy(1:length))
    IERR = FMAT_VARFREE(MATVAR)

    if (verbose) then
        write(*,*) 'Loaded uy vector, check',sum(uy(1:no_tot))
    end if

    ! uz vector
    ierr = FMat_VarReadInfo(MAT,'uz',MATVAR)
    length  = PRODUCT(MATVAR%dims(1:MATVAR%rank))
    if (length.ne.no_tot) then
        write (*,*) 'Error, vector length does not match: uz',length, no_tot
    end if
    ierr = FMat_VarReadData(MAT,MATVAR,uz(1:length))
    IERR = FMAT_VARFREE(MATVAR)

    if (verbose) then
        write(*,*) 'Loaded uz vector, check',sum(uz(1:no_tot))
    end if
    ! uz vector
    ierr = FMat_VarReadInfo(MAT,'stype',MATVAR)
    length  = PRODUCT(MATVAR%dims(1:MATVAR%rank))
    if (length.ne.no_tot) then
        write (*,*) 'Error, vector length does not match: stype',length, no_tot
    end if
    ierr = FMat_VarReadData(MAT,MATVAR,stype(1:length))
    IERR = FMAT_VARFREE(MATVAR)

    if (verbose) then
        write(*,*) 'Loaded stype vector, check',sum(float(stype(1:no_tot)))
    end if





    IERR = FMAT_CLOSE(MAT)
    ! do a couple of simple checks
    do i=1,no_tot
        ! May fail for dy != 1.0
        if (y(i).lt.0.or.y(i).gt.float(Ny-1)) then
            write(*,*) 'Warning: y check failed',y(i),Ny
        end if
        ! May fail for scenario 2
        if (z(i).lt.0) then
            write(*,*) 'Error: z check failed',z(i),Nz
        end if

        if (stype(i).lt.0.or.stype(i).gt.no_species) then
            write(*,*) 'Error: stype check failed',stype(i),no_species
        end if



    end do

end subroutine


subroutine restore_run_9_histories(no_spec,snumber,Nz,Ny,Nv,z_g,y_g,rho_tot,Escz,Escy,Pot,count,t,Esct,dPHIqn,bx,by,bz,&
        tfile,debug,Nt,pot_chi,&
        Potvac,no_part_proc,proc_max,h_pos,no_diag_reg,diag_regions,diag_histories,diag_hist,fv_arrays,fv_ar,hist_limit,Np,Na,&
        fv_bin,fv_sum,fv_limits,u_perp_a,fv_perp_array_count,N0,diag_ntimes,verbose,edge_charge,flag_m,equipot_m,flag, &
        iteration_time_hist,injection_rate_hist,objects,edges,diagm,objects_enum,nobjects,objects_current,objects_power_flux,history_ntimes, &
        rho,i_rel_history,float_constant,n,Nc,Nz_max,dz)
    USE MATIO
    use spice2_types

    implicit none
    !    include "struct.h"
    real*8:: bx,by,bz,erreur2,dz
    character*2:: sp_str
    integer:: Nz,Nt,Ny,Nv,Np,proc_max,count,icr,no_spec,h_pos,Na,diag_ntimes,history_ntimes,sp,nobjects,n,no_species,use_compression,Nc,Nz_max
    ! real*8 test_z,test_y, test_ux, test_uy,test_uz
    real*8, dimension(Nz):: z_g
    real*8, dimension(Ny):: y_g
    real*8, dimension(Nz,Ny):: rho_tot,Escz,Escy,Pot,Potvac,equipot_m
    integer, dimension(Nz,Ny):: flag_m,objects,edges,diagm,objects_enum
    integer, dimension(n - Nz):: flag
    real*8, dimension(history_ntimes):: t,Esct,dPHIqn,pot_chi
    integer, dimension(no_spec,history_ntimes):: snumber
    integer, dimension(proc_max +1,history_ntimes):: no_part_proc
    integer*8:: ifile
    character*2048:: tfile
    logical:: debug,verbose
    !diag regions
    integer:: no_diag_reg, diag_histories,fv_arrays,fv_bin,fv_perp_array_count
    integer, dimension(diag_histories):: hist_limit
    real*8, dimension(diag_histories,Np - Nc):: diag_hist
    real*8, dimension(Np):: dumhist
    real*8, dimension(fv_arrays,fv_bin):: fv_ar
    real*8, dimension(fv_bin):: dumar
    type(diag_region), dimension(no_diag_reg):: diag_regions
    integer:: diag_count,hist_count,fv_count,hist_length,fv_pa_count
    integer, dimension(fv_arrays):: fv_sum
    real*8, dimension(fv_arrays,2):: fv_limits
    real*8, dimension(fv_perp_array_count,fv_bin,fv_bin):: u_perp_a
    real*8, dimension(fv_bin,fv_bin):: dumar2

    real*8:: fvn
    integer:: N0,IERR,version
    integer:: dumi
    real*8:: dumr
    real*8, dimension(Nz,Ny):: dumg
    real*8, dimension(Nz):: dumz
    real*8, dimension(Ny):: dumy
    real*8, dimension(history_ntimes):: dumh
    real*8, dimension(Nz,Ny):: edge_charge
    real*8, dimension(proc_max +1,history_ntimes):: iteration_time_hist
    real*8, dimension(no_spec,history_ntimes):: injection_rate_hist
    real*8, dimension(2,nobjects,Np):: objects_current,objects_power_flux
    real*8, dimension(no_spec,Nz_max,Ny):: rho
    real*8, dimension(nobjects,10):: i_rel_history
    real*8, dimension(nobjects):: float_constant
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR


    if (debug) then
        write(*,*) 'Restoring histories from mat file ', trim(tfile)//'.mat'
    end if
    CALL FMAT_LOGINIT('temporary')
    ierr = FMat_Open(trim(tfile)//'.mat'//char(0),MAT_ACC_RDONLY,MAT)


    ! extract version
    ierr = FMat_VarReadInfo(MAT,'version',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,version)

    if (verbose) then
        write(*,*) 'Restoring from data file version',version
    end if
    if (version.lt.50) then
        write(*,*) '--------- ERROR ---------'
        write(*,*) 'This MAT file is too old'
        write(*,*) 'Restore function only works with MAT v5 files generated by matIO library'
        stop
    end if

    ierr = FMat_VarReadInfo(MAT,'count',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,count)
    if (verbose) then
        write(*,*) 'Restored count',count
    end if
    ierr = FMat_VarReadInfo(MAT,'hpos',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,h_pos)
    if (verbose) then
        write(*,*) 'Restored h_pos',h_pos
    end if


    ! we need potvac!
    ierr = FMat_VarReadInfo(MAT,'Potvac',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,Potvac)
    if (verbose) then
        write(*,*) 'Restored Vacuum potential',sum(Potvac)
    end if
    ! we need pot!
    ierr = FMat_VarReadInfo(MAT,'Pot',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,Pot)
    if (verbose) then
        write(*,*) 'Restored potential',sum(Pot)
    end if


    ! edge charge
    ierr = FMat_VarReadInfo(MAT,'edgecharge',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,edge_charge)
    if (verbose) then
        write(*,*) 'Restored Edge charge',sum(edge_charge)
    end if


    ! histories need to be restored
    ierr = FMat_VarReadInfo(MAT,'snumber',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,snumber(:,1:h_pos))
    if (verbose) then
        write(*,*) 'Restored snumber',sum(snumber(:,1:h_pos))
    end if
    ierr = FMat_VarReadInfo(MAT,'Esct',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,Esct(1:h_pos))


    !go through them and load one after another
    hist_count = 0
    fv_count = 0
    fv_pa_count = 0
    do diag_count=1,no_diag_reg
        if ((diag_regions(diag_count)%record_property.eq.1)) then
            !potential or density
            hist_count = hist_count + 1
            hist_length = count
            if (hist_length.lt.1) then
                hist_length = 1
            end if
            write(*,*) 'Loading diag history records:',hist_length
            ierr = FMat_VarReadInfo(MAT,trim(diag_regions(diag_count)%name)//char(0),MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,dumhist(1:hist_length))

            diag_hist(hist_count,1:hist_length) = dumhist(1:hist_length)
            if (verbose)	 then
                write(*,*) 'Steady state history loaded ',trim(diag_regions(diag_count)%name),&
                    sum(diag_hist(hist_count,1:hist_length))

            end if

        else if (diag_regions(diag_count)%record_property.eq.4) then

            hist_count = hist_count + 1
            hist_length = count
            if (hist_length.lt.1) then
                hist_length = 1
            end if
            write(*,*) 'Loading diag history records:',hist_length
            ierr = FMat_VarReadInfo(MAT,trim(diag_regions(diag_count)%name)//'x'//char(0),MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,dumhist(1:hist_length))

            diag_hist(hist_count,1:hist_length) = dumhist(1:hist_length)
            if (verbose)	 then
                write(*,*) 'Steady state history loaded ',trim(diag_regions(diag_count)%name//'x'),&
                    sum(diag_hist(hist_count,1:hist_length))

            end if
            hist_count = hist_count + 1
            hist_length = count
            if (hist_length.lt.1) then
                hist_length = 1
            end if
            write(*,*) 'Loading diag history records:',hist_length
            ierr = FMat_VarReadInfo(MAT,trim(diag_regions(diag_count)%name)//'y'//char(0),MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,dumhist(1:hist_length))

            diag_hist(hist_count,1:hist_length) = dumhist(1:hist_length)
            if (verbose)	 then
                write(*,*) 'Steady state history loaded ',trim(diag_regions(diag_count)%name//'y'),&
                    sum(diag_hist(hist_count,1:hist_length))

            end if
            hist_count = hist_count + 1
            hist_length = count
            if (hist_length.lt.1) then
                hist_length = 1
            end if
            write(*,*) 'Loading diag history records:',hist_length
            ierr = FMat_VarReadInfo(MAT,trim(diag_regions(diag_count)%name)//'z'//char(0),MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,dumhist(1:hist_length))

            diag_hist(hist_count,1:hist_length) = dumhist(1:hist_length)
            if (verbose)	 then
                write(*,*) 'Steady state history loaded ',trim(diag_regions(diag_count)%name//'z'),&
                    sum(diag_hist(hist_count,1:hist_length))

            end if


        else if (diag_regions(diag_count)%record_property.eq.7) then
            if (count.gt.Nc) then
                fvn = real((count - Nc +1)*N0)
            else
                fvn = 1.0
            end if
            write(*,*) 'Loading resonance diag ',trim(diag_regions(diag_count)%name)
            ! load -  multiply back by normalisation factor
            ierr = FMat_VarReadInfo(MAT,trim(diag_regions(diag_count)%name)//char(0),MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,dumar)

            fv_ar(fv_count+1,1:fv_bin) = dumar
            fv_ar(fv_count+1,1:fv_bin) = fv_ar(fv_count+1,1:fv_bin)*fvn
            fv_count = fv_count + 1
        else if ((diag_regions(diag_count)%record_property.eq.2).or.(diag_regions(diag_count)%record_property.eq.3)) then
            !f(v)
            if (count.gt.Nc) then
                fvn = real((count - Nc +1)*N0)
            else
                fvn = 1.0
            end if
            write(*,*) 'Loading f(v) diag ',trim(diag_regions(diag_count)%name)
            ierr = FMat_VarReadInfo(MAT,trim(diag_regions(diag_count)%name)//'x1'//char(0),MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,dumar)

            fv_ar(fv_count+1,1:fv_bin) = dumar
            fv_ar(fv_count+1,1:fv_bin) = fv_ar(fv_count+1,1:fv_bin)*fvn

            ierr = FMat_VarReadInfo(MAT,trim(diag_regions(diag_count)%name)//'x2'//char(0),MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,dumar)
            fv_ar(fv_count+2,1:fv_bin) = dumar
            fv_ar(fv_count+2,1:fv_bin) = fv_ar(fv_count+2,1:fv_bin)*fvn
            ierr = FMat_VarReadInfo(MAT,trim(diag_regions(diag_count)%name)//'x3'//char(0),MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,dumar)

            fv_ar(fv_count+3,1:fv_bin) = dumar
            fv_ar(fv_count+3,1:fv_bin) = fv_ar(fv_count+3,1:fv_bin)*fvn

            fv_count = fv_count + 3
        else if  (diag_regions(diag_count)%record_property.eq.5) then
            if (count.gt.Nc) then
                fvn = real((count - Nc +1)*N0)
            else
                fvn = 1.0
            end if

            write(*,*) 'Loading f(v_par) | f(v_perp1,v_perp2) diag ',trim(diag_regions(diag_count)%name)
            ierr = FMat_VarReadInfo(MAT,trim(diag_regions(diag_count)%name)//'par'//char(0),MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,dumar)

            fv_ar(fv_count+1,1:fv_bin) = dumar
            fv_ar(fv_count+1,1:fv_bin) = fv_ar(fv_count+1,1:fv_bin)*fvn
            ierr = FMat_VarReadInfo(MAT,trim(diag_regions(diag_count)%name)//'perp'//char(0),MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,dumar2)

            u_perp_a(fv_pa_count+1,1:fv_bin,1:fv_bin) = dumar2
            u_perp_a(fv_pa_count+1,1:fv_bin,1:fv_bin) = u_perp_a(fv_pa_count+1,1:fv_bin,1:fv_bin)*fvn

            fv_count = fv_count + 1
            fv_pa_count =fv_pa_count +1
        else if  (diag_regions(diag_count)%record_property.eq.6) then
            ! test particle tracking
            write(*,*) 'Loading test particle ',trim(diag_regions(diag_count)%name)
            hist_count = hist_count + 1
            hist_length = count
            if (hist_length.lt.1) then
                hist_length = 1
            end if
            ierr = FMat_VarReadInfo(MAT,trim(diag_regions(diag_count)%name)//'y'//char(0),MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,dumhist(1:hist_length))


            diag_hist(hist_count,1:hist_length) = dumhist(1:hist_length)
            hist_count = hist_count + 1
            ierr = FMat_VarReadInfo(MAT,trim(diag_regions(diag_count)%name)//'z'//char(0),MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,dumhist(1:hist_length))

            diag_hist(hist_count,1:hist_length) = dumhist(1:hist_length)

            hist_count = hist_count + 1
            ierr = FMat_VarReadInfo(MAT,trim(diag_regions(diag_count)%name)//'ux'//char(0),MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,dumhist(1:hist_length))

            diag_hist(hist_count,1:hist_length) = dumhist(1:hist_length)

            hist_count = hist_count + 1
            ierr = FMat_VarReadInfo(MAT,trim(diag_regions(diag_count)%name)//'uy'//char(0),MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,dumhist(1:hist_length))
            diag_hist(hist_count,1:hist_length) = dumhist(1:hist_length)

            hist_count = hist_count + 1
            ierr = FMat_VarReadInfo(MAT,trim(diag_regions(diag_count)%name)//'uz'//char(0),MATVAR)
            ierr = FMat_VarReadData(MAT,MATVAR,dumhist(1:hist_length))

            diag_hist(hist_count,1:hist_length) = dumhist(1:hist_length)



        end if
    end do

    ! new potentials scenario specific
    ! call SAVEMATF(ifile,20,'flagm'//char(0),Nz,Ny,0,flag_m,flag_m,icr)
    ! s = sum(equipot_m)
    ! write(*,*) 'Sum of equipot:', s
    ! call SAVEMATF(ifile,0,'equipotm'//char(0),Nz,Ny,0,equipot_m,equipot_m,icr)
    ! call SAVEMATF(ifile,20,'flag'//char(0),n - Nz,1,0,flag,flag,icr)
    ! !iteration time duration
    ! call SAVEMATF(ifile,0,'itertime'//char(0),proc_max +1,h_pos,0,iteration_time_hist,iteration_time_hist,icr)
    ! ! injection rate hist
    ! call SAVEMATF(ifile,0,'injrate'//char(0),no_spec,h_pos,0,injection_rate_hist,injection_rate_hist,icr)
    ! call SAVEMATF(ifile,20,'objects'//char(0),Nz,Ny,0,objects,objects,icr)
    ! call SAVEMATF(ifile,20,'edges'//char(0),Nz,Ny,0,edges,edges,icr)
    ! call SAVEMATF(ifile,20,'diagm'//char(0),Nz,Ny,0,diagm,diagm,icr)
    ! call SAVEMATF(ifile,20,'objectsenum'//char(0),Nz,Ny,0,objects_enum,objects_enum,icr)
    ! call SAVEMATF(ifile,0,'objectscurrenti'//char(0),nobjects,h_pos-1,0,objects_current(1,:,1:(h_pos -1)),&
        ! objects_current(1,:,1:(h_pos -1)),icr)
    ! call SAVEMATF(ifile,0,'objectscurrente'//char(0),nobjects,h_pos-1,0,objects_current(2,:,1:(h_pos -1)),&
        ! objects_current(2,:,1:(h_pos -1)),icr)
    !
    ! ! charge densities
    ! do sp=1,no_spec
    ! 	write(sp_str(1:2),'(I2.2)') sp
    ! 	call SAVEMATF(ifile,0,'rho'//sp_str//char(0),Nz,Ny,0,rho(sp,:,:),rho(sp,:,:),icr)
    ! !	write(*,*) 'Saving rho:',sp,sum(rho(sp,:,:))
    !
    ! end do
    !
    !
    ! 		ierr = FMat_VarReadInfo(MAT,'flagm',MATVAR)
    ! 		ierr = FMat_VarReadData(MAT,MATVAR,flag_m)
    ! 		write(*,*) 'Loaded flagm'
    !
    ! 		ierr = FMat_VarReadInfo(MAT,'equipotm',MATVAR)
    ! 		ierr = FMat_VarReadData(MAT,MATVAR,equipot_m)
    ! 		write(*,*) 'Loaded equipotm'
    !
    ! 		ierr = FMat_VarReadInfo(MAT,'flag',MATVAR)
    ! 		ierr = FMat_VarReadData(MAT,MATVAR,flag)
    ! 		write(*,*) 'Loaded flag'


    if (dz.ge.0.1) then

        ierr = FMat_VarReadInfo(MAT,'itertime',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,iteration_time_hist)
        write(*,*) 'Loaded itertime'

        ierr = FMat_VarReadInfo(MAT,'Esct',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,Esct(1:h_pos))

        if (verbose) then
            write(*,*) 'Restored Esct',sum(Esct(1:h_pos))
        end if

        ierr = FMat_VarReadInfo(MAT,'dPHIqn',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,dPHIqn(1:h_pos))
        if (verbose) then
            write(*,*) 'Restored dPHIqn',sum(dPHIqn(1:h_pos))
        end if

        ierr = FMat_VarReadInfo(MAT,'pchi',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,pot_chi(1:h_pos))
        if (verbose) then
            write(*,*) 'Restored pot_chi',sum(pot_chi(1:h_pos))
        end if
        ierr = FMat_VarReadInfo(MAT,'npartproc',MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,no_part_proc(:,1:h_pos))

        if (verbose) then
            write(*,*) 'Restored no_part_proc',sum(no_part_proc(:,1:h_pos))
        end if
    end if

    ierr = FMat_VarReadInfo(MAT,'injrate',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,injection_rate_hist)
    write(*,*) 'Loaded injrate'

    ! 		ierr = FMat_VarReadInfo(MAT,'objects',MATVAR)
    ! 		ierr = FMat_VarReadData(MAT,MATVAR,objects)
    ! 		write(*,*) 'Loaded objects'
    !
    ! 		ierr = FMat_VarReadInfo(MAT,'edges',MATVAR)
    ! 		ierr = FMat_VarReadData(MAT,MATVAR,edges)
    ! 		write(*,*) 'Loaded edges'
    !
    ! 		ierr = FMat_VarReadInfo(MAT,'diagm',MATVAR)
    ! 		ierr = FMat_VarReadData(MAT,MATVAR,diagm)
    ! 		write(*,*) 'Loaded diagm'

    ierr = FMat_VarReadInfo(MAT,'objectscurrenti',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,objects_current(1,:,1:(count -1)))
    write(*,*) 'Loaded objectscurrenti'

    ierr = FMat_VarReadInfo(MAT,'objectscurrente',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,objects_current(2,:,1:(count -1)))
    write(*,*) 'Loaded objectscurrente'
    ierr = FMat_VarReadInfo(MAT,'objectspowerfluxi',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,objects_power_flux(1,:,1:(count -1)))

    write(*,*) 'Loaded objectspowerfluxi'

    ierr = FMat_VarReadInfo(MAT,'objectspowerfluxe',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,objects_power_flux(2,:,1:(count -1)))
    write(*,*) 'Loaded objectspowerfluxe'



    ierr = FMat_VarReadInfo(MAT,'irel',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,i_rel_history)
    write(*,*) 'Loaded irel'

    ierr = FMat_VarReadInfo(MAT,'floatconstant',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,float_constant)
    write(*,*) 'Loaded floatconstant'

    ierr = FMat_VarReadInfo(MAT,'histlimits',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,hist_limit)
    write(*,*) 'Loaded hist_limit'


    ! do sp=1,no_spec
    ! 	write(sp_str(1:2),'(I2.2)') sp
    ! ! 	call SAVEMATF(ifile,0,'rho'//sp_str//char(0),Nz,Ny,0,rho(sp,:,:),rho(sp,:,:),icr)
    ! 		ierr = FMat_VarReadInfo(MAT,'rho'//sp_str//char(0),MATVAR)
    ! 		ierr = FMat_VarReadData(MAT,MATVAR,rho(sp,:,:))
    ! 		write(*,*) 'Loaded rho', sp
    !
    !   !	write(*,*) 'Saving rho:',sp,sum(rho(sp,:,:))
    !
    ! end do


    IERR = FMAT_CLOSE(MAT)


end subroutine


subroutine restore_9_avdiag(dt,Nz,Ny,dens,Epar,Eperp,Potav,vzav,vyav,vxav,vz2av,vy2av,vx2av,vz3av,vy3av,&
        vx3av,ofile,debug,no_spec,nav,verbose,edge_flux,edge_energy_flux)
    USE MATIO
    implicit none
    integer:: N_e,N_i,Nz,Nv,Np,N0,Ny,no_spec,sp,Nz1,IERR,version

    real*8:: dt,dz
    real*8, dimension(Nz,Ny):: Potav,Epar,Eperp
    real*8, dimension(no_spec,Nz,Ny):: q_grid,T_g,vzav,vyav,vxav,vz2av,vy2av,vx2av,vz3av,vy3av,vx3av,dens
    real*8,dimension(Nz,Ny,no_spec)::edge_flux,edge_energy_flux
    real*8, dimension(Nz):: z_g
    real*8, dimension(Ny):: y_g

    !real*8, dimension(Np):: t
    real*8, dimension(no_spec):: Iinj
    logical*1:: time_diag
    integer*8 ifile
    character*2048 ofile
    character*2:: sp_str
    real*8:: ang_bin
    integer:: abin,nav
    integer:: dumi
    real*8:: dumr
    real*8, dimension(Nz,Ny):: dumg
    real*8, dimension(Nz):: dumz
    real*8, dimension(Ny):: dumy
    logical:: verbose,debug
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR

    !abin = int(180.0/ang_bin) +1
    Nz1  =1
    write (6,*) 'Loading averaged diagnostics',trim(ofile)//'.mat'

    CALL FMAT_LOGINIT('averaged')
    ierr = FMat_Open(trim(ofile)//'.mat'//char(0),MAT_ACC_RDONLY,MAT)


    ! extract version
    ierr = FMat_VarReadInfo(MAT,'version',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,version)

    if (verbose) then
        write(*,*) 'Restoring from data file version',version
    end if
    if (version.lt.50) then
        write(*,*) '--------- ERROR ---------'
        write(*,*) 'This MAT file is too old'
        write(*,*) 'Restore function only works with MAT v5 files generated by matIO library'
        stop
    end if

    !first important value
    ierr = FMat_VarReadInfo(MAT,'nav',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,nav)
    if (verbose) then
        write(*,*) 'Restored nav',nav
    end if

    ! averaged fields and potential
    ierr = FMat_VarReadInfo(MAT,'Epar',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,Epar)
    if (verbose) then
        write(*,*) 'Restored Epar',sum(Epar)
    end if

    ierr = FMat_VarReadInfo(MAT,'Eperp',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,Eperp)
    if (verbose) then
        write(*,*) 'Restored Eperp',sum(Eperp)
    end if

    ierr = FMat_VarReadInfo(MAT,'Potav',MATVAR)
    ierr = FMat_VarReadData(MAT,MATVAR,Potav)
    if (verbose) then
        write(*,*) 'Restored Potav',sum(Potav)
    end if

    !now specie - specific diags
    !and finally we go specie by specie and write it's diag into the file
    do sp=1,no_spec
        ! sp_str is sp but it is a char
        write(*,*) 'Restoring diagnostics for specie',sp
        sp_str = '  '
        write(sp_str(1:2),'(I2.2)') sp
        ierr = FMat_VarReadInfo(MAT,'dens'//sp_str//char(0),MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,dumg)

        dens(sp,:,:) = dumg
        if (verbose) then
            write(*,*) 'Restored dens',sum(dens(sp,:,:))
        end if

        !           ierr = FMat_VarReadInfo(MAT,'q'//sp_str//char(0),MATVAR)
        ! 	 ierr = FMat_VarReadData(MAT,MATVAR,dumg)
        ! q_grid(sp,:,:) = dumg
        ! if (verbose) then
        ! 	write(*,*) 'Restored q',sum(q_grid(sp,:,:))
        ! end if
        !           ierr = FMat_VarReadInfo(MAT,'T'//sp_str//char(0),MATVAR)
        ! 	 ierr = FMat_VarReadData(MAT,MATVAR,dumg)
        !
        ! T_g(sp,:,:) = dumg
        ! if (verbose) then
        ! 	write(*,*) 'Restored T',sum(T_g(sp,:,:))
        ! end if

        ierr = FMat_VarReadInfo(MAT,'vzav'//sp_str//char(0),MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,dumg)
        vzav(sp,:,:) = dumg
        if (verbose) then
            write(*,*) 'Restored vzav',sum(vzav(sp,:,:))
        end if

        ierr = FMat_VarReadInfo(MAT,'vyav'//sp_str//char(0),MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,dumg)
        vyav(sp,:,:) = dumg
        if (verbose) then
            write(*,*) 'Restored vyav',sum(vyav(sp,:,:))
        end if

        ierr = FMat_VarReadInfo(MAT,'vxav'//sp_str//char(0),MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,dumg)
        vxav(sp,:,:) = dumg
        if (verbose) then
            write(*,*) 'Restored vxav',sum(vxav(sp,:,:))
        end if

        ierr = FMat_VarReadInfo(MAT,'vz2av'//sp_str//char(0),MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,dumg)
        vz2av(sp,:,:) = dumg
        if (verbose) then
            write(*,*) 'Restored vz2av',sum(vz2av(sp,:,:))
        end if

        ierr = FMat_VarReadInfo(MAT,'vz3av'//sp_str//char(0),MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,dumg)
        vz3av(sp,:,:) = dumg
        if (verbose) then
            write(*,*) 'Restored vz3av',sum(vz3av(sp,:,:))
        end if
        ierr = FMat_VarReadInfo(MAT,'vy2av'//sp_str//char(0),MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,dumg)
        vy2av(sp,:,:) = dumg
        if (verbose) then
            write(*,*) 'Restored vy2av',sum(vy2av(sp,:,:))
        end if

        ierr = FMat_VarReadInfo(MAT,'vy3av'//sp_str//char(0),MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,dumg)
        vy3av(sp,:,:) = dumg
        if (verbose) then
            write(*,*) 'Restored vy3av',sum(vy3av(sp,:,:))
        end if

        ierr = FMat_VarReadInfo(MAT,'vx2av'//sp_str//char(0),MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,dumg)
        vx2av(sp,:,:) = dumg
        if (verbose) then
            write(*,*) 'Restored vx2av',sum(vx2av(sp,:,:))
        end if

        ierr = FMat_VarReadInfo(MAT,'vx3av'//sp_str//char(0),MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,dumg)
        vx3av(sp,:,:) = dumg
        if (verbose) then
            write(*,*) 'Restored vx3av',sum(vx3av(sp,:,:))
        end if

        ierr = FMat_VarReadInfo(MAT,'edgeflux'//sp_str//char(0),MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,dumg)
        edge_flux(:,:,sp) = dumg
        if (verbose) then
            write(*,*) 'Restored edge flux',sum(edge_flux(:,:,sp))
        end if

        ierr = FMat_VarReadInfo(MAT,'edgeenergyflux'//sp_str//char(0),MATVAR)
        ierr = FMat_VarReadData(MAT,MATVAR,dumg)
        edge_energy_flux(:,:,sp) = dumg
        if (verbose) then
            write(*,*) 'Restored edge energy flux',sum(edge_energy_flux(:,:,sp))
        end if


    end do


    IERR = FMAT_CLOSE(MAT)
end subroutine



!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine SAVE_RESULTS2(dt,N0,Nv,Nz,Ny,z_g,y_g,dens,Epar,Eperp,Potav,q_grid,T_g,vzav,vyav,vxav,vz2av,vy2av,vx2av,vz3av,&
        vy3av,vx3av,Np,t,dz,Iinj,ofile,debug,no_spec,nav,edge_flux,edge_energy_flux,edge_velocity_x,edge_velocity_y,edge_velocity_z,use_compression,domain_decomp,proc_max,temperature)
    USE MATIO

    implicit none

    real*8:: dt,dz

    real*8, dimension(Nz):: z_g
    real*8, dimension(Ny):: y_g

    real*8, dimension(no_spec):: Iinj
    integer:: N_e,N_i,Nz,Nv,Np,N0,Ny,no_spec,sp,icr,version,IERR,use_compression,proc_max,specie_count
    logical:: time_diag,debug
    real*8, dimension(Nz,Ny):: Potav,Epar,Eperp
    real*8, dimension(no_spec,Nz,Ny):: vzav, vyav,vxav,vz2av,vy2av,vx2av,vz3av,vy3av,vx3av,dens,q_grid, T_g,temperature
    !real*8, dimension(no_spec,Nz,Ny):: vzavx, vyavx,vxavx,vz2avx,vy2avx,vx2avx,vz3avx,vy3avx,vx3avx,densx,q_gridx, T_gx,temperature_x

    real*8, dimension(Np):: t
    real*8, dimension(Nz,Ny,no_spec)::edge_flux,edge_energy_flux,edge_velocity_x,edge_velocity_y,edge_velocity_z
    !   real*8, dimension(Nz,Ny,no_spec)::edge_fluxx,edge_energy_fluxx,edge_velocity_xx,edge_velocity_yx,edge_velocity_zx

    logical:: domain_decomp,specie_recomp
    !dimension Iinji(Np)
    !dimension Iinje(Np)
    integer*8:: ifile
    character*2048:: ofile
    character*2:: sp_str
    real*8:: ang_bin
    integer:: abin,nav
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    !write(*,*) 'Temperatue csum',sum(temperature(1,:,:)),sum(temperature(2,:,:))

    !if (monoi) then
    !   filename='shRDmonoi_tile.mat'//char(0)
    !else
    !   filename='shRDarbitr_tile.mat'//char(0)
    !endif
    ifile = 0
    icr = 0
    version = 50
    !     densx = 0.0
    !     vxavx = 0.0
    !     vyavx = 0.0
    !     vzavx = 0.0
    !     vx2avx = 0.0
    !     vy2avx = 0.0
    !     vz2avx = 0.0
    !     vx3avx = 0.0
    !     vy3avx = 0.0
    !     vz3avx = 0.0
    !     T_gx = 0.0
    !     q_gridx = 0.0
    !     edge_fluxx = 0.0
    !     edge_energy_fluxx = 0.0
    !     edge_velocity_xx = 0.0
    !     edge_velocity_yx = 0.0
    !     edge_velocity_zx = 0.0

    specie_recomp = .false.
    write (6,*) 'Writing results to ',trim(ofile)//'.mat'

    CALL FMAT_LOGINIT('averaged')
    ! OPEN FILE FOR WRITING
    IERR = FMAT_CREATE(trim(ofile)//'.mat'//char(0), MAT)

    !MK changed order of saving to allow restore
    ! first goes the file version
    IERR = FMAT_VARCREATE('version', version, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, version,use_compression)
    IERR = FMAT_VARFREE(MATVAR)
    ! then the number of species
    IERR = FMAT_VARCREATE('nspec', no_spec, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, no_spec,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    !then dimensions of the grid

    IERR = FMAT_VARCREATE('Nz', Nz, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Nz)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Ny', Ny, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Ny,use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    ! number of steps taken
    IERR = FMAT_VARCREATE('Np', Np, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Np,use_compression)
    IERR = FMAT_VARFREE(MATVAR)




    ! number of samples to diag taken
    IERR = FMAT_VARCREATE('nav', nav, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, nav,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    !then some useful 1D parameters

    IERR = FMAT_VARCREATE('dt', dt, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, dt,use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    IERR = FMAT_VARCREATE('dz', dz, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, dz,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    !angular

    IERR = FMAT_VARCREATE('angbin', ang_bin, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, ang_bin,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    !now the grid coordinates

    IERR = FMAT_VARCREATE('z', z_g, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, z_g,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('y', y_g, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, y_g,use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    ! general grid diag

    IERR = FMAT_VARCREATE('Epar', Epar, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Epar,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Eperp', Eperp, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Eperp,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Potav', Potav, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Potav,use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    !and finally we go specie by specie and write it's diag into the file
    specie_count = 0
    do sp=1,no_spec
        ! sp_str is sp but it is a char
        sp_str = '  '
        write(sp_str(1:2),'(I2.2)') sp
        !         if (specie_recomp) then
        !             densx(specie_count*(proc_max+1) + proc_max + 1,:,:) = densx(specie_count*(proc_max+1) + proc_max + 1,:,:) + dens(sp,:,:)
        !             q_gridx(specie_count*(proc_max+1) + proc_max + 1,:,:) = q_gridx(specie_count*(proc_max+1) + proc_max + 1,:,:) + q_grid(sp,:,:)
        !             T_gx(specie_count*(proc_max+1) + proc_max + 1,:,:) = T_gx(specie_count*(proc_max+1) + proc_max + 1,:,:) + T_g(sp,:,:)
        !             vzavx(specie_count*(proc_max+1) + proc_max + 1,:,:) = vzavx(specie_count*(proc_max+1) + proc_max + 1,:,:) + vzav(sp,:,:)
        !             vyavx(specie_count*(proc_max+1) + proc_max + 1,:,:) = vyavx(specie_count*(proc_max+1) + proc_max + 1,:,:) + vyav(sp,:,:)
        !             vxavx(specie_count*(proc_max+1) + proc_max + 1,:,:) = vxavx(specie_count*(proc_max+1) + proc_max + 1,:,:) + vxav(sp,:,:)
        !             vz2avx(specie_count*(proc_max+1) + proc_max + 1,:,:) = vz2avx(specie_count*(proc_max+1) + proc_max + 1,:,:) + vz2av(sp,:,:)
        !             vy2avx(specie_count*(proc_max+1) + proc_max + 1,:,:) = vy2avx(specie_count*(proc_max+1) + proc_max + 1,:,:) + vy2av(sp,:,:)
        !             vx2avx(specie_count*(proc_max+1) + proc_max + 1,:,:) = vx2avx(specie_count*(proc_max+1) + proc_max + 1,:,:) + vx2av(sp,:,:)
        !             vz3avx(specie_count*(proc_max+1) + proc_max + 1,:,:) = vz3avx(specie_count*(proc_max+1) + proc_max + 1,:,:) + vz3av(sp,:,:)
        !             vy3avx(specie_count*(proc_max+1) + proc_max + 1,:,:) = vy3avx(specie_count*(proc_max+1) + proc_max + 1,:,:) + vy3av(sp,:,:)
        !             vx3avx(specie_count*(proc_max+1) + proc_max + 1,:,:) = vx3avx(specie_count*(proc_max+1) + proc_max + 1,:,:) + vx3av(sp,:,:)
        !             edge_fluxx(:,:,specie_count*(proc_max+1) + proc_max + 1) = edge_fluxx(:,:,specie_count*(proc_max+1) + proc_max + 1) + edge_flux(:,:,sp)
        !             edge_energy_fluxx(:,:,specie_count*(proc_max+1) + proc_max + 1) = edge_energy_fluxx(:,:,specie_count*(proc_max+1) + proc_max + 1) + edge_energy_flux(:,:,sp)
        !             edge_velocity_xx(:,:,specie_count*(proc_max+1) + proc_max + 1) = edge_velocity_xx(:,:,specie_count*(proc_max+1) + proc_max + 1) + edge_velocity_x(:,:,sp)
        !             edge_velocity_yx(:,:,specie_count*(proc_max+1) + proc_max + 1) = edge_velocity_yx(:,:,specie_count*(proc_max+1) + proc_max + 1) + edge_velocity_y(:,:,sp)
        !             edge_velocity_zx(:,:,specie_count*(proc_max+1) + proc_max + 1) = edge_velocity_zx(:,:,specie_count*(proc_max+1) + proc_max + 1) + edge_velocity_z(:,:,sp)
        ! temperature_x(specie_count*(proc_max+1) + proc_max + 1,:,:) = temperature_x(specie_count*(proc_max+1) + proc_max + 1,:,:) + temperature(sp,:,:)
        !
        !
        !             if (sp.eq.int(real(sp)/real(proc_max+1))*(proc_max+1)) then
        !                 specie_count = specie_count + 1
        !                 sp_str = '  '
        !                 write(sp_str(1:2),'(I2.2)') specie_count
        !
        !                 IERR = FMAT_VARCREATE('dens'//sp_str//char(0), densx(specie_count*(proc_max+1),:,:), MATVAR)
        !                 IERR = FMAT_VARWRITE(MAT, MATVAR, densx(specie_count*(proc_max+1),:,:),use_compression)
        !                 IERR = FMAT_VARFREE(MATVAR)
        !
        !                 IERR = FMAT_VARCREATE('q'//sp_str//char(0), q_gridx(specie_count*(proc_max+1),:,:), MATVAR)
        !                 IERR = FMAT_VARWRITE(MAT, MATVAR, q_gridx(specie_count*(proc_max+1),:,:),use_compression)
        !                 IERR = FMAT_VARFREE(MATVAR)
        !
        !                 IERR = FMAT_VARCREATE('T'//sp_str//char(0), T_gx(specie_count*(proc_max+1),:,:), MATVAR)
        !                 IERR = FMAT_VARWRITE(MAT, MATVAR, T_gx(specie_count*(proc_max+1),:,:),use_compression)
        !                 IERR = FMAT_VARFREE(MATVAR)
        !
        !                 IERR = FMAT_VARCREATE('vzav'//sp_str//char(0), vzavx(specie_count*(proc_max+1),:,:), MATVAR)
        !                 IERR = FMAT_VARWRITE(MAT, MATVAR, vzavx(specie_count*(proc_max+1),:,:),use_compression)
        !                 IERR = FMAT_VARFREE(MATVAR)
        !
        !                 IERR = FMAT_VARCREATE('vyav'//sp_str//char(0), vyavx(specie_count*(proc_max+1),:,:), MATVAR)
        !                 IERR = FMAT_VARWRITE(MAT, MATVAR, vyavx(specie_count*(proc_max+1),:,:),use_compression)
        !                 IERR = FMAT_VARFREE(MATVAR)
        !
        !                 IERR = FMAT_VARCREATE('vxav'//sp_str//char(0), vxavx(specie_count*(proc_max+1),:,:), MATVAR)
        !                 IERR = FMAT_VARWRITE(MAT, MATVAR, vxavx(specie_count*(proc_max+1),:,:),use_compression)
        !                 IERR = FMAT_VARFREE(MATVAR)
        !
        !                 IERR = FMAT_VARCREATE('vz2av'//sp_str//char(0), vz2avx(specie_count*(proc_max+1),:,:), MATVAR)
        !                 IERR = FMAT_VARWRITE(MAT, MATVAR, vz2avx(specie_count*(proc_max+1),:,:),use_compression)
        !                 IERR = FMAT_VARFREE(MATVAR)
        !
        !                 IERR = FMAT_VARCREATE('vy2av'//sp_str//char(0), vy2avx(specie_count*(proc_max+1),:,:), MATVAR)
        !     IERR = FMAT_VARWRITE(MAT, MATVAR, vy2avx(specie_count*(proc_max+1),:,:),use_compression)
        !                 IERR = FMAT_VARFREE(MATVAR)
        !
        !                 IERR = FMAT_VARCREATE('vx2av'//sp_str//char(0), vx2avx(specie_count*(proc_max+1),:,:), MATVAR)
        !                 IERR = FMAT_VARWRITE(MAT, MATVAR, vx2avx(specie_count*(proc_max+1),:,:),use_compression)
        !                 IERR = FMAT_VARFREE(MATVAR)
        !
        !                 IERR = FMAT_VARCREATE('vz3av'//sp_str//char(0), vz3avx(specie_count*(proc_max+1),:,:), MATVAR)
        !                 IERR = FMAT_VARWRITE(MAT, MATVAR, vz3avx(specie_count*(proc_max+1),:,:),use_compression)
        !                 IERR = FMAT_VARFREE(MATVAR)
        !
        !                 IERR = FMAT_VARCREATE('vy3av'//sp_str//char(0), vy3avx(specie_count*(proc_max+1),:,:), MATVAR)
        !                 IERR = FMAT_VARWRITE(MAT, MATVAR, vy3avx(specie_count*(proc_max+1),:,:),use_compression)
        !                 IERR = FMAT_VARFREE(MATVAR)
        !
        !                 IERR = FMAT_VARCREATE('vx3av'//sp_str//char(0), vx3avx(specie_count*(proc_max+1),:,:), MATVAR)
        !                 IERR = FMAT_VARWRITE(MAT, MATVAR, vx3avx(specie_count*(proc_max+1),:,:),use_compression)
        !                 IERR = FMAT_VARFREE(MATVAR)
        !
        !     IERR = FMAT_VARCREATE('temperature'//sp_str//char(0), temperature_x(specie_count*(proc_max+1),:,:), MATVAR)
        !     IERR = FMAT_VARWRITE(MAT, MATVAR, temperature_x(specie_count*(proc_max+1),:,:),use_compression)
        !     IERR = FMAT_VARFREE(MATVAR)
        !
        !
        !                 IERR = FMAT_VARCREATE('edgeflux'//sp_str//char(0), edge_fluxx(:,:,specie_count*(proc_max+1)), MATVAR)
        !                 IERR = FMAT_VARWRITE(MAT, MATVAR, edge_fluxx(:,:,specie_count*(proc_max+1)),use_compression)
        !                 IERR = FMAT_VARFREE(MATVAR)
        !
        !                 IERR = FMAT_VARCREATE('edgeenergyflux'//sp_str//char(0), edge_energy_fluxx(:,:,specie_count*(proc_max+1)), MATVAR)
        !                 IERR = FMAT_VARWRITE(MAT, MATVAR, edge_energy_fluxx(:,:,specie_count*(proc_max+1)),use_compression)
        !                 IERR = FMAT_VARFREE(MATVAR)
        !
        !                 IERR = FMAT_VARCREATE('edgevelocityx'//sp_str//char(0), edge_velocity_xx(:,:,specie_count*(proc_max+1)), MATVAR)
        !                 IERR = FMAT_VARWRITE(MAT, MATVAR, edge_velocity_xx(:,:,specie_count*(proc_max+1)),use_compression)
        !                 IERR = FMAT_VARFREE(MATVAR)
        !
        !                 IERR = FMAT_VARCREATE('edgevelocityy'//sp_str//char(0), edge_velocity_yx(:,:,specie_count*(proc_max+1)), MATVAR)
        !                 IERR = FMAT_VARWRITE(MAT, MATVAR, edge_velocity_yx(:,:,specie_count*(proc_max+1)),use_compression)
        !                 IERR = FMAT_VARFREE(MATVAR)
        !
        !                 IERR = FMAT_VARCREATE('edgevelocityz'//sp_str//char(0), edge_velocity_zx(:,:,specie_count*(proc_max+1)), MATVAR)
        !                 IERR = FMAT_VARWRITE(MAT, MATVAR, edge_velocity_zx(:,:,specie_count*(proc_max+1)),use_compression)
        !                 IERR = FMAT_VARFREE(MATVAR)
        !
        !
        !             else
        !
        !
        !
        !             end if
        !         else

        IERR = FMAT_VARCREATE('dens'//sp_str//char(0), dens(sp,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, dens(sp,:,:),use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('q'//sp_str//char(0), q_grid(sp,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, q_grid(sp,:,:),use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('T'//sp_str//char(0), T_g(sp,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, T_g(sp,:,:),use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('vzav'//sp_str//char(0), vzav(sp,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, vzav(sp,:,:),use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('vyav'//sp_str//char(0), vyav(sp,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, vyav(sp,:,:),use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('vxav'//sp_str//char(0), vxav(sp,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, vxav(sp,:,:),use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('vz2av'//sp_str//char(0), vz2av(sp,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, vz2av(sp,:,:),use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('vy2av'//sp_str//char(0), vy2av(sp,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, vy2av(sp,:,:),use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('vx2av'//sp_str//char(0), vx2av(sp,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, vx2av(sp,:,:),use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('vz3av'//sp_str//char(0), vz3av(sp,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, vz3av(sp,:,:),use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('vy3av'//sp_str//char(0), vy3av(sp,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, vy3av(sp,:,:),use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('vx3av'//sp_str//char(0), vx3av(sp,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, vx3av(sp,:,:),use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('temperature'//sp_str//char(0), temperature(sp,:,:), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR,temperature(sp,:,:),use_compression)
        IERR = FMAT_VARFREE(MATVAR)


        IERR = FMAT_VARCREATE('edgeflux'//sp_str//char(0), edge_flux(:,:,sp), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, edge_flux(:,:,sp),use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('edgeenergyflux'//sp_str//char(0), edge_energy_flux(:,:,sp), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, edge_energy_flux(:,:,sp),use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('edgevelocityx'//sp_str//char(0), edge_velocity_x(:,:,sp), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, edge_velocity_x(:,:,sp),use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('edgevelocityy'//sp_str//char(0), edge_velocity_y(:,:,sp), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, edge_velocity_y(:,:,sp),use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('edgevelocityz'//sp_str//char(0), edge_velocity_z(:,:,sp), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, edge_velocity_z(:,:,sp),use_compression)
        IERR = FMAT_VARFREE(MATVAR)
        !     end if

    end do
    IERR = FMAT_VARCREATE('N0'//char(0), N0, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, N0,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_CLOSE(MAT)
    domain_decomp = .true.
    return
END

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




subroutine LANG_MONITOR(no_spec,snumber,Nz,Ny,Nv,z_g,y_g,rho_tot,Escz,Escy,Pot,count,t,Esct,dPHIqn,bx,by,bz,tfile,debug,Nt,&
        pot_chi,Potvac,no_part_proc,proc_max,h_pos,no_diag_reg,diag_regions,diag_histories,diag_hist,fv_arrays,fv_ar,hist_limit,Np,Na,&
        fv_bin,fv_sum,fv_limits,u_perp_a,fv_perp_array_count,N0,flag_m,equipot_m,n,flag,iteration_time_hist,injection_rate_hist,objects,&
        edges,edge_charge,rho,diagm,nobjects,objects_enum,objects_current,objects_power_flux,ksi,tau,SOL_W,SOL_Ns,use_compression,&
        i_rel_history,float_constant,Nc,mks_info,alpha_yz,alpha_xz,mu,dz,dt,m,q,Temp,time_history,Nz_max,slice_proc,domain_decomp,&
        surface_matrix,delta_h,Npc,total_energy,have_impact_diag,impact_diag,emmit_diag,orientation)
    USE MATIO
    use spice2_types

    implicit none

    !include "struct.h"
    type(mks_t):: mks_info
    integer:: Nz,Nt,Ny,Nv,Np,proc_max,count,icr,no_spec,h_pos,Na,nobjects,IERR,version,use_compression,Nc,Nz_max,specie_count
    real*8, dimension(Nz,Ny)::Escz,Escy,Pot,Potvac,rho_tot,surface_matrix

    real*8, dimension(nobjects,10):: i_rel_history
    real*8, dimension(nobjects):: float_constant
    real*8, dimension(Nz):: z_g
    real*8, dimension(Ny):: y_g
    real*8, dimension(h_pos):: t,Esct,dPHIqn,pot_chi
    real*8:: bx,by,bz,erreur2,s,alpha_yz,alpha_xz,dz,dt
    integer:: sp,delta_h,Npc
    integer, dimension(no_spec,h_pos):: snumber
    integer, dimension(proc_max +1,h_pos):: no_part_proc
    integer*8:: ifile
    character*2048:: tfile
    logical:: debug,domain_decomp,specie_recomp,have_impact_diag
    !diag regions
    integer:: no_diag_reg, diag_histories,fv_arrays,fv_bin,fv_perp_array_count
    integer, dimension(diag_histories):: hist_limit
    real*8, dimension(diag_histories,count):: diag_hist
    real*8, dimension(fv_arrays,fv_bin):: fv_ar
    type(diag_region), dimension(no_diag_reg):: diag_regions
    integer:: diag_count,hist_count,fv_count,hist_length,fv_pa_count
    integer, dimension(fv_arrays):: fv_sum
    real*8, dimension(fv_arrays,2):: fv_limits
    real*8, dimension(fv_perp_array_count,fv_bin,fv_bin):: u_perp_a
    real*8:: fvn,ksi,tau,mu,minimal
    integer:: N0
    integer, dimension(Nz,Ny):: flag_m,objects, edges,objects_enum,orientation
    real*8, dimension(Nz,Ny):: equipot_m,edge_charge
    integer:: n
    integer, dimension(n - Nz):: flag
    real*8, dimension(proc_max +1,h_pos):: iteration_time_hist
    real*8, dimension(no_spec,h_pos):: injection_rate_hist
    real*8, dimension(no_spec,Nz,Ny):: SOL_Ns,SOL_W
    real*8, dimension(no_spec,Nz_max,Ny):: rho

    real*8, dimension(no_spec,Nz,Ny):: SOL_Nsx,SOL_Wx
    real*8, dimension(no_spec,Nz_max,Ny):: rhox

    character*2:: sp_str
    integer, dimension(Nz,Ny):: diagm
    real*8, dimension(2,nobjects,Np):: objects_current,objects_power_flux
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    real*8, dimension(no_spec):: m,q,Temp
    real*8, dimension(12,count):: time_history
    integer, dimension(Nz_max,Ny):: slice_proc
    real*8, dimension(no_spec,4,Np):: total_energy
    real*8, dimension(no_spec,nobjects,fv_bin,fv_bin):: impact_diag,emmit_diag

    sp_str = '  '
    icr  = 0
    ifile = 0
    version = 50
    specie_recomp = .false.


    ! domain_decomp = .false.
    !RD le 13/01/05   test avec chantal pour lire un fichier .mat
    !integer type, nlig, ncol, imag
    !character*40 name
    !real zz(Nz)
    ! if (debug) then
    write(*,*) 'Entering Lang. Monitor, writting to file ', trim(tfile)//'.mat'
    !end if
    !debug number writeout
    ! write(*,*) 'no_species',no_spec,count,Nt,proc_max
    ! write(*,*) 'Number1',snumber(1,:)
    ! write(*,*) 'Number2',snumber(2,:)

    !filename='shRDt_tile.mat'//char(0)
    CALL FMAT_LOGINIT('temporary')
    if (debug) then
        write(*,*) 'FMAT_LOGINIT'
    end if

    ! OPEN FILE FOR WRITING
    IERR = FMAT_CREATE(trim(tfile)//'.mat'//char(0), MAT)

    if (debug) then
        write(*,*) 'FMAT_CREATE ', IERR
    end if

    !MK changed order of saving to allow restore
    ! first goes the file version
    IERR = FMAT_VARCREATE('version', version, MATVAR)
    if (debug) then
        write(*,*) 'FMAT_VARCREATE ', IERR
    end if

    IERR = FMAT_VARWRITE(MAT, MATVAR, version)
    if (debug) then
        write(*,*) 'FMAT_VARWRITE ', IERR
    end if

    IERR = FMAT_VARFREE(MATVAR)
    if (debug) then
        write(*,*) 'FMAT_VARFREE ', IERR
    end if

    !necessary dimensions
    IERR = FMAT_VARCREATE('Nz', Nz, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Nz,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Nzmax', Nz_max, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Nz_max,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Ny', Ny, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Ny,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('count', count, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, count,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('hpos', h_pos, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, h_pos,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('deltah', delta_h, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, delta_h,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Npc', Npc, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Npc,use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    IERR = FMAT_VARCREATE('dt', dt, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, dt,use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    IERR = FMAT_VARCREATE('dz', dz, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, dz,use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    !number of processors
    IERR = FMAT_VARCREATE('nproc', proc_max +1, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, proc_max +1,use_compression)
    IERR = FMAT_VARFREE(MATVAR)
    ! some particle properties
    IERR = FMAT_VARCREATE('q', q, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,q,use_compression)
    IERR = FMAT_VARFREE(MATVAR)
    ! some particle properties
    IERR = FMAT_VARCREATE('m', m, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,m,use_compression)
    IERR = FMAT_VARFREE(MATVAR)
    ! some particle properties
    IERR = FMAT_VARCREATE('Temp', Temp, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,Temp,use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    IERR = FMAT_VARCREATE('zg', z_g, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, z_g,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('yg', y_g, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, y_g,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('rho', rho_tot, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, rho_tot,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Escz', Escz, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Escz,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Escy', Escy, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Escy,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('surfacematrix', surface_matrix, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, surface_matrix,use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    IERR = FMAT_VARCREATE('Pot', Pot, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Pot,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Potvac', Potvac, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Potvac,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('sliceproc', slice_proc, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, slice_proc,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('orientation', orientation, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, orientation,use_compression)
    IERR = FMAT_VARFREE(MATVAR)



    IERR = FMAT_VARCREATE('edgecharge', edge_charge, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, edge_charge,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('t', t, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, t,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('snumber', snumber, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, snumber,use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    if (count.lt.1000000) then
        IERR = FMAT_VARCREATE('totalenergy',total_energy(:,:,1:count), MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, total_energy(:,:,1:count),use_compression)
        IERR = FMAT_VARFREE(MATVAR)
    end if



    IERR = FMAT_VARCREATE('Esct', Esct, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, Esct,use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    IERR = FMAT_VARCREATE('dPHIqn', dPHIqn, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, dPHIqn,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('pchi', pot_chi, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, pot_chi,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('bx', bx, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, bx,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('by', by, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, by,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('bz', bz, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, bz,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('npartproc', no_part_proc, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, no_part_proc,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('nodiagreg', no_diag_reg, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, no_diag_reg,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('diaghistories', diag_histories, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, diag_histories,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('fvarrays', fv_arrays, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, fv_arrays,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('fvbin', fv_bin, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR, fv_bin,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('fvperparraycount', fv_perp_array_count, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,fv_perp_array_count,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('fvlimits', fv_limits, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,fv_limits,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('histlimits', hist_limit, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,hist_limit,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('timehistory', time_history, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,time_history,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    !go through them and save one after another by their name
    hist_count = 0
    fv_count = 0
    fv_pa_count = 0
    do diag_count=1,no_diag_reg
        if ((diag_regions(diag_count)%record_property.eq.1)) then
            !potential
            hist_count = hist_count + 1
            hist_length = count

            write(*,*) 'Saving diag potential records:',hist_length

            IERR = FMAT_VARCREATE(trim(diag_regions(diag_count)%name)//char(0), diag_hist(hist_count,1:hist_length), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,diag_hist(hist_count,1:hist_length),use_compression)
            IERR = FMAT_VARFREE(MATVAR)
        else if (diag_regions(diag_count)%record_property.eq.4) then
            ! mean velocity components
            write(*,*) 'Saving diag mean velocity records:',hist_length

            hist_count = hist_count + 1
            hist_length = count
            IERR = FMAT_VARCREATE(trim(diag_regions(diag_count)%name)//'x'//char(0), diag_hist(hist_count,1:hist_length), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,diag_hist(hist_count,1:hist_length),use_compression)
            IERR = FMAT_VARFREE(MATVAR)

            hist_count = hist_count + 1
            hist_length = count
            IERR = FMAT_VARCREATE(trim(diag_regions(diag_count)%name)//'y'//char(0), diag_hist(hist_count,1:hist_length), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,diag_hist(hist_count,1:hist_length),use_compression)
            IERR = FMAT_VARFREE(MATVAR)

            hist_count = hist_count + 1
            hist_length = count
            IERR = FMAT_VARCREATE(trim(diag_regions(diag_count)%name)//'z'//char(0), diag_hist(hist_count,1:hist_length), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,diag_hist(hist_count,1:hist_length),use_compression)
            IERR = FMAT_VARFREE(MATVAR)


        else if (diag_regions(diag_count)%record_property.eq.7) then

            write(*,*) 'Saving resonance diag ',trim(diag_regions(diag_count)%name)

            IERR = FMAT_VARCREATE(trim(diag_regions(diag_count)%name)//char(0), fv_ar(fv_count+1,1:fv_bin), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,fv_ar(fv_count+1,1:fv_bin),use_compression)
            IERR = FMAT_VARFREE(MATVAR)


            fv_count = fv_count + 1


        else if ((diag_regions(diag_count)%record_property.eq.2).or.(diag_regions(diag_count)%record_property.eq.3)) then
            !f(v)
            if (count.gt.Nc) then
                fvn = real((count - Nc +1)*N0)
            else
                fvn = 1.0
            end if
            write(*,*) 'Saving f(v) diag ',trim(diag_regions(diag_count)%name)
            IERR = FMAT_VARCREATE(trim(diag_regions(diag_count)%name)//'x1'//char(0), fv_ar(fv_count+1,1:fv_bin), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,fv_ar(fv_count+1,1:fv_bin)/fvn,use_compression)
            IERR = FMAT_VARFREE(MATVAR)

            IERR = FMAT_VARCREATE(trim(diag_regions(diag_count)%name)//'x2'//char(0), fv_ar(fv_count+2,1:fv_bin), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,fv_ar(fv_count+2,1:fv_bin)/fvn,use_compression)
            IERR = FMAT_VARFREE(MATVAR)

            IERR = FMAT_VARCREATE(trim(diag_regions(diag_count)%name)//'x3'//char(0), fv_ar(fv_count+3,1:fv_bin), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,fv_ar(fv_count+3,1:fv_bin)/fvn,use_compression)
            IERR = FMAT_VARFREE(MATVAR)
            fv_count = fv_count + 3
        else if  (diag_regions(diag_count)%record_property.eq.5) then
            if (count.gt.Nc) then
                fvn = real((count - Nc +1)*N0)
            else
                fvn = 1.0
            end if

            write(*,*) 'Saving f(v_par) | f(v_perp1,v_perp2) diag ',trim(diag_regions(diag_count)%name)
            IERR = FMAT_VARCREATE(trim(diag_regions(diag_count)%name)//'par'//char(0), fv_ar(fv_count+1,1:fv_bin)/fvn, MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,fv_ar(fv_count+1,1:fv_bin)/fvn,use_compression)
            IERR = FMAT_VARFREE(MATVAR)

            IERR = FMAT_VARCREATE(trim(diag_regions(diag_count)%name)//'perp'//char(0), u_perp_a(fv_pa_count+1,1:fv_bin,1:fv_bin)/fvn, MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,u_perp_a(fv_pa_count+1,1:fv_bin,1:fv_bin)/fvn,use_compression)
            IERR = FMAT_VARFREE(MATVAR)

            fv_count = fv_count + 1
            fv_pa_count =fv_pa_count +1


        else if  (diag_regions(diag_count)%record_property.eq.8) then
            if (count.gt.Nc) then
                fvn = real((count - Nc +1)*N0)
            else
                fvn = 1.0
            end if
            minimal = minval(u_perp_a(fv_pa_count+1,1:fv_bin,1:fv_bin))
            write(*,*) count,'Saving  f(E,alpha) diag ',trim(diag_regions(diag_count)%name),fvn,minimal

            IERR = FMAT_VARCREATE(trim(diag_regions(diag_count)%name)//'impact'//char(0), u_perp_a(fv_pa_count+1,1:fv_bin,1:fv_bin)/fvn, MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,u_perp_a(fv_pa_count+1,1:fv_bin,1:fv_bin)/fvn,use_compression)
            IERR = FMAT_VARFREE(MATVAR)

            fv_pa_count =fv_pa_count +1

        else if  (diag_regions(diag_count)%record_property.eq.6) then
            ! test particle tracking
            write(*,*) 'Saving test particle ',trim(diag_regions(diag_count)%name)
            hist_count = hist_count + 1
            hist_length = count
            if (hist_length.lt.1) then
                hist_length = 1
            end if
            IERR = FMAT_VARCREATE(trim(diag_regions(diag_count)%name)//'y'//char(0), diag_hist(hist_count,1:hist_length), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,diag_hist(hist_count,1:hist_length),use_compression)
            IERR = FMAT_VARFREE(MATVAR)

            hist_count = hist_count + 1

            IERR = FMAT_VARCREATE(trim(diag_regions(diag_count)%name)//'z'//char(0), diag_hist(hist_count,1:hist_length), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,diag_hist(hist_count,1:hist_length),use_compression)
            IERR = FMAT_VARFREE(MATVAR)

            hist_count = hist_count + 1
            IERR = FMAT_VARCREATE(trim(diag_regions(diag_count)%name)//'ux'//char(0), diag_hist(hist_count,1:hist_length), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,diag_hist(hist_count,1:hist_length),use_compression)
            IERR = FMAT_VARFREE(MATVAR)

            hist_count = hist_count + 1

            IERR = FMAT_VARCREATE(trim(diag_regions(diag_count)%name)//'uy'//char(0), diag_hist(hist_count,1:hist_length), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,diag_hist(hist_count,1:hist_length),use_compression)
            IERR = FMAT_VARFREE(MATVAR)

            hist_count = hist_count + 1

            IERR = FMAT_VARCREATE(trim(diag_regions(diag_count)%name)//'uz'//char(0), diag_hist(hist_count,1:hist_length), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,diag_hist(hist_count,1:hist_length),use_compression)
            IERR = FMAT_VARFREE(MATVAR)



        end if
    end do



    ! new potentials scenario specific
    IERR = FMAT_VARCREATE('flagm'//char(0), flag_m, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,flag_m,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    s = sum(equipot_m)
    write(*,*) 'Sum of equipot:', s
    IERR = FMAT_VARCREATE('equipotm'//char(0), equipot_m, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,equipot_m,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('flag'//char(0), flag, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,flag,use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    ! disable some really demanding diagnostics for fine runs
    if (dz.ge.0.1) then
        !iteration time duration
        IERR = FMAT_VARCREATE('itertime'//char(0), iteration_time_hist, MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR,iteration_time_hist,use_compression)
        IERR = FMAT_VARFREE(MATVAR)


        IERR = FMAT_VARCREATE('Esct', Esct, MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, Esct,use_compression)
        IERR = FMAT_VARFREE(MATVAR)


        IERR = FMAT_VARCREATE('dPHIqn', dPHIqn, MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, dPHIqn,use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('pchi', pot_chi, MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, pot_chi,use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('timehistory', time_history, MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR,time_history,use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        IERR = FMAT_VARCREATE('npartproc', no_part_proc, MATVAR)
        IERR = FMAT_VARWRITE(MAT, MATVAR, no_part_proc,use_compression)
        IERR = FMAT_VARFREE(MATVAR)

    end if
    ! injection rate hist

    IERR = FMAT_VARCREATE('injrate'//char(0), injection_rate_hist, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,injection_rate_hist,use_compression)
    IERR = FMAT_VARFREE(MATVAR)



    IERR = FMAT_VARCREATE('objects'//char(0), objects, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,objects,use_compression)
    IERR = FMAT_VARFREE(MATVAR)



    IERR = FMAT_VARCREATE('edges'//char(0), edges, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,edges,use_compression)
    IERR = FMAT_VARFREE(MATVAR)



    IERR = FMAT_VARCREATE('diagm'//char(0), diagm, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,diagm,use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    IERR = FMAT_VARCREATE('objectsenum'//char(0), objects_enum, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,objects_enum,use_compression)
    IERR = FMAT_VARFREE(MATVAR)



    IERR = FMAT_VARCREATE('objectscurrenti'//char(0), objects_current(1,:,1:(count -1)), MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,objects_current(1,:,1:(count -1)),use_compression)
    IERR = FMAT_VARFREE(MATVAR)



    IERR = FMAT_VARCREATE('objectscurrente'//char(0), objects_current(2,:,1:(count -1)), MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,objects_current(2,:,1:(count -1)),use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('objectspowerfluxi'//char(0), objects_power_flux(1,:,1:(count -1)), MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,objects_power_flux(1,:,1:(count -1)),use_compression)
    IERR = FMAT_VARFREE(MATVAR)



    IERR = FMAT_VARCREATE('objectspowerfluxe'//char(0), objects_power_flux(2,:,1:(count -1)), MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,objects_power_flux(2,:,1:(count -1)),use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    ! charge densities
    specie_count = 0
    rhox = 0.0
    sol_Wx = 0.0
    sol_Nsx = 0.0
    do sp=1,no_spec
        if (specie_recomp) then
            rhox(specie_count*(proc_max+1) + proc_max + 1,:,:) = rhox(specie_count*(proc_max+1) + proc_max + 1,:,:) + rho(sp,:,:)
            sol_Wx(specie_count*(proc_max+1) + proc_max + 1,:,:) = sol_Wx(specie_count*(proc_max+1) + proc_max + 1,:,:) + sol_W(sp,:,:)
            sol_Nsx(specie_count*(proc_max+1) + proc_max + 1,:,:) = sol_Nsx(specie_count*(proc_max+1) + proc_max + 1,:,:) + sol_Ns(sp,:,:)

            if (sp.eq.int(real(sp)/real(proc_max+1))*(proc_max+1)) then
                specie_count = specie_count + 1
                sp_str = '  '
                write(sp_str(1:2),'(I2.2)') specie_count
                IERR = FMAT_VARCREATE('rho'//sp_str//char(0), rhox(specie_count*(proc_max+1),:,:), MATVAR)
                IERR = FMAT_VARWRITE(MAT, MATVAR,rhox(specie_count*(proc_max+1),:,:),use_compression)
                IERR = FMAT_VARFREE(MATVAR)


                IERR = FMAT_VARCREATE('solw'//sp_str//char(0), SOL_Wx(specie_count*(proc_max+1),:,:), MATVAR)
                IERR = FMAT_VARWRITE(MAT, MATVAR,SOL_Wx(specie_count*(proc_max+1),:,:),use_compression)
                IERR = FMAT_VARFREE(MATVAR)



                IERR = FMAT_VARCREATE('solns'//sp_str//char(0), SOL_Nsx(specie_count*(proc_max+1),:,:), MATVAR)
                IERR = FMAT_VARWRITE(MAT, MATVAR,SOL_Nsx(specie_count*(proc_max+1),:,:),use_compression)
                IERR = FMAT_VARFREE(MATVAR)

            end if

        else
            write(sp_str(1:2),'(I2.2)') sp

            IERR = FMAT_VARCREATE('rho'//sp_str//char(0), rho(sp,:,:), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,rho(sp,:,:),use_compression)
            IERR = FMAT_VARFREE(MATVAR)


            IERR = FMAT_VARCREATE('solw'//sp_str//char(0), SOL_W(sp,:,:), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,SOL_W(sp,:,:),use_compression)
            IERR = FMAT_VARFREE(MATVAR)



            IERR = FMAT_VARCREATE('solns'//sp_str//char(0), SOL_Ns(sp,:,:), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,SOL_Ns(sp,:,:),use_compression)
            IERR = FMAT_VARFREE(MATVAR)
        end if
        !	write(*,*) 'Saving rho:',sp,sum(rho(sp,:,:))
        if (have_impact_diag) then
            IERR = FMAT_VARCREATE('impactdiag'//sp_str//char(0), impact_diag(sp,:,:,:), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,impact_diag(sp,:,:,:),use_compression)
            IERR = FMAT_VARFREE(MATVAR)

            IERR = FMAT_VARCREATE('emmitdiag'//sp_str//char(0), emmit_diag(sp,:,:,:), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR,emmit_diag(sp,:,:,:),use_compression)
            IERR = FMAT_VARFREE(MATVAR)
        end if

    end do

    IERR = FMAT_VARCREATE('ksi'//char(0), ksi, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,ksi,use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    IERR = FMAT_VARCREATE('tau'//char(0), tau, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,tau,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('mu'//char(0), mu, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,mu,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('mksn0'//char(0), mks_info%mks_n0, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,mks_info%mks_n0,use_compression)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('mksTe'//char(0), mks_info%mks_Te, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,mks_info%mks_Te,use_compression)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('mksB'//char(0), mks_info%mks_B, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,mks_info%mks_B,use_compression)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('mksmainionm'//char(0), mks_info%mks_main_ion_m, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,mks_info%mks_main_ion_m,use_compression)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('mksmainionq'//char(0), mks_info%mks_main_ion_q, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,mks_info%mks_main_ion_q,use_compression)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('mkspar1'//char(0), mks_info%mks_par1, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,mks_info%mks_par1,use_compression)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('mkspar2'//char(0), mks_info%mks_par2, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,mks_info%mks_par2,use_compression)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('mkspar3'//char(0), mks_info%mks_par3, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,mks_info%mks_par3,use_compression)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('alphayz'//char(0), alpha_yz, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,alpha_yz,use_compression)
    IERR = FMAT_VARFREE(MATVAR)
    IERR = FMAT_VARCREATE('alphaxz'//char(0), alpha_xz, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,alpha_xz,use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    IERR = FMAT_VARCREATE('Nc'//char(0), Nc, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,Nc,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Na'//char(0), Na, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,Na,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('Np'//char(0), Np, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,Np,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('irel'//char(0), i_rel_history, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,i_rel_history,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('floatconstant'//char(0), float_constant, MATVAR)
    IERR = FMAT_VARWRITE(MAT, MATVAR,float_constant,use_compression)
    IERR = FMAT_VARFREE(MATVAR)



    IERR = FMAT_CLOSE(MAT)

    !open(7, file='dispo.txt',status='unknown')
    !write(7,'(I1)') (1)
    !close (7)

    !RD le 13/01/05   test avec chantal pour lire un fichier .mat
    !call OPENMATR(filename,ifile)
    !write(6,*) ' ifile ',ifile
    !call LOADMATF(ifile,type,name,nlig,ncol,imag,zz,zz,icr)
    !write(6,*) ' icr ',icr
    !write(6,*) ' type ', type,' nlig ',nlig
    !write(6,*) ' name ',name
    !call CLOSEMAT(ifile)
    !END test
    if (debug) then
        write(*,*) 'Finished in Lang. monitor'
    end if

    ! domain_decomp = .true.


    return
END


subroutine save_temp_proc_file(tfile,proc_no,no_tot,z,y,ux,uy,uz,vtot_init,use_vtot,stype,count,use_compression)
    USE MATIO

    implicit none

    integer:: no_tot, proc_no,icr,version,IERR,count
    character*2048:: tfile
    real*8, dimension(no_tot):: z,y,ux,uy,uz,vtot_init
    logical:: use_vtot
    integer*2, dimension(no_tot):: stype
    integer*8:: ifile
    character*2:: sp_str
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR
    integer:: use_compression
    version = 50
    sp_str = '  '
    write(sp_str(1:2),'(I2.2)') proc_no
    CALL FMAT_LOGINIT('particle')

    ! defunct - don't know how to include integers in strings
    write (6,*) 'Writing results to ',trim(tfile)//sp_str//'.mat'
    ! OPEN FILE FOR WRITING
    IERR = FMAT_CREATE(trim(tfile)//sp_str//'.mat'//char(0), MAT)
    ! WRITE FIRST VARIABLE
    IERR = FMAT_VARCREATE('version', version, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,version,use_compression)

    !    IERR = FMAT_VARWRITE(MAT, MATVAR, version)
    IERR = FMAT_VARFREE(MATVAR)
    !second goes the number of particles

    IERR = FMAT_VARCREATE('ntot', no_tot, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,no_tot,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('count', count, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,count,use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    IERR = FMAT_VARCREATE('nproc', proc_no, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,proc_no,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    !that's it, now the particles
    IERR = FMAT_VARCREATE('y', y, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,y,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('z', z, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,z,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('ux', ux, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,ux,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('uy', uy, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,uy,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('uz', uz, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,uz,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    if (use_vtot) then
        IERR = FMAT_VARCREATE('vtotinit', uz, MATVAR)
        ierr = FMat_VarWrite(mat,matvar,vtot_init,use_compression)
        IERR = FMAT_VARFREE(MATVAR)
    end if

    IERR = FMAT_VARCREATE('stype', stype, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,stype,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_CLOSE(MAT)


end subroutine


subroutine save_test_particle(tfile,proc_no,no_species,test_particle,count,test_particle_i,test_particle_data,use_compression)
    USE MATIO

    implicit none

    integer:: proc_no,no_species,version,IERR,count,test_particle,sp
    character*2048:: tfile
    real*8, dimension(count,no_species,test_particle,7):: test_particle_data
    integer, dimension(no_species,test_particle):: test_particle_i
    character*2:: sp_str
    character*5:: spm_str
    TYPE(MAT_T)     :: MAT
    TYPE(MATVAR_T)  :: MATVAR

    integer :: pt_from, pt_to, part_count, i_c

    integer:: use_compression
    version = 50
    sp_str = '  '
    write(sp_str(1:2),'(I2.2)') proc_no
    CALL FMAT_LOGINIT('testpart')

    ! defunct - don't know how to include integers in strings
    write (6,*) 'Writing test particles to ',trim(tfile)//sp_str//'tp.mat'
    ! OPEN FILE FOR WRITING
    IERR = FMAT_CREATE(trim(tfile)//sp_str//'tp.mat'//char(0), MAT)
    ! WRITE FIRST VARIABLE
    IERR = FMAT_VARCREATE('version', version, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,version,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('count', count, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,count,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('test_particle', test_particle, MATVAR)
    ierr = FMat_VarWrite(mat,matvar,test_particle,use_compression)
    IERR = FMAT_VARFREE(MATVAR)

    IERR = FMAT_VARCREATE('test_particle_i', transpose(test_particle_i), MATVAR)
    ierr = FMat_VarWrite(mat,matvar,transpose(test_particle_i),use_compression)
    IERR = FMAT_VARFREE(MATVAR)


    if (count*test_particle*7.gt.266000000) then
        part_count = 266000000 / (test_particle*7)

        IERR = FMAT_VARCREATE('split', 1, MATVAR)
        ierr = FMat_VarWrite(mat,matvar,1,use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        pt_from = 1
        do i_c=1,99
            pt_to = min(pt_from + part_count, count)

            if (i_c.eq.1) then
                do sp=1,no_species
                    sp_str = '  '
                    write(sp_str(1:2),'(I2.2)') sp
                    IERR = FMAT_VARCREATE('particles'//sp_str//char(0), test_particle_data(pt_from:pt_to,sp,:,:), MATVAR)
                    IERR = FMAT_VARWRITE(MAT, MATVAR, test_particle_data(pt_from:pt_to,sp,:,:),use_compression)
                    IERR = FMAT_VARFREE(MATVAR)
                enddo
            else
                do sp=1,no_species
                    spm_str = '  p  '
                    write(spm_str(1:2),'(I2.2)') sp
                    write(spm_str(4:5),'(I2.2)') i_c-1
                    IERR = FMAT_VARCREATE('particles'//spm_str//char(0), test_particle_data(pt_from:pt_to,sp,:,:), MATVAR)
                    IERR = FMAT_VARWRITE(MAT, MATVAR, test_particle_data(pt_from:pt_to,sp,:,:),use_compression)
                    IERR = FMAT_VARFREE(MATVAR)
                enddo
            endif

            pt_from = pt_to + 1
            if (pt_from.ge.count) then
                exit
            endif
        enddo
    else
        IERR = FMAT_VARCREATE('split', 0, MATVAR)
        ierr = FMat_VarWrite(mat,matvar,0,use_compression)
        IERR = FMAT_VARFREE(MATVAR)

        do sp=1,no_species
            sp_str = '  '
            write(sp_str(1:2),'(I2.2)') sp
            IERR = FMAT_VARCREATE('particles'//sp_str//char(0), test_particle_data(:,sp,:,:), MATVAR)
            IERR = FMAT_VARWRITE(MAT, MATVAR, test_particle_data(:,sp,:,:),use_compression)
            IERR = FMAT_VARFREE(MATVAR)
        enddo
    endif


    IERR = FMAT_CLOSE(MAT)

end subroutine

subroutine null_sub()
    ! just a empty subroutine, useful for debugging mainloop
end subroutine

subroutine saveChrg(outputFile, n_chrg, chrg, nz, ny, rho_tot, dz, dy)
    use matio
    implicit none

    character*2048, intent(in):: outputFile

    integer, intent(in):: n_chrg, nz, ny
    real*8, intent(in) :: dz, dy

    real*8, dimension(n_chrg), intent(in) :: chrg
    real*8, dimension(nz, ny), intent(in) :: rho_tot

    integer:: use_compression, ierr, version
    type(MAT_T):: MAT
    type(MATVAR_T):: MATVAR

    use_compression = 0

    write (*,*) '#	Saving output file '//trim(outputFile)

    CALL fmat_loginit('savePlasma')

    ierr = fmat_create(trim(outputFile)//char(0), mat)

    ierr = fmat_varcreate('chrg', chrg, matvar)
    ierr = fmat_varwrite(mat, matvar, chrg, use_compression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('rho_tot', rho_tot, matvar)
    ierr = fmat_varwrite(mat, matvar, rho_tot, use_compression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('dz', dz, matvar)
    ierr = fmat_varwrite(mat, matvar, dz, use_compression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('dy', dy, matvar)
    ierr = fmat_varwrite(mat, matvar, dy, use_compression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_close(mat)
    write (6,*) '#	Plasma written.'

end subroutine saveChrg


subroutine saveMatrix(outputFile, n_p, Ap, n_i, Ai, n_x, Ax)
    use matio
    implicit none

    character*2048, intent(in):: outputFile

    integer, intent(in):: n_p, n_i, n_x

    integer, dimension(n_p), intent(in) :: Ap
    integer, dimension(n_i), intent(in) :: Ai
    real*8, dimension(n_x), intent(in) :: Ax

    integer:: use_compression, ierr, version
    type(MAT_T):: MAT
    type(MATVAR_T):: MATVAR

    use_compression = 0

    write (*,*) '#	Saving output file '//trim(outputFile)

    CALL fmat_loginit('savePlasma')

    ierr = fmat_create(trim(outputFile)//char(0), mat)

    ierr = fmat_varcreate('n_p', n_p, matvar)
    ierr = fmat_varwrite(mat, matvar, n_p, use_compression)
    ierr = fmat_varfree(matvar)


    ierr = fmat_varcreate('n_i', n_i, matvar)
    ierr = fmat_varwrite(mat, matvar, n_i, use_compression)
    ierr = fmat_varfree(matvar)


    ierr = fmat_varcreate('n_x', n_x, matvar)
    ierr = fmat_varwrite(mat, matvar, n_x, use_compression)
    ierr = fmat_varfree(matvar)


    ierr = fmat_varcreate('Ai', Ai, matvar)
    ierr = fmat_varwrite(mat, matvar, Ai, use_compression)
    ierr = fmat_varfree(matvar)


    ierr = fmat_varcreate('Ap', Ap, matvar)
    ierr = fmat_varwrite(mat, matvar, Ap, use_compression)
    ierr = fmat_varfree(matvar)


    ierr = fmat_varcreate('Ax', Ax, matvar)
    ierr = fmat_varwrite(mat, matvar, Ax, use_compression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_close(mat)
    write (6,*) '#	Plasma written.'

end subroutine saveMatrix
