! subroutines concerning communication among processors

subroutine receive_diag (rho,no_part_proc,mpi_rank,count,number,debug,no_species,proc_max,h_pos,iteration_time_hist,&
        Nz,Ny,objects_current,objects_power_flux,objects_current_tot,objects_power_flux_tot,nobjects,Nz_max,have_impact_diag,impact_diag,emmit_diag,fv_bin,dz,dt,Npc)
    implicit none
    include 'mpif.h'
    ! we receive only global diag, not particle-related data
    !this subroutine receives data for all types of particles
    real*8:: dz,dt
    integer:: i,no_part,sp,count,no_species,proc_max,proc_no,ierr,np,h_pos,Nz,Ny,nobjects,Nz_max,pr,fv_bin,Npc
    integer, dimension(proc_max +1,1:h_pos):: no_part_proc
    integer, dimension(no_species,1:h_pos):: number
    integer, dimension(no_species):: mpi_rank
    integer*8, dimension(proc_max +1,1:h_pos):: iteration_time_hist
    logical:: debug,have_impact_diag
    integer par_status(MPI_STATUS_SIZE)
    real*8, dimension(Nz_max,Ny):: dumr
    real*8, dimension(no_species,Nz_max,Ny):: rho
    real*8, dimension(2,nobjects,1:count):: objects_current,objects_power_flux,objects_current_tot,objects_power_flux_tot,dumc
    real*8, dimension(Nz,Ny,no_species)::edge_flux,edge_energy_flux,edge_velocity_x,edge_velocity_y,edge_velocity_z
    real*8, dimension(no_species,nobjects,fv_bin,fv_bin):: impact_diag,dumimp,emmit_diag
    ! receives data from all other procs so it can be stored in a temporary file
    !receiver
    ! Processor - related diag
    !for simplicity we store actual number of particles running at each processor
    if (debug) then
        write(*,*) 'Receive diag - proc_max',proc_max
    end if
    i = 18
    do np=1,proc_max
        call MPI_RECV(no_part_proc(np +1,1:h_pos),h_pos,MPI_INTEGER,np,i*np,MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) 'MPI Received no_part_proc',sum(no_part_proc(np +1,1:h_pos)),np
        end if

    end do

    ! i = 31
    ! ! type-of-particle related diag
    ! do sp=1,no_species
    ! 	if (mpi_rank(sp).ne.0) then
    ! 		call MPI_RECV(number(sp,1:h_pos),h_pos,MPI_INTEGER,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)
    ! 		if (debug) then
    ! 			write(*,*) 'MPI Received number',sum(number(sp,1:h_pos))
    ! 		end if
    ! 	end if
    ! end do
    i = 43
    do np=1,proc_max
        call MPI_RECV(iteration_time_hist(np +1,1:h_pos),h_pos,MPI_INTEGER8,np,i*np,MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) 'MPI Received iteration_time_hist',sum(iteration_time_hist(np +1,1:h_pos)),np
        end if
    end do
    i  = 45
    do pr=1,proc_max
        do sp=1,no_species
            if (debug) then
                write(*,*) 'MPI Receiving rho',i*sp*pr,pr
            end if

            call MPI_RECV(dumr,Ny*Nz_max,MPI_DOUBLE_PRECISION,pr,i*sp*pr,MPI_COMM_WORLD,par_status,ierr)
            if (debug) then
                write(*,*) 'MPI Received rho',pr,sp,sum(dumr)
            end if
            rho(sp,:,:) =rho(sp,:,:) + dumr
        end do
    end do

   
    i  = 46
    
    objects_current_tot = objects_current
    do np=1,proc_max
        do sp=1,2
         if (debug) then
                write(*,*) 'MPI Receiving objects current',i*sp*np,sp,np
            end if
            call MPI_RECV(dumc(sp,:,:),nobjects*count,MPI_DOUBLE_PRECISION,np,i*sp*np,MPI_COMM_WORLD,par_status,ierr)
            if (debug) then
                write(*,*) 'MPI Received objects_current',sum(dumc(sp,:,:))
            end if
        end do
        objects_current_tot = objects_current_tot + dumc
        
    end do
  !  write(*,*) 'objects current tot', sum(objects_current_tot)
    i  = 47
    objects_power_flux_tot = objects_power_flux/dt/Npc*dz
do np=1,proc_max
do sp=1,2
		call MPI_RECV(dumc(sp,:,:),nobjects*count,MPI_DOUBLE_PRECISION,np,i*sp*np,MPI_COMM_WORLD,par_status,ierr)
		if (debug) then
			write(*,*) 'MPI Received objects_power_flux',sum(dumc(sp,:,:))
		end if
end do
        objects_power_flux_tot = objects_power_flux_tot + dumc/dt/Npc*dz
end do

if (have_impact_diag) then
 i  = 48
 do np=1,proc_max
 call MPI_RECV(dumimp,no_species*nobjects*fv_bin*fv_bin,MPI_DOUBLE_PRECISION,np,i*np,MPI_COMM_WORLD,par_status,ierr)
		if (debug) then
			write(*,*) 'MPI Received impact_diag',sum(dumimp)
		end if
		impact_diag = impact_diag + dumimp

end do

 i  = 52
 do np=1,proc_max
 call MPI_RECV(dumimp,no_species*nobjects*fv_bin*fv_bin,MPI_DOUBLE_PRECISION,np,i*np,MPI_COMM_WORLD,par_status,ierr)
		if (debug) then
			write(*,*) 'MPI Received impact_diag',sum(dumimp)
		end if
		emmit_diag = emmit_diag + dumimp

end do

end if
    ! i  = 47
    ! do sp=1,no_species
    ! 	if (mpi_rank(sp).ne.0) then
    ! 		call MPI_RECV(edge_flux(:,:,sp),Ny*Nz,MPI_DOUBLE_PRECISION,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)
    ! 		if (debug) then
    ! 			write(*,*) 'MPI Received bedge flux',sum(edge_flux(:,:,sp))
    ! 		end if
    ! 	end if
    ! end do
    !
    ! i  = 48
    ! do sp=1,no_species
    ! 	if (mpi_rank(sp).ne.0) then
    ! 		call MPI_RECV(edge_energy_flux(:,:,sp),Ny*Nz,MPI_DOUBLE_PRECISION,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)
    ! 		if (debug) then
    ! 			write(*,*) 'MPI Received edge energy flux',sum(edge_energy_flux(:,:,sp))
    ! 		end if
    ! 	end if
    ! end do
    !
    ! i  = 49
    ! do sp=1,no_species
    ! 	if (mpi_rank(sp).ne.0) then
    ! 		call MPI_RECV(edge_velocity_x(:,:,sp),Ny*Nz,MPI_DOUBLE_PRECISION,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)
    ! 		if (debug) then
    ! 			write(*,*) 'MPI Received edge velocity x',sum(edge_velocity_x(:,:,sp))
    ! 		end if
    ! 	end if
    ! end do
    !
    ! i  = 50
    ! do sp=1,no_species
    ! 	if (mpi_rank(sp).ne.0) then
    ! 		call MPI_RECV(edge_velocity_y(:,:,sp),Ny*Nz,MPI_DOUBLE_PRECISION,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)
    ! 		if (debug) then
    ! 			write(*,*) 'MPI Received edge velocity y',sum(edge_velocity_y(:,:,sp))
    ! 		end if
    ! 	end if
    ! end do
    !
    ! i  = 51
    ! do sp=1,no_species
    ! 	if (mpi_rank(sp).ne.0) then
    ! 		call MPI_RECV(edge_velocity_z(:,:,sp),Ny*Nz,MPI_DOUBLE_PRECISION,mpi_rank(sp),i*sp,MPI_COMM_WORLD,par_status,ierr)
    ! 		if (debug) then
    ! 			write(*,*) 'MPI Received edge velocity z',sum(edge_velocity_z(:,:,sp))
    ! 		end if
    ! 	end if
    ! end do
    !

end subroutine

subroutine send_diag(rho,no_species,mpi_rank,no_part,count,number,debug,proc_no,no_part_proc,h_pos,iteration_time_hist,Nz,Ny,&
        objects_current,objects_power_flux,nobjects,Nz_max,have_impact_diag,impact_diag,emmit_diag,fv_bin)
    !we only send global characteristics, we keep z,y,... to each own processor's mat file
    implicit none
    integer:: i,no_species,no_part,count,sp,proc_no,ierr,h_pos,Nz,Ny,nobjects,Nz_max,fv_bin
    integer, dimension(no_species):: mpi_rank
    integer, dimension(no_species,1:h_pos):: number

    integer, dimension(1:h_pos):: no_part_proc
    logical:: debug,have_impact_diag
    integer*8, dimension(1:h_pos):: iteration_time_hist
    real*8, dimension(no_species,Nz_max,Ny):: rho
    real*8, dimension(2,nobjects,1:count):: objects_current,objects_power_flux
    real*8, dimension(Nz,Ny,no_species)::edge_flux,edge_energy_flux,edge_velocity_x,edge_velocity_y,edge_velocity_z
    real*8, dimension(no_species,nobjects,fv_bin,fv_bin):: impact_diag,dumimp,emmit_diag


    include 'mpif.h'

    !sender
    i = 18
    if (debug) then
        write(*,*) 'MPI:: Sending no_part_proc',sum(no_part_proc)
    end if
    call MPI_SEND(no_part_proc(1:h_pos),h_pos,MPI_INTEGER,0,i*proc_no,MPI_COMM_WORLD,ierr)
    ! 	i=31
    ! 	do sp=1,no_species
    ! 		if (mpi_rank(sp).eq.proc_no) then
    ! 			if (debug) then
    ! 				write(*,*) 'MPI:: Sending number',sum(number(sp,:))
    ! 			end if
    ! 			call MPI_SEND(number(sp,1:h_pos),h_pos,MPI_INTEGER,0,i*sp,MPI_COMM_WORLD,ierr)
    ! 		end if
    ! 	end do
    i = 43
    if (debug) then
        write(*,*) 'MPI:: Sending iteration_time_hist',sum(iteration_time_hist),proc_no
    end if
    call MPI_SEND(iteration_time_hist(1:h_pos),h_pos,MPI_INTEGER8,0,i*proc_no,MPI_COMM_WORLD,ierr)
    i = 45
    do sp=1,no_species
        if (debug) then
            write(*,*),proc_no,'Specie mpi_rank',sp,mpi_rank(sp)
        end if
        if (mpi_rank(sp).eq.proc_no) then
            if (debug) then
                write(*,*) proc_no,'MPI:: Sending rho',count,sp,i*sp*proc_no,sum(rho(sp,:,:))
            end if
            call MPI_SEND(rho(sp,:,:),Ny*Nz_max,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)
        end if
    end do
    
    i = 46
    do sp=1,2
        call MPI_SEND(objects_current(sp,:,:),nobjects*count,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)

    end do
    i = 47
	do sp=1,2
        call MPI_SEND(objects_power_flux(sp,:,:),nobjects*count,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)

    end do
    
    
      i = 48
if (have_impact_diag) then
        call MPI_SEND(impact_diag,nobjects*no_species*fv_bin*fv_bin,MPI_DOUBLE_PRECISION,0,i*proc_no,MPI_COMM_WORLD,ierr)
      i = 52
        call MPI_SEND(emmit_diag,nobjects*no_species*fv_bin*fv_bin,MPI_DOUBLE_PRECISION,0,i*proc_no,MPI_COMM_WORLD,ierr)
        
        
end if
    ! 	i = 47
    ! 	do sp=1,no_species
    ! 		if (mpi_rank(sp).eq.proc_no) then
    ! 			if (debug) then
    ! 				write(*,*) 'MPI:: Sending edge flux',sum(edge_flux(:,:,sp))
    ! 			end if
    ! 			call MPI_SEND(edge_flux(:,:,sp),Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp,MPI_COMM_WORLD,ierr)
    ! 		end if
    ! 	end do
    !
    ! 	i = 48
    ! 	do sp=1,no_species
    ! 		if (mpi_rank(sp).eq.proc_no) then
    ! 			if (debug) then
    ! 				write(*,*) 'MPI:: Sending edge energy flux',sum(edge_energy_flux(:,:,sp))
    ! 			end if
    ! 			call MPI_SEND(edge_flux(:,:,sp),Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp,MPI_COMM_WORLD,ierr)
    ! 		end if
    ! 	end do
    !
    ! 	i = 49
    ! 	do sp=1,no_species
    ! 		if (mpi_rank(sp).eq.proc_no) then
    ! 			if (debug) then
    ! 				write(*,*) 'MPI:: Sending edge velocity x',sum(edge_velocity_x(:,:,sp))
    ! 			end if
    ! 			call MPI_SEND(edge_velocity_x(:,:,sp),Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp,MPI_COMM_WORLD,ierr)
    ! 		end if
    ! 	end do
    ! 	i = 50
    ! 	do sp=1,no_species
    ! 		if (mpi_rank(sp).eq.proc_no) then
    ! 			if (debug) then
    ! 				write(*,*) 'MPI:: Sending edge velocity y',sum(edge_velocity_y(:,:,sp))
    ! 			end if
    ! 			call MPI_SEND(edge_velocity_y(:,:,sp),Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp,MPI_COMM_WORLD,ierr)
    ! 		end if
    ! 	end do
    ! 	i = 51
    ! 	do sp=1,no_species
    ! 		if (mpi_rank(sp).eq.proc_no) then
    ! 			if (debug) then
    ! 				write(*,*) 'MPI:: Sending edge velocity z',sum(edge_velocity_z(:,:,sp))
    ! 			end if
    ! 			call MPI_SEND(edge_velocity_z(:,:,sp),Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp,MPI_COMM_WORLD,ierr)
    ! 		end if
    ! 	end do
    !

end subroutine
subroutine receive_final_diag(edge_flux,count,sp,Nz,Ny,mpi_rank,dens,vxav,vyav,vzav,vx2av,vy2av,vz2av,vx3av,vy3av,vz3av,debug,&
        edge_energy_flux,edge_velocity_x,edge_velocity_y,edge_velocity_z,proc_max)
    implicit none
    include 'mpif.h'
    integer:: i,count,no_part,sp,Nz,Ny,mpi_rank,ierr,Nz1,abin,pr,proc_max
    real*8:: ang_bin
    real*8, dimension(Nz,Ny):: dens,dumr
    real*8, dimension(Nz,Ny):: vxav,vyav,vzav,vx2av,vy2av,vz2av,vx3av,vy3av,vz3av
    real*8, dimension(Nz,Ny):: edge_flux,edge_energy_flux,edge_velocity_x,edge_velocity_y,edge_velocity_z

    logical:: debug
    integer par_status(MPI_STATUS_SIZE)
    dumr  = 0.0
    ierr = 0

    do pr=1,proc_max
        i = 141
        if (debug) then
            write(*,*) 'Receiving dens from',pr,i*sp*pr
        end if
        call MPI_RECV(dumr,Ny*Nz,MPI_DOUBLE_PRECISION,pr,i*sp*pr,MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) 'Received edge_flux',sum(dumr)
        end if
        edge_flux = edge_flux + dumr
        ! 	  write(*,*) '0,Received edgeflux from',pr,sum(dumr)
        i = i+ 1


        call MPI_RECV(dumr,Ny*Nz,MPI_DOUBLE_PRECISION,pr,i*sp*pr,MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) 'Received dens',sum(dumr)
        end if

        dens = dens + dumr

        i = i+ 1
        call MPI_RECV(dumr,Ny*Nz,MPI_DOUBLE_PRECISION,pr,i*sp*pr,MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) 'Received vzav',sum(dumr)
        end if
        vzav = vzav + dumr
        i = i+ 1
        call MPI_RECV(dumr,Ny*Nz,MPI_DOUBLE_PRECISION,pr,i*sp*pr,MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) 'Received vyav',sum(dumr)
        end if
        vyav = vyav + dumr
        i = i+ 1
        call MPI_RECV(dumr,Ny*Nz,MPI_DOUBLE_PRECISION,pr,i*sp*pr,MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) 'Received vxav',sum(dumr)
        end if
        vxav = vxav + dumr
        i = i+ 1
        call MPI_RECV(dumr,Ny*Nz,MPI_DOUBLE_PRECISION,pr,i*sp*pr,MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) 'Received vz2av',sum(dumr)
        end if
        vz2av = vz2av + dumr
        i = i+ 1
        call MPI_RECV(dumr,Ny*Nz,MPI_DOUBLE_PRECISION,pr,i*sp*pr,MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) 'Received vy2av',sum(dumr)
        end if
        vy2av = vy2av + dumr
        i = i+ 1
        call MPI_RECV(dumr,Ny*Nz,MPI_DOUBLE_PRECISION,pr,i*sp*pr,MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) 'Received vx2av',sum(dumr)
        end if
        vx2av = vx2av + dumr
        i = i+ 1
        call MPI_RECV(dumr,Ny*Nz,MPI_DOUBLE_PRECISION,pr,i*sp*pr,MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) 'Received vz3av',sum(dumr)
        end if
        vz3av = vz3av + dumr
        i = i+ 1
        call MPI_RECV(dumr,Ny*Nz,MPI_DOUBLE_PRECISION,pr,i*sp*pr,MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) 'Received vy3av',sum(dumr)
        end if
        vy3av = vy3av + dumr

        i = i+ 1
        call MPI_RECV(dumr,Ny*Nz,MPI_DOUBLE_PRECISION,pr,i*sp*pr,MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) 'Received vx3av',sum(dumr)
        end if
        vx3av = vx3av + dumr
        i = i+ 1
        call MPI_RECV(dumr,Ny*Nz,MPI_DOUBLE_PRECISION,pr,i*sp*pr,MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) 'Received edge_energy_flux',sum(dumr)
        end if
        edge_energy_flux = edge_energy_flux + dumr
        i = i+ 1
        call MPI_RECV(edge_velocity_x,Ny*Nz,MPI_DOUBLE_PRECISION,pr,i*sp*pr,MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) 'Received edge_velocity_x',sum(dumr)
        end if
        edge_velocity_x = edge_velocity_x + dumr
        i = i+ 1

        call MPI_RECV(dumr,Ny*Nz,MPI_DOUBLE_PRECISION,pr,i*sp*pr,MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) 'Received edge_velocity_y',sum(dumr)
        end if
        edge_velocity_y = edge_velocity_y + dumr
        i = i+ 1

        call MPI_RECV(dumr,Ny*Nz,MPI_DOUBLE_PRECISION,pr,i*sp*pr,MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) 'Received edge_velocity_z',sum(dumr)
        end if
        edge_velocity_z = edge_velocity_z + dumr
    end do
end subroutine
subroutine send_final_diag(edge_flux,count,sp,Nz,Ny,dens,vxav,vyav,vzav,vx2av,vy2av,vz2av,vx3av,vy3av,vz3av,debug,edge_energy_flux,edge_velocity_x,edge_velocity_y,edge_velocity_z,proc_no)
    implicit none
    include 'mpif.h'
    integer:: i,count,no_part,sp,Nz,Ny,ierr,Nz1,abin,proc_no
    real*8:: ang_bin
    real*8, dimension(Nz,Ny):: dens
    real*8, dimension(Nz,Ny):: vxav,vyav,vzav,vx2av,vy2av,vz2av,vx3av,vy3av,vz3av
    real*8, dimension(Nz,Ny):: edge_flux,edge_energy_flux,edge_velocity_x,edge_velocity_y,edge_velocity_z

    logical:: debug
    ! write(*,*) 'send EF',sum(edge_flux)
    if (debug) then
        write(*,*) 'Sending dens',sum(dens)
    end if
    i = 141
    call MPI_SEND(edge_flux,Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)
    ! 	write(*,*) proc_no,'edgeflux send',sum(edge_flux)

    if (debug) then
        write(*,*) 'Sending edge_energy_flux',sum(edge_energy_flux)
    end if
    i = i +1

    call MPI_SEND(dens,Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)

    if (debug) then
        write(*,*) 'Sending vzav',sum(vzav)
    end if
    i = i +1
    call MPI_SEND(vzav,Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)

    if (debug) then
        write(*,*) 'Sending vyav',sum(vyav)
    end if
    i = i +1
    call MPI_SEND(vyav,Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)

    if (debug) then
        write(*,*) 'Sending vxav',sum(vxav)
    end if
    i = i +1
    call MPI_SEND(vxav,Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)

    if (debug) then
        write(*,*) 'Sending vz2av',sum(vz2av)
    end if
    i = i +1
    call MPI_SEND(vz2av,Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)

    if (debug) then
        write(*,*) 'Sending vy2av',sum(vy2av)
    end if
    i = i +1
    call MPI_SEND(vy2av,Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)

    if (debug) then
        write(*,*) 'Sending vx2av',sum(vx2av)
    end if
    i = i +1
    call MPI_SEND(vx2av,Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)

    if (debug) then
        write(*,*) 'Sending vz3av',sum(vz3av)
    end if
    i = i +1
    call MPI_SEND(vz3av,Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)

    if (debug) then
        write(*,*) 'Sending vy3av',sum(vy3av)
    end if
    i = i +1
    call MPI_SEND(vy3av,Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)

    if (debug) then
        write(*,*) 'Sending vx3av',sum(vx3av)
    end if
    i = i +1
    call MPI_SEND(vx3av,Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)

    if (debug) then
        write(*,*) 'Sending edge_flux',sum(edge_flux)
    end if
    i = i +1
    call MPI_SEND(edge_energy_flux,Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)

    if (debug) then
        write(*,*) 'Sending edge_velocity_x',sum(edge_velocity_x)
    end if
    i = i +1
    call MPI_SEND(edge_velocity_x,Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)

    if (debug) then
        write(*,*) 'Sending edge_velocity_y',sum(edge_velocity_y)
    end if
    i = i +1
    call MPI_SEND(edge_velocity_y,Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)

    if (debug) then
        write(*,*) 'Sending edge_velocity_z',sum(edge_velocity_z)
    end if
    i = i +1
    call MPI_SEND(edge_velocity_z,Ny*Nz,MPI_DOUBLE_PRECISION,0,i*sp*proc_no,MPI_COMM_WORLD,ierr)


end subroutine


subroutine send_rates(proc_max,no_species,injection_rates,debug)
    implicit none
    include 'mpif.h'
    integer:: proc_max,no_species,i,j,par_status, ierr
    real*8, dimension(no_species):: injection_rates
    logical:: debug
    j=66
    do i=1,proc_max
        call MPI_SEND(injection_rates,no_species,MPI_DOUBLE_PRECISION,i,j*i,MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) 'Sending injection rates',sum(injection_rates)
        end if

    end do
end subroutine

subroutine receive_rates(proc_max,no_species,injection_rates,proc_no,debug)
    implicit none
    include 'mpif.h'
    integer:: proc_max,no_species,i,j,proc_no,par_status, ierr
    real*8, dimension(no_species):: injection_rates
    logical:: debug
    j=66
    i = proc_no

    call MPI_RECV(injection_rates,no_species,MPI_DOUBLE_PRECISION,proc_no,i*j,MPI_COMM_WORLD,par_status,ierr)
    if (debug) then
        write(*,*) 'Received injection_rates',sum(injection_rates)
    end if

end subroutine

subroutine transfer_particles_1(Npts,no_tot,y,z,ux,uy,uz,stype,Ny,Nz_max,slice_proc,verbose,debug,proc_no,proc_max,dy,dz,no_species,stype_transfer)
    use spice2_types
    implicit none
    include 'mpif.h'
    !include 'struct.h'
    integer:: no_tot,Nz_max,Ny,i,sp,proc_no,proc_max,iyx,izx,left_i,right_i,incoming_left_i,incoming_right_i,N_temp,ierr,no_species,Npts
    real*8, dimension(Npts):: y,z,ux,uy,uz,right_y,right_z,right_ux,right_uy,right_uz,left_y,left_z,left_ux,left_uy,left_uz,dumr
    integer*2, dimension(Npts):: stype,left_stype,right_stype
    integer,dimension(Npts):: dumi
    real*8:: dy,dz
    integer:: left_proc_no,right_proc_no
    integer,dimension(Nz_max,Ny):: slice_proc
    logical:: debug,verbose
    integer par_status(MPI_STATUS_SIZE)
    integer,dimension(no_species):: stype_transfer
    left_i = 0
    right_i = 0

    ! determine the neighbors
    if (proc_no.eq.0) then
        left_proc_no = proc_max
    else
        left_proc_no = proc_no - 1
    end if

    if (proc_no.eq.proc_max) then
        right_proc_no = 0
    else
        right_proc_no = proc_no + 1
    end if
    N_temp = no_tot
    ! now loop through the particles and choose those which have left our region
    do i=no_tot,1,-1
        ! grid them
        iyx = int(y(i)/dy) + 1
        izx = int(z(i)/dz) + 1
        if (slice_proc(izx,iyx).ne.proc_no) then
            ! if ((y(i).lt.Ly_start.and.abs(y(i)-Ly_start).lt.20.0).or.(proc_no.eq.0.and.(y(i) - Ly_stop).gt.20.0)) then
            ! this particle needs to be shifted to the left
            if (slice_proc(izx,iyx).eq.left_proc_no) then
                if (debug) then
                    write(*,*) 'Left shift',y(i)
                end if
                left_i = left_i + 1
                ! copy the particle to the buffer
                left_z(left_i) = z(i)
                left_y(left_i) = y(i)
                left_uz(left_i) = uz(i)
                left_uy(left_i) = uy(i)
                left_ux(left_i) = ux(i)
                left_stype(left_i) = stype(i)
                ! eliminate the particle
                if (i<N_temp) then
                    !Replace the neutralized ion by the last one in the list.
                    z(i)=z(N_temp)
                    y(i)=y(N_temp)
                    uz(i)=uz(N_temp)
                    uy(i)=uy(N_temp)
                    ux(i)=ux(N_temp)

                    stype(i) = stype(N_temp)
                endif  !if (i<N_itemp) then
                N_temp=N_temp-1


                ! end if
                ! if ((y(i).gt.Ly_stop.and.abs(y(i) - Ly_stop).lt.20.0).or.(proc_no.eq.proc_max.and.(Ly_start - y(i)).gt.20.0)) then
            elseif (slice_proc(izx,iyx).eq.right_proc_no) then
                ! this particle needs to be shifted to the right
                if (debug) then
                    write(*,*) 'Right shift',y(i)
                end if

                right_i = right_i + 1
                ! copy the particle to the buffer
                right_z(right_i) = z(i)
                right_y(right_i) = y(i)
                right_uz(right_i) = uz(i)
                right_uy(right_i) = uy(i)
                right_ux(right_i) = ux(i)
                right_stype(right_i) = stype(i)
                ! eliminate the particle
                if (i<N_temp) then
                    !Replace the neutralized ion by the last one in the list.
                    z(i)=z(N_temp)
                    y(i)=y(N_temp)
                    uz(i)=uz(N_temp)
                    uy(i)=uy(N_temp)
                    ux(i)=ux(N_temp)

                    stype(i) = stype(N_temp)
                endif  !if (i<N_itemp) then
                N_temp=N_temp-1

            else
                write(*,*) 'Particle shift error!'
                write(*,*) 'Slice',proc_no
                write(*,*) 'Pos',stype(i),z(i),y(i)
                write(*,*) 'Vel',ux(i),uy(i),uz(i)
                write(*,*) 'index',izx,iyx
                write(*,*) 'Next slice',slice_proc(izx,iyx)
                read(*,*) iyx


            end if ! left/right
        end if ! out of slice
    end do ! particle loop
    no_tot = N_temp

    ! now the parallel part

    i = 31
    call MPI_SEND(left_i,1,MPI_INTEGER,left_proc_no,i*(left_proc_no +1),MPI_COMM_WORLD,ierr)
    if (debug) then
        write(*,*) proc_no,'Sending left i, proc:',left_i,left_proc_no,i*(left_proc_no  +1)
    end if
    ! get the right i
    i = 31

    call MPI_RECV(incoming_right_i,1,MPI_INTEGER,right_proc_no,i*(proc_no +1),MPI_COMM_WORLD,par_status,ierr)

    if (debug) then
        write(*,*) proc_no,'Received right i, proc:',incoming_right_i,proc_no,i*(proc_no  +1)
    end if


    ! now the particles
    ! this ain't so nice and simple since we cannot rely on buffering of messages
    ! we have to send and receive at the same time
    i = 32

    if (left_i.gt.0) then
        call MPI_SEND(left_y(1:left_i),left_i,MPI_DOUBLE_PRECISION,left_proc_no,i*(left_proc_no +1),MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Sending left y:',left_i
        end if

    end if
    call dummysub()
    if (incoming_right_i.gt.0) then
        call MPI_RECV(dumr(1:incoming_right_i),incoming_right_i,MPI_DOUBLE_PRECISION,right_proc_no,i*(proc_no +1),MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) proc_no,'Received  y:',incoming_right_i
        end if
        y((no_tot+1):(no_tot  + incoming_right_i)) = dumr(1:incoming_right_i)

    end if


    i = 33
    if (left_i.gt.0) then
        call MPI_SEND(left_z(1:left_i),left_i,MPI_DOUBLE_PRECISION,left_proc_no,i*(left_proc_no +1),MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Sending  z:',left_i,i
        end if

    end if
    if (incoming_right_i.gt.0) then
        if (debug) then
            write(*,*) proc_no,'Receiving  z:',incoming_right_i,i
        end if

        call MPI_RECV(dumr(1:incoming_right_i),incoming_right_i,MPI_DOUBLE_PRECISION,right_proc_no,i*(proc_no +1),MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) proc_no,'Received  z:',incoming_right_i,i
        end if
        z((no_tot+1):(no_tot  + incoming_right_i)) = dumr(1:incoming_right_i)

    end if
    ! ! check for the incoming particles
    ! do i=1,incoming_right_i
    ! if (slice_proc(int(left_z(i)/dz)+1,int(left_y(i)/dy) + 1).ne.proc_no) then
    !   write(*,*) 'Bad incoming particle!'
    !   write(*,*) 'Slice:',proc_no,Ly_start,Ly_stop
    !   write(*,*) 'Coming from the right',right_proc_no
    !   write(*,*) 'Pos',left_z(i),left_y(i)
    !     read(*,*) dy
    !
    ! end if
    !
    ! end do

    i = 34
    if (left_i.gt.0) then
        call MPI_SEND(left_ux(1:left_i),left_i,MPI_DOUBLE_PRECISION,left_proc_no,i*(left_proc_no +1),MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Sending  ux:',left_i
        end if

    end if
    if (incoming_right_i.gt.0) then
        call MPI_RECV(dumr(1:incoming_right_i),incoming_right_i,MPI_DOUBLE_PRECISION,right_proc_no,i*(proc_no +1),MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) proc_no,'Received  ux:',incoming_right_i
        end if
        ux((no_tot+1):(no_tot  + incoming_right_i)) = dumr(1:incoming_right_i)

    end if

    i = 35
    if (left_i.gt.0) then
        call MPI_SEND(left_uy(1:left_i),left_i,MPI_DOUBLE_PRECISION,left_proc_no,i*(left_proc_no +1),MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Sending  uy:',left_i
        end if

    end if
    if (incoming_right_i.gt.0) then
        call MPI_RECV(dumr(1:incoming_right_i),incoming_right_i,MPI_DOUBLE_PRECISION,right_proc_no,i*(proc_no +1),MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) proc_no,'Received  uy:',incoming_right_i
        end if
        uy((no_tot+1):(no_tot  + incoming_right_i)) = dumr(1:incoming_right_i)

    end if


    i = 36
    if (left_i.gt.0) then
        call MPI_SEND(left_uz(1:left_i),left_i,MPI_DOUBLE_PRECISION,left_proc_no,i*(left_proc_no +1),MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Sending  uz:',left_i
        end if

    end if
    if (incoming_right_i.gt.0) then
        call MPI_RECV(dumr(1:incoming_right_i),incoming_right_i,MPI_DOUBLE_PRECISION,right_proc_no,i*(proc_no +1),MPI_COMM_WORLD,par_status,ierr)
        if (debug) then
            write(*,*) proc_no,'Received  uz:',incoming_right_i
        end if
        uz((no_tot+1):(no_tot  + incoming_right_i)) = dumr(1:incoming_right_i)

    end if

    i = 37
    if (left_i.gt.0) then
        call MPI_SEND(int(left_stype(1:left_i)),left_i,MPI_INTEGER,left_proc_no,i*(left_proc_no +1),MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Sending left stype:',left_i
        end if

        !             write(*,*) proc_no,' sent left stype',left_stype(1:left_i)
    end if
    if (incoming_right_i.gt.0) then
        call MPI_RECV(dumi(1:incoming_right_i),incoming_right_i,MPI_INTEGER,right_proc_no,i*(proc_no +1),MPI_COMM_WORLD,par_status,ierr)
        ! 		left_stype(1:incoming_right_i) = dumi(1:incoming_right_i)
        if (debug) then
            write(*,*) proc_no,'Received right stype:'
        end if
        do i=1,incoming_right_i
            !           write(*,*) proc_no,'Running left stype change',left_stype(i),stype_transfer(left_stype(i))
            stype(no_tot + i) = stype_transfer(dumi(i))
        end do

    end if
    no_tot  = no_tot + incoming_right_i


    ! send the right i
    i = 41
    call MPI_SEND(right_i,1,MPI_INTEGER,right_proc_no,i*(right_proc_no +1),MPI_COMM_WORLD,ierr)
    if (debug) then
        write(*,*) proc_no,'Sending right i, proc:',right_i,right_proc_no,i*(right_proc_no +1)
    end if
    call MPI_RECV(incoming_left_i,1,MPI_INTEGER,left_proc_no,i*(proc_no +1),MPI_COMM_WORLD,par_status,ierr)

    if (debug) then
        write(*,*) proc_no,'Received left i, proc:',incoming_left_i,left_proc_no,i*(left_proc_no  +1)
    end if





    ! now the particles
    i = 43



    if (right_i.gt.0) then
        call MPI_SEND(right_y(1:right_i),right_i,MPI_DOUBLE_PRECISION,right_proc_no,i*(right_proc_no +1),MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Sending right z, proc:',right_i,right_proc_no,i*(right_proc_no +1)
        end if
    end if
    if (incoming_left_i.gt.0) then
        call MPI_RECV(dumr(1:incoming_left_i),incoming_left_i,MPI_DOUBLE_PRECISION,left_proc_no,i*(proc_no +1),MPI_COMM_WORLD,par_status,ierr)
        y((no_tot+1):(no_tot  + incoming_left_i)) = dumr(1:incoming_left_i)

    end if


    i = 44
    if (right_i.gt.0) then
        call MPI_SEND(right_z(1:right_i),right_i,MPI_DOUBLE_PRECISION,right_proc_no,i*(right_proc_no +1),MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Sending right ux, proc:',right_i,right_proc_no,i*(right_proc_no +1)
        end if
    end if


    if (incoming_left_i.gt.0) then
        call MPI_RECV(dumr(1:incoming_left_i),incoming_left_i,MPI_DOUBLE_PRECISION,left_proc_no,i*(proc_no +1),MPI_COMM_WORLD,par_status,ierr)
        z((no_tot+1):(no_tot  + incoming_left_i)) = dumr(1:incoming_left_i)

    end if
    ! do i=1,incoming_left_i
    ! if (slice_proc(int(right_z(i)/dz)+1,int(right_y(i)/dy) + 1).ne.proc_no) then
    !   write(*,*) 'Bad incoming particle!'
    !   write(*,*) 'Slice:',proc_no,Ly_start,Ly_stop
    !   write(*,*) 'Coming from the left',left_proc_no
    !   write(*,*) 'Pos',right_z(i),right_y(i)
    !     read(*,*) dy
    !
    ! end if
    !
    ! end do


    i = 45
    if (right_i.gt.0) then

        call MPI_SEND(right_ux(1:right_i),right_i,MPI_DOUBLE_PRECISION,right_proc_no,i*(right_proc_no +1),MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Sending right uy, proc:',right_i,right_proc_no,i*(right_proc_no +1)
        end if
    end if
    if (incoming_left_i.gt.0) then
        call MPI_RECV(dumr(1:incoming_left_i),incoming_left_i,MPI_DOUBLE_PRECISION,left_proc_no,i*(proc_no +1),MPI_COMM_WORLD,par_status,ierr)
        ux((no_tot+1):(no_tot  + incoming_left_i)) = dumr(1:incoming_left_i)

    end if


    i = 46
    if (right_i.gt.0) then


        call MPI_SEND(right_uy(1:right_i),right_i,MPI_DOUBLE_PRECISION,right_proc_no,i*(right_proc_no +1),MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Sending right uz, proc:',right_i,right_proc_no,i*(right_proc_no +1)
        end if
    end if
    if (incoming_left_i.gt.0) then
        call MPI_RECV(dumr(1:incoming_left_i),incoming_left_i,MPI_DOUBLE_PRECISION,left_proc_no,i*(proc_no +1),MPI_COMM_WORLD,par_status,ierr)
        uy((no_tot+1):(no_tot  + incoming_left_i)) = dumr(1:incoming_left_i)

    end if


    i = 47
    if (right_i.gt.0) then

        call MPI_SEND(right_uz(1:right_i),right_i,MPI_DOUBLE_PRECISION,right_proc_no,i*(right_proc_no +1),MPI_COMM_WORLD,ierr)
        if (debug) then
            write(*,*) proc_no,'Sending right stype, proc:',right_i,right_proc_no,i*(right_proc_no +1)
        end if
    end if
    if (incoming_left_i.gt.0) then
        call MPI_RECV(dumr(1:incoming_left_i),incoming_left_i,MPI_DOUBLE_PRECISION,left_proc_no,i*(proc_no +1),MPI_COMM_WORLD,par_status,ierr)
        uz((no_tot+1):(no_tot  + incoming_left_i)) = dumr(1:incoming_left_i)

    end if

    i = 48
    if (right_i.gt.0) then
        call MPI_SEND(int(right_stype(1:right_i)),right_i,MPI_INTEGER,right_proc_no,i*(right_proc_no +1),MPI_COMM_WORLD,ierr)
    end if
    if (incoming_left_i.gt.0) then
        call MPI_RECV(dumi(1:incoming_left_i),incoming_left_i,MPI_INTEGER,left_proc_no,i*(proc_no +1),MPI_COMM_WORLD,par_status,ierr)
        ! right_stype(1:incoming_left_i) = dumi(1:incoming_left_i)
        do i=1,incoming_left_i
            !           write(*,*) proc_no,'Running left stype change',left_stype(i),stype_transfer(left_stype(i))
            stype(no_tot + i) = stype_transfer(dumi(i))
        end do

    end if
    no_tot = no_tot + incoming_left_i



end subroutine



subroutine transfer_particles_2(Npts,no_tot,y,z,ux,uy,uz,vtot_init,use_vtot,stype,Ny,Nz_max,slice_proc,verbose,debug,proc_no,proc_max,dy,dz,no_species,stype_transfer,t_sum,t_grid,t_start,no_part)
    use spice2_types
    implicit none
    include 'mpif.h'
    !include 'struct.h'
    integer:: no_tot,Nz_max,Ny,i,sp,proc_no,proc_max,iyx,izx,shift_i,inc_i,N_temp,ierr,no_species,Npts,act_proc
    real*8, dimension(Npts):: y,z,ux,uy,uz,vtot_init
    real*8, dimension(int(Npts/10))::shift_y,shift_z,shift_ux,shift_uy,shift_uz,dumr,inc_y,inc_z,inc_ux,inc_uy,inc_uz,shift_vtot_init,inc_vtot_init
    integer*2, dimension(Npts):: stype
    integer*2, dimension(int(Npts/10)):: shift_stype,inc_stype

    integer,dimension(int(Npts/10)):: dumi
    real*8:: dy,dz
    integer:: left_proc_no,right_proc_no
    integer,dimension(Nz_max,Ny):: slice_proc
    logical:: debug,verbose
    integer par_status(MPI_STATUS_SIZE)
    integer,dimension(no_species):: stype_transfer,no_part
    integer*8:: timestamp
    integer*8:: t_sum,t_start,t_grid
    logical:: use_vtot
    shift_i = 0
    inc_i = 0

    N_temp = no_tot
    ! now loop through the particles and choose those which have left our region
    do i=no_tot,1,-1
        ! grid them
        iyx = int(y(i)/dy) + 1
        izx = int(z(i)/dz) + 1
        if (slice_proc(izx,iyx).ne.proc_no) then
            ! if ((y(i).lt.Ly_start.and.abs(y(i)-Ly_start).lt.20.0).or.(proc_no.eq.0.and.(y(i) - Ly_stop).gt.20.0)) then
            ! this particle needs to be shifted to the left
            shift_i = shift_i + 1
            ! copy the particle to the buffer
            shift_z(shift_i) = z(i)
            shift_y(shift_i) = y(i)
            shift_uz(shift_i) = uz(i)
            shift_uy(shift_i) = uy(i)
            shift_ux(shift_i) = ux(i)
            if (use_vtot) then
	      shift_vtot_init(shift_i) = vtot_init(i)
            end if
            shift_stype(shift_i) = stype(i)
            ! eliminate the particle

            no_part(stype(i)) =no_part(stype(i)) -1
            if (i<N_temp) then
                !Replace the neutralized ion by the last one in the list.
                z(i)=z(N_temp)
                y(i)=y(N_temp)
                uz(i)=uz(N_temp)
                uy(i)=uy(N_temp)
                ux(i)=ux(N_temp)
                if (use_vtot) then
		    vtot_init(i) = vtot_init(N_temp)
		end if
                stype(i) = stype(N_temp)
            endif  !if (i<N_itemp) then
            N_temp=N_temp-1


        end if ! out of slice
    end do ! particle loop
    no_tot = N_temp
    timestamp = 0
    call gettime(timestamp)

    t_grid = timestamp - t_sum - t_start
    ! t_grid= 0
    if (t_grid.lt.0) then
        t_grid = 0
    end if

    t_sum = t_sum + t_grid

    ! now the parallel part
    do act_proc=0,proc_max


        if (act_proc.eq.proc_no) then

            ! SEND DATA
            call MPI_BCAST(shift_i,1,MPI_INTEGER,proc_no,MPI_COMM_WORLD,ierr)
            if (debug) then
                write(*,*) proc_no,'Sending shift i, proc:',proc_no,shift_i
            end if
            if (shift_i.gt.0) then
                call MPI_BCAST(shift_y(1:shift_i),shift_i,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Sending shift y, proc:',proc_no
                end if
                call MPI_BCAST(shift_z(1:shift_i),shift_i,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Sending shift z, proc:',proc_no
                end if
                call MPI_BCAST(shift_ux(1:shift_i),shift_i,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Sending shift ux, proc:',proc_no
                end if
                call MPI_BCAST(shift_uy(1:shift_i),shift_i,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Sending shift uy, proc:',proc_no
                end if
                call MPI_BCAST(shift_uz(1:shift_i),shift_i,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Sending shift uz, proc:',proc_no
                end if
                if (use_vtot) then
                call MPI_BCAST(shift_vtot_init(1:shift_i),shift_i,MPI_DOUBLE_PRECISION,proc_no,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Sending shift vtot, proc:',proc_no
                end if
                end if
                call MPI_BCAST(int(shift_stype(1:shift_i)),shift_i,MPI_INTEGER,proc_no,MPI_COMM_WORLD,ierr)

                if (debug) then
                    write(*,*) proc_no,'Sending shift stype, proc:',proc_no
                end if
            end if




        else


            ! RECEIVE DATA

            ! get the right i

            call MPI_BCAST(inc_i,1,MPI_INTEGER,act_proc,MPI_COMM_WORLD,ierr)

            if (debug) then
                write(*,*) proc_no,'Received inc i, proc:',inc_i
            end if

            if (inc_i.gt.0) then
                call MPI_BCAST(inc_y(1:inc_i),inc_i,MPI_DOUBLE_PRECISION,act_proc,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Received  y:'
                end if

                call MPI_BCAST(inc_z(1:inc_i),inc_i,MPI_DOUBLE_PRECISION,act_proc,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Received  z:'
                end if
                call MPI_BCAST(inc_ux(1:inc_i),inc_i,MPI_DOUBLE_PRECISION,act_proc,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Received  ux:'
                end if
                call MPI_BCAST(inc_uy(1:inc_i),inc_i,MPI_DOUBLE_PRECISION,act_proc,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Received  uy:'
                end if
                call MPI_BCAST(inc_uz(1:inc_i),inc_i,MPI_DOUBLE_PRECISION,act_proc,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Received  uz:'
                end if
                if (use_vtot) then
                call MPI_BCAST(inc_vtot_init(1:inc_i),inc_i,MPI_DOUBLE_PRECISION,act_proc,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Received  vtot:'
                end if
		end if
                call MPI_BCAST(dumi(1:inc_i),inc_i,MPI_INTEGER,act_proc,MPI_COMM_WORLD,ierr)
                if (debug) then
                    write(*,*) proc_no,'Received  stype:'
                end if
                inc_stype(1:inc_i) = stype_transfer(dumi(1:inc_i))

                ! run through the incoming particles and select those which belong to our slice


                do i=1,inc_i
                    iyx = int(inc_y(i)/dy) + 1
                    izx = int(inc_z(i)/dz) + 1
                    if (slice_proc(izx,iyx).eq.proc_no) then
                        ! add this particle to our stack
                        no_tot = no_tot + 1
                        z(no_tot) = inc_z(i)
                        y(no_tot) = inc_y(i)
                        ux(no_tot) = inc_ux(i)
                        uy(no_tot) = inc_uy(i)
                        uz(no_tot) = inc_uz(i)
                        if (use_vtot) then
			  vtot_init(no_tot) = inc_vtot_init(i)
                        end if
                        stype(no_tot) = inc_stype(i)
                        no_part(stype(no_tot)) = no_part(stype(no_tot)) +1
                    end if
                end do

            end if

        end if ! proc_no
    end do ! proc

end subroutine

! star communication - more effective
! call       transfer_particles_3(Npts,no_tot,y,z,ux,uy,uz,stype,Ny,Nz_max,slice_proc,verbose,debug,proc_no,proc_max,dy,dz,no_species,stype_transfer,neigh_slices,t_sum,t_grid,t_start)

subroutine transfer_particles_3(Npts,no_tot,y,z,ux,uy,uz,stype,Ny,Nz_max,slice_proc,verbose,debug,proc_no,proc_max,&
        dy,dz,no_species,stype_transfer,neigh_slices,t_sum,t_grid,t_start,no_part)
    use spice2_types
    implicit none
    include 'mpif.h'
    !include 'struct.h'
    integer:: no_tot,Nz_max,Ny,i,sp,proc_no,proc_max,iyx,izx,inc_i,N_temp,ierr,no_species,Npts,act_proc,k,l,j
    integer, dimension(8):: shift_i
    real*8, dimension(int(Npts/100),8):: shift_y,shift_z,shift_ux,shift_uy,shift_uz
    real*8, dimension(Npts)::y,z,ux,uy,uz
    real*8, dimension(int(Npts/100)):: dumr,inc_y,inc_z,inc_ux,inc_uy,inc_uz
    integer*2, dimension(int(Npts/100),8):: shift_stype
    integer*2, dimension(Npts):: stype
    integer*2, dimension(int(Npts/100)):: inc_stype
    integer, dimension(no_species):: no_part
    integer,dimension(int(Npts/100)):: dumi
    real*8:: dy,dz
    integer:: left_proc_no,right_proc_no,next_slice
    integer,dimension(Nz_max,Ny):: slice_proc
    logical:: debug,verbose,found_slice
    integer par_status(MPI_STATUS_SIZE)
    integer,dimension(no_species):: stype_transfer
    integer, dimension(8):: neigh_slices
    integer*8:: t_sum, t_grid,t_start
    integer*8:: timestamp

    shift_i = 0
    inc_i = 0
    ! write(*,*) 'Nz_max',Nz_max
    !  write (*,*) proc_no,'lines - transfer:', slice_proc(40,:)
    !  write (*,*) proc_no,'lines - transfer:', slice_proc(60,:)

    N_temp = no_tot
    ! now loop through the particles and choose those which have left our region
    do i=no_tot,1,-1
        ! grid them

        iyx = int(y(i)/dy) + 1
        izx = int(z(i)/dz) + 1
        ! if (izx.gt.Nz_max) then
        ! write(*,*) proc_no,'Particle out of boudanry'
        ! write(*,*) proc_no,'Pos:',z(i),y(i)
        ! stop
        !
        ! end if
        if (slice_proc(izx,iyx).ne.proc_no) then
            ! check if we are not out of neighbors
            found_slice = .false.
            do k=1,8
                if (slice_proc(izx,iyx).eq.neigh_slices(k)) then
                    found_slice = .true.
                    next_slice = k
                    if (debug) then
                        write(*,*) proc_no,'Next slice:',k,z(i),y(i)
                    end if
                end if
            end do

            if (found_slice) then
                ! if ((y(i).lt.Ly_start.and.abs(y(i)-Ly_start).lt.20.0).or.(proc_no.eq.0.and.(y(i) - Ly_stop).gt.20.0)) then
                ! this particle needs to be shifted to the left
                ! no_part(stype(i)) =no_part(stype(i)) -1
                shift_i(next_slice) = shift_i(next_slice) + 1
                ! copy the particle to the buffer
                shift_z(shift_i(next_slice),next_slice) = z(i)
                shift_y(shift_i(next_slice),next_slice) = y(i)
                shift_uz(shift_i(next_slice),next_slice) = uz(i)
                shift_uy(shift_i(next_slice),next_slice) = uy(i)
                shift_ux(shift_i(next_slice),next_slice) = ux(i)
                shift_stype(shift_i(next_slice),next_slice) = stype(i)
                ! if (debug) then
                !      write(*,*) proc_no,'tranfer particle to',slice_proc(izx,iyx)
                !      write(*,*) proc_no,'transfer position',z(i),y(i)
                !      write(*,*) proc_no,'transfer neighbor',next_slice
                !
                !
                ! end if
                ! eliminate the particle
            else
                write(*,*) proc_no,'Supefast particle eliminated'
                write(*,*) proc_no, 'Pos',z(i),y(i)
                write(*,*) proc_no, 'Vel',ux(i),uy(i),uz(i)
                write(*,*) proc_no, 'stype',stype(i)
                write(*,*) proc_no, 'Next slice',slice_proc(izx,iyx)
            end if
            no_part(stype(i)) =no_part(stype(i)) -1
            if (i<N_temp) then
                !Replace the neutralized ion by the last one in the list.
                z(i)=z(N_temp)
                y(i)=y(N_temp)
                uz(i)=uz(N_temp)
                uy(i)=uy(N_temp)
                ux(i)=ux(N_temp)

                stype(i) = stype(N_temp)
            endif  !if (i<N_itemp) then
            N_temp=N_temp-1
        end if
    end do ! particle loop

    no_tot = N_temp
    timestamp = 0
    !call gettime(timestamp)

    !t_grid = timestamp - t_sum - t_start
    t_grid= 0
    if (abs(t_grid).gt.1000e6) then
        t_grid = 0
    end if

    t_sum = t_sum + t_grid

    ! hack - no send
    ! shift_i = 0

    ! now the parallel part
    j = 9
    do k=1,8
        j = j -1

        if (debug) then
            write(*,*) proc_no,'Slices',k,j
            write(*,*) proc_no,'Comm --->',neigh_slices(k),shift_i(k)
            write(*,*) proc_no,'Comm <---',neigh_slices(j)
        end if

        i = 331

        if (neigh_slices(k).gt.-1.and.neigh_slices(k).ne.proc_no) then
            ! SEND SEND DATA TO NEXT PROC
            call MPI_SEND(shift_i(k),1,MPI_INTEGER,neigh_slices(k),i,MPI_COMM_WORLD,ierr)
            if (debug) then
                write(*,*) proc_no,'Sending shift i, proc:',shift_i(k),neigh_slices(k)
            end if
        end if

        if (neigh_slices(j).gt.-1.and.neigh_slices(j).ne.proc_no) then
            ! RECEIVED DATA FROM PREVIOUS PROC
            if (debug) then
                write(*,*) proc_no,'Expecting shift i from proc:',neigh_slices(j)
            end if

            call MPI_RECV(inc_i,1,MPI_INTEGER,neigh_slices(j),i,MPI_COMM_WORLD,par_status,ierr)
            if (debug) then
                write(*,*) proc_no,'Received shift i, proc:',inc_i,neigh_slices(j)
            end if
        end if

        i = 332

        if (neigh_slices(k).gt.-1.and.neigh_slices(k).ne.proc_no.and.shift_i(k).gt.0) then
            ! SEND & RECV DATA
            if (debug) then
                write(*,*) proc_no,'Sending shift y, proc:',shift_i(k),neigh_slices(k),k
            end if

            call MPI_SEND(shift_y(1:shift_i(k),k),shift_i(k),MPI_DOUBLE_PRECISION,neigh_slices(k),i,MPI_COMM_WORLD,ierr)
            if (debug) then
                write(*,*) proc_no,'Sent	 shift y, proc:',shift_i(k),neigh_slices(k),k
            end if

        end if
        if (neigh_slices(j).gt.-1.and.neigh_slices(j).ne.proc_no.and.inc_i.gt.0) then
            if (debug) then
                write(*,*) proc_no,'Expecting shift y, proc:',inc_i,neigh_slices(j)
            end if
            call MPI_RECV(dumr(1:inc_i),inc_i,MPI_DOUBLE_PRECISION,neigh_slices(j),i,MPI_COMM_WORLD,par_status,ierr)
            y((no_tot+1):(no_tot + inc_i)) = dumr(1:inc_i)
            if (debug) then
                write(*,*) proc_no,'Received shift y, proc:',inc_i,neigh_slices(j)
            end if
        end if


        i = 333

        if (neigh_slices(k).gt.-1.and.neigh_slices(k).ne.proc_no.and.shift_i(k).gt.0) then
            ! SEND & RECV DATA

            call MPI_SEND(shift_z(1:shift_i(k),k),shift_i(k),MPI_DOUBLE_PRECISION,neigh_slices(k),i,MPI_COMM_WORLD,ierr)
            if (debug) then
                write(*,*) proc_no,'Sending shift z, proc:',shift_i(k),neigh_slices(k),shift_z(1:shift_i(k),k)
            end if
        end if
        if (neigh_slices(j).gt.-1.and.neigh_slices(j).ne.proc_no.and.inc_i.gt.0) then
            call MPI_RECV(dumr(1:inc_i),inc_i,MPI_DOUBLE_PRECISION,neigh_slices(j),i,MPI_COMM_WORLD,par_status,ierr)
            z((no_tot+1):(no_tot + inc_i)) = dumr(1:inc_i)
            if (debug) then
                write(*,*) proc_no,'Received shift z, proc:',inc_i,neigh_slices(j),dumr(1:inc_i)
            end if
        end if


        i = 334

        if (neigh_slices(k).gt.-1.and.neigh_slices(k).ne.proc_no.and.shift_i(k).gt.0) then
            ! SEND & RECV DATA

            call MPI_SEND(shift_ux(1:shift_i(k),k),shift_i(k),MPI_DOUBLE_PRECISION,neigh_slices(k),i,MPI_COMM_WORLD,ierr)
            if (debug) then
                write(*,*) proc_no,'Sending shift ux, proc:',shift_i(k),neigh_slices(k)
            end if
        end if
        if (neigh_slices(j).gt.-1.and.neigh_slices(j).ne.proc_no.and.inc_i.gt.0) then
            call MPI_RECV(dumr(1:inc_i),inc_i,MPI_DOUBLE_PRECISION,neigh_slices(j),i,MPI_COMM_WORLD,par_status,ierr)
            ux((no_tot+1):(no_tot + inc_i)) = dumr(1:inc_i)
            if (debug) then
                write(*,*) proc_no,'Received shift ux, proc:',inc_i,neigh_slices(j)
            end if
        end if


        i = 335

        if (neigh_slices(k).gt.-1.and.neigh_slices(k).ne.proc_no.and.shift_i(k).gt.0) then
            ! SEND & RECV DATA

            call MPI_SEND(shift_uy(1:shift_i(k),k),shift_i(k),MPI_DOUBLE_PRECISION,neigh_slices(k),i,MPI_COMM_WORLD,ierr)
            if (debug) then
                write(*,*) proc_no,'Sending shift uy, proc:',shift_i(k),neigh_slices(k)
            end if
        end if
        if (neigh_slices(j).gt.-1.and.neigh_slices(j).ne.proc_no.and.inc_i.gt.0) then
            call MPI_RECV(dumr(1:inc_i),inc_i,MPI_DOUBLE_PRECISION,neigh_slices(j),i,MPI_COMM_WORLD,par_status,ierr)
            uy((no_tot+1):(no_tot + inc_i)) = dumr(1:inc_i)
            if (debug) then
                write(*,*) proc_no,'Received shift uy, proc:',inc_i,neigh_slices(j)
            end if
        end if

        i = 336

        if (neigh_slices(k).gt.-1.and.neigh_slices(k).ne.proc_no.and.shift_i(k).gt.0) then
            ! SEND & RECV DATA

            call MPI_SEND(shift_uz(1:shift_i(k),k),shift_i(k),MPI_DOUBLE_PRECISION,neigh_slices(k),i,MPI_COMM_WORLD,ierr)
            if (debug) then
                write(*,*) proc_no,'Sending shift uz, proc:',shift_i(k),neigh_slices(k)
            end if
        end if
        if (neigh_slices(j).gt.-1 .and.neigh_slices(j).ne.proc_no.and.inc_i.gt.0) then
            call MPI_RECV(dumr(1:inc_i),inc_i,MPI_DOUBLE_PRECISION,neigh_slices(j),i,MPI_COMM_WORLD,par_status,ierr)
            uz((no_tot+1):(no_tot + inc_i)) = dumr(1:inc_i)
            if (debug) then
                write(*,*) proc_no,'Received shift uz, proc:',inc_i,neigh_slices(j)
            end if
        end if

        i = 337

        if (neigh_slices(k).gt. -1 .and.neigh_slices(k).ne.proc_no.and.shift_i(k).gt.0) then
            ! SEND & RECV DATA

            call MPI_SEND(int(shift_stype(1:shift_i(k),k)),shift_i(k),MPI_INTEGER,neigh_slices(k),i,MPI_COMM_WORLD,ierr)
            if (debug) then
                write(*,*) proc_no,'Sending shift stype, proc:',shift_i(k),neigh_slices(k)
            end if
        end if
        if (neigh_slices(j).gt.-1.and.neigh_slices(j).ne.proc_no.and.inc_i.gt.0) then
            call MPI_RECV(dumi(1:inc_i),inc_i,MPI_INTEGER,neigh_slices(j),i,MPI_COMM_WORLD,par_status,ierr)
            ! no need to do this anymore
            !                      do l=1,inc_i
            !                         stype(no_tot+l) = stype_transfer(dumi(l))
            !                      end do
            stype((no_tot+1):(no_tot + inc_i)) = dumi(1:inc_i)
            ! fix no_part
            do i=1,inc_i
                no_part(dumi(i)) = no_part(dumi(i)) +1
            end do

            if (debug) then
                write(*,*) proc_no,'Received shift stype, proc:',inc_i,neigh_slices(j)
            end if
            no_tot = no_tot + inc_i
            if(no_tot.gt.Npts) then
                write(*,*) proc_no,'Error, particle vector exhausted (particle transfer)'
                write(*,*) proc_no,'Increase the npts_ratio parameter in the input file and restart the simulation with -c flag!'
                write(*,*) proc_no, 'Emergency halt'
                stop
            end if


        end if


    end do ! proc
    if (debug) then
        write(*,*) 'Done in particle transfer'
    end if
end subroutine
