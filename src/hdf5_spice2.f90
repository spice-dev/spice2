module hdf5_spice2

    use hdf5_io
    use spice2_types

    implicit none

contains

    subroutine h5_SAVE_RESULTS2(dt,N0,Nv,Nz,Ny,z_g,y_g,dens,Epar,Eperp,Potav,q_grid,T_g,vzav,vyav,vxav,vz2av,vy2av,vx2av,vz3av,&
            vy3av,vx3av,Np,t,dz,Iinj,ofile,debug,no_spec,nav,edge_flux,edge_energy_flux,edge_velocity_x,edge_velocity_y,edge_velocity_z,use_compression_i,domain_decomp,proc_max,temperature)

        implicit none

        real*8:: dt,dz

        real*8, dimension(Nz):: z_g
        real*8, dimension(Ny):: y_g

        real*8, dimension(no_spec):: Iinj
        integer:: N_e,N_i,Nz,Nv,Np,N0,Ny,no_spec,sp,icr,version,IERR,use_compression_i,proc_max,specie_count
        logical:: time_diag,debug
        real*8, dimension(Nz,Ny):: Potav,Epar,Eperp
        real*8, dimension(no_spec,Nz,Ny):: vzav, vyav,vxav,vz2av,vy2av,vx2av,vz3av,vy3av,vx3av,dens,q_grid, T_g,temperature
        real*8, dimension(no_spec,Nz,Ny):: vzavx, vyavx,vxavx,vz2avx,vy2avx,vx2avx,vz3avx,vy3avx,vx3avx,densx,q_gridx, T_gx,temperature_x

        real*8, dimension(Np):: t
        real*8, dimension(Nz,Ny,no_spec)::edge_flux,edge_energy_flux,edge_velocity_x,edge_velocity_y,edge_velocity_z
        real*8, dimension(Nz,Ny,no_spec)::edge_fluxx,edge_energy_fluxx,edge_velocity_xx,edge_velocity_yx,edge_velocity_zx

        logical:: domain_decomp,specie_recomp
        !dimension Iinji(Np)
        !dimension Iinje(Np)
        integer*8:: ifile
        character*2048:: ofile
        character*2:: sp_str
        real*8:: ang_bin
        integer:: abin,nav

        integer(HID_T) :: file_id       ! File identifier
        integer :: h5err
        logical :: use_compression

        use_compression = use_compression_i .ge. 1

        !write(*,*) 'Temperatue csum',sum(temperature(1,:,:)),sum(temperature(2,:,:))

        !if (monoi) then
        !   filename='shRDmonoi_tile.mat'//char(0)
        !else
        !   filename='shRDarbitr_tile.mat'//char(0)
        !endif
        ifile = 0
        icr = 0
        version = 50
        densx = 0.0
        vxavx = 0.0
        vyavx = 0.0
        vzavx = 0.0
        vx2avx = 0.0
        vy2avx = 0.0
        vz2avx = 0.0
        vx3avx = 0.0
        vy3avx = 0.0
        vz3avx = 0.0
        T_gx = 0.0
        q_gridx = 0.0
        edge_fluxx = 0.0
        edge_energy_fluxx = 0.0
        edge_velocity_xx = 0.0
        edge_velocity_yx = 0.0
        edge_velocity_zx = 0.0

        specie_recomp = .false.
        write (6,*) 'Writing results to ',trim(ofile)//'.h5'

        ! initialize HDF5
        call h5open_f(h5err)
        ! create HDF5 file
        call h5fcreate_f(trim(ofile)//'.h5'//char(0), H5F_ACC_TRUNC_F, file_id, h5err)

        !MK changed order of saving to allow restore
        ! first goes the file version
        call hdf5_w_i(file_id, version, 'version'//char(0))
        ! then the number of species
        call hdf5_w_i(file_id, no_spec, 'no_spec'//char(0))

        !then dimensions of the grid
        call hdf5_w_i(file_id, Nz, 'Nz'//char(0))
        call hdf5_w_i(file_id, Ny, 'Ny'//char(0))

        ! number of steps taken
        call hdf5_w_i(file_id, Np, 'Np'//char(0))

        ! number of samples to diag taken
        call hdf5_w_i(file_id, nav, 'nav'//char(0))

        !then some useful 1D parameters
        call hdf5_w_d(file_id, dt, 'dt'//char(0))
        call hdf5_w_d(file_id, dz, 'dz'//char(0))

        !angular
        call hdf5_w_d(file_id, ang_bin, 'angbin'//char(0))

        !now the grid coordinates
        call hdf5_w_d1d(file_id, use_compression, Nz, z_g, 'z'//char(0))
        call hdf5_w_d1d(file_id, use_compression, Ny, y_g, 'y'//char(0))

        ! general grid diag
        call hdf5_w_d2d(file_id, use_compression, Nz, Ny, Epar, 'Epar'//char(0))
        call hdf5_w_d2d(file_id, use_compression, Nz, Ny, Eperp, 'Eperp'//char(0))
        call hdf5_w_d2d(file_id, use_compression, Nz, Ny, Potav, 'Potav'//char(0))

        !and finally we go specie by specie and write it's diag into the file
        specie_count = 0
        do sp=1,no_spec
            ! sp_str is sp but it is a char
            sp_str = '  '
            write(sp_str(1:2),'(I2.2)') sp
            if (specie_recomp) then
                densx(specie_count*(proc_max+1) + proc_max + 1,:,:) = densx(specie_count*(proc_max+1) + proc_max + 1,:,:) + dens(sp,:,:)
                q_gridx(specie_count*(proc_max+1) + proc_max + 1,:,:) = q_gridx(specie_count*(proc_max+1) + proc_max + 1,:,:) + q_grid(sp,:,:)
                T_gx(specie_count*(proc_max+1) + proc_max + 1,:,:) = T_gx(specie_count*(proc_max+1) + proc_max + 1,:,:) + T_g(sp,:,:)
                vzavx(specie_count*(proc_max+1) + proc_max + 1,:,:) = vzavx(specie_count*(proc_max+1) + proc_max + 1,:,:) + vzav(sp,:,:)
                vyavx(specie_count*(proc_max+1) + proc_max + 1,:,:) = vyavx(specie_count*(proc_max+1) + proc_max + 1,:,:) + vyav(sp,:,:)
                vxavx(specie_count*(proc_max+1) + proc_max + 1,:,:) = vxavx(specie_count*(proc_max+1) + proc_max + 1,:,:) + vxav(sp,:,:)
                vz2avx(specie_count*(proc_max+1) + proc_max + 1,:,:) = vz2avx(specie_count*(proc_max+1) + proc_max + 1,:,:) + vz2av(sp,:,:)
                vy2avx(specie_count*(proc_max+1) + proc_max + 1,:,:) = vy2avx(specie_count*(proc_max+1) + proc_max + 1,:,:) + vy2av(sp,:,:)
                vx2avx(specie_count*(proc_max+1) + proc_max + 1,:,:) = vx2avx(specie_count*(proc_max+1) + proc_max + 1,:,:) + vx2av(sp,:,:)
                vz3avx(specie_count*(proc_max+1) + proc_max + 1,:,:) = vz3avx(specie_count*(proc_max+1) + proc_max + 1,:,:) + vz3av(sp,:,:)
                vy3avx(specie_count*(proc_max+1) + proc_max + 1,:,:) = vy3avx(specie_count*(proc_max+1) + proc_max + 1,:,:) + vy3av(sp,:,:)
                vx3avx(specie_count*(proc_max+1) + proc_max + 1,:,:) = vx3avx(specie_count*(proc_max+1) + proc_max + 1,:,:) + vx3av(sp,:,:)
                edge_fluxx(:,:,specie_count*(proc_max+1) + proc_max + 1) = edge_fluxx(:,:,specie_count*(proc_max+1) + proc_max + 1) + edge_flux(:,:,sp)
                edge_energy_fluxx(:,:,specie_count*(proc_max+1) + proc_max + 1) = edge_energy_fluxx(:,:,specie_count*(proc_max+1) + proc_max + 1) + edge_energy_flux(:,:,sp)
                edge_velocity_xx(:,:,specie_count*(proc_max+1) + proc_max + 1) = edge_velocity_xx(:,:,specie_count*(proc_max+1) + proc_max + 1) + edge_velocity_x(:,:,sp)
                edge_velocity_yx(:,:,specie_count*(proc_max+1) + proc_max + 1) = edge_velocity_yx(:,:,specie_count*(proc_max+1) + proc_max + 1) + edge_velocity_y(:,:,sp)
                edge_velocity_zx(:,:,specie_count*(proc_max+1) + proc_max + 1) = edge_velocity_zx(:,:,specie_count*(proc_max+1) + proc_max + 1) + edge_velocity_z(:,:,sp)
                temperature_x(specie_count*(proc_max+1) + proc_max + 1,:,:) = temperature_x(specie_count*(proc_max+1) + proc_max + 1,:,:) + temperature(sp,:,:)


                if (sp.eq.int(real(sp)/real(proc_max+1))*(proc_max+1)) then
                    specie_count = specie_count + 1
                    sp_str = '  '
                    write(sp_str(1:2),'(I2.2)') specie_count

                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, densx(specie_count*(proc_max+1),:,:), 'dens'//sp_str//char(0))
                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, q_gridx(specie_count*(proc_max+1),:,:), 'q'//sp_str//char(0))

                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, T_gx(specie_count*(proc_max+1),:,:), 'T'//sp_str//char(0))

                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, vzavx(specie_count*(proc_max+1),:,:), 'vzav'//sp_str//char(0))
                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, vyavx(specie_count*(proc_max+1),:,:), 'vyav'//sp_str//char(0))
                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, vxavx(specie_count*(proc_max+1),:,:), 'vxav'//sp_str//char(0))

                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, vz2avx(specie_count*(proc_max+1),:,:), 'vz2av'//sp_str//char(0))
                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, vy2avx(specie_count*(proc_max+1),:,:), 'vy2av'//sp_str//char(0))
                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, vx2avx(specie_count*(proc_max+1),:,:), 'vx2av'//sp_str//char(0))

                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, vz3avx(specie_count*(proc_max+1),:,:), 'vz3av'//sp_str//char(0))
                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, vy3avx(specie_count*(proc_max+1),:,:), 'vy3av'//sp_str//char(0))
                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, vx3avx(specie_count*(proc_max+1),:,:), 'vx3av'//sp_str//char(0))

                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, temperature_x(specie_count*(proc_max+1),:,:), 'temperature'//sp_str//char(0))

                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, edge_fluxx(:,:,specie_count*(proc_max+1)), 'edgeflux'//sp_str//char(0))
                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, edge_energy_fluxx(:,:,specie_count*(proc_max+1)), 'edgeenergyflux'//sp_str//char(0))

                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, edge_velocity_xx(:,:,specie_count*(proc_max+1)), 'edgevelocityx'//sp_str//char(0))
                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, edge_velocity_yx(:,:,specie_count*(proc_max+1)), 'edgevelocityy'//sp_str//char(0))
                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, edge_velocity_zx(:,:,specie_count*(proc_max+1)), 'edgevelocityz'//sp_str//char(0))
                end if
            else
                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, dens(sp,:,:), 'dens'//sp_str//char(0))
                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, q_grid(sp,:,:), 'q'//sp_str//char(0))

                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, T_g(sp,:,:), 'T'//sp_str//char(0))

                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, vzav(sp,:,:), 'vzav'//sp_str//char(0))
                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, vyav(sp,:,:), 'vyav'//sp_str//char(0))
                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, vxav(sp,:,:), 'vxav'//sp_str//char(0))

                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, vz2av(sp,:,:), 'vz2av'//sp_str//char(0))
                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, vy2av(sp,:,:), 'vy2av'//sp_str//char(0))
                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, vx2av(sp,:,:), 'vx2av'//sp_str//char(0))

                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, vz3av(sp,:,:), 'vz3av'//sp_str//char(0))
                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, vy3av(sp,:,:), 'vy3av'//sp_str//char(0))
                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, vx3av(sp,:,:), 'vx3av'//sp_str//char(0))

                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, temperature(sp,:,:), 'temperature'//sp_str//char(0))

                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, edge_flux(:,:,sp), 'edgeflux'//sp_str//char(0))
                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, edge_energy_flux(:,:,sp), 'edgeenergyflux'//sp_str//char(0))

                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, edge_velocity_x(:,:,sp), 'edgevelocityx'//sp_str//char(0))
                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, edge_velocity_y(:,:,sp), 'edgevelocityy'//sp_str//char(0))
                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, edge_velocity_z(:,:,sp), 'edgevelocityz'//sp_str//char(0))

            end if

        end do

        call hdf5_w_i(file_id, N0, 'N0'//char(0))


        domain_decomp = .true.

        ! close HDF5 file
        call h5fclose_f(file_id, h5err)
        ! shutdown HDF5
        call h5close_f(h5err)

        return
    END

    subroutine h5_LANG_MONITOR(no_spec,snumber,Nz,Ny,Nv,z_g,y_g,rho_tot,Escz,Escy,Pot,count,t,Esct,dPHIqn,bx,by,bz,tfile,debug,Nt,&
            pot_chi,Potvac,no_part_proc,proc_max,h_pos,no_diag_reg,diag_regions,diag_histories,diag_hist,fv_arrays,fv_ar,hist_limit,Np,Na,&
            fv_bin,fv_sum,fv_limits,u_perp_a,fv_perp_array_count,N0,flag_m,equipot_m,n,flag,iteration_time_hist,injection_rate_hist,objects,&
            edges,edge_charge,rho,diagm,nobjects,objects_enum,objects_current,objects_power_flux,ksi,tau,SOL_W,SOL_Ns,use_compression_i,&
            i_rel_history,float_constant,Nc,mks_info,alpha_yz,alpha_xz,mu,dz,dt,m,q,Temp,time_history,Nz_max,slice_proc,domain_decomp,surface_matrix,delta_h,Npc,total_energy)
        use ap_utils

        implicit none

        !include "struct.h"
        type(mks_t):: mks_info
        integer:: Nz,Nt,Ny,Nv,Np,proc_max,count,icr,no_spec,h_pos,Na,nobjects,IERR,version,use_compression_i,Nc,Nz_max,specie_count
        real*8, dimension(Nz,Ny)::Escz,Escy,Pot,Potvac,rho_tot,surface_matrix

        real*8, dimension(nobjects,10):: i_rel_history
        real*8, dimension(nobjects):: float_constant
        real*8, dimension(Nz):: z_g
        real*8, dimension(Ny):: y_g
        real*8, dimension(h_pos):: t,Esct,dPHIqn,pot_chi
        real*8:: bx,by,bz,erreur2,s,alpha_yz,alpha_xz,dz,dt
        integer:: sp,delta_h,Npc
        integer, dimension(no_spec,h_pos):: snumber
        integer, dimension(proc_max +1,h_pos):: no_part_proc
        integer*8:: ifile
        character*2048:: tfile
        logical:: debug,domain_decomp,specie_recomp
        !diag regions
        integer:: no_diag_reg, diag_histories,fv_arrays,fv_bin,fv_perp_array_count
        integer, dimension(diag_histories):: hist_limit
        real*8, dimension(diag_histories,Np - Nc):: diag_hist
        real*8, dimension(fv_arrays,fv_bin):: fv_ar
        type(diag_region), dimension(no_diag_reg):: diag_regions
        integer:: diag_count,hist_count,fv_count,hist_length,fv_pa_count
        integer, dimension(fv_arrays):: fv_sum
        real*8, dimension(fv_arrays,2):: fv_limits
        real*8, dimension(fv_perp_array_count,fv_bin,fv_bin):: u_perp_a
        real*8:: fvn,ksi,tau,mu
        integer:: N0
        integer, dimension(Nz,Ny):: flag_m,objects, edges,objects_enum
        real*8, dimension(Nz,Ny):: equipot_m,edge_charge
        integer:: n
        integer, dimension(n - Nz):: flag
        real*8, dimension(proc_max +1,h_pos):: iteration_time_hist
        real*8, dimension(no_spec,h_pos):: injection_rate_hist
        real*8, dimension(no_spec,Nz,Ny):: SOL_Ns,SOL_W
        real*8, dimension(no_spec,Nz_max,Ny):: rho

        real*8, dimension(no_spec,Nz,Ny):: SOL_Nsx,SOL_Wx
        real*8, dimension(no_spec,Nz_max,Ny):: rhox

        character*2:: sp_str
        integer, dimension(Nz,Ny):: diagm
        real*8, dimension(2,nobjects,h_pos):: objects_current,objects_power_flux


        real*8, dimension(no_spec):: m,q,Temp
        real*8, dimension(11,count):: time_history

        integer, dimension(Nz_max,Ny):: slice_proc
        real*8, dimension(no_spec,4,Np):: total_energy

        integer(HID_T) :: file_id       ! File identifier
        integer :: h5err

        logical :: use_compression

        use_compression = use_compression_i .ge. 1


        sp_str = '  '
        icr  = 0
        ifile = 0
        version = 50
        specie_recomp = .false.

        write(*,*) 'Entering Lang. Monitor, writting to file ', trim(tfile)//'.h5'

        ! initialize HDF5
        call h5open_f(h5err)
        ! create HDF5 file
        call h5fcreate_f(trim(tfile)//'.h5'//char(0), H5F_ACC_TRUNC_F, file_id, h5err)

        if (debug) then
            write(*,*) 'h5fcreate_f ', h5err
        end if

        call hdf5_w_i(file_id, version, 'version'//char(0))

        call hdf5_w_i(file_id, Nz, 'Nz'//char(0))
        call hdf5_w_i(file_id, Nz_max, 'Nzmax'//char(0))
        call hdf5_w_i(file_id, Ny, 'Ny'//char(0))
        call hdf5_w_i(file_id, count, 'count'//char(0))
        call hdf5_w_i(file_id, h_pos, 'hpos'//char(0))
        call hdf5_w_i(file_id, delta_h, 'deltah'//char(0))
        call hdf5_w_i(file_id, Npc, 'Npc'//char(0))

        call hdf5_w_d(file_id, dt, 'dt'//char(0))
        call hdf5_w_d(file_id, dz, 'dz'//char(0))

        call hdf5_w_i(file_id, proc_max + 1, 'nproc'//char(0))

        call hdf5_w_d1d(file_id, use_compression, no_spec, q, 'q'//char(0))
        call hdf5_w_d1d(file_id, use_compression, no_spec, m, 'm'//char(0))
        call hdf5_w_d1d(file_id, use_compression, no_spec, Temp, 'Temp'//char(0))

        call hdf5_w_d1d(file_id, use_compression, Nz, z_g, 'zg'//char(0))
        call hdf5_w_d1d(file_id, use_compression, Ny, y_g, 'yg'//char(0))

        call hdf5_w_d2d(file_id, use_compression, Nz, Ny, rho_tot, 'rho'//char(0))
        call hdf5_w_d2d(file_id, use_compression, Nz, Ny, Escz, 'Escz'//char(0))
        call hdf5_w_d2d(file_id, use_compression, Nz, Ny, Escy, 'Escy'//char(0))

        call hdf5_w_d2d(file_id, use_compression, Nz, Ny, surface_matrix, 'surface_matrix'//char(0))
        call hdf5_w_d2d(file_id, use_compression, Nz, Ny, Pot, 'Pot'//char(0))
        call hdf5_w_d2d(file_id, use_compression, Nz, Ny, Potvac, 'Potvac'//char(0))

        call hdf5_w_i2d(file_id, use_compression, Nz_max, Ny, slice_proc, 'sliceproc'//char(0))
        call hdf5_w_d2d(file_id, use_compression, Nz, Ny, edge_charge, 'edgecharge'//char(0))

        call hdf5_w_d1d(file_id, use_compression, h_pos, t, 't'//char(0))
        call hdf5_w_i2d(file_id, use_compression, no_spec, h_pos, snumber, 'snumber'//char(0))

        call hdf5_w_d3d(file_id, use_compression, no_spec, 4, count, total_energy(:,:,1:count), 'totalenergy'//char(0))

        call hdf5_w_d1d(file_id, use_compression, h_pos, Esct, 'Esct'//char(0))

        call hdf5_w_d1d(file_id, use_compression, h_pos, dPHIqn, 'dPHIqn'//char(0))
        call hdf5_w_d1d(file_id, use_compression, h_pos, pot_chi, 'pchi'//char(0))

        call hdf5_w_d(file_id, bx, 'bx'//char(0))
        call hdf5_w_d(file_id, by, 'by'//char(0))
        call hdf5_w_d(file_id, bz, 'bz'//char(0))

        call hdf5_w_i2d(file_id, use_compression, proc_max + 1, h_pos, no_part_proc, 'npartproc'//char(0))

        call hdf5_w_i(file_id, no_diag_reg, 'nodiagreg'//char(0))
        call hdf5_w_i(file_id, diag_histories, 'diaghistories'//char(0))
        call hdf5_w_i(file_id, fv_arrays, 'fvarrays'//char(0))
        call hdf5_w_i(file_id, fv_bin, 'fvbin'//char(0))
        call hdf5_w_i(file_id, fv_perp_array_count, 'fvperparraycount'//char(0))
        call hdf5_w_d2d(file_id, use_compression, fv_arrays, 2, fv_limits, 'fvlimits'//char(0))

        call hdf5_w_d2d(file_id, use_compression, 11, count, time_history, 'timehistory'//char(0))

        !go through them and save one after another by their name
        hist_count = 0
        fv_count = 0
        fv_pa_count = 0
        do diag_count=1,no_diag_reg
            if ((diag_regions(diag_count)%record_property.eq.1).or.(diag_regions(diag_count)%record_property.eq.4)) then
                !potential or density
                hist_count = hist_count + 1
                hist_length = hist_limit(hist_count) -1
                if (count.gt.Nc) then
                    fvn = real((count - Na +1)*N0)
                else
                    fvn = 1.0
                end if
                ! 		write(*,*) 'Using normalization factor',fvn
                if (hist_length.lt.1) then
                    hist_length = 1
                end if
                write(*,*) 'Saving diag history records:',hist_length

                call hdf5_w_d1d(file_id, use_compression, hist_length, diag_hist(hist_count,1:hist_length), trim(diag_regions(diag_count)%name)//char(0))

            else if (diag_regions(diag_count)%record_property.eq.7) then
                if (count.gt.Nc) then
                    fvn = real((count - Na +1)*N0)
                else
                    fvn = 1.0
                end if
                write(*,*) 'Saving resonance diag ',trim(diag_regions(diag_count)%name)


                call hdf5_w_d1d(file_id, use_compression, fv_bin, fv_ar(fv_count+1,1:fv_bin), trim(diag_regions(diag_count)%name)//char(0))

                fv_count = fv_count + 1
            else if ((diag_regions(diag_count)%record_property.eq.2).or.(diag_regions(diag_count)%record_property.eq.3)) then
                !f(v)
                if (count.gt.Nc) then
                    fvn = real((count - Na +1)*N0)
                else
                    fvn = 1.0
                end if
                write(*,*) 'Saving f(v) diag ',trim(diag_regions(diag_count)%name)

                call hdf5_w_d1d(file_id, use_compression, fv_bin, fv_ar(fv_count+1,1:fv_bin)/fvn, trim(diag_regions(diag_count)%name)//'x1'//char(0))
                call hdf5_w_d1d(file_id, use_compression, fv_bin, fv_ar(fv_count+2,1:fv_bin)/fvn, trim(diag_regions(diag_count)%name)//'x2'//char(0))
                call hdf5_w_d1d(file_id, use_compression, fv_bin, fv_ar(fv_count+3,1:fv_bin)/fvn, trim(diag_regions(diag_count)%name)//'x3'//char(0))

                fv_count = fv_count + 3
            else if  (diag_regions(diag_count)%record_property.eq.5) then
                if (count.gt.Nc) then
                    fvn = real((count - Na +1)*N0)
                else
                    fvn = 1.0
                end if

                write(*,*) 'Saving f(v_par) | f(v_perp1,v_perp2) diag ',trim(diag_regions(diag_count)%name)

                call hdf5_w_d1d(file_id, use_compression, fv_bin, fv_ar(fv_count+1,1:fv_bin)/fvn, trim(diag_regions(diag_count)%name)//'par'//char(0))
                call hdf5_w_d2d(file_id, use_compression, fv_bin, fv_bin, u_perp_a(fv_pa_count+1,1:fv_bin,1:fv_bin)/fvn, trim(diag_regions(diag_count)%name)//'perp'//char(0))

                fv_count = fv_count + 1
                fv_pa_count =fv_pa_count +1
            else if  (diag_regions(diag_count)%record_property.eq.6) then
                ! test particle tracking
                write(*,*) 'Saving test particle ',trim(diag_regions(diag_count)%name)
                hist_count = hist_count + 1
                hist_length = count - Nc -1
                if (hist_length.lt.1) then
                    hist_length = 1
                end if

                call hdf5_w_d1d(file_id, use_compression, hist_length, diag_hist(hist_count,1:hist_length), trim(diag_regions(diag_count)%name)//'y'//char(0))
                hist_count = hist_count + 1

                call hdf5_w_d1d(file_id, use_compression, hist_length, diag_hist(hist_count,1:hist_length), trim(diag_regions(diag_count)%name)//'z'//char(0))
                hist_count = hist_count + 1

                call hdf5_w_d1d(file_id, use_compression, hist_length, diag_hist(hist_count,1:hist_length), trim(diag_regions(diag_count)%name)//'ux'//char(0))
                hist_count = hist_count + 1

                call hdf5_w_d1d(file_id, use_compression, hist_length, diag_hist(hist_count,1:hist_length), trim(diag_regions(diag_count)%name)//'uy'//char(0))
                hist_count = hist_count + 1

                call hdf5_w_d1d(file_id, use_compression, hist_length, diag_hist(hist_count,1:hist_length), trim(diag_regions(diag_count)%name)//'uz'//char(0))
            end if
        end do

        call hdf5_w_i2d(file_id, use_compression, Nz, Ny, flag_m, 'flagm'//char(0))

        s = sum(equipot_m)
        write(*,*) 'Sum of equipot:', s

        call hdf5_w_d2d(file_id, use_compression, Nz, Ny, equipot_m, 'equipotm'//char(0))

        call hdf5_w_i1d(file_id, use_compression, n - Nz, flag, 'flag'//char(0))

        call hdf5_w_d2d(file_id, use_compression, proc_max + 1, h_pos, iteration_time_hist, 'itertime'//char(0))
        ! injection rate hist

        call hdf5_w_d2d(file_id, use_compression, no_spec, h_pos, injection_rate_hist, 'injrate'//char(0))

        call hdf5_w_i2d(file_id, use_compression, Nz, Ny, objects, 'objects'//char(0))
        call hdf5_w_i2d(file_id, use_compression, Nz, Ny, edges, 'edges'//char(0))
        call hdf5_w_i2d(file_id, use_compression, Nz, Ny, diagm, 'diagm'//char(0))
        call hdf5_w_i2d(file_id, use_compression, Nz, Ny, objects_enum, 'objectsenum'//char(0))

        call hdf5_w_d2d(file_id, use_compression, nobjects, h_pos - 1, objects_current(1,:,1:(h_pos-1)), 'objectscurrenti'//char(0))
        call hdf5_w_d2d(file_id, use_compression, nobjects, h_pos - 1, objects_current(2,:,1:(h_pos-1)), 'objectscurrente'//char(0))

        call hdf5_w_d2d(file_id, use_compression, nobjects, h_pos - 1, objects_power_flux(1,:,1:(h_pos-1)), 'objectspowerfluxi'//char(0))
        call hdf5_w_d2d(file_id, use_compression, nobjects, h_pos - 1, objects_power_flux(2,:,1:(h_pos-1)), 'objectspowerfluxe'//char(0))

        ! charge densities
        specie_count = 0
        rhox = 0.0
        sol_Wx = 0.0
        sol_Nsx = 0.0
        do sp=1,no_spec
            if (specie_recomp) then
                rhox(specie_count*(proc_max+1) + proc_max + 1,:,:) = rhox(specie_count*(proc_max+1) + proc_max + 1,:,:) + rho(sp,:,:)
                sol_Wx(specie_count*(proc_max+1) + proc_max + 1,:,:) = sol_Wx(specie_count*(proc_max+1) + proc_max + 1,:,:) + sol_W(sp,:,:)
                sol_Nsx(specie_count*(proc_max+1) + proc_max + 1,:,:) = sol_Nsx(specie_count*(proc_max+1) + proc_max + 1,:,:) + sol_Ns(sp,:,:)

                if (sp.eq.int(real(sp)/real(proc_max+1))*(proc_max+1)) then
                    specie_count = specie_count + 1
                    sp_str = '  '
                    write(sp_str(1:2),'(I2.2)') specie_count

                    call hdf5_w_d2d(file_id, use_compression, Nz_max, Ny, rhox(specie_count*(proc_max+1),:,:), 'rho'//sp_str//char(0))
                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, SOL_Wx(specie_count*(proc_max+1),:,:), 'solw'//sp_str//char(0))
                    call hdf5_w_d2d(file_id, use_compression, Nz, Ny, SOL_Nsx(specie_count*(proc_max+1),:,:), 'solns'//sp_str//char(0))

                end if

            else
                write(sp_str(1:2),'(I2.2)') sp

                call hdf5_w_d2d(file_id, use_compression, Nz_max, Ny, rho(sp,:,:), 'rho'//sp_str//char(0))
                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, SOL_W(sp,:,:), 'solw'//sp_str//char(0))
                call hdf5_w_d2d(file_id, use_compression, Nz, Ny, SOL_Ns(sp,:,:), 'solns'//sp_str//char(0))


            end if
            !	write(*,*) 'Saving rho:',sp,sum(rho(sp,:,:))

        end do

        call hdf5_w_d(file_id, ksi, 'ksi'//char(0))
        call hdf5_w_d(file_id, tau, 'tau'//char(0))
        call hdf5_w_d(file_id, mu, 'mu'//char(0))

        call hdf5_w_d(file_id, mks_info%mks_n0, 'mksn0'//char(0))
        call hdf5_w_d(file_id, mks_info%mks_Te, 'mksTe'//char(0))
        call hdf5_w_d(file_id, mks_info%mks_B, 'mksB'//char(0))
        call hdf5_w_d(file_id, mks_info%mks_main_ion_m, 'mksmainionm'//char(0))
        call hdf5_w_d(file_id, mks_info%mks_main_ion_q, 'mksmainionq'//char(0))
        call hdf5_w_d(file_id, mks_info%mks_par1, 'mkspar1'//char(0))
        call hdf5_w_d(file_id, mks_info%mks_par2, 'mkspar2'//char(0))
        call hdf5_w_d(file_id, mks_info%mks_par3, 'mkspar3'//char(0))

        call hdf5_w_d(file_id, alpha_yz, 'alphayz'//char(0))
        call hdf5_w_d(file_id, alpha_xz, 'alphaxz'//char(0))

        call hdf5_w_i(file_id, Nc, 'Nc'//char(0))
        call hdf5_w_i(file_id, Na, 'Na'//char(0))
        call hdf5_w_i(file_id, Np, 'Np'//char(0))

        call hdf5_w_d2d(file_id, use_compression, nobjects, 10, i_rel_history, 'irel'//char(0))

        call hdf5_w_d1d(file_id, use_compression, nobjects, float_constant, 'floatconstant'//char(0))

        ! close HDF5 file
        call h5fclose_f(file_id, h5err)
        ! shutdown HDF5
        call h5close_f(h5err)

        !open(7, file='dispo.txt',status='unknown')
        !write(7,'(I1)') (1)
        !close (7)

        !RD le 13/01/05   test avec chantal pour lire un fichier .mat
        !call OPENMATR(filename,ifile)
        !write(6,*) ' ifile ',ifile
        !call LOADMATF(ifile,type,name,nlig,ncol,imag,zz,zz,icr)
        !write(6,*) ' icr ',icr
        !write(6,*) ' type ', type,' nlig ',nlig
        !write(6,*) ' name ',name
        !call CLOSEMAT(ifile)
        !END test
        if (debug) then
            write(*,*) 'Finished in Lang. monitor'
        end if

        ! domain_decomp = .true.


        return
    END

    subroutine h5_save_temp_proc_file(tfile,proc_no,no_tot,z,y,ux,uy,uz,stype,count,use_compression_i)

        implicit none

        integer:: no_tot, proc_no,icr,version,IERR,count
        character*2048:: tfile
        real*8, dimension(no_tot):: z,y,ux,uy,uz
        integer*2, dimension(no_tot):: stype

        integer, dimension(no_tot):: stype_h

        character*2:: sp_str

        integer:: use_compression_i
        logical :: use_compression
        integer(HID_T) :: file_id       ! File identifier
        integer :: h5err

        use_compression = use_compression_i .ge. 1
        version = 50
        sp_str = '  '
        write(sp_str(1:2),'(I2.2)') proc_no

        ! defunct - don't know how to include integers in strings
        write (6,*) 'Writing results to ',trim(tfile)//sp_str//'.h5'

        ! initialize HDF5
        call h5open_f(h5err)
        ! create HDF5 file
        call h5fcreate_f(trim(tfile)//sp_str//'.h5'//char(0), H5F_ACC_TRUNC_F, file_id, h5err)

        call hdf5_w_i(file_id, version, 'version'//char(0))
        call hdf5_w_i(file_id, no_tot, 'ntot'//char(0))
        call hdf5_w_i(file_id, count, 'count'//char(0))
        call hdf5_w_i(file_id, proc_no, 'nproc'//char(0))

        call hdf5_w_d1d(file_id, use_compression, no_tot, y, 'y'//char(0))
        call hdf5_w_d1d(file_id, use_compression, no_tot, z, 'z'//char(0))
        call hdf5_w_d1d(file_id, use_compression, no_tot, ux, 'ux'//char(0))
        call hdf5_w_d1d(file_id, use_compression, no_tot, uy, 'uy'//char(0))
        call hdf5_w_d1d(file_id, use_compression, no_tot, uz, 'uz'//char(0))

        stype_h = int(stype)
        call hdf5_w_i1d(file_id, use_compression, no_tot, stype_h, 'stype'//char(0))

        ! close HDF5 file
        call h5fclose_f(file_id, h5err)
        ! shutdown HDF5
        call h5close_f(h5err)

    end subroutine

    subroutine h5_save_test_particle(tfile,proc_no,no_species,test_particle,count,test_particle_i,test_particle_data,use_compression_i)
        implicit none

        integer:: proc_no,no_species,version,IERR,count,test_particle,sp
        character*2048:: tfile
        real*8, dimension(count,no_species,test_particle,7):: test_particle_data
        integer, dimension(no_species,test_particle):: test_particle_i
        character*2:: sp_str
        character*5:: spm_str

        integer :: pt_from, pt_to, part_count, i_c

        integer:: use_compression_i
        logical :: use_compression

        integer(HID_T) :: file_id       ! File identifier
        integer :: h5err

        use_compression = use_compression_i .ge. 1
        version = 50
        sp_str = '  '
        write(sp_str(1:2),'(I2.2)') proc_no

        ! defunct - don't know how to include integers in strings
        write (6,*) 'Writing test particles to ',trim(tfile)//sp_str//'tp.h5'

        ! initialize HDF5
        call h5open_f(h5err)
        ! create HDF5 file
        call h5fcreate_f(trim(tfile)//sp_str//'tp.h5'//char(0), H5F_ACC_TRUNC_F, file_id, h5err)

        call hdf5_w_i(file_id, version, 'version'//char(0))
        call hdf5_w_i(file_id, count, 'count'//char(0))
        call hdf5_w_i(file_id, test_particle, 'test_particle'//char(0))
        call hdf5_w_i2d(file_id, use_compression, test_particle, no_species, transpose(test_particle_i), 'test_particle_i'//char(0))

        call hdf5_w_i(file_id, 0, 'split'//char(0))

        do sp=1,no_species
            sp_str = '  '
            write(sp_str(1:2),'(I2.2)') sp

            call hdf5_w_d3d(file_id, use_compression, count, test_particle, 7, test_particle_data(:,sp,:,:), 'particles'//sp_str//char(0))
        enddo


        ! close HDF5 file
        call h5fclose_f(file_id, h5err)
        ! shutdown HDF5
        call h5close_f(h5err)

    end subroutine

    subroutine h5_restore_pot(pfile, Nz, Ny, Pot, verbose)
        implicit none
        integer:: Nz,Ny,ierr
        real*8, dimension(Nz,Ny):: Pot
        logical:: verbose
        character*2048:: pfile

        integer(HID_T) :: file_id       ! File identifier
        integer :: h5err

        if (verbose) then
            write(*,*) 'Restoring run from ',trim(pfile)//'.h5'
        end if

        ! initialize HDF5
        call h5open_f(h5err)
        ! open HDF5 file for reading
        CALL h5fopen_f(trim(pfile)//'.h5'//char(0), H5F_ACC_RDONLY_F, file_id, h5err)

        call hdf5_r_d2d(file_id, Nz, Ny, Pot, 'Potav'//char(0))

        ! close HDF5 file
        call h5fclose_f(file_id, h5err)
        ! shutdown HDF5
        call h5close_f(h5err)

        write(*,*) 'Fixed potential restored',sum(Pot)

    end subroutine

    subroutine h5_advanced_restore_run_9_particles (tfile,verbose,debug,y,z,ux,uy,uz,Nz,Ny,Npts,no_species,stype,no_tot,proc_no,count,par_size,dz,dy,Nz_max,slice_proc,regroup_runs)
        implicit none
        integer:: Npts,ierr,length,version,proc_no,Nz,Ny,no_species,count,i,regroup_runs
        real*8, dimension(Npts):: y,z,ux,uy,uz
        integer*2, dimension(Npts):: stype
        character*2048:: tfile
        logical:: verbose,debug
        integer*8:: ifile
        integer:: dumi,no_tot
        real*8:: dumr,dz,dy
        character*2:: sp_str
        integer::par_size,global_ntot,n_limit,k,iyx,izx,Nz_max
        integer, dimension(Nz_max,Ny):: slice_proc
        real*8, dimension(Npts*par_size):: y_large,z_large,ux_large,uy_large,uz_large
        integer*2, dimension(Npts*par_size):: stype_large
        logical:: found_end,file_exists

        version = 0
        dumi = 0
        sp_str = '  '
        write(sp_str(1:2),'(I2.2)') proc_no
        global_ntot = 0
        if (regroup_runs.gt.0) then
            n_limit = regroup_runs
        else
            n_limit = 256
        end if
        found_end = .false.
        do i=0,n_limit
            if (found_end) then
                ! do nothing
            else
                ! check for existing file
                write(sp_str(1:2),'(I2.2)') i
                INQUIRE(FILE=trim(tfile)//sp_str//'.h5'//char(0), EXIST=file_exists)
                if (file_exists) then
                    ! load particles from tile
                    write(*,*) proc_no,'Loading particles from processor ',i
                    call h5_restore_run_9_particles (tfile,verbose,debug,y,z,ux,uy,uz,Nz,Ny,Npts,no_species,stype,no_tot,i,count)
                    ! copy the particles to the large vectors
                    do k=1,no_tot
                        z_large(k + global_ntot) = z(k)
                        y_large(k + global_ntot) = y(k)
                        ux_large(k + global_ntot) = ux(k)
                        uy_large(k + global_ntot) = uy(k)
                        uz_large(k + global_ntot) = uz(k)
                        stype_large(k + global_ntot) = stype(k)
                    end do
                    global_ntot = global_ntot + no_tot
                else
                    found_end = .true.
                end if
            end if
        end do

        write(*,*) proc_no,'Discarding irrelevant particles'
        ! keep only the particles which are within our slice
        no_tot = 0
        do i=1,global_ntot
            iyx = int(y_large(i)/dy) + 1
            izx = int(z_large(i)/dz) + 1
            if (slice_proc(izx,iyx).eq.proc_no) then
                no_tot = no_tot +1
                z(no_tot) = z_large(i)
                y(no_tot) = y_large(i)
                ux(no_tot) = ux_large(i)
                uy(no_tot) = uy_large(i)
                uz(no_tot) = uz_large(i)
                stype(no_tot) = stype_large(i)
            end if
        end do

        write(*,*) proc_no,'Loaded particles',no_tot
    end subroutine


    subroutine h5_restore_run_9_particles(tfile,verbose,debug,y,z,ux,uy,uz,Nz,Ny,Npts,no_species,stype,no_tot,proc_no,count)

        implicit none
        integer:: Npts,ierr,length,version,proc_no,Nz,Ny,no_species,count,i
        real*8, dimension(Npts):: y,z,ux,uy,uz
        integer*2, dimension(Npts):: stype
        character*2048:: tfile
        logical:: verbose,debug
        integer*8:: ifile
        integer:: dumi,no_tot
        real*8:: dumr
        character*2:: sp_str

        integer(HID_T) :: file_id       ! File identifier
        integer :: h5err
        integer, dimension(Npts):: stype_h

        version = 0
        dumi = 0
        sp_str = '  '
        write(sp_str(1:2),'(I2.2)') proc_no


        if (verbose) then
            write(*,*) 'Restoring run from ',trim(tfile)//sp_str//'.h5'
        end if

        ! initialize HDF5
        call h5open_f(h5err)
        ! open HDF5 file for reading
        CALL h5fopen_f(trim(tfile)//sp_str//'.h5'//char(0), H5F_ACC_RDONLY_F, file_id, h5err)

        ! extract version
        call hdf5_r_i(file_id, version, 'version'//char(0))


        if (verbose) then
            write(*,*) 'Restoring from data file version',version
        end if
        if (version.lt.50) then
            write(*,*) '--------- ERROR ---------'
            write(*,*) 'This H5 file is too old'
            write(*,*) 'Restore function only works with HDF5 files generated by HDF5 library'
            stop
        end if
        ! extract no_tot
        call hdf5_r_i(file_id, no_tot, 'ntot'//char(0))

        if (verbose) then
            write(*,*) 'Restoring ',no_tot, 'particles'
        end if

        call hdf5_r_i(file_id, proc_no, 'nproc'//char(0))

        if (verbose) then
            write(*,*) 'Processor rank: ',proc_no
        end if

        call hdf5_r_i(file_id, count, 'count'//char(0))

        if (verbose) then
            write(*,*) 'Count: ',count
        end if

        ! TODO Matio had some checks for correct vector length extraction, it can be done for HDF5 as well.

        ! extract particle vectors
        call hdf5_r_d1d(file_id, no_tot, y(1:no_tot), 'y'//char(0))
        if (verbose) then
            write(*,*) 'Loaded y vector, check',sum(y(1:no_tot))
        end if

        call hdf5_r_d1d(file_id, no_tot, z(1:no_tot), 'z'//char(0))
        if (verbose) then
            write(*,*) 'Loaded z vector, check',sum(z(1:no_tot))
        end if

        call hdf5_r_d1d(file_id, no_tot, ux(1:no_tot), 'ux'//char(0))
        if (verbose) then
            write(*,*) 'Loaded ux vector, check',sum(ux(1:no_tot))
        end if

        call hdf5_r_d1d(file_id, no_tot, uy(1:no_tot), 'uy'//char(0))
        if (verbose) then
            write(*,*) 'Loaded uy vector, check',sum(uy(1:no_tot))
        end if

        call hdf5_r_d1d(file_id, no_tot, uz(1:no_tot), 'uz'//char(0))
        if (verbose) then
            write(*,*) 'Loaded uz vector, check',sum(uz(1:no_tot))
        end if

        call hdf5_r_i1d(file_id, no_tot, stype_h(1:no_tot), 'stype'//char(0))
        stype = int(stype_h, kind=2)
        if (verbose) then
            write(*,*) 'Loaded stype vector, check',sum(float(stype(1:no_tot)))
        end if

        ! close HDF5 file
        call h5fclose_f(file_id, h5err)
        ! shutdown HDF5
        call h5close_f(h5err)

        ! do a couple of simple checks
        do i=1,no_tot
            ! May fail for dy != 1.0
            if (y(i).lt.0.or.y(i).gt.float(Ny-1)) then
                write(*,*) 'Warning: y check failed',y(i),Ny
            end if
            ! May fail for scenario 2
            if (z(i).lt.0) then
                write(*,*) 'Error: z check failed',z(i),Nz
            end if

            if (stype(i).lt.0.or.stype(i).gt.no_species) then
                write(*,*) 'Error: stype check failed',stype(i),no_species
            end if



        end do

    end subroutine


    subroutine h5_restore_run_9_histories(no_spec,snumber,Nz,Ny,Nv,z_g,y_g,rho_tot,Escz,Escy,Pot,count,t,Esct,dPHIqn,bx,by,bz,&
            tfile,debug,Nt,pot_chi,&
            Potvac,no_part_proc,proc_max,h_pos,no_diag_reg,diag_regions,diag_histories,diag_hist,fv_arrays,fv_ar,hist_limit,Np,Na,&
            fv_bin,fv_sum,fv_limits,u_perp_a,fv_perp_array_count,N0,diag_ntimes,verbose,edge_charge,flag_m,equipot_m,flag, &
            iteration_time_hist,injection_rate_hist,objects,edges,diagm,objects_enum,nobjects,objects_current,objects_power_flux,history_ntimes, &
            rho,i_rel_history,float_constant,n,Nc,Nz_max)
        use ap_utils
        implicit none
        !include "struct.h"
        real*8:: bx,by,bz,erreur2
        character*2:: sp_str
        integer:: Nz,Nt,Ny,Nv,Np,proc_max,count,icr,no_spec,h_pos,Na,diag_ntimes,history_ntimes,sp,nobjects,n,no_species,use_compression,Nc,Nz_max
        ! real*8 test_z,test_y, test_ux, test_uy,test_uz
        real*8, dimension(Nz):: z_g
        real*8, dimension(Ny):: y_g
        real*8, dimension(Nz,Ny):: rho_tot,Escz,Escy,Pot,Potvac,equipot_m
        integer, dimension(Nz,Ny):: flag_m,objects,edges,diagm,objects_enum
        integer, dimension(n - Nz):: flag
        real*8, dimension(history_ntimes):: t,Esct,dPHIqn,pot_chi
        integer, dimension(no_spec,history_ntimes):: snumber
        integer, dimension(proc_max +1,history_ntimes):: no_part_proc
        integer*8:: ifile
        character*2048:: tfile
        logical:: debug,verbose
        !diag regions
        integer:: no_diag_reg, diag_histories,fv_arrays,fv_bin,fv_perp_array_count
        integer, dimension(diag_histories):: hist_limit
        real*8, dimension(diag_histories,Np - Nc):: diag_hist
        real*8, dimension(Np):: dumhist
        real*8, dimension(fv_arrays,fv_bin):: fv_ar
        real*8, dimension(fv_bin):: dumar
        type(diag_region), dimension(no_diag_reg):: diag_regions
        integer:: diag_count,hist_count,fv_count,hist_length,fv_pa_count
        integer, dimension(fv_arrays):: fv_sum
        real*8, dimension(fv_arrays,2):: fv_limits
        real*8, dimension(fv_perp_array_count,fv_bin,fv_bin):: u_perp_a
        real*8, dimension(fv_bin,fv_bin):: dumar2

        real*8:: fvn
        integer:: N0,IERR,version
        integer:: dumi
        real*8:: dumr
        real*8, dimension(Nz,Ny):: dumg
        real*8, dimension(Nz):: dumz
        real*8, dimension(Ny):: dumy
        real*8, dimension(history_ntimes):: dumh
        real*8, dimension(Nz,Ny):: edge_charge
        real*8, dimension(proc_max +1,history_ntimes):: iteration_time_hist
        real*8, dimension(no_spec,history_ntimes):: injection_rate_hist
        real*8, dimension(2,nobjects,history_ntimes):: objects_current,objects_power_flux
        real*8, dimension(no_spec,Nz_max,Ny):: rho
        real*8, dimension(nobjects,10):: i_rel_history
        real*8, dimension(nobjects):: float_constant


        integer(HID_T) :: file_id       ! File identifier
        integer :: h5err

        if (debug) then
            write(*,*) 'Restoring histories from mat file ', trim(tfile)//'.h5'
        end if

        ! initialize HDF5
        call h5open_f(h5err)
        ! open HDF5 file for reading
        call h5fopen_f(trim(tfile)//'.h5', H5F_ACC_RDONLY_F, file_id, h5err)

        call hdf5_r_i(file_id, version, 'version'//char(0))

        if (verbose) then
            write(*,*) 'Restoring from data file version',version
        end if
        if (version.lt.50) then
            write(*,*) '--------- ERROR ---------'
            write(*,*) 'This h5 file is too old'
            write(*,*) 'Restore function only works with HDF5 files generated by HDF5 library'
            stop
        end if

        call hdf5_r_i(file_id, count, 'count'//char(0))
        if (verbose) then
            write(*,*) 'Restored count',count
        end if
        call hdf5_r_i(file_id, h_pos, 'hpos'//char(0))
        if (verbose) then
            write(*,*) 'Restored h_pos',h_pos
        end if


        ! we need potvac
        call hdf5_r_d2d(file_id, Nz, Ny, Potvac, 'Potvac'//char(0))
        if (verbose) then
            write(*,*) 'Restored Vacuum potential',sum(Potvac)
        end if

        ! edge charge
        call hdf5_r_d2d(file_id, Nz, Ny, edge_charge, 'edgecharge'//char(0))
        if (verbose) then
            write(*,*) 'Restored Edge charge',sum(edge_charge)
        end if

        ! histories need to be restored
        call hdf5_r_i2d(file_id, no_spec, h_pos, snumber(:,1:h_pos), 'snumber'//char(0))
        if (verbose) then
            write(*,*) 'Restored snumber',sum(snumber(:,1:h_pos))
        end if

        call hdf5_r_d1d(file_id, h_pos, Esct(1:h_pos), 'Esct'//char(0))
        if (verbose) then
            write(*,*) 'Restored Esct',sum(Esct(1:h_pos))
        end if

        call hdf5_r_d1d(file_id, h_pos, dPHIqn(1:h_pos), 'dPHIqn'//char(0))
        if (verbose) then
            write(*,*) 'Restored dPHIqn',sum(dPHIqn(1:h_pos))
        end if

        call hdf5_r_d1d(file_id, h_pos, pot_chi(1:h_pos), 'pchi'//char(0))
        if (verbose) then
            write(*,*) 'Restored pot_chi',sum(pot_chi(1:h_pos))
        end if

        call hdf5_r_i2d(file_id, proc_max+1, h_pos, no_part_proc(:,1:h_pos), 'npartproc'//char(0))
        if (verbose) then
            write(*,*) 'Restored no_part_proc',sum(no_part_proc(:,1:h_pos))
        end if

        !go through them and load one after another
        hist_count = 0
        fv_count = 0
        fv_pa_count = 0
        do diag_count=1,no_diag_reg
            if ((diag_regions(diag_count)%record_property.eq.1).or.(diag_regions(diag_count)%record_property.eq.4)) then
                !potential or density
                hist_count = hist_count + 1
                hist_length = count - Nc -1
                if (hist_length.lt.1) then
                    hist_length = 1
                end if
                write(*,*) 'Loading diag history records:',hist_length
                call hdf5_r_d1d(file_id, hist_length, dumhist(1:hist_length), trim(diag_regions(diag_count)%name)//char(0))

                diag_hist(hist_count,1:hist_length) = dumhist(1:hist_length)
                if (verbose)	 then
                    write(*,*) 'Steady state history loaded ',trim(diag_regions(diag_count)%name),&
                        sum(diag_hist(hist_count,1:hist_length))

                end if

            else if (diag_regions(diag_count)%record_property.eq.7) then
                if (count.gt.Nc) then
                    fvn = real((count - Nc +1)*N0)
                else
                    fvn = 1.0
                end if
                write(*,*) 'Loading resonance diag ',trim(diag_regions(diag_count)%name)
                ! load -  multiply back by normalisation factor

                call hdf5_r_d1d(file_id, fv_bin, dumar, trim(diag_regions(diag_count)%name)//char(0))

                fv_ar(fv_count+1,1:fv_bin) = dumar
                fv_ar(fv_count+1,1:fv_bin) = fv_ar(fv_count+1,1:fv_bin)*fvn
                fv_count = fv_count + 1
            else if ((diag_regions(diag_count)%record_property.eq.2).or.(diag_regions(diag_count)%record_property.eq.3)) then
                !f(v)
                if (count.gt.Na) then
                    fvn = real((count - Nc +1)*N0)
                else
                    fvn = 1.0
                end if
                write(*,*) 'Loading f(v) diag ',trim(diag_regions(diag_count)%name)

                call hdf5_r_d1d(file_id, fv_bin, dumar, trim(diag_regions(diag_count)%name)//'x1'//char(0))
                fv_ar(fv_count+1,1:fv_bin) = dumar
                fv_ar(fv_count+1,1:fv_bin) = fv_ar(fv_count+1,1:fv_bin)*fvn

                call hdf5_r_d1d(file_id, fv_bin, dumar, trim(diag_regions(diag_count)%name)//'x2'//char(0))
                fv_ar(fv_count+2,1:fv_bin) = dumar
                fv_ar(fv_count+2,1:fv_bin) = fv_ar(fv_count+2,1:fv_bin)*fvn


                call hdf5_r_d1d(file_id, fv_bin, dumar, trim(diag_regions(diag_count)%name)//'x3'//char(0))
                fv_ar(fv_count+3,1:fv_bin) = dumar
                fv_ar(fv_count+3,1:fv_bin) = fv_ar(fv_count+3,1:fv_bin)*fvn

                fv_count = fv_count + 3
            else if  (diag_regions(diag_count)%record_property.eq.5) then
                if (count.gt.Na) then
                    fvn = real((count - Nc +1)*N0)
                else
                    fvn = 1.0
                end if

                write(*,*) 'Loading f(v_par) | f(v_perp1,v_perp2) diag ',trim(diag_regions(diag_count)%name)
                call hdf5_r_d1d(file_id, fv_bin, dumar, trim(diag_regions(diag_count)%name)//'par'//char(0))

                fv_ar(fv_count+1,1:fv_bin) = dumar
                fv_ar(fv_count+1,1:fv_bin) = fv_ar(fv_count+1,1:fv_bin)*fvn

                call hdf5_r_d2d(file_id, fv_bin, fv_bin, dumar2, trim(diag_regions(diag_count)%name)//'perp'//char(0))

                u_perp_a(fv_pa_count+1,1:fv_bin,1:fv_bin) = dumar2
                u_perp_a(fv_pa_count+1,1:fv_bin,1:fv_bin) = u_perp_a(fv_pa_count+1,1:fv_bin,1:fv_bin)*fvn

                fv_count = fv_count + 1
                fv_pa_count =fv_pa_count +1
            else if  (diag_regions(diag_count)%record_property.eq.6) then
                ! test particle tracking
                write(*,*) 'Loading test particle ',trim(diag_regions(diag_count)%name)
                hist_count = hist_count + 1
                hist_length = count - Nc -1
                if (hist_length.lt.1) then
                    hist_length = 1
                end if

                call hdf5_r_d1d(file_id, hist_length, dumhist(1:hist_length), trim(diag_regions(diag_count)%name)//'y'//char(0))
                diag_hist(hist_count,1:hist_length) = dumhist(1:hist_length)

                hist_count = hist_count + 1

                call hdf5_r_d1d(file_id, hist_length, dumhist(1:hist_length), trim(diag_regions(diag_count)%name)//'z'//char(0))
                diag_hist(hist_count,1:hist_length) = dumhist(1:hist_length)

                hist_count = hist_count + 1

                call hdf5_r_d1d(file_id, hist_length, dumhist(1:hist_length), trim(diag_regions(diag_count)%name)//'ux'//char(0))
                diag_hist(hist_count,1:hist_length) = dumhist(1:hist_length)

                hist_count = hist_count + 1

                call hdf5_r_d1d(file_id, hist_length, dumhist(1:hist_length), trim(diag_regions(diag_count)%name)//'uy'//char(0))
                diag_hist(hist_count,1:hist_length) = dumhist(1:hist_length)

                hist_count = hist_count + 1

                call hdf5_r_d1d(file_id, hist_length, dumhist(1:hist_length), trim(diag_regions(diag_count)%name)//'uz'//char(0))
                diag_hist(hist_count,1:hist_length) = dumhist(1:hist_length)

            end if
        end do

        call hdf5_r_d2d(file_id, proc_max + 1, history_ntimes, iteration_time_hist, 'itertime'//char(0))
        write(*,*) 'Loaded itertime'

        call hdf5_r_d2d(file_id, no_spec, history_ntimes, injection_rate_hist, 'injrate'//char(0))
        write(*,*) 'Loaded injrate'

        call hdf5_r_i2d(file_id, Nz, Ny, objects, 'objects'//char(0))
        write(*,*) 'Loaded objects'

        call hdf5_r_i2d(file_id, Nz, Ny, edges, 'edges'//char(0))
        write(*,*) 'Loaded edges'

        call hdf5_r_i2d(file_id, Nz, Ny, diagm, 'diagm'//char(0))
        write(*,*) 'Loaded diagm'

        call hdf5_r_d2d(file_id, nobjects, h_pos - 1, objects_current(1,:,1:(h_pos -1)), 'objectscurrenti'//char(0))
        write(*,*) 'Loaded objectscurrenti'

        call hdf5_r_d2d(file_id, nobjects, h_pos - 1, objects_current(1,:,1:(h_pos -1)), 'objectscurrente'//char(0))
        write(*,*) 'Loaded objectscurrente'

        call hdf5_r_d2d(file_id, nobjects, h_pos - 1, objects_power_flux(1,:,1:(h_pos -1)), 'objectspowerfluxi'//char(0))
        write(*,*) 'Loaded objectspowerfluxi'

        call hdf5_r_d2d(file_id, nobjects, h_pos - 1, objects_power_flux(1,:,1:(h_pos -1)), 'objectspowerfluxe'//char(0))
        write(*,*) 'Loaded objectspowerfluxe'


        call hdf5_r_d2d(file_id, nobjects, 10, i_rel_history, 'irel'//char(0))
        write(*,*) 'Loaded irel'

        call hdf5_r_d1d(file_id, nobjects, float_constant, 'floatconstant'//char(0))
        write(*,*) 'Loaded floatconstant'


        call hdf5_r_i1d(file_id, diag_histories, hist_limit, 'histlimits'//char(0))
        write(*,*) 'Loaded hist_limit'

        ! do sp=1,no_spec
        ! 	write(sp_str(1:2),'(I2.2)') sp
        ! ! 	call SAVEMATF(ifile,0,'rho'//sp_str//char(0),Nz,Ny,0,rho(sp,:,:),rho(sp,:,:),icr)
        ! 		ierr = FMat_VarReadInfo(MAT,'rho'//sp_str//char(0),MATVAR)
        ! 		ierr = FMat_VarReadData(MAT,MATVAR,rho(sp,:,:))
        ! 		write(*,*) 'Loaded rho', sp
        !
        !   !	write(*,*) 'Saving rho:',sp,sum(rho(sp,:,:))
        !
        ! end do


        ! close HDF5 file
        call h5fclose_f(file_id, h5err)
        ! shutdown HDF5
        call h5close_f(h5err)


    end subroutine

    subroutine h5_restore_9_avdiag(dt,Nz,Ny,dens,Epar,Eperp,Potav,vzav,vyav,vxav,vz2av,vy2av,vx2av,vz3av,vy3av,&
            vx3av,ofile,debug,no_spec,nav,verbose,edge_flux,edge_energy_flux)

        implicit none
        integer:: N_e,N_i,Nz,Nv,Np,N0,Ny,no_spec,sp,Nz1,IERR,version

        real*8:: dt,dz
        real*8, dimension(Nz,Ny):: Potav,Epar,Eperp
        real*8, dimension(no_spec,Nz,Ny):: q_grid,T_g,vzav,vyav,vxav,vz2av,vy2av,vx2av,vz3av,vy3av,vx3av,dens
        real*8,dimension(Nz,Ny,no_spec)::edge_flux,edge_energy_flux
        real*8, dimension(Nz):: z_g
        real*8, dimension(Ny):: y_g

        !real*8, dimension(Np):: t
        real*8, dimension(no_spec):: Iinj
        logical*1:: time_diag
        integer*8 ifile
        character*2048 ofile
        character*2:: sp_str
        real*8:: ang_bin
        integer:: abin,nav
        integer:: dumi
        real*8:: dumr
        real*8, dimension(Nz,Ny):: dumg
        real*8, dimension(Nz):: dumz
        real*8, dimension(Ny):: dumy
        logical:: verbose,debug

        integer(HID_T) :: file_id       ! File identifier
        integer :: h5err


        !abin = int(180.0/ang_bin) +1
        Nz1  =1
        write (6,*) 'Loading averaged diagnostics',trim(ofile)//'.h5'

        ! initialize HDF5
        call h5open_f(h5err)
        ! open HDF5 file for reading
        call h5fopen_f(trim(ofile)//'.h5'//char(0), H5F_ACC_RDONLY_F, file_id, h5err)

        call hdf5_r_i(file_id, version, 'version'//char(0))

        if (verbose) then
            write(*,*) 'Restoring from data file version',version
        end if
        if (version.lt.50) then
            write(*,*) '--------- ERROR ---------'
            write(*,*) 'This h5 file is too old'
            write(*,*) 'Restore function only works with HDF5 files generated by HDF5 library'
            stop
        end if

        !first important value

        call hdf5_r_i(file_id, nav, 'nav'//char(0))

        if (verbose) then
            write(*,*) 'Restored nav',nav
        end if


        call hdf5_r_d2d(file_id, Nz, Ny, Epar, 'Epar'//char(0))
        if (verbose) then
            write(*,*) 'Restored Epar',sum(Epar)
        end if

        call hdf5_r_d2d(file_id, Nz, Ny, Eperp, 'Eperp'//char(0))
        if (verbose) then
            write(*,*) 'Restored Eperp',sum(Eperp)
        end if

        call hdf5_r_d2d(file_id, Nz, Ny, Potav, 'Potav'//char(0))
        if (verbose) then
            write(*,*) 'Restored Potav',sum(Potav)
        end if

        !now specie - specific diags
        !and finally we go specie by specie and write it's diag into the file
        do sp=1,no_spec
            ! sp_str is sp but it is a char
            write(*,*) 'Restoring diagnostics for specie',sp
            sp_str = '  '
            write(sp_str(1:2),'(I2.2)') sp

            call hdf5_r_d2d(file_id, Nz, Ny, dumg, 'dens'//sp_str//char(0))
            dens(sp,:,:) = dumg
            if (verbose) then
                write(*,*) 'Restored dens',sum(dens(sp,:,:))
            end if

            !           ierr = FMat_VarReadInfo(MAT,'q'//sp_str//char(0),MATVAR)
            ! 	 ierr = FMat_VarReadData(MAT,MATVAR,dumg)
            ! q_grid(sp,:,:) = dumg
            ! if (verbose) then
            ! 	write(*,*) 'Restored q',sum(q_grid(sp,:,:))
            ! end if
            !           ierr = FMat_VarReadInfo(MAT,'T'//sp_str//char(0),MATVAR)
            ! 	 ierr = FMat_VarReadData(MAT,MATVAR,dumg)
            !
            ! T_g(sp,:,:) = dumg
            ! if (verbose) then
            ! 	write(*,*) 'Restored T',sum(T_g(sp,:,:))
            ! end if

            call hdf5_r_d2d(file_id, Nz, Ny, dumg, 'vzav'//sp_str//char(0))
            vzav(sp,:,:) = dumg
            if (verbose) then
                write(*,*) 'Restored vzav',sum(vzav(sp,:,:))
            end if

            call hdf5_r_d2d(file_id, Nz, Ny, dumg, 'vyav'//sp_str//char(0))
            vyav(sp,:,:) = dumg
            if (verbose) then
                write(*,*) 'Restored vyav',sum(vyav(sp,:,:))
            end if

            call hdf5_r_d2d(file_id, Nz, Ny, dumg, 'vxav'//sp_str//char(0))
            vxav(sp,:,:) = dumg
            if (verbose) then
                write(*,*) 'Restored vxav',sum(vxav(sp,:,:))
            end if


            call hdf5_r_d2d(file_id, Nz, Ny, dumg, 'vz2av'//sp_str//char(0))
            vz2av(sp,:,:) = dumg
            if (verbose) then
                write(*,*) 'Restored vz2av',sum(vz2av(sp,:,:))
            end if

            call hdf5_r_d2d(file_id, Nz, Ny, dumg, 'vy2av'//sp_str//char(0))
            vy2av(sp,:,:) = dumg
            if (verbose) then
                write(*,*) 'Restored vy2av',sum(vy2av(sp,:,:))
            end if

            call hdf5_r_d2d(file_id, Nz, Ny, dumg, 'vx2av'//sp_str//char(0))
            vx2av(sp,:,:) = dumg
            if (verbose) then
                write(*,*) 'Restored vx2av',sum(vx2av(sp,:,:))
            end if

            call hdf5_r_d2d(file_id, Nz, Ny, dumg, 'vz3av'//sp_str//char(0))
            vz3av(sp,:,:) = dumg
            if (verbose) then
                write(*,*) 'Restored vz3av',sum(vz3av(sp,:,:))
            end if

            call hdf5_r_d2d(file_id, Nz, Ny, dumg, 'vy3av'//sp_str//char(0))
            vy3av(sp,:,:) = dumg
            if (verbose) then
                write(*,*) 'Restored vy3av',sum(vy3av(sp,:,:))
            end if

            call hdf5_r_d2d(file_id, Nz, Ny, dumg, 'vx3av'//sp_str//char(0))
            vx3av(sp,:,:) = dumg
            if (verbose) then
                write(*,*) 'Restored vx3av',sum(vx3av(sp,:,:))
            end if


            call hdf5_r_d2d(file_id, Nz, Ny, dumg, 'edgeflux'//sp_str//char(0))
            edge_flux(:,:,sp) = dumg
            if (verbose) then
                write(*,*) 'Restored edgeflux',sum(edge_flux(:,:,sp))
            end if

            call hdf5_r_d2d(file_id, Nz, Ny, dumg, 'edgeenergyflux'//sp_str//char(0))
            edge_energy_flux(:,:,sp) = dumg
            if (verbose) then
                write(*,*) 'Restored edge energy flux',sum(edge_energy_flux(:,:,sp))
            end if


        end do

        ! close HDF5 file
        call h5fclose_f(file_id, h5err)
        ! shutdown HDF5
        call h5close_f(h5err)


    end subroutine
end module hdf5_spice2
