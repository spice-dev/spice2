! subroutines concerning the leapfrog
subroutine leapfrog(no_part,Nz,Ny,Ax_lf,Bx_lf,Cx_lf,Dx_lf,Ex_lf,Ay_lf,By_lf,Cy_lf,Dy_lf,Ey_lf,Az_lf,Bz_lf,Cz_lf,Dz_lf,Ez_lf,Lz,&
        dtz,z,y,ux,uy,uz,iz1,iy1,Escz,Escy,q,stype,m,no_species,collision_prob,driftz,Nc,count,dt,utherm,supra,spec_params,&
        cosalpha_yz,ksi,cosalpha_xz,alpha_yz,alpha_xz,y_start,y_stop,dz,dy,z_start,z_stop,z_start_box,z_stop_box,total_energy,approximate_pot,Fx_lf,Fy_lf,Fz_lf,bx,by,bz,sqbx2by2)
    ! this is the absolute bottleneck of the whole code
    use spice2_types

    implicit none
    ! calculates Electric field felt by a particle and solves the full equation of motion
    ! use Escz_big to get rid of if clauses in particle loop
    ! Ez,Ey are just temporary parameters (saves memory)
    !include "struct.h"
    integer:: i,no_part,Nz,Ny,no_species,count,Nc,supra,mm,test,y_start,y_stop,z_start,z_stop,z_start_box,z_stop_box
    type(pinfo), dimension(no_species):: spec_params
    real*8:: driftz,dt,cosalpha_yz,ksi,sinalpha_yz,cosalpha_xz, sinalpha_xz,alpha_xz,alpha_yz
    real*8,dimension(no_species):: q,m,Ax_lf,Bx_lf,Cx_lf,Dx_lf,Ex_lf,Ay_lf,By_lf,Cy_lf,Dy_lf,Ey_lf,Az_lf,Bz_lf,Cz_lf,Dz_lf,Ez_lf,utherm,gyro_factor,Fx_lf,Fy_lf,Fz_lf
    integer*2, dimension(no_part):: stype
    real*8:: Lz,ux_temp,uy_temp,uz_temp,dtz
    real*8, dimension(no_part):: z,y,ux,uy,uz
    integer, dimension(no_part):: iz1,iy1
    real*8, dimension(Nz,Ny):: Escz,Escy
    real*8,dimension(1:Nz,1:(Ny+1)):: Ez_big,Ey_big
    real*8:: Ez,Ey,q_act
    integer:: zc,yc,zc1,yc1
    integer*2 :: sp_act
    real*8, dimension(no_species):: collision_prob
    real*8:: angle1,angle2,v_tot,rndx,dr_par,dr_perpz, dr_perpy,kx,ky,kz,Ex
    ! copy Escz and Escy into the right regions of Ez_big, Ey_big
    real*8:: A11x,A12x,A22x,A21x,iz,iy,dz,dy
    real*8, dimension(no_species,4):: total_energy
    logical:: approximate_pot
    real*8:: bx,by,bz,sqbx2by2,dr_perp
    gyro_factor = q/m*dtz*ksi*ksi

    if (bx.eq.0.d0.AND.by.eq.0.d0) then
        kx = 0.0
        ky = 0.0
        kz = 1.0
    else if (alpha_yz.eq.90.0.and.alpha_xz.ne.90.0) then
       ! kx = 1.0/dsqrt(1.0 + tan(alpha_xz)*tan(alpha_xz))
       ! ky = 0.0
       ! kz = 1.0/dsqrt(1.0 + 1.0/tan(alpha_xz)/tan(alpha_xz))
        
        kx = cosalpha_xz
        ky = 0.0
        kz = dsqrt(1.0 - cosalpha_xz*cosalpha_xz)
        
    else if (alpha_yz.ne.90.0.and.alpha_xz.eq.90.0) then
        !kx = 0.0
        !ky = 1.0/dsqrt(1.0 + tan(alpha_yz)*tan(alpha_yz))
        !kz = 1.0/dsqrt(1.0 + 1.0/tan(alpha_yz)/tan(alpha_yz))
        kx = 0.0
        ky = cosalpha_yz
        kz = dsqrt(1.0 - cosalpha_yz*cosalpha_yz)
        
        
        
    else
        kx = 1.0/dsqrt(1.0 + tan(alpha_xz)*tan(alpha_xz) + tan(alpha_xz)*tan(alpha_xz)/tan(alpha_yz)/tan(alpha_yz))
        ky = 1.0/dsqrt(1.0 + tan(alpha_yz)*tan(alpha_yz)  + tan(alpha_yz)*tan(alpha_yz)/tan(alpha_xz)/tan(alpha_xz))
        kz = 1.0/dsqrt(1.0 + 1.0/tan(alpha_yz)/tan(alpha_yz) + 1.0/tan(alpha_xz)/tan(alpha_xz))


    end if
    ! write(*,*) 'K: ',kx,ky,kz,kx**2 + ky**2 + kz**2

    ! write(*,*) 'Box dims y', y_start, y_stop
    ! write(*,*) 'Box dims z', z_start_box, z_stop_box

    Ez_big(z_start_box:z_stop_box,y_start:y_stop) = 0.0
    Ey_big(z_start_box:z_stop_box,y_start:y_stop) = 0.0
    sinalpha_yz = sqrt(1.0 - cosalpha_yz**2)
    sinalpha_xz = sqrt(1.0 - cosalpha_xz**2)


    if (z_start.lt.Nz) then
        Ez_big(z_start_box:z_stop_box,y_start:y_stop) = Escz(z_start_box:z_stop_box,y_start:y_stop)

        Ey_big(z_start_box:z_stop_box,y_start:y_stop) = Escy(z_start_box:z_stop_box,y_start:y_stop)

    end if
    if (y_stop.eq.Ny) then
        Ez_big(z_start_box:z_stop_box,Ny+1) = Escz(z_start_box:z_stop_box,1)
        Ey_big(z_start_box:z_stop_box,Ny+1) = Escy(z_start_box:z_stop_box,1)
    else
        Ez_big(z_start_box:z_stop_box,y_stop+1) = Escz(z_start_box:z_stop_box,y_stop+1)
        Ey_big(z_start_box:z_stop_box,y_stop+1) = Escy(z_start_box:z_stop_box,y_stop+1)
    end if
    ! write(*,*) 'Survived E field'

    do i=1,no_part
        Ez = 0.0
        Ey = 0.0
        Ex = 0.0
        ux_temp = 0.0
        uy_temp = 0.0
        uz_temp = 0.0
        !if (int(stype(i)).gt.4.or.int(stype(i)).lt.0) then
        !	write(*,*) 'Wrong stype:',stype(i), i
        !	write(*,*) z(i),y(i)

        !end if
    !             if (abs(ux(i)).lt.100.0) then
     !               write(*,*) 'Cold LP init',ux(i),uy(i),uz(i)
      !              write(*,*) 'Cold LP init POS',y(i),z(i),stype(i)                    
                    
       !             end if
     
        sp_act = stype(i)
        if (sp_act.lt.1) then
            write(*,*) 'Error, got wrong stype!',sp_act
        end if
        q_act  = q(sp_act)
        ! pre-copying of iz1 and iy1 might be a little faster
        zc = iz1(i)
        yc = iy1(i)
        test = spec_params(sp_act)%motion_method - supra
        !disable this stuff
        test = 0

        ! write(*,*) 'Particle at yz:', yc, zc, y(i), z(i)
        ! if (zc.eq.0.or.yc.eq.0) then
        !     cycle
        ! endif

        if (test.eq.1.or.test.eq.0) then
            if ((zc.lt.Nz).and.(z(i).gt.0.0)) then
                !Calculate electric field felt by the ion = weighting back
                !=========================================================
                !---------------------A-weighting back 2D--------------------
                !          Ez=Ez_big(iz1(i),iy1(i))*A11(i)
                !          Ey=Ey_big(iz1(i),iy1(i))*A11(i)
                !---------------------B-weighting back 2D--------------------
                !          Ez=Ez+Ez_big(iz1(i)+1,iy1(i))*A21(i)
                ! 	 Ey=Ey+Ey_big(iz1(i)+1,iy1(i))*A21(i)
                !---------------------C-weighting back 2D--------------------
                !          Ez=Ez+Ez_big(iz1(i)+1,iy1(i)+1)*A22(i)
                ! 	 Ey=Ey+Ey_big(iz1(i)+1,iy1(i)+1)*A22(i)
                !---------------------D-weighting back 2D--------------------
                !          Ez=Ez+Ez_big(iz1(i),iy1(i)+1)*A12(i)
                ! 	 Ey=Ey+Ey_big(iz1(i),iy1(i)+1)*A12(i)
                !----------------------End weighting back--------------------
                ! MK new way - let's do all weighting at once!
                !        iz1(i)=idint(z(i)/dz) + 1
                iz= z(i)/dz - iz1(i) +1.0
                !iz1(i)=iz1(i)+1          !index of cell in j-direction

                !        iy1(i)=idint(y(i)/dy) + 1
                iy =y(i)/dy - iy1(i) + 1.0
                !        iy1(i)=iy1(i)+1          !index of cell in k-direction

                !NEW JG 6/12/5
                !  	A11(i)=(1.d0-iz)*(1.d0-iy)
                !  	A12(i)=(1.d0-iz)*iy
                !  	A21(i)=iz*(1.d0-iy)
                !  	A22(i)=iz*iy
                A11x=(1.d0-iz)*(1.d0-iy)
                A12x=(1.d0-iz)*iy
                A21x=iz*(1.d0-iy)
                A22x=iz*iy

                !       write(*,*) 'A',A11x,A12x,A21x,A22x,A11x+A12x+A21x+A22x


                !          Ez=Ez_big(zc,yc)*A11(i) + Ez_big(zc +1,yc)*A21(i) + Ez_big(zc +1,yc+1)*A22(i) + Ez_big(zc,yc+1)*A12(i)
                !
                !          Ey=Ey_big(zc,yc)*A11(i) + Ey_big(zc +1,yc)*A21(i) + Ey_big(zc +1,yc +1)*A22(i) + Ey_big(zc,yc + 1)*A12(i)
                !
                Ez=Ez_big(zc,yc)*A11x + Ez_big(zc +1,yc)*A21x + Ez_big(zc +1,yc+1)*A22x + Ez_big(zc,yc+1)*A12x

                Ey=Ey_big(zc,yc)*A11x + Ey_big(zc +1,yc)*A21x + Ey_big(zc +1,yc +1)*A22x + Ey_big(zc,yc + 1)*A12x
                ! write(*,*)  "Ez Ey:",Ez,Ey,z(i),y(i)
                 if (abs(driftz).gt.0.0.and.(.not.approximate_pot)) then
		      Ex = driftz
		  end if
            

                !Solve the equations of motion (leapfrog)
                !========================================
                !N.B: No 1/2 wind-back (if yes, see module injection for that!)
                if (spec_params(sp_act)%motion_method.eq.1) then
                    ! gyro-motion
                    ! ux = upar
                    !uy = uperp
                    ! uz = 0.0
                    ! constant motion
                    !   write(*,*) 'Ez_gyro',q(sp_act)*dt/m(sp_act)*ksi*ksi
                    !         write(*,*) 'LPG: ',q(sp_act)*dt/m(sp_act)*ksi*ksi*(Ey*cosalpha_yz + Ez*sinalpha_yz),  ux(i)
                    !       ux_temp = q(sp_act)*dt/m(sp_act)*ksi*ksi*(Ey*abs(cosalpha_yz) + Ez*sinalpha_yz)*sinalpha_xz   +    ux(i)
                    ! ExB drift
                    ! ExB component in the y direction
                    !   uy_temp = (Ey*sinalpha_yz - Ez*abs(cosalpha_yz))*ksi*ksi
                    !      uy_temp = Ez*ksi*ksi*abs(cosalpha_xz)*sinalpha_yz
                    ! ExB component in the z direction
                    !     uz_temp = Ey*ksi*ksi*abs(cosalpha_xz)*sinalpha_yz
                    ! parallel velocity
                  !  write(*,*) 'LP',i,ux(i)
                    ux_temp =  gyro_factor(sp_act)*(-Ey*ky + Ez*kz) + ux(i)
            !        if (abs(ux_temp).lt.100.0) then
             !       write(*,*) 'Cold LP',ux_temp,ux(i),i
              !      write(*,*) 'Cold LP POS',y(i),z(i),stype(i)                    
                    
               !     end if
                    ! ExB component in perp direction
                    uy_temp  = (Ey*kz+ Ez*ky)
                    ! third component is the velocity in the direction of the Larmor rotation
                    uz_temp  = uz(i)
                    
                    
             !       write(*,*) 'GVX',ux_temp,uy_temp,uz_temp
                elseif (spec_params(sp_act)%motion_method.eq.3) then
				! motion with B=0
				                  !velocities calculation
                    !----------------------
                    ux_temp=ux(i) +  Ex*ksi*ksi*dt*q_act/m(sp_act)

                    uy_temp= uy(i) + Ey*ksi*ksi*dt*q_act/m(sp_act)

                    uz_temp= uz(i) + Ez*ksi*ksi*dt*q_act/m(sp_act)

				
                else
                    ! regular motion
                
                    !velocities calculation
                    !----------------------
                    ux_temp=ux(i)*Ax_lf(sp_act) + uy(i)*Bx_lf(sp_act) + uz(i)*Cx_lf(sp_act) + Ey*Dx_lf(sp_act) - Ez*Ex_lf(sp_act) + Ex*Fx_lf(sp_act)

                    uy_temp=ux(i)*Ay_lf(sp_act) + uy(i)*By_lf(sp_act) + uz(i)*Cy_lf(sp_act) + q_act*Ey*Dy_lf(sp_act) + Ez*Ey_lf(sp_act) + Ex*Fy_lf(sp_act)

                    uz_temp=ux(i)*Az_lf(sp_act) + uy(i)*Bz_lf(sp_act) + uz(i)*Cz_lf(sp_act) + Ey*Dz_lf(sp_act) + q_act*Ez*Ez_lf(sp_act) + Ex*Fz_lf(sp_act)
			
                end if

            else

                ! particle outside the box


                ! particle in the injection box - no E field, hence simplier eqs
                if (spec_params(sp_act)%motion_method.eq.2.or.spec_params(sp_act)%motion_method.eq.1) then
                    ! ux = upar
                    !uy = uperp
                    ! uz = 0.0
                    ! constant motion
                    ux_temp = ux(i)
                    ! no drift
                    uy_temp = 0.0
                    uz_temp = uz(i)
                elseif (spec_params(sp_act)%motion_method.eq.3) then
                ! no B field
                ux_temp=ux(i)
                uy_temp=uy(i)
                uz_temp=uz(i)
          
                else
                    ux_temp=ux(i)*Ax_lf(sp_act) + uy(i)*Bx_lf(sp_act) + uz(i)*Cx_lf(sp_act)

                    uy_temp=ux(i)*Ay_lf(sp_act) + uy(i)*By_lf(sp_act) + uz(i)*Cy_lf(sp_act)

                    uz_temp=ux(i)*Az_lf(sp_act) + uy(i)*Bz_lf(sp_act) + uz(i)*Cz_lf(sp_act)
                end if

            end if



            ux(i)=ux_temp
            uy(i)=uy_temp
            uz(i)=uz_temp
            
            ! external drift application
            ! differentiate from the lambda parameter
           
            
	    total_energy(stype(i),2) = total_energy(stype(i),2) + ux(i)*ux(i) + uy(i)*uy(i) + uz(i)*uz(i) 
            ! external drift application  - debug code
            ! it's a real drift  - we need to apply extra velocity in the x direction in order to get a z drift :)
            ! the drift in normalized units depends in mass
            ! multiplication by dt makes easier to determine field strength
            ! if (count.gt.Nc.and.driftz.ne.0.0) then
            ! ! normal drift
            ! ! 	ux(i) = ux(i) - utherm(stype(i))*driftz*dt/sqrt(m(stype(i)))
            ! ! sinus drift
            ! ! 	ux(i) = ux(i) - driftz*dt/sqrt(m(stype(i)))
            ! 	ux(i) = ux(i) - driftz*dt/m(stype(i))*q(stype(i))
            !  end if


            ! vectorized form - might be faster?
            !positions calculation
            if (spec_params(sp_act)%motion_method.eq.1) then
            
                dr_par  = ux(i)*dtz
                dr_perp  = uy(i)*dtz
               ! write(*,*) 'DR',dr_par,dr_perp
                !       dr_perpy  = uy(i)*dtz
                !       dr_perpz  = uz(i)*dtz

                ! transform back to y z
                !        write(*,*) 'ymove',dr_par*cosalpha_yz - dr_perp*sinalpha_yz

                !         y(i) = dr_par*abs(cosalpha_yz) + dr_perp*sinalpha_yz + y(i)
                !         z(i) = dr_par*sinalpha_yz - dr_perp*abs(cosalpha_yz) + z(i)
                !         y(i) = dr_par*abs(cosalpha_yz)*sinalpha_xz + dr_perpy + y(i)
                !         z(i) = dr_par*sinalpha_yz*sinalpha_xz + dr_perpz + z(i)

!                y(i) = dr_par*kz + dr_perp*ky +  y(i)
 !               z(i) = dr_par*ky  + dr_perp*kz + z(i)
                    ! from the injection routine
                     if (bx.eq.0.d0.AND.by.eq.0.d0) then
                    z(i) = dr_par + z(i)
                    y(i) = dr_perp + y(i)
   
                    else
           !         write(*,*) 'dz',bz,sqbx2by2,dr_par,dr_perp
            !        write(*,*) 'dy',by,-by*bz/sqbx2by2,dr_par,dr_perp
            
         !   if (Ey.ne.0.0) then
          !  write(*,*) 'DR',dr_par,dr_perp,Ey
            
          
          !write(*,*) 'DZ',-dr_par*bz + dr_perp*sqbx2by2
           !               write(*,*) 'DY',-dr_par*by + (- dr_perp*by*bz)/sqbx2by2
            !    end if                          
         
                    z(i) = -dr_par*bz + dr_perp*sqbx2by2 + z(i)
                    y(i) = -dr_par*by + (- dr_perp*by*bz)/sqbx2by2 + y(i)
                    end if
            else
            
          !  if (stype(i).eq.3) then
	   !   write(*,*) 'LP Therm',dtz,uz(i),uy(i)
           ! end if
                z(i) = z(i) + dtz*uz(i)
                y(i) = y(i) + dtz*uy(i)
            end if
            ! !collisions - based onthe probability
            ! if (collision_prob(sp_act).gt.0.0) then
            !      call G05FAF(0.d0,1.d0,1,rndx)
            !  if (rndx.lt.collision_prob(sp_act)) then
            ! !elastic collisions - we keep the energy and uz - only cross-field diffusion!
            ! 	v_tot = ux(i)*ux(i) + uy(i)*uy(i)
            ! 	!choose 2 random angles
            !      	call G05FAF(0.d0,1.d0,1,rndx)
            ! 	angle1 = 3.14159*rndx
            ! !     	call G05FAF(0.d0,1.d0,1,rndx)
            ! !	angle2 = 3.14159*rndx
            ! !recalculate the velocities
            ! 	ux(i) = dsqrt(v_tot)*sin(angle1 - 3.14159/2.0)
            !  	call G05FAF(0.d0,1.d0,1,rndx)
            !
            ! 	uy(i) = dsqrt(v_tot - ux(i)*ux(i))*(rndx - 0.5)/abs(rndx - 0.5)
            ! end if
            !
            ! end if
        end if
        
        
       !   if (y(i).lt.0.or.z(i).lt.0) then
        
        !    write(*,*) 'LP Particle out of bounds POS:',stype(i),z(i),y(i)
         !   write(*,*) 'LP Particle out of bounds VEL:',ux(i),uy(i),uz(i)
       
        
!        end if
    end do


end subroutine


subroutine leapfrog_init(mu2,m,delta_half,dt0,dtu,bx,by,bz,midLz,q,ksi,Lz_inj,Lz,Ax_lf,Bx_lf,Cx_lf,Dx_lf,Ex_lf,Ay_lf,By_lf,&
        Cy_lf,Dy_lf,Ey_lf,Az_lf,Bz_lf,Cz_lf,Dz_lf,Ez_lf,Fx_lf,Fy_lf,Fz_lf)
    implicit none
    real*8:: mu2,m,delta_half,dt0,dtu,bx,by,bz,midLz,q,ksi,Lz_inj,Lz
    real*8:: Ax_lf,Bx_lf,Cx_lf,Dx_lf,Ex_lf,Ay_lf,By_lf,Cy_lf,Dy_lf,Ey_lf,Fx_lf,Fy_lf,Fz_lf
    real*8:: Az_lf,Bz_lf,Cz_lf,Dz_lf,Ez_lf
    mu2 = 1/(m*m)
    delta_half = 1+(1/m)**2*dt0**2/4.0
    !For optimization                         !NEW!!
    !midLz=(Lz+Lz_inj)/2.d0
    !------------LF----------------------------------
    Ax_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2*q*q)*(8.d0+2.d0*mu2*(bx**2-by**2-bz**2)*dtu**2*q**2)
    !fixed very old bug
    Bx_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2*q*q)*(q/m*8.d0*bz*dtu + 4.d0*bx*by*dtu*dtu*mu2*q**2)
    Cx_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2*q*q)*(-q/m*8.d0*by*dtu + 4.d0*bx*bz*dtu*dtu*mu2*q**2)
    Dx_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2*q*q)*2.d0*ksi**2*mu2*dtu**2*q**2*(2.d0*bz + q/m*bx*by*dtu)
    Ex_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2*q*q)*2.d0*ksi**2*mu2*dtu**2*q**2*(2.d0*by - q/m*bx*bz*dtu)
    Fx_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2*q*q)*2.d0*ksi**2*mu2*dtu**2*q**2*(4.d0 + bx**2*dtu**2*mu2*q**2)
    
    Ay_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2*q*q)*(4.d0*bx*by*mu2*dtu*dtu*q**2 - q/m*8.d0*bz*dtu)
    By_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2*q*q)*(8.d0+2.d0*mu2*q**2*(-bx**2+by**2-bz**2)*dtu**2)
    Cy_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2*q*q)*(q/m*8.d0*bx*dtu + 4.d0*by*bz*dtu*dtu*mu2*q**2)
    Dy_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2*q*q)*2.d0/m*ksi**2*dtu*(4.d0 + by**2*dtu**2*mu2*q**2)
    Ey_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2*q*q)*2.d0*ksi**2*mu2*dtu**2*q**2*(2.d0*bx + q/m*by*bz*dtu)
    Fy_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2*q*q)*2.d0*ksi**2*mu2*dtu**2*q**2*(2.d0*bz - q/m*by*bz*dtu)


    Az_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2*q*q)*(8.d0*q/m*by*dtu + 4.d0*bx*bz*dtu*dtu*mu2*q**2)
    Bz_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2*q*q)*(4.d0*by*bz*dtu*dtu*mu2*q**2 - q/m*8.d0*bx*dtu)
    Cz_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2*q*q)*(8.d0+2.d0*mu2*q**2*(-bx**2-by**2+bz**2)*dtu**2)
    Dz_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2*q*q)*2.d0*ksi**2*mu2*dtu**2*q**2*(q/m*by*bz*dtu - 2.d0*bx)
    Ez_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2*q*q)*2.d0*ksi**2/m*dtu*(4.d0 + bz**2*dtu**2*mu2*q**2)
    Fz_lf=1.d0/(8.d0+2.d0*dtu*dtu*mu2*q*q)*2.d0*ksi**2*mu2*dtu**2*q**2*(q/m*by*bz*dtu + 2.d0*by)


end subroutine
subroutine leapfrog_nofield(no_part,Nz,Ny,Ax_lf,Bx_lf,Cx_lf,Dx_lf,Ex_lf,Ay_lf,By_lf,Cy_lf,Dy_lf,Ey_lf,Az_lf,Bz_lf,Cz_lf,Dz_lf,Ez_lf,Lz,&
        dtz,z,y,ux,uy,uz,iz1,iy1,Escz,Escy,q,stype,m,no_species,collision_prob,driftz,Nc,count,dt,utherm,supra,spec_params,&
        cosalpha_yz,ksi,cosalpha_xz,alpha_yz,alpha_xz,y_start,y_stop,dz,dy,z_start,z_stop,z_start_box,z_stop_box,total_energy,approximate_pot,Fx_lf,Fy_lf,Fz_lf)
    ! this is the absolute bottleneck of the whole code
    use spice2_types

    implicit none
    ! calculates Electric field felt by a particle and solves the full equation of motion
    ! use Escz_big to get rid of if clauses in particle loop
    ! Ez,Ey are just temporary parameters (saves memory)
    integer:: i,no_part,Nz,Ny,no_species,count,Nc,supra,mm,test,y_start,y_stop,z_start,z_stop,z_start_box,z_stop_box
    type(pinfo), dimension(no_species):: spec_params
    real*8:: driftz,dt,cosalpha_yz,ksi,sinalpha_yz,cosalpha_xz, sinalpha_xz,alpha_xz,alpha_yz
    real*8,dimension(no_species):: q,m,Ax_lf,Bx_lf,Cx_lf,Dx_lf,Ex_lf,Ay_lf,By_lf,Cy_lf,Dy_lf,Ey_lf,Az_lf,Bz_lf,Cz_lf,Dz_lf,Ez_lf,utherm,gyro_factor,Fx_lf,Fy_lf,Fz_lf
    integer*2, dimension(no_part):: stype
    real*8:: Lz,ux_temp,uy_temp,uz_temp,dtz
    real*8, dimension(no_part):: z,y,ux,uy,uz
    integer, dimension(no_part):: iz1,iy1
    real*8, dimension(Nz,Ny):: Escz,Escy
    real*8,dimension(1:Nz,1:(Ny+1)):: Ez_big,Ey_big
    real*8:: Ez,Ey,q_act
    integer:: zc,yc,zc1,yc1
    integer*2 :: sp_act
    real*8, dimension(no_species):: collision_prob
    real*8:: angle1,angle2,v_tot,rndx,dr_par,dr_perpz, dr_perpy,kx,ky,kz,Ex
    ! copy Escz and Escy into the right regions of Ez_big, Ey_big
    real*8:: A11x,A12x,A22x,A21x,iz,iy,dz,dy
    real*8, dimension(no_species,4):: total_energy
    logical:: approximate_pot
    gyro_factor = q/m*dtz*ksi*ksi

  
    do i=1,no_part
                z(i) = z(i) + dtz*uz(i)
                y(i) = y(i) + dtz*uy(i)
    end do


end subroutine
