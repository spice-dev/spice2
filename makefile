# you should source config/environment.sh and config/output.sh before running makefile

# all library definitions
include ./config/Makefile.inc
# these libraries are defined in Makefile.inc
#$(MUMPS_LIBS)
#$(COMMON_LIBS)
#$(CLOCK_LIB)
#$(UMFPACK_LIBS_BASIC)
#$(MUMPS_LIBS_BASIC)

# compiler flags are now taken from Makefile.inc
#MPIF_FLAGS_DEBUG=-g -C
#MPIF_FLAGS_RELEASE=-O3

# executable outputs
BASE_EXECUTABLE_NAME=$(SPICE2_NAME)-$(SPICE2_VERSION)
TRACER_EXECUTABLE_NAME=$(SPICE2_NAME)-tracer-$(SPICE2_VERSION)
PIN_EXECUTABLE_NAME=pin
SUFFIX_DEBUG=$(SPICE2_SUFFIX_DEBUG)
SUFFIX_RELEASE=$(SPICE2_SUFFIX_RELEASE)

UMFPACK_LIBS=$(UMFPACK_INCLUDE) $(UMFPACK_LIBS_BASIC)
SUPPORT_LIBS=$(MATH_LIBS)

SPICE2_LIBS=$(MATIO_LIBS) $(HDF5_LIBS) $(PES2D_LIBS) $(MUMPS_LIBS) $(UMFPACK_LIBS) $(CLOCK_LIB) $(SUPPORT_LIBS) $(COMMON_LIBS)

# source and output directories
BIN_DIR=$(SPICE2_DIR)/bin
SRC_DIR=./src

# files
#SOLVER_FILES=PES.f90 PESdist.f90 sm_tools.f90 sorting.f90
HDF5_OBJ=hdf5_spice2.o hdf5_io.o
SOLVER_OBJ=pes_md.o pes_mc.o pes_m.o sm_tools.o sorting.o
SPICE_FILES=mpicom.f90 injection.f90 grid.f90 leapfrog.f90 poisson.f90 gan.f90 see.f90 collisions.f90 spice2.f90
TRACER_FILES=mpicom.f90 injection.f90 grid.f90 leapfrog.f90 poisson.f90 gan.f90 see.f90 collisions.f90 spice2.f90
MATIO_FILES=matfile_matio.f90
UTILS_FILES=
UTILS_OBJ=ap_utils.o getoptions.o

SOURCE_FILES=$(SPICE_FILES) $(MATIO_FILES) $(UTILS_FILES)

# default flag values
#
# ADDITIONAL_SUFFICES=


all: debug release

# dependencies
# utilities module

# spice data types
types:
	cd $(SRC_DIR); \
	$(MPIF) $(MPIF_FLAGS) $(MPIF_INC) $(COMMON_INC) -c spice2_types.f90

# command line parsing
getoptions:
	cd $(SRC_DIR); \
	$(MPIF) $(MPIF_FLAGS) $(MPIF_INC) $(COMMON_INC) -c getoptions.f90

aputils: getoptions
	cd $(SRC_DIR); \
	$(MPIF) $(MPIF_FLAGS) $(MPIF_INC) $(COMMON_INC) -c ap_utils.f90

# matrix creation
smtools: sorting
	cd $(SRC_DIR); \
	$(MPIF) $(MPIF_FLAGS) $(MPIF_INC) $(COMMON_INC) -c sm_tools.f90

# sorting tools
sorting: aputils
	cd $(SRC_DIR); \
	$(MPIF) $(MPIF_FLAGS) $(MPIF_INC) $(COMMON_INC) -c sorting.f90

# common solver routines
pesm: smtools
	cd $(SRC_DIR); \
	$(MPIF) $(MPIF_FLAGS) $(MPIF_INC) $(COMMON_INC) -c pes_m.f90

# centralized solver module
pesmc: smtools pesm
	cd $(SRC_DIR); \
	$(MPIF) $(MPIF_FLAGS) $(MPIF_INC) $(COMMON_INC) -c pes_mc.f90

# centralized solver module
pesmd: smtools pesm
	cd $(SRC_DIR); \
	$(MPIF) $(MPIF_FLAGS) $(MPIF_INC) $(COMMON_INC) -c pes_md.f90

# all solver routines
pes: pesmc pesmd

# hdf5 storage
hdf5: types
	cd $(SRC_DIR); \
	$(MPIF) $(MPIF_FLAGS) $(MPIF_INC) $(COMMON_INC) -c hdf5_io.f90; \
	$(MPIF) $(MPIF_FLAGS) $(MPIF_INC) $(COMMON_INC) -c hdf5_spice2.f90

# release target
# use for real simulations -- code is compiler-optimized
# let's prepare target specific variables
release: MPIF_FLAGS=$(MPIF_FLAGS_RELEASE) $(MPIF_FLAGS_COMMON)
release: BINARY_SUFFIX=$(SUFFIX_RELEASE)
release: EXECUTABLE_NAME=$(BASE_EXECUTABLE_NAME)$(ADDITIONAL_SUFFICES)$(BINARY_SUFFIX)$(SPICE2_SUFFIX_EXE)
release: types pes aputils hdf5
	@echo "Creating release binary files"
	cd $(SRC_DIR); \
	$(MPIF) $(MPIF_FLAGS) $(MPIF_INC) $(COMMON_INC) $(UTILS_OBJ) $(SPICE_FILES) $(MATIO_FILES) $(SOLVER_OBJ) $(HDF5_OBJ) $(UTILS_FILES) $(SPICE2_LIBS) -I$(SPICE2_INCDIR) -o $(BIN_DIR)/$(EXECUTABLE_NAME)

# debug target
# debug symbols are added to the code
debug: MPIF_FLAGS=$(MPIF_FLAGS_DEBUG) $(MPIF_FLAGS_COMMON)
debug: BINARY_SUFFIX=$(SUFFIX_DEBUG)
debug: EXECUTABLE_NAME=$(BASE_EXECUTABLE_NAME)$(ADDITIONAL_SUFFICES)$(BINARY_SUFFIX)$(SPICE2_SUFFIX_EXE)
debug: types pes aputils hdf5
	@echo "Creating debug binary files"
	cd $(SRC_DIR); \
	$(MPIF) $(MPIF_FLAGS) $(MPIF_INC) $(COMMON_INC) $(UTILS_OBJ) $(SPICE_FILES) $(MATIO_FILES) $(SOLVER_OBJ) $(HDF5_OBJ) $(UTILS_FILES) $(SPICE2_LIBS) -I$(SPICE2_INCDIR) -o $(BIN_DIR)/$(EXECUTABLE_NAME)

# particle tracer
tracer: EXECUTABLE_NAME=$(TRACER_EXECUTABLE_NAME)$(ADDITIONAL_SUFFICES)$(BINARY_SUFFIX)$(SPICE2_SUFFIX_EXE)
tracer: BINARY_SUFFIX=$(SUFFIX_RELEASE)
tracer: MPIF_FLAGS=$(MPIF_FLAGS_RELEASE) $(MPIF_FLAGS_COMMON)
tracer: types pes aputils hdf5
	cd $(SRC_DIR); \
	$(MPIF) -DSPICE_TRACER $(MPIF_FLAGS) $(MPIF_INC) $(COMMON_INC) $(UTILS_OBJ) $(TRACER_FILES) $(MATIO_FILES) $(SOLVER_OBJ) $(HDF5_OBJ) $(UTILS_FILES) $(SPICE2_LIBS) -I$(SPICE2_INCDIR) -o $(BIN_DIR)/$(EXECUTABLE_NAME)
	

# pin diag fix
pin: EXECUTABLE_NAME=$(PIN_EXECUTABLE_NAME)$(ADDITIONAL_SUFFICES)$(BINARY_SUFFIX)$(SPICE2_SUFFIX_EXE)
pin: MPIF_FLAGS=$(MPIF_FLAGS_RELEASE) $(MPIF_FLAGS_COMMON)
pin: getoptions
	cd $(SRC_DIR); \
	$(FC) $(COMMON_INC) pin.f90 getoptions.o $(MATIO_LIBS) $(COMMON_LIBS) -I$(SPICE2_INCDIR) -o $(BIN_DIR)/$(EXECUTABLE_NAME)

# cleans only compiled executables
clean-exe:
	cd bin; rm -f *$(SPICE2_SUFFIX_EXE)
	@echo "Executables cleaned"

# cleans results of testing runs
clean-test:
# log files from mpirun do not have *.log extension, rather *.log.i.j (i, j -- core numbers)
	cd testing/log; rm -f *log*
	cd testing/output; rm -f *.mat
	@echo "Testing files cleaned"

# danger!
# cleans external libraries from aggregated lib/obj folder
clean-libs:
	cd externals; rm -f *.log
	cd externals/lib; rm -f *.a
	cd externals/obj; rm -f *.o
	cd externals/inc; rm -f *.inc
	cd externals/mod; rm -f *.mod
	@echo "Libraries cleaned"

# danger!
# cleans external library sources (aggregated compiled libraries will be kept)
clean-externals:
	cd externals; rm -f *.log
	./externals/remove_libs.sh
	@echo "External library sources cleaned"


clean-mod:
	cd bin; rm -f *.mod
	@echo "Modules cleaned"

# default clean
clean: clean-exe clean-mod

# clean everything
purge: clean-exe clean-test clean-externals clean-libs

