# SPICE2

This is SPICE2 installation package.

Requirements:

*	MPI (works with openmpi 1.6.2)
*	some external libraries (see below) -- provided by repository spice-lib

Recommended compilers:

*	ifort / mpif90 / mpif77
*	icc / mpicc

## Short installation guide

Select an empty directory in your filesystem.
Clone this repository.

```
    $ git clone https://bitbucket.org/spice-dev/spice2
```

Fetch all changes and update to the last changeset

```
    $ git pull
```

Copy config files and setup correct paths and machine settings. Most of the configuration of the system is in the spice-lib.

Compile SPICE2

```
    $ ./compile.sh
```

Test it. Each run takes ~20 min.

```
    $ cd testing
    $ ./simple_test.sh
```

## Setup

* See `./config` directory. Config directory should contain following files:
    * `environment.sh` -- compiler executables and paths
    * `output.sh` -- output names, version etc.
    * `Makefile.inc` -- libraries

There is a sample directory with commented files. You can create your own sample dir -- only this should be managed by git.

Files `environment.sh` and `Makefile.inc` are machine-specific and are
excluded from the source management (see `.gitignore`). Do not delete them
from `.gitignore`.

The file `output.sh` is meant to be included during the compilation of
SPICE2. It contains some versioning information and executable names --
change it when appropriate.

### Dependencies 

*	System libraries: lz, lutil, ldl, lpthread
*	MPI (works with OpenMPI 1.6.2, Intel MPI 4.1.0.024 and probably
    higher)
*	Clock (homemade precise timer, see
    externals/archives/clock.tar.gz)
*	Matio (MATLAB I/O library, 1.3.4 -- newer versions do not
    contain FORTRAN interface)
*   HDF5
* 	UMFPACK (serial linear equation system solver, UMFPACK 5.2.0)
    *	AMD (matrix ordering; AMD 2.2.0)
    *	UFConfig (configuration for UMFPACK and AMD)
    *	BLAS (GotoBLAS2, Intel MKL BLAS (2013.5.192+), OpenBLAS)
    *	F2C (FORTRAN-C translator)
*	MUMPS (parallel linear equation system solver; MUMPS 4.10)
    *	BLAS (see above)
    *	LAPACK (linear algebra; LAPACK 3.4.2)
    *	BLACS (parallel linear algebra; BLACS for MPI)
    *	ScaLAPACK (parallel linear algebra; ScaLAPACK 2.0.2)
        BLAS, LAPACK, BLACS and ScaLAPACK can be replaced by Intel
        MKL, although MKL implementation is slower
    *	SCOTCH (matrix ordering; Scotch 5.1.12b -- DO NOT use higher until new version of MUMPS is released)

If you have any of these ready, you can change appropriate paths in
Makefile.inc in your ./config directory. However, this distribution
contains all packages mentioned above (except system libraries and MPI).

If you decide to use some of them (or all of them) use the repository spice-lib at `https://bitbucket.org/spice-dev/spice-lib`


## Compilation of SPICE2

*	Go to the parent directory of this repository.
*	Compile the code using makefile. Possible options are:

``` 
    $ make debug
    # creates an executable with debug symbols
```
    
``` 
    $ make release
    # creates an executable with optimization
```
    
``` 
    $ make all
    # creates both release and debug
```
    
```
    $ make clean
    # deletes all output
```

Another targets are specified inside the makefile. See comments.
Do not write any machine-specific variables/paths into the
makefile, use `Makefile.inc` if needed.

This will create an executable located in `./bin` directory. The name of
the executable will be generated using `./config/output.sh`. If the
Poisson solver is disabled, a suffix will be added. It is possible to
define another flags using the target constants which will pass
compile-time parameters to SPICE2 sources.

#### Conditional compilation

We use `-fpp` flag as FORTRAN pre-processor, so far, following flags are supported

* `-DSPICE_TRACER` disables several routines and compiles the version which only traces particles on imposed potential. Invoke it with `make tracer`.

## Testing of the code

There is a testing directory (`./testing`) for debugging etc. The process
is managed by a shell script `./simple_test.sh`. So far, three options are to be tested.

You can compare the testing result with Matlab script in the directory.

## Matlab file access

To load data produced by SPICE, you can use Matlab or HDF5 files (by flag `-5`). Loading routines are in `./matlab` directory.

## Changelog

Newest on the top, please :).

### 2019-10-25: AP

* Some clean-up and debugging.
* Note that Intel MPI 18+ needs a new environment variable `export I_MPI_SHM_LMT=shm` to allow the transfer large amounts of data.

### 2019-10-01: AP

* HDF5 storage added. Use flag `-5`.
* Poisson solver based upon Hypre library was added, use `psolver=6` (new libraries needed).
* SPICE libraries detached into separate repository (see `spice-lib`).
* Types moved from header file to a module.

### 2014-12-09: AP

*	Poisson solver MPI communicator can be restricted to lesser number 
of processes than the whole program. 
Usage:
    *	add a property `ps_commsize` into control namelist in input file, 
    set it to desired number of processes, eg:
    `ps_commsize=4`
    *	possible values are from 1 to np (a size of the `MPI_COMM_WORLD`
    communicator)
    *	others are treated as np 
    *	if ps_commsize is not set, the value is still treated as np
    *	this option is valid for `psolver == 4`
*	MPI initialization set to `MPI_INIT_THREAD` so that the solver can use
OpenMP more effectively.
    *	This is intended for future use -- for the time being, it is
    recommended to run the solver with `export OMP_NUM_THREADS == 1` (this is an
    environment variable).
*	Step limit flag. Usage: 
    *	run SPICE2 with flag `-s (number)`
    *	execution will stop after specified number of steps
    *	intended for debug purposes

### 2014-11-30: AP

*	New Poisson equation solver introduced.
Usage: 
    -	Set `psolver=4` in control namelist.
